import React, { useEffect } from 'react'
import Image from 'next/image'
import styles from '../../styles/footercopyright.module.scss'
import AOS from "aos";
import Link from 'next/link'
function FooterCopyright() {
    useEffect(() => {
        AOS.init();
    },[])
    return(
        <div className={styles.copyright} >
            <div className="container">
            <div className="row" >
              
                 
                    <div className={styles.footer_right+" col-md-9"}>
                      <p>© 2022 · Footprint · All Rights Reserved  </p>
                    </div>
                    <div className={styles.left+" col-md-3"}>
                     <Link href="/privacy-policy" ><a className={styles.privacy}>Privacy Policy</a></Link>
                     <Link href="/terms-and-conditions" ><a className={styles.terms}>Terms of Service</a></Link>
                    </div>
                </div>
              </div>
            </div>
        
    )
}
export default FooterCopyright