import styles from '../../styles/Login.module.scss'

function CopyRight(){
    return(  
      <div className={styles.footer_container}>
        <p>© 2022 Footprint.io . All Rights Reserved. </p>
      </div>
      )
}

export default CopyRight;