import React, { Component } from "react";
import Link from 'next/link'
import Image from 'next/image'
import styles from '../../styles/Header.module.scss'

class DeviceMenu extends Component {
  state ={
    dropdown_status:false,
    dropdown_sub:"",
    dropdown_sub_level3:""
  }
  openNav = () => {
    document.getElementById("mySidebar").style.width = "100%";
  };

  closeNav = () => {
    document.getElementById("mySidebar").style.width = "0";
  };
 
 showDropdown=(val)=>{
   this.setState({dropdown_status:val})
 }
 setSubDropdownStatus=(val)=>{
   this.setState({dropdown_sub:val})
 }
 setSubDropdownLevel3=(val)=>{
  this.setState({dropdown_sub_level3:val})
 }

 componentDidMount=()=>{
  const concerned = document.querySelector(".hover_drop")
  document.addEventListener("mousedown", (event) => {
      if (concerned.contains(event.target)) {
          this.setState({dropdown_status:true})
      }
       else {
          this.setState({dropdown_status:false});
          this.setState({dropdown_sub:""})
          this.setState({dropdown_sub_level3:""})
      }
    });
 }
  render() {
    let {dropdown_status,dropdown_sub,dropdown_sub_level3}=this.state;

    return (
      <React.Fragment>
        <div className="mobile-main">
          <div className="container">
            <div className="device-menu">
            <div className={styles.logo}>
              <Link href="/" passHref><img src="/images/logo.png" alt="Footprint Logo"  width={127} height={28}/></Link> 
            </div>
              <div className="item-burger">
                <div className="menu-burger" onClick={this.openNav}>
                  <span></span>
                  <span className="middle"></span>
                  <span></span>
                </div>
              </div>
            </div>
            <div id="mySidebar" className="sidebar">
              <a
                href="javascript:void(0)"
                className="closebtn"
                onClick={this.closeNav}
              >
                &times;
              </a>
              <ul className="navbar-nav mr-auto mt-2 mt-lg-0"> 
              <li className="nav-item">
              <Link href="/about-us"><a className="nav-link">About </a></Link>
                </li>
                <li className="nav-item hover_drop" >
                <a className={styles.dropdown_white+ " nav-link"} href="#" >Services </a>
                {dropdown_status?<div className={styles.dropdown_list_mob}>
                        <div className={styles.dropdown_container_mob}>
                            <ul className={styles.dropdown_first}>
                                <li>
                                    <a href='#'  onMouseOver={()=>this.setSubDropdownStatus("website_development")}>Website Development</a>
                                    {dropdown_sub=="website_development"? 
                                    <div className={styles.dropdown_container_mob+" "+styles.dropdown_sub}>
                                        <ul>
                                            <li> <Link href="/brochure-website"> Brochure Websites</Link></li>
                                            <li> <Link href="/ecommerce-website"> E-Commerce Websites</Link></li>
                                            <li> <Link href="/company-website"> Custom Websites</Link></li>
                    
                                            <li> <Link href="/mobile-websites-amp">Mobile Websites and AMP</Link></li>
                                            <li><Link href="/website-applications">Web Apps</Link></li>
                                        </ul>
                                    </div>:""
                                    }
                                </li>
                                <li>
                                  <a href='#' onMouseOver={()=>this.setSubDropdownStatus("mobile_appilcation")}>Mobile Application Development</a>
                                  {dropdown_sub=="mobile_appilcation"? 
                                  <div className={styles.dropdown_container_mob+" "+styles.dropdown_sub}>
                                        <ul>
                                        <li> <Link href="/native-ios-development">Native iOS Development</Link></li>
                                          <li> <Link href="#">Native Android Development</Link></li>
                                          <li> <Link href="#">Hybrid Mobile Application Development</Link></li>
                                      </ul>
                                  </div>:""
                                  }
                                </li>
                                <li>
                                    <a href='#' onMouseOver={()=>this.setSubDropdownStatus("seo")}>SEO</a>
                                    {dropdown_sub=="seo"? 
                                    <div className={styles.dropdown_container_mob+" "+styles.dropdown_sub}>
                                          <ul>
                                                <li> <Link href="#">On-page SEO</Link></li>
                                                <li> <Link href="/off-page-seo">Off-page SEO</Link></li>
                                                    <li> <Link href="/technical-seo">Technical SEO</Link></li>
                                                <li> <Link href="#">Plans</Link></li>
                                          </ul>
                                    </div>:""
                                    }
                                </li>
                                <li>
                                    <a href='#' onMouseOver={()=>this.setSubDropdownStatus("ppc")}>PPC</a>
                                    {dropdown_sub=="ppc"? 
                                    <div className={styles.dropdown_container_mob+" "+styles.dropdown_sub}>
                                        <ul>
                                            <li  onMouseOver={()=>this.setSubDropdownLevel3("")}> <Link href="#">Paid search marketing</Link></li>
                                            <li  onMouseOver={()=>this.setSubDropdownLevel3("")}> <Link href="#" >Display Ad Marketing</Link></li>
                                            <li onClick={()=>this.setSubDropdownLevel3("social")}> 
                                              <Link href="#" >Social Media Marketing</Link>
                                              {dropdown_sub_level3=="social"? 
                                                <div className={styles.dropdown_container_mob+" "+styles.dropdown_sub}>
                                                    <ul>
                                                        <li><Link href="#">Facebook</Link></li>
                                                        <li><Link href="#">Instagram</Link></li>
                                                        <li><Link href="#">Youtube</Link></li>
                                                        <li><Link href="#">Tiktok</Link></li>
                                                    </ul>
                                                </div>:""
                                                }
                                              </li>
                                            <li> <Link href="#" onMouseOver={()=>this.setSubDropdownLevel3("")}>Retargeting PPC Ads</Link></li>
                              
                                        </ul>
                                    </div>:""
                                    }
                                </li>
                                <li>
                                    <a href='#' onMouseOver={()=>this.setSubDropdownStatus("reputaion_manager")}>Reputation Management</a>
                                    {dropdown_sub=="reputaion_manager"? 
                                    <div className={styles.dropdown_container_mob+" "+styles.dropdown_sub}>
                                        <ul>
                                            <li onMouseOver={()=>this.setSubDropdownLevel3("")}><Link href="#" > Social Media Management</Link></li>
                                            <li  onMouseOver={()=>this.setSubDropdownLevel3("review_management")}> <Link href="#">Review Management</Link></li>
                                            {dropdown_sub_level3=="review_management"? 
                                            <div className={styles.dropdown_container_mob+" "+styles.dropdown_sub}>
                                                <ul>
                                                    <li><Link href="#"> Google My Business</Link></li>
                                                    <li><Link href="#">TrustPilot</Link></li>
                                                    <li><Link href="#">Yelp</Link></li>
                                                </ul>
                                            </div>:""
                                            }
                                        </ul>
                                    </div>:""
                                    }
                                </li>
                                <li>
                                    <a href='#' onMouseOver={()=>this.setSubDropdownStatus("for_agencies")}>For Agencies</a>
                                    {dropdown_sub=="for_agencies"? 
                                    <div className={styles.dropdown_container_mob+" "+styles.dropdown_sub}>
                                        <ul>
                                            <li><Link href="#"> White-Label Design</Link></li>
                                            <li> <Link href="#">White-Label Development</Link></li>
                                            <li> <Link href="#">White-Label Marketing</Link></li>
                                            <li> <Link href="#">Franchise</Link></li>
                                        </ul>
                                    </div>:""
                                    }
                                </li>
                            </ul>
                        </div>

                    </div>:""}
                </li>
                {/* <li className="nav-item">
                <Link href="/pricing" passHref><a  className="nav-link">Pricing </a></Link>
                </li> */}
                <li className="nav-item">
                <Link href="/portfolio"><a className="nav-link">Portfolio</a></Link>
                </li>
                <li className="nav-item">
                <Link href="/success-stories"><a className="nav-link">Success Stories</a></Link>
                </li>
                {/* <li className="nav-item">
                <a className="nav-link" href="#">Blog</a>
                </li> */}
                <li className="nav-item">
                <Link href="/contact-us"><a className="nav-link">Contact</a></Link>
                </li>
                <li className="nav-item">
      
                {!this.props.currentUser ?<Link href="/login"><a className={styles.sign_link} >Login</a></Link>: <><a className={styles.sign_link} onClick={this.props.logout}>Logout</a></>}
                     
                </li>
                <li className="nav-item">
                {!this.props.currentUser?<Link href="/request-sample "><a className="nav-link" href="#">Request a Free Sample</a></Link> :<Link href="/dashboard"><a  className="nav-link">Account</a></Link>}
                </li>
              </ul>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default DeviceMenu;
