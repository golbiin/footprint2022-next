
function ValidationError(props) {
 return (
    <span className="red-validate-error"><label>{props?props.message:""}</label></span>
 )
}
export default ValidationError