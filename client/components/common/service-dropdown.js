import React, { useEffect ,useState} from 'react'
import styles from '../../styles/Header.module.scss'
import Link from 'next/link'
function ServiceDropdown() {
    return(
        <div className={styles.dropdown_container}>
            <div className={styles.dropdown_head}>
                <div className="container">
                     SERVICES
                 </div>
            </div>
            <div className="container">
            <div className={styles.dropdown_section}>
                <div className={styles.dropdown_col + " "+ styles.first}>
                    <h5>Website Development</h5>
                    <ul>
                        <li> <Link href="#">New Website</Link></li>
                        <li> <Link href="#"> Existing Website</Link></li>
                        <li> <Link href="#">Class aptent</Link></li>
                        <li> <Link href="#">Nullam velsem</Link></li>
                        <li><Link href="#"> Curabitur Suscipitm/</Link></li>
                    </ul>
                </div>
                <div className={styles.dropdown_col}>
                <h5>Mobile Application Development</h5>
                    <ul>
                        <li> <Link href="#">iOs Application</Link></li>
                        <li> <Link href="#">Android Application</Link></li>
                        <li> <Link href="#">Hybrid Application</Link></li>
                        <li> <Link href="#">Loremipsum Doloresit</Link></li>
                        <li> <Link href="#">Amet Contserter</Link></li>
                    </ul>
                </div>
                <div className={styles.dropdown_col}>
                <h5>SEO</h5>
                    <ul>
                        <li> <Link href="#">On-page SEO</Link></li>
                        <li> <Link href="#">Off-page SEO</Link></li>
                        <li> <Link href="#">Technical SEO</Link></li>
                        <li> <Link href="#">Lipsum Condser</Link></li>
                    </ul>
                </div>
                <div className={styles.dropdown_col}>
                <h5>PPC</h5>
                    <ul>
                        <li> <Link href="#">Paid search marketing</Link></li>
                        <li> <Link href="#">Display advertising</Link></li>
                        <li> <Link href="#">Social media advertising</Link></li>
                        <li> <Link href="#">Retargeting PPC Ads</Link></li>
                        <li> <Link href="#">Affiliate marketing</Link></li>
                    </ul>
                </div>
                <div className={styles.dropdown_col}>
                <h5>Reputation Manager</h5>
                    <ul>
                        <li><Link href="#"> Monitoring reputation</Link></li>
                        <li> <Link href="#">Addressing any content</Link></li>
                        <li> <Link href="#">Prevent & solve problems</Link></li>
                        <li> <Link href="#">Praesent venenatis</Link></li>
                        <li> <Link href="#">ortor pulvinar varius</Link></li>
                    </ul>
                </div>
                <div className={styles.dropdown_col+ " "+ styles.last}>
                <h5>Website Development</h5>
                    <ul>
                        <li><Link href="#"> Software Updation</Link></li>
                        <li> <Link href="#">Speed Improvement</Link></li>
                        <li> <Link href="#">Backing up Files</Link></li>
                        <li> <Link href="#">Developing new content</Link></li>
                        <li> <Link href="#">Design Consistency</Link></li>
                    </ul>
                </div>
            </div>
            </div>
        </div>
    )
}
export default ServiceDropdown;