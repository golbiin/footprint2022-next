import Link from 'next/link'
import styles from '../../styles/Footer.module.scss'
import ServiceNewsletter from './newsLetter';
import FooterCopyright from './footercopyright';
function Footer() {
   
    return (
        
        <main>
        <ServiceNewsletter />
        
    
      <footer className={styles.footer}>
        <div className='container'>
        <div className={styles.footer_row}>
               <div className={styles.footer_section+" col-md-6"}>
                    <h5>Footprint</h5>
                    <p> Footprint simplifies software and digital marketing. Our dream, however, expands beyond that. We build the software your business requires to make an impact and then help you carve a footprint on the internet for your business</p>
               </div>
               <div className={styles.col_6}>
                    <h5>Pages</h5>
                    <ul className={styles.footer_menu}>
                        <li className={styles.menu_item}><Link href='/home'>Home</Link></li>
                        <li className={styles.menu_item}><Link href='/about-us'>About Us</Link></li>
                        <li className={styles.menu_item}><Link href='/portfolio'>Portfolio</Link></li>
                        <li className={styles.menu_item}><Link href='/success-stories'>Success Stories</Link></li>
                        <li className={styles.menu_item}><Link href='/contact-us'>Contact Us</Link></li>
                        <li className={styles.menu_item}><Link href='/terms-and-conditions'>Terms and Conditions</Link></li>
                        <li className={styles.menu_item}><Link href='/privacy-policy'>Privacy</Link></li>
                        {/* <li className={styles.menu_item}><a href='#'>Influence</a></li>
                        <li className={styles.menu_item}><a href='#'>Review Syndication</a></li>
                        <li className={styles.menu_item}><a href='#'>Price</a></li> */}
                    </ul>
               </div>
                <div className={styles.col_6}>
                    <h5> Services</h5>
                    <ul className={styles.footer_menu}>
                        <li className={styles.menu_item}><Link href='#'>Website Development</Link></li>
                        <li className={styles.menu_item}><Link href='#'>Mobile Application Development</Link></li>
                        <li className={styles.menu_item}><Link href='#'>SEO</Link></li>
                        <li className={styles.menu_item}><Link href='#'>PPC</Link></li>
                        <li className={styles.menu_item}><Link href='#'>Reputation Management</Link></li>
                        <li className={styles.menu_item}><Link href='#'>For Agencies</Link></li>
                        {/* <li className={styles.menu_item}><a href='#'>Influence</a></li>
                        <li className={styles.menu_item}><a href='#'>Review Syndication</a></li>
                        <li className={styles.menu_item}><a href='#'>Price</a></li>  */}
                    </ul>
                </div>
                <div className={styles.col_6}>
                    <h5>Specialized Services </h5>
                    <ul className={styles.footer_menu}>
                        <li className={styles.menu_item}><Link href='#'>Brochure Websites</Link></li>
                        <li className={styles.menu_item}><Link href='/ecommerce-website'>E-Commerce Websites</Link></li>
                        <li className={styles.menu_item}><Link href='/company-website'>Custom Websites</Link></li>
                        <li className={styles.menu_item}><Link href='#'>Mobile Websites and AMP</Link></li>
                        <li className={styles.menu_item}><Link href='#'>Web Apps</Link></li>
                        <li className={styles.menu_item}><Link href='#'>Native IOS Development</Link></li>
                        <li className={styles.menu_item}> <Link href="#">Native Android Application</Link></li>
                        <li className={styles.menu_item}><Link href="#">Hybrid Mobile Application Development</Link></li>
                        <li className={styles.menu_item}><Link href='#'>On-page SEO</Link></li>
                        <li className={styles.menu_item}><Link href='#'>Off-page SEO</Link></li>
                        <li className={styles.menu_item}><Link href='#'>Technical SEO</Link></li>
                        {/* <li className={styles.menu_item}><Link href='#'>Price</Link></li> */}
                    </ul>
                </div>
                
                { /* <div className={styles.col_6}> 
                    <h5>Support</h5>
                    <ul className={styles.footer_menu}>
                        <li className={styles.menu_item}><a href='#'>Overview</a></li>
                        <li className={styles.menu_item}><a href='#'>Company Review</a></li>
                        <li className={styles.menu_item}><a href='#'>Product Reviews</a></li>
                        <li className={styles.menu_item}><a href='#'>Local Reviews</a></li>
                        <li className={styles.menu_item}><a href='#'>In-store Reviews</a></li>
                        <li className={styles.menu_item}><a href='#'>google Seller Ratings</a></li>
                        <li className={styles.menu_item}><a href='#'>Influence</a></li>
                        <li className={styles.menu_item}><a href='#'>Review Syndication</a></li>
                        <li className={styles.menu_item}><a href='#'>Price</a></li>
                    </ul>
                </div> */}
                {/* <div className={styles.col_6}> 
                    <h5>Contact Footprint</h5>
                    <ul className={styles.footer_menu}>
                        <li className={styles.menu_item}><a href='#'>Overview</a></li>
                        <li className={styles.menu_item}><a href='#'>Company Review</a></li>
                        <li className={styles.menu_item}><a href='#'>Product Reviews</a></li>
                        <li className={styles.menu_item}><a href='#'>Local Reviews</a></li>
                        <li className={styles.menu_item}><a href='#'>In-store Reviews</a></li>
                        <li className={styles.menu_item}><a href='#'>google Seller Ratings</a></li>
                        <li className={styles.menu_item}><a href='#'>Influence</a></li>
                        <li className={styles.menu_item}><a href='#'>Review Syndication</a></li>
                        <li className={styles.menu_item}><a href='#'>Price</a></li>
                    </ul>
                </div>  */}
               
        </div>
        {/* <div className={styles.social_row}>
            <div className={styles.icons}>
                <a className={styles.ln + " "+ styles.social}>
                </a>
                <a className={styles.ytb + " "+  styles.social}>
                </a>
                <a className={styles.tw + " "+  styles.social}>
                </a>
                <a className={styles.insta+ " "+  styles.social}>
                </a>
            </div>
        </div> */}
              
        </div>
       
      </footer>
       <FooterCopyright />
      </main>
       
      
    )
    
    
}


    

export default Footer