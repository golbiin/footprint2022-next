import Head from 'next/head'

function SeoHead(props) {
    console.log("props>>>>", props.seoData.title)
  return (
    <div>
      <Head>
        <title>{props.seoData.title !== undefined ? props.seoData.title: 'Website Development, Apps, Design and Marketing | Footprint.io'}</title>
        <meta name="description" content={props.seoData.description !== undefined ? props.seoData.description: 'Hire Website & App Developers, Graphic Designers, and Digital Marketers at affordable rates. You have the vision. We can make it a reality.'} />
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
      </Head>
    </div>
  )
}

export default SeoHead