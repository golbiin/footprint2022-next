import React, { useEffect ,useState} from 'react'
import Head from 'next/head'
import Image from 'next/image'
import styles from '../../styles/Header.module.scss'
import Link from 'next/link'
import { slide as Menu } from 'react-burger-menu'
import DeviceMenu from './deviceMenu'
import * as authServices from "../../services/authService";
import { useRouter } from 'next/router'
//import ServiceDropdown from './service-dropdown'
import SeoHead from './seoHead';

function Header(props) {
    const router = useRouter()
    const [dropdown_status,setDropdownStatus]=useState(false);
    const [dropdown_sub,setSubDropdownStatus]=useState("");
    const [dropdown_sub_level3,setSubDropdownLevel3]=useState("");
    const logout=()=>{
        authServices.logout();
        router.push('/login')
    }
    useEffect(()=>{
        const concernedElement = document.querySelector(".hover_drop_down");
       
        document.addEventListener("mousemove", (event) => {
           
            if (concernedElement.contains(event.target)) {
                console.log(event.target);
                setDropdownStatus(true);
            }
             else {
                setDropdownStatus(false);
                setSubDropdownStatus("");
                setSubDropdownLevel3("");
            }
          });

    },[])
    return (
        <>
            <SeoHead seoData={props.seoData !== undefined ? props.seoData : {}} />
            <div className={styles.header}>
                <div className={styles.header_content+ " container"}>
                    <div className='lg_menu'>
                    <div className={styles.header_main}>
                        <div className={styles.logo}>
                            <Link href="/"><img src="/images/logo.svg" alt="Footprint Logo"  width={127} height={28}/></Link>
                        </div>
                        <div className={styles.header_menu}>
                            <nav className="navbar navbar-expand-sm">
                                <ul className="navbar-nav" >
                                    <li className="nav-item" >
                                    <Link href="/about-us"><a  className="nav-link">About </a></Link>
                                    </li>
                                    <li className="nav-item hover_drop_down">
                                    <a className={styles.dropdown+ " nav-link"} href="#" >Services </a>
                                    {dropdown_status?<div className={dropdown_sub?styles.dropdown_list+" "+styles.dropdown_full:styles.dropdown_list}>
                                        <div className={styles.dropdown_container+ " "+styles.show_top_arrow}>
                                            <ul className={styles.dropdown_first}>
                                                <li onMouseOver={()=>setSubDropdownStatus("website_development")}>
                                                    <a href='#' className={styles.has_child} >Website Development</a>
                                                </li>
                                                <li onMouseOver={()=>setSubDropdownStatus("mobile_appilcation")}><Link href="/mobile-application-development"><a href='#' className={styles.has_child}>Mobile Application Development</a></Link></li>
                                                <li onMouseOver={()=>setSubDropdownStatus("seo")}><Link href="/seo"><a href='#' className={styles.has_child} >SEO</a></Link></li>
                                                <li onMouseOver={()=>setSubDropdownStatus("ppc")}><Link href="#"><a href='#' className={styles.has_child} >PPC</a></Link></li>
                                                <li onMouseOver={()=>setSubDropdownStatus("reputaion_manager")}><Link href="#"><a href='#' className={styles.has_child}>Reputation Management</a></Link></li>
                                                <li onMouseOver={()=>setSubDropdownStatus("for_agencies")}><Link href="#"><a href='#' className={styles.has_child} >For Agencies</a></Link></li>
                                            </ul>
                                        </div>
                                        {dropdown_sub=="website_development"? 
                                        <div className={styles.dropdown_container+" "+styles.dropdown_sub}>
                                            <ul>
                                                <li> <Link href="/brochure-website">Brochure Websites</Link></li>
                                                <li> <Link href="/ecommerce-website">E-Commerce Websites</Link></li>
                                                <li> <Link href="/company-website">Custom Websites</Link></li>
                                                <li> <Link href="/mobile-websites-amp">Mobile Websites and AMP</Link></li>
                                                <li><Link href="/website-applications">Web Apps</Link></li>
                                            </ul>
                                        </div>:""
                                        }
                                        {dropdown_sub=="mobile_appilcation"? 
                                        <div className={styles.dropdown_container+" "+styles.dropdown_sub}>
                                            <ul>
                                                <li> <Link href="/native-ios-development">Native iOS Development</Link></li>
                                                <li> <Link href="#">Native Android Development</Link></li>
                                                <li> <Link href="#">Hybrid Mobile Application Development</Link></li>
                                            </ul>
                                        </div>:""
                                        }
                                        {dropdown_sub=="seo"? 
                                        <div className={styles.dropdown_container+" "+styles.dropdown_sub}>
                                            <ul>
                                                    <li> <Link href="#">On-page SEO</Link></li>
                                                    <li> <Link href="/off-page-seo">Off-page SEO</Link></li>
                                                    <li> <Link href="/technical-seo">Technical SEO</Link></li>
                                                    <li> <Link href="#">Plans</Link></li>
                                            </ul>
                                        </div>:""
                                        }
                                        {dropdown_sub=="ppc"? 
                                        <div className={styles.dropdown_container+" "+styles.dropdown_sub}>
                                            <ul>
                                                <li onMouseOver={()=>setSubDropdownLevel3("")}> <Link href="#">Paid search marketing</Link></li>
                                                <li onMouseOver={()=>setSubDropdownLevel3("")}> <Link href="#">Display advertising</Link></li>
                                                <li onMouseOver={()=>setSubDropdownLevel3("social")} className={styles.has_child}> <Link href="#"><a href='#'>Social media advertising </a></Link></li>
                                                <li onMouseOver={()=>setSubDropdownLevel3("")}> <Link href="#">Retargeting PPC Ads</Link></li>
                                            </ul>
                                        </div>:""
                                        }
                                        {dropdown_sub=="reputaion_manager"? 
                                        <div className={styles.dropdown_container+" "+styles.dropdown_sub}>
                                            <ul>
                                                <li  onMouseOver={()=>setSubDropdownLevel3("")}><Link href="#">Social Media Management</Link></li>
                                                <li className={styles.has_child}  onMouseOver={()=>setSubDropdownLevel3("review_management")} > <Link href="#">Review Management</Link></li>
                                            </ul>
                                        </div>:""
                                        }
                                        {dropdown_sub=="for_agencies"? 
                                        <div className={styles.dropdown_container+" "+styles.dropdown_sub}>
                                            <ul>
                                                <li><Link href="#"> White-Label Design</Link></li>
                                                <li> <Link href="#">White-Label Development</Link></li>
                                                <li> <Link href="#">White-Label Marketing</Link></li>
                                                <li> <Link href="#">Franchise</Link></li>
                                            </ul>
                                        </div>:""
                                        }
                                        {dropdown_sub_level3=="social"? 
                                        <div className={styles.dropdown_container+" "+styles.dropdown_sub}>
                                            <ul>
                                                <li><Link href="#"> Facebook</Link></li>
                                                <li><Link href="#">Instagram</Link></li>
                                                <li><Link href="#">Youtube</Link></li>
                                                <li><Link href="#">Tiktok</Link></li>
                                            </ul>
                                        </div>:""
                                        }
                                        {dropdown_sub_level3=="review_management"? 
                                        <div className={styles.dropdown_container+" "+styles.dropdown_sub}>
                                            <ul>
                                                <li><Link href="#"> Google My Business</Link></li>
                                                <li><Link href="#">TrustPilot</Link></li>
                                                <li><Link href="#">Yelp</Link></li>
                                            </ul>
                                        </div>:""
                                        }
                                    </div>:""}
                                    </li>
                              {/*       <li className="nav-item">
                                    <Link href="/pricing"><a  className="nav-link" >Pricing </a></Link>
                                    </li> */}
                                    <li className="nav-item">
                                    <Link href="/portfolio"><a  className="nav-link">Portfolio </a></Link>
                                    </li>
                                  {/*   <li className="nav-item">
                                    <a className="nav-link" href="#">Blog</a>
                                    </li> */}
                                    <li className="nav-item">
                                    <Link href="/success-stories"><a  className="nav-link">Success Stories </a></Link>
                                    </li>
                                    <li className="nav-item">
                                    <Link href="/contact-us"><a  className="nav-link">Contact </a></Link>
                                    </li>
                                
                                </ul>
                            </nav>
                        </div>
                        <div className={styles.nav_right}>
                        {!authServices.getCurrentUser() ? <><Link href="/login"><a className={styles.sign_link} >Login</a></Link> <Link href="/request-sample"><a  className="fp_green_btn header_btn">Request a Free Sample</a></Link> </>: <><a className={styles.sign_link} onClick={logout}>Logout</a><Link href="/dashboard"><a  className="fp_green_btn header_btn">Account</a></Link></>}
                        
                            
                        </div>
                    </div>
                    
                    </div>
                    <div className='mobile_menu'>
                    <DeviceMenu currentUser={authServices.getCurrentUser()} logout={logout} />
                    </div>
                </div>
                
                {/* {dropdown_status? <ServiceDropdown />:""} */}
            </div>
        </>
    )
}
export default Header