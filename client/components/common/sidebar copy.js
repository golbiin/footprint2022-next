import styles from '../../styles/Dashboard.module.scss'
import Link from 'next/link'
import Image from 'next/image'
import * as authServices from "../../services/authService";
import * as userServices from "../../services/userUservice";
import { useRouter } from 'next/router';
import React, { useEffect,useState } from 'react';
import { slide as Menu } from "react-burger-menu";
function Sidebar() {
  const router = useRouter();
  const [dropdown,setDropdown]=useState(false);
  const [profile,setProfile]=useState({});
  const [profileLoad,setProfileLoad]=useState(false);
  const logout=()=>{
    authServices.logout();
    router.push('/login')
}
const toogleDropdown = () => {
  setDropdown(!dropdown);
};

const  getUserdetails=async()=>{

  const response = await userServices.geUserProfile();
  if (response && response.data) {
    if (response.data.status === 1) {
     
      if(response.data.data.billing !== undefined)
          setProfile(response.data.data.billing)
          setProfileLoad(true)
    } 
  }

}
useEffect(async () => {
  let user=authServices.getCurrentUser();
  let current_user_id=user._id;
  getUserdetails();
  
  if(router.pathname === "/transactions" || router.pathname === "/payment-method" || router.pathname === "/billing-details")
    setDropdown(true);
},[])
console.log("profile", profile)
    return (
      <>
        <div className={styles.dashboard_burger_menu}>
          <div className={styles.logo}>
            <Link href="/home"><Image src="/images/Footprint.png" alt="Footprint Logo"  width={127} height={28}/></Link>
          </div>
          <Menu>
            <div className={styles.sidebar}>
              <div className={styles.menu}>
                <Link className={styles.each_menu} href="/dashboard">
                  <div className={styles.title}>
                    <p>Dashboard</p>
                  </div>
                </Link>
                <a  className={styles.each_menu} onClick={toogleDropdown}>

                  <div className={styles.title + " " +styles.dropdown}>
                    <p>Billing</p>
                    <span
                      className={
                        dropdown
                          ?styles.drop_arrow + " " + styles.up
                          : styles.drop_arrow + " " + styles.down
                      }
                    ></span>
                  </div>
                </a>
                <div
                  className={
                   dropdown
                   ? styles.drop_arrow + " " + styles.up
                   : styles.drop_arrow + " " + styles.down
                  }
                >
                  <Link className={styles.each_menu} href="/billing-details">
                    <div>
                      <p>Billing Information</p>
                    </div>
                  </Link>
                  <Link className={styles.each_menu} href="/billing">
                    <div>
                      <p>Payment Methods</p>
                    </div>
                  </Link>
                  <Link className={styles.each_menu} href="/wire-details">
                    <div>
                      <p>Wire Details</p>
                    </div>
                  </Link>
                  <Link className={styles.each_menu} href="/transaction">
                    <div>
                      <p>Transactions</p>
                    </div>
                  </Link>
                     <Link className={styles.each_menu} href="/my-wallet">
                    <div>
                      <p>Redeem Coupon</p>
                    </div>
                  </Link>   
                </div>
                

                <Link className={styles.each_menu} href="/settings">
                  <div className={styles.title}>
                    <p>Settings</p>
                  </div>
                </Link>
              </div>
              <div className={styles.account}>
                <div className={styles.icon}>
                  <Link className={styles.letter} href="/settings">
                    <span>A</span>
                  </Link>
                </div>
                <div className={styles.title}>
                  <h3>TEST</h3>
                  <Link className={styles.project_name} href="/settings">
                    <p>TEST</p>
                  </Link>
                  <a className={styles.logout}>
                    Logout
                  </a>
                </div>
              </div>
            </div>
          </Menu>
        </div>
        <div className={styles.left_box+" "+ "col-md-3"}>
          <div className={styles.sidebar}>
          <div className={styles.logo}>
           <Link href="/home"><Image src="/images/Footprint.png" alt="Footprint Logo"  width={127} height={28}/></Link>
          </div>
          <div className={styles.menus}>
            <Link  href="/dashboard"><div className={styles.each_menu} >
            <div className={router.pathname === "/dashboard" ? styles.title+" "+styles.icon1+" "+styles.active : styles.title+" "+styles.icon1 }>
                <p>Dashboard</p>
              </div>
              </div>
            </Link>
         
            <Link  href="#"><div className={styles.each_menu}  onClick={toogleDropdown}>
              <div className={router.pathname === "/billing-details" || router.pathname === "/transactions" || router.pathname === "/payment-method" ? styles.dropdown+" "+styles.title+" "+styles.icon2+" "+styles.active : styles.dropdown+" "+styles.title+" "+styles.icon2 }>
                <p>Billing</p>
                <span className={dropdown?styles.up_arrow:styles.down_arrow}></span>
              </div>
              </div>
            </Link>
               <div
                     className={
                      dropdown
                        ? styles.billing_drop + " " +styles.show
                        : styles.billing_drop + " " +styles.hide
                    }
                >
                  <Link  href="/transactions">
                    <div className={styles.each_menu}>
                      <p>Transactions</p>
                    </div>
                  </Link> 
                  <Link href="/billing-details">
                    <div  className={styles.each_menu}>
                      <p>Billing Information</p>
                    </div>
                  </Link>
                  <Link  href="/payment-method">
                    <div className={styles.each_menu}>
                      <p>Payment Methods</p>
                    </div>
                  </Link> 
                  
                </div>
            <Link  href="/pricing"><div className={styles.each_menu} >
            <div className={styles.title+" "+styles.icon3}>
                <p>Add A Plan</p>
              </div>
              </div>
            </Link>
            <Link  href="/settings"><div className={styles.each_menu} >
            <div className={router.pathname === "/settings" ? styles.title+" "+styles.icon4+" "+styles.active : styles.title+" "+styles.icon4 }>
                <p>Settings</p>
              </div>
              </div>
            </Link>
          </div>
          <div className={styles.logout_area}>
             <div className={styles.side_user_name}>
                 
                   {profile?  profile.first_name !== undefined ? 
                   <>
                      <div className={styles.letter}>{profile.first_name.charAt(0)}</div>
                      <div className={styles.fullname}> 
                        <h3>{profile.first_name}</h3>
                        <p>{profile.company}</p>
                     </div>
                   </>
                   : profileLoad ? 
                   <>
                      <div className={styles.letter}>O</div>
                      <div className={styles.fullname}> 
                        <Link  href="/settings" className={styles.updatename}>
                          Update Name
                        </Link>
                      </div>
                   </>
                   : null
                    :""}
                
             </div>
             <a onClick={logout} className={styles.signout_btn+' fp_green_btn'}>Logout</a>
          </div>
        </div>
      </div>
      </>
    )
}
export default Sidebar