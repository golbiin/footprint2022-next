import React, { useEffect,useState  } from 'react'
import Image from 'next/image'
import styles from '../../styles/newsletter.module.scss' 
import { useRouter } from 'next/router';
import Joi, { errors } from "joi-browser";
import ValidationError from '../../components/common/validation-error';
import SubmitButton from '../../components/submit-button';
import * as userService from "../../services/userUservice";
function ServiceNewsletter() {
    const router = useRouter();
    const [newsData,setNewsData]=useState({email:""})
    const [errors,setErrors]=useState([]);
    const [submit_status,setSubscribe]=useState(false);
    const [response_type,setResponseType]=useState("");
    const [response_message,setResponseMessage]=useState("");
 
    const schema = {
        email: Joi.string()
        .required()
        .email()
        .error(() => {
          return {
            message:
              "Please enter a properly formatted email."
          };
        }),
    }
    const newsChange = async ({ currentTarget: input }) => {
        setResponseMessage("");
        const errors = { ...errors };
        const errorMessage = validateProperty(input);
        if (errorMessage) errors[input.name] = errorMessage;
        else delete errors[input.name];
        newsData[input.name] = input.value;
        setNewsData(newsData);
        setErrors(errors);
      };
      const validateProperty = ({ name, value }) => {
        const obj = { [name]: value };
        const lschema = { [name]: schema[name] };
        const { error } = Joi.validate(obj, lschema);
        return error ? error.details[0].message : null;
      };
      const newsSubmit = async () => {
        const errors = { ...errors }; 
        const { error } = Joi.validate(newsData, schema);
        if (error) {
          let path = error.details[0].path[0];
          let errormessage = error.details[0].message;
          errors[path] = errormessage;
          setErrors(errors);
        } else {
          setSubscribe(true);
        
          try {
                const response = await userService.add_news_letter(
                newsData
              );
               if (Object.keys(response).length > 0) {
             
                  
                  newsData.email = "";
                  
                setNewsData(newsData);
                setResponseType( response.data.status);
                setResponseMessage(response.data.message);
                setSubscribe(false);
              }
          } catch (err) {
            console.log(err);
          }
      
            
        
        }
      };
      const checkBtnenable = () => {
        return null;
        const { error } = Joi.validate(newsData, schema);
        console.log(error,newsData)
        if (!error) {
          return null;
        }
        const errors = {};
        for (let item of error.details) errors[item.path[0]] = item.message;
        return errors;
    };
    return(
        <div className={styles.service_newsletter}>
        <div className="container">
        <div className={styles.res_contain+' response_container'}>
                          <div className={response_type==1?"sucsess":"error"}>
                             <span>{response_message} </span>
                          </div>
                    </div>
              <div className="row" >
                  <div className={styles.col_9+" col-md-9"}>
                      <h5>Stay in the loop</h5>
                      <p>Please subscribe the newsletter and get all the updates from Footprint.</p>
                      <div className={styles.col_12+" col-md-12"}>
                      <input type="email" placeholder="Email Address"  name="email" value={newsData.email} onChange={newsChange}></input>
                      
                      <SubmitButton btn_label="Subscribe"className={styles.news_btn} submit_status={submit_status} submit={newsSubmit} Btnenable={checkBtnenable}/>
                      {errors.email?<ValidationError message={errors.email} />:null}
                      </div>
                  </div>
                  <div className={styles.col_3+" col-md-3"}>
                   <h5>Join Our Community</h5>
                        <div className={styles.col_12+" col-md-12"}>
                           <a href="https://www.google.ca/search?ved=1t:65428&hl=en&_ga=2.249145345.764093815.1646964071-807656218.1646964071&q=Footprint.io+-+Design,+Websites,+Mobile+%26+Digital+Marketing&ludocid=8001397384966699252&lsig=AB86z5VPlIY0SwzQmAvVjBM5WDum#fpstate=lie" target="_blank" rel="noopener noreferrer" className={styles.news_google}></a>
                           <a href="https://www.facebook.com/footprint.io" target="_blank" rel="noopener noreferrer" className={styles.news_facebook}></a>
                           <a href="https://www.instagram.com/footprint_io" target="_blank" rel="noopener noreferrer" className={styles.news_insta}></a>
                        </div>  
                  </div>
              </div>
            </div>
        </div>
        )

}
export default ServiceNewsletter