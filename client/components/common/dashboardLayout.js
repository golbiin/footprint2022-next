import React, { useEffect ,useState} from 'react'
import styles from '../../styles/Dashboard.module.scss'
import * as authServices from "../../services/authService";
import { useRouter } from 'next/router'
import SeoHead from './seoHead';
 

function DashboardLayout(props) {
    const router = useRouter()
    const [loginCheck, SetLoginCheck] = useState(false);

    
    useEffect(() => {
        let res=authServices.getCurrentUser();
        if(res) {
            SetLoginCheck(true);
        }
        else {
            router.push('/login')
        }
    },[])

    return(
        loginCheck?
        <>
            <SeoHead seoData={props.seoData !== undefined ? props.seoData : {}} />
            <div className={styles.dashboard_container}>
                <div className={styles.row}>
                    {props.children}
                </div>
            </div>
        </>
        :
        null
    )
}

export default DashboardLayout