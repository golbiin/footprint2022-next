import React, { useEffect ,useState} from 'react'
import Head from 'next/head'
import Image from 'next/image'
import styles from '../../styles/Header.module.scss'
import Link from 'next/link'
import { slide as Menu } from 'react-burger-menu'
import DeviceMenu from './deviceMenu'
import * as authServices from "../../services/authService";
import { useRouter } from 'next/router'
import SeoHead from './seoHead'
//import ServiceDropdown from './service-dropdown'

function Header(props) {
    const router = useRouter()
    const [dropdown_status,setDropdownStatus]=useState(false);
    const [dropdown_sub,setSubDropdownStatus]=useState("");
    const logout=()=>{
        authServices.logout();
        router.push('/login')
    }
    useEffect(()=>{
        const concernedElement = document.querySelector(".hover_drop_down");
       
        document.addEventListener("mousemove", (event) => {
           
            if (concernedElement.contains(event.target)) {
                console.log(event.target);
                setDropdownStatus(true);
            }
             else {
                setDropdownStatus(false);
                setSubDropdownStatus(false);
            }
          });

    },[]);
   
    return (
        <>
            <SeoHead seoData={props.seoData !== undefined ? props.seoData : {}} />
            <div className={styles.header}>
                <div className={styles.header_content+ " container"}>
                    <div className='lg_menu'>
                    <div className={styles.header_main}>
                        <div className={styles.logo}>
                            <Link href="/home"><Image src="/images/logo.png" alt="Footprint Logo"  width={127} height={28}/></Link>
                        </div>
                        <div className={styles.header_menu}>
                            <nav className="navbar navbar-expand-sm">
                                <ul className="navbar-nav" >
                                    <li className="nav-item" >
                                    <Link href="/about"><a  className="nav-link">About </a></Link>
                                    </li>
                                    <li className="nav-item hover_drop_down">
                                    <Link href="/services"><a className={styles.dropdown+ " nav-link"} href="#" >Services </a></Link>
                                    {dropdown_status?<div className={dropdown_sub?styles.dropdown_list+" "+styles.dropdown_full:styles.dropdown_list}>
                                        <div className={styles.dropdown_container+ " "+styles.show_top_arrow}>
                                            <ul className={styles.dropdown_first}>
                                                <li>
                                                    <a href='#' onMouseOver={()=>setSubDropdownStatus("website_development")}>Website Development</a>
                                                </li>
                                                <li><Link href="#"><a href='#' onMouseOver={()=>setSubDropdownStatus("mobile_appilcation")}>Mobile Application Development</a></Link></li>
                                                <li><Link href="#"><a href='#' onMouseOver={()=>setSubDropdownStatus("seo")}>SEO</a></Link></li>
                                                <li><Link href="#"><a href='#' onMouseOver={()=>setSubDropdownStatus("ppc")}>PPC</a></Link></li>
                                                <li><Link href="#"><a href='#' onMouseOver={()=>setSubDropdownStatus("reputaion_manager")}>Reputation Manager</a></Link></li>
                                                <li><Link href="#"><a href='#' onMouseOver={()=>setSubDropdownStatus("for_agencies")}>For Agencies</a></Link></li>
                                            </ul>
                                        </div>
                                        {dropdown_sub=="website_development"? 
                                        <div className={styles.dropdown_container+" "+styles.dropdown_sub}>
                                            <ul>
                                                <li> <Link href="#">New Website</Link></li>
                                                <li> <Link href="#"> Existing Website</Link></li>
                                                <li> <Link href="#">Class aptent</Link></li>
                                                <li> <Link href="#">Nullam velsem</Link></li>
                                                <li><Link href="#"> Curabitur Suscipitm</Link></li>
                                            </ul>
                                        </div>:""
                                        }
                                        {dropdown_sub=="mobile_appilcation"? 
                                        <div className={styles.dropdown_container+" "+styles.dropdown_sub}>
                                            <ul>
                                                <li> <Link href="#">iOs Application</Link></li>
                                                <li> <Link href="#">Android Application</Link></li>
                                                <li> <Link href="#">Hybrid Application</Link></li>
                                                <li> <Link href="#">Loremipsum Doloresit</Link></li>
                                                <li> <Link href="#">Amet Contserter</Link></li>
                                            </ul>
                                        </div>:""
                                        }
                                        {dropdown_sub=="seo"? 
                                        <div className={styles.dropdown_container+" "+styles.dropdown_sub}>
                                            <ul>
                                                    <li> <Link href="#">On-page SEO</Link></li>
                                                    <li> <Link href="#">Off-page SEO</Link></li>
                                                    <li> <Link href="#">Technical SEO</Link></li>
                                                    <li> <Link href="#">Lipsum Condser</Link></li>
                                            </ul>
                                        </div>:""
                                        }
                                        {dropdown_sub=="ppc"? 
                                        <div className={styles.dropdown_container+" "+styles.dropdown_sub}>
                                            <ul>
                                                <li> <Link href="#">Paid search marketing</Link></li>
                                                <li> <Link href="#">Display advertising</Link></li>
                                                <li> <Link href="#">Social media advertising</Link></li>
                                                <li> <Link href="#">Retargeting PPC Ads</Link></li>
                                                <li> <Link href="#">Affiliate marketing</Link></li>
                                            </ul>
                                        </div>:""
                                        }
                                        {dropdown_sub=="reputaion_manager"? 
                                        <div className={styles.dropdown_container+" "+styles.dropdown_sub}>
                                            <ul>
                                                <li><Link href="#"> Monitoring reputation</Link></li>
                                                <li> <Link href="#">Addressing any content</Link></li>
                                                <li> <Link href="#">Prevent & solve problems</Link></li>
                                                <li> <Link href="#">Praesent venenatis</Link></li>
                                                <li> <Link href="#">ortor pulvinar varius</Link></li>
                                            </ul>
                                        </div>:""
                                        }
                                        {dropdown_sub=="for_agencies"? 
                                        <div className={styles.dropdown_container+" "+styles.dropdown_sub}>
                                            <ul>
                                                <li><Link href="#"> Software Updation</Link></li>
                                                <li> <Link href="#">Speed Improvement</Link></li>
                                                <li> <Link href="#">Backing up Files</Link></li>
                                                <li> <Link href="#">Developing new content</Link></li>
                                                <li> <Link href="#">Design Consistency</Link></li>
                                            </ul>
                                        </div>:""
                                        }
                                    </div>:""}
                                    </li>
                                    <li className="nav-item">
                                    <Link href="/pricing"><a  className="nav-link" >Pricing </a></Link>
                                    </li>
                                    <li className="nav-item">
                                    <Link href="/portfolio"><a  className="nav-link">Portfolio </a></Link>
                                    </li>
                                    <li className="nav-item">
                                    <a className="nav-link" href="#">Blog</a>
                                    </li>
                                    <li className="nav-item">
                                    <Link href="/success-stories"><a  className="nav-link">Success Stories </a></Link>
                                    </li>
                                    <li className="nav-item">
                                    <Link href="/contact"><a  className="nav-link">Contact </a></Link>
                                    </li>
                                
                                </ul>
                            </nav>
                        </div>
                        <div className={styles.nav_right}>
                        {!authServices.getCurrentUser() ? <><Link href="/login"><a className={styles.sign_link} >Login</a></Link> <Link href="/request-sample"><a  className="fp_green_btn header_btn">Request a Free Sample</a></Link> </>: <><a className={styles.sign_link} onClick={logout}>Logout</a><Link href="/dashboard"><a  className="fp_green_btn header_btn">Account</a></Link></>}
                        
                            
                        </div>
                    </div>
                    
                    </div>
                    <div className='mobile_menu'>
                    <DeviceMenu currentUser={authServices.getCurrentUser()} logout={logout} />
                    </div>
                </div>
                
                {/* {dropdown_status? <ServiceDropdown />:""} */}
            </div>
        </>
    )
}
export default Header