import React, { Component } from "react";
import Link from 'next/link'
import Image from 'next/image'
import styles from '../../styles/Dashboard.module.scss'
import * as authServices from "../../services/authService";



class DeviceMenuDashboard extends Component {
    state={
        dropdown:false
    }
  openNav = () => {
    document.getElementById("mySidebar").style.width = "100%";
  };

  closeNav = () => {
    document.getElementById("mySidebar").style.width = "0";
  };
 
   toogleDropdown = (val) => {
       console.log(val);
   this.setState({dropdown:val})
  };
 
  render() {
    console.log("*****************",this.props.first_name)
    return (
      <React.Fragment>
        <div className="mobile-main-dashboard">
          <div className="container">
            <div className="device-menu">
            <div className={styles.logo}>
                        <Link href="/" passHref><img src="/images/logo.svg" alt="Footprint Logo"  width={127} height={28}/></Link>
            </div>
              <div className="item-burger">
                <div className="menu-burger" onClick={this.openNav}>
                  <span></span>
                  <span className="middle"></span>
                  <span></span>
                </div>
              </div>
            </div>
            <div id="mySidebar" className="sidebar">
              <a
                href="javascript:void(0)"
                className="closebtn"
                onClick={this.closeNav}
              >
                &times;
              </a>
              <div className={styles.burger_menu}>
                <Link  href="/dashboard"><a  className={styles.each_menu}><div className={styles.title}>
                    <p>Dashboard</p>
                  </div></a></Link>
                <a  className={styles.each_menu} onClick={()=>this.toogleDropdown(!this.state.dropdown)}>

                  <div className={styles.title + " " +styles.dropdown}>
                    <p>Billing</p>
                    <span
                      className={
                        this.state.dropdown
                          ?styles.drop_arrow_m + " " + styles.up
                          : styles.drop_arrow_m + " " + styles.down
                      }
                    ></span>
                  </div>
                </a>
                <div
                  className={
                    this.state.dropdown
                   ? styles.drop_arrow_m + " " +styles.show
                   : styles.drop_arrow_m + " " +styles.hide
                  }
                >
                  <Link className={styles.each_menu} href="/transactions">
                    <div>
                      <p>Transactions</p>
                    </div>
                  </Link>
                  <Link className={styles.each_menu} href="/billing-details">
                    <div>
                      <p>Billing Information</p>
                    </div>
                  </Link>
                  <Link className={styles.each_menu} href="/payment-method">
                    <div>
                      <p>Payment Methods</p>
                    </div>
                  </Link>
                  {/* <Link className={styles.each_menu} href="/wire-details">
                    <div>
                      <p>Wire Details</p>
                    </div>
                  </Link> */}
                  
                     {/* <Link className={styles.each_menu} href="/my-wallet">
                    <div>
                      <p>Redeem Coupon</p>
                    </div>
                  </Link>    */}
                </div>
                {/* <Link  href="/pricing">
                  <a className={styles.each_menu} >
                  <div className={styles.title}>
                      <p>Add A Plan</p>
                        </div>
                    </a>
                </Link> */}

                <Link href="/settings">
                <a  className={styles.each_menu}>
                  <div className={styles.title}>
                    <p>Settings</p>
                  </div>
                  </a>
                </Link>
              </div>
              <div className={styles.burger_account}>
                <div className={styles.icon}>
                  <Link  href="/settings">
                  <a  className={styles.each_menu}>
                    <span>{this.props.first_name?this.props.first_name.charAt(0):""}</span>
                    </a>
                  </Link>
                </div>
                <div className={styles.title}>
                  <h3>{this.props.first_name}</h3>
                  <Link href="/settings">
                      <a  className={styles.project_name}>
                      <p>{this.props.company}</p>
                    </a>
                  </Link>
                  <a onClick={this.props.logout} className={styles.logout+' fp_green_btn'}>
                    Logout
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default DeviceMenuDashboard;

