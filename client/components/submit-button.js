import Spinner from "react-bootstrap/Spinner";
import React from "react";
function SubmitButton(props) {
    return(
      <React.Fragment>
          {props.submit_status===true?<button className={props.className} disabled>
            <Spinner animation={props.animation} variant={props.varient} size={props.size} />
            {" "} Submitting...
            </button>:<button className={props.className} onClick={props.submit} disabled={props.Btnenable()}>{props.btn_label}</button>}
      </React.Fragment>
    
    )
}

export default SubmitButton