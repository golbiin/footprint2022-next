const {
  PHASE_DEVELOPMENT_SERVER,
  PHASE_PRODUCTION_BUILD,
} = require('next/constants')

module.exports = (phase) => {

  // when started in development mode `next dev` or `npm run dev` regardless of the value of STAGING environmental variable
  const isDev = phase === PHASE_DEVELOPMENT_SERVER
  // when `next build` or `npm run build` is used
  const isProd = phase === PHASE_PRODUCTION_BUILD && process.env.STAGING !== '1'
  // when `next build` or `npm run build` is used
  const isStaging =
  phase === PHASE_PRODUCTION_BUILD && process.env.STAGING === '1'

  console.log(`phase:${phase} isDev:${isDev}  isProd:${isProd}   isStaging:${isStaging}`)

  const env = {
    env: {
      customKey: isProd? 'production - true' : 'production - false',
      customKeyisDev: isDev? 'isDev - true' : 'isDev - false',
      customKeyisStaging: isStaging? 'isStaging - true' : 'isStaging - false',
      STRIPE_PUBLISHABLE: isProd ? 'pk_test_51KHNO1KnRwwcFxBMHMM9XYDRkENYuVaeQJh4vihC8TLsW6SmHmshPkx6ykj2d4fnmlnD5omGyE3EjyfZwFeoQkQL00L0KymWN0' : 'pk_test_51KHNO1KnRwwcFxBMHMM9XYDRkENYuVaeQJh4vihC8TLsW6SmHmshPkx6ykj2d4fnmlnD5omGyE3EjyfZwFeoQkQL00L0KymWN0',
       //sk_test_51KHNO1KnRwwcFxBMleCnhqtCb6x0tdmp0wN2CnungPXjwCLLpBaoQrzx3O72ha2Qyl7M3vJLMeWlvO7o0oDwLKLi00YbxLib9E
    },
    reactStrictMode: true,
    images: {
      domains: ['unsplash.it', 'outsourceme.s3.amazonaws.com'],
    },
    async redirects() {
    return [
      {
        source: '/home',
        destination: '/',
        permanent: true,
      },
      {
        source: '/payment-card',
        destination: '/payment/card',
        permanent: true,
      },
      ]
    },
    async rewrites() {
      return [
        { 
          source: '/payment/card', destination: '/payment-card' 
        },
        { 
          source: '/', destination: '/home' 
        },
      ];
    }
  }
  return env;
}
