import { useCountUp } from 'react-countup';
import { React } from 'react';
import { useState, useEffect, useRef } from "react";
import CountUp from 'react-countup';
import VisibilitySensor from 'react-visibility-sensor';


const CompleteHook = () => {
 
  return (
      <div style={{paddingTop:'250px', color:'red'}}>
    <VisibilitySensor partialVisibility offset={{ bottom: 200 }}>
    {({ isVisible }) => (
      <div style={{ height: 100 }}>
        {isVisible ? <CountUp end={1000} /> : null}
      </div>
    )}
  </VisibilitySensor>
  </div>
  );
};

export default CompleteHook;