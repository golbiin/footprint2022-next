import Footer from "../../components/common/footer"
import Header from "../../components/common/header"
import styles from '../../styles/Cancellation.module.scss'
function CancellationPolicy() {
    return(
        <div>
            <Header />
            <div className="container">
                <div className={styles.cancellation_cotainer}>
                <h1>Terms of use</h1>
                <h2>Effective Date: January 14, 2021</h2>
                <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using &apos;Content here, content here&apos;, making it look like readable English. 
                </p>

                <h2>Effective Date: January 14, 2021</h2>
                <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don&apos;t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn&apos;t anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc. 
                </p>

                <h2>Effective Date: January 14, 2021</h2>
                <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using &apos;Content here, content here&apos;, making it look like readable English. 
                </p>
                </div>
            </div>
            <Footer />
        </div>
    )
}

export default CancellationPolicy
