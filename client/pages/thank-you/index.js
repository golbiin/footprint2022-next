import Footer from "../../components/common/footer"
import Header from "../../components/common/header"
import { useRouter } from 'next/router'
import styles from '../../styles/PaymentCard.module.scss'
import Image from "next/image";


function Payment(props){
    const router = useRouter();
    console.log("propssssssssss222222", props)
    console.log("router", router)
    return (
    <main>
        <Header />
            <div className={styles.thank_you}>
                <div className="container">
                    <Image src="/images/success.png" width={120} height={120} alt="success" loading="eager" />
                    <h2>Subscription successfull</h2>
                    {/* <p>Your Payment id: </p>  */}
                </div>
            </div>
        <Footer />
    </main>)
}
export default Payment