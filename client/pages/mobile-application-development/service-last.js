import React, { useEffect } from 'react'
import Image from 'next/image'
import styles from '../../styles/services.module.scss'
import AOS from "aos";
function ServiceLast() {
    useEffect(() => {
        AOS.init();
    },[])
    return(
        <div className="container">
            <div className={styles.service_last+ " services_last"} data-aos="zoom-in">
                    <div className="row last" data-aos="zoom-in">
                        <div className={styles.col_6+" col-md-6"+" brochure_sectionlast"}>
                                <div className={styles.service_sectionthree}>
                                    <ul>
                                        <li>
                                            <h5> Android Application Development</h5>
                                            Android phones dominate the mobile market by a wide margin. We provide full Android application development services. Our team can help you make the best application for multiple Android devices available today. 
                                        </li>
                                          <li>
                                            <h5>iOS Application Development </h5>
                                            We also provide a full range of application development services for iOS devices including iPhone, iPad, Apple TV, and even Apple watch development. Tapping into the iOS user market is essential if you want to take your business objectives further.  
                                        </li>
                                        <li>
                                            <h5>Comprehensive Code Audits</h5>
                                            Our team at Footprint also provides code audit services to customers that have existing iOS or Android. This service can help detect bugs and architectural flaws that may hamper the app&apos;s performance. We will analyze and inspect the code to find all possible weaknesses and make a comprehensive list of improvements that can make your app faster, more productive, and stable. 
                                        </li> 
                                    </ul>
                                </div>
                        </div>
                        <div className={styles.col_6+" col-md-6"+" service_three center"}>
                            <img src="/images/service_sectionthree.png" className='common_next_img_updated' width={500} height={523} alt="duis" loading="eager"/>
                        </div>
                    </div>
                   
            </div>
        </div>
        )

    }
    export default ServiceLast