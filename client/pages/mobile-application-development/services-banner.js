import React, { useEffect } from 'react'
import Image from 'next/image'
import styles from '../../styles/services.module.scss'
import AOS from "aos";
function ServiceBanner() {
    useEffect(() => {
        AOS.init();
    },[])
    return(
        <div className="container">
            <div className={styles.service_section} data-aos="zoom-in">
             <h1>Mobile Application Development</h1>
                
                <p style={{maxWidth: "900px"}}>
                Leveraging mobile technology is one of the smartest moves any business owner can take in our current age of technology. Here at Footprint, we have the best app developers that are ready to work with you on your next big mobile app project.</p>
            </div>
        </div>
    )

}
export default ServiceBanner