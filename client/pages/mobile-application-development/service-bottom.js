import React, { useEffect } from 'react'
import Image from 'next/image'
import styles from '../../styles/services.module.scss'
import AOS from "aos";
function ServiceBottom() {
    useEffect(() => {
        AOS.init();
    },[])
    return(
        <div className="container">
             <div className={styles.servicebottom_section+ " services_bottom"} data-aos="zoom-in">
                <h2>Mobile App Consulting Service</h2>
                <p style={{maxWidth: "900px"}}>We also have expert consultants that can assist you and your team on app concept finalization, professional advice on device/platform compatibility for your app, and also how to optimize project costs. </p> 
            
                <div className={styles.service_bottom+ " bottom_service"} data-aos="zoom-in">
                    <div className="row" data-aos="zoom-in">
                    <div className={styles.col_6+" col-md-6"+" service_two center"}>
                        <img src="/images/service_sectiontwo.png" className='common_next_img_updated' width={500} height={523} alt="duis" loading="eager"/>
                    </div>
                    <div className={styles.col_6+" col-md-6"}>
                            <div className={styles.service_sectiontwo}>
                                <ul>
                                    <li>
                                        <h5>Mobile App UI and UX Design</h5>
                                        Our designers at Footprint can create intuitive and slick interface designs that can fit your needs. Our designs ensure optimal user experience and engagement.  
                                    </li>
                                    <li>
                                        <h5>Mobile App Integration</h5>
                                        Our mobile app experts are well-versed in a variety of APIs. We can guarantee seamless integration with any third-party software your project may need. 
                                    </li>
                                    <li>
                                        <h5>Mobile App Testing and QA</h5>
                                        We have certified QA testers and engineers that can perform security, UX, accessibility, and performance testing. We will ensure that your mobile app is the best version it can be before it is shipped out to your users.   
                                    </li>
                                </ul>
                            </div>
                        </div>
                    
                    </div>
                </div>
            </div>
         </div>
    )

}
export default ServiceBottom