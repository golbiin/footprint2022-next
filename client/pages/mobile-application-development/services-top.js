import React, { useEffect } from 'react'
import Image from 'next/image'
import styles from '../../styles/services.module.scss'
import AOS from "aos";
function ServiceTop() {
    useEffect(() => {
        AOS.init();
    },[])
    return(
        <div className="container">
            <div className={styles.service_topsection + " service_top"} data-aos="zoom-in">
                <div className="row first" data-aos="zoom-in">
                   <div className={styles.col_6+" col-md-6"+" brochure_sectionone"}>
                        <div className={styles.service_sectionone}>
                            
                            <ul>
                                <li>
                                    <h5>Consistent Delivery</h5>
                                    
                                    We can provide consistent and optimal outputs for our mobile application projects. We achieve this consistency by using various software development techniques and methodologies that can adapt to various project types and sizes. We can ensure that your project is delivered on time while avoiding schedule slips, reworks, and any last-minute emergency work.  
                                </li>
                                <li>
                                    <h5>Informed Decision Making</h5>
                                    
                                    To have the highest chance of bringing your business objective, we at Footprint perform thorough market and business research when creating a mobile app. The data and insights we gather from research can allow our team to form a comprehensive feature list that can satisfy our customer’s needs. 
                                </li>  
                                <li>
                                    <h5>Clear Communication</h5>
                                    
                                    Clear communication is essential for a smooth and successful project. We ensure that communication between our teams and the client is streamlined by creating effective communication plans. These plans schedule and describe in detail every daily, weekly, and monthly meeting. 
                                </li>
                               
                            </ul>
                        </div>
                       
                  </div>
                <div className={styles.col_6+" col-md-6"+" service_one center"}>
                    <img src="/images/service_sectionone.png" className='common_next_img_updated' width={500} height={523} alt="duis" loading="eager"/>
                </div>
                </div>
            </div>
        </div>
    )

}
export default ServiceTop