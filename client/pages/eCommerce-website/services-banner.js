import React, { useEffect } from 'react'
import Image from 'next/image'
import styles from '../../styles/services.module.scss'
import AOS from "aos";
function ServiceBanner() {
    useEffect(() => {
        AOS.init();
    },[])
    return(
        <div className="container">
            <div className={styles.service_section} data-aos="zoom-in">
             <h1>eCommerce Websites</h1>
                <p>Utilize powerful CMS and marketing tools and offer your products on a wide range of platforms such as Google Shopping and Amazon.</p>
            </div>
        </div>
    )

}
export default ServiceBanner