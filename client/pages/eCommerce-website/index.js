import Footer from '../../components/common/footer'
import Header from '../../components/common/header'
import ServiceBanner from './services-banner'
import ServiceTop from './services-top'
import ServiceBottom from './service-bottom'
import ServiceLast from './service-last'
import ServiceFeatures from './services-features'
import ServiceTestimony from './service-testimony'

function Services() {
        const seoData = {
                title: "Sell Online | Footprint.io",
                description: "Don't be limited by geographic location. Sell your products & services worldwide by introducing selling tools such as Google Shopping and Amazon."
                }
    return (
    <main>
            <Header seoData={seoData}/>
            <ServiceBanner />
            <ServiceTop />
            <ServiceBottom />
            <ServiceLast />
            <ServiceFeatures />
            <ServiceTestimony />
            
            <Footer />
    </main>)
      }
export default Services