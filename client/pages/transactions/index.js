import React, { useEffect ,useState} from 'react'
import styles from '../../styles/Dashboard.module.scss'
import * as authServices from "../../services/authService";
import { useRouter } from 'next/router'
import Sidebar from '../../components/common/sidebar';
import MiddleBox from './middle-box';
import RightBox from './../dashboard/right-box';
import DashboardLayout from './../../components/common/dashboardLayout';

function Dashboard() {
    const router = useRouter()
    
    useEffect(() => {
        let res=authServices.getCurrentUser();
        if(!res) {
            router.push('/login')
        }
    },[])

    const seoData = {
        title: "Transactions | Footprint.io"
    }
    return(
        <DashboardLayout seoData={seoData}>
            <Sidebar />
            <MiddleBox />
        </DashboardLayout>
    )
}

export default Dashboard