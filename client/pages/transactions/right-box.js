import styles from '../../styles/Dashboard.module.scss'
import Link from 'next/link'
import Image from 'next/image'

function RightBox() {
    return (
        <div className={styles.right_box+" "+ "col"}>
         <div className={styles.inner_container}>
         <div className={styles.title}>Billing</div>
          <div className={styles.content}>
               <div className={styles.each}>
                   <div className={styles.heading}>1</div>
                   <div className={styles.sub_contnet}>you have currently one plan.</div>
               </div>
               <div className={styles.each}>
                   <div className={styles.heading}>Payment</div>
                   <div className={styles.sub_contnet}>Next payment is due December 23rd - Pay Now.</div>
               </div>
             
                <div className={styles.concern}>
                    <p>Have a problem or concern?</p>
                    <a className='fp_green_btn'>Chat With Us</a>
                </div>
               
          </div>
         </div>
        </div>
    )
}
export default RightBox