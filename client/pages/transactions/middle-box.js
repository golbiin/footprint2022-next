import styles from '../../styles/Dashboard.module.scss'
 
import Joi from "joi-browser";
import Image from 'next/image'
import React, { useEffect ,useState} from 'react'
import { Modal, Button } from "react-bootstrap";
import * as paymentService from "../../services/paymentServices";
import SubmitButton from '../../components/submit-button';

function MiddleBox() {
    const [paymentList,setPaymentList]=useState([]);

    useEffect(() => {
      getPayments();
      
    },[1]);


  const getPayments = async() => {
    const response = await paymentService.getPayments();
    console.log("response", response)
    if (response && response.data.status === 1) {
      //setLoader(false)
      setPaymentList(response.data.data);
     
      
    } else {
     // setLoader(false)
    }
  }
 
   

   
 
  console.log("paymentList", paymentList)
    return (
    <div className={styles.middle_box+" "+ "col"}>
      <div className={styles.inner_container}>
          <div className={styles.title}>Transactions</div>
          <div className={styles.content}>
            <div className={styles.transactions}>
            
              <table className="table table-striped">
                <thead>
                  <tr>
                    <th scope="col">DATE</th>
                    <th scope="col">SERVICE(S)</th>
                    <th scope="col">AMOUNT</th>
                    <th scope="col">STATUS</th>
                  </tr>
                </thead>
                <tbody>
                {
                  paymentList && paymentList.length > 0 ?
                  paymentList.map((payment, index) => (
                 
                    <tr key={payment._id}>
                    <td>{payment.payment_date}</td>
                    <td>{Object.keys(payment.package).length !== 0 ? payment.package.planDescription : null}</td>
                    <td>{Object.keys(payment.package).length !== 0 ? "Total $" + payment.package.totalAmount + "/Mo": null}</td>
                    <td>Completed</td>
                  </tr>
                  )) : <td className={styles.nopayment} colSpan="4">You have yet to make a purchase.</td>
                }
 
                </tbody>
              </table>
              
            </div>
          </div>
      </div>
      
    </div>
    )
}
export default MiddleBox