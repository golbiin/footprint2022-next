import React, { useEffect ,useState} from 'react'
import styles from '../../styles/Login.module.scss'
import Image from 'next/image'
import AOS from "aos";
import Link from 'next/link'
import Joi from "joi-browser";
import ValidationError from '../../components/common/validation-error';
import CopyRight from '../../components/common/copyright';
import SubmitButton from '../../components/submit-button';
import * as userService from "../../services/userUservice";
import * as authServices from "../../services/authService";
import { useRouter } from "next/router" 
import SeoHead from './../../components/common/seoHead';
function Register() {
    const seoData = {
      title: "Register | Footprint.io",
      description: "Register to create your project and begin creating your digital footprint."
  }
    const router = useRouter();
    const [userData,setUserdata]=useState({email:"",password:"",retype_password:"", notify_me:false, leadsData:{}});
    const [errors,setErrors]=useState([]);
    const [password_type,setPwdType]=useState("password");
    const [showHide,setShowHide]=useState(styles.show_eye);
    const [lidata,setLidata]=useState({length:"",lower:"",upper:"",num:"",special:"",});
    const [submit_status,setSumbit]=useState(false);
    const [response_type,setResponseType]=useState("");
    const [response_message,setResponseMessage]=useState("");
    const [loginCheck, SetLoginCheck] = useState(false);

    const schema = {
        email: Joi.string()
        .required()
          .email()
          .error(() => {
            return {
              message:
                "The email that you entered is invalid. Please enter a properly formatted email."
            };
          }),
        password: Joi.string()
          .required()
          .min(8)
          .regex(
            /^(?=.*?[A-Z])(?=(.*[a-z]){1,})(?=(.*[\d]){1,})(?=(.*[\W]){1,})(?!.*\s).{8,}$/
          )
          .label("Password"),
        retype_password: Joi.any()
          .valid(Joi.ref("password"))
          .required()
          .options({ language: { any: { allowOnly: "must match password" } } })
          .error(() => {
            return {
              message: "The passwords you’ve entered does not match."
            };
          }),
        notify_me: Joi.any().optional(),
        leadsData: Joi.any().optional(),
      };
      
    useEffect(() => {
        let res=authServices.getCurrentUser();
        if(res) {
            router.push('/dashboard')
        }
        else{
          SetLoginCheck(true);
        }
      AOS.init();
      if(window.sessionStorage.getItem("request-sample") !== null){
        let data={...userData}
        data.leadsData = JSON.parse(window.sessionStorage.getItem("request-sample"));
        setUserdata(data);
      } 
      
    },[])

    const handleInputchange=({ currentTarget: input })=> {
        let newErrors = { ...errors };
        setResponseMessage("");
        if (input.name === "retype_password") {
            let password = userData.password;
            if (password !== input.value) {
              let errorMessage = "The passwords you’ve entered does not match.";
              newErrors[input.name] = errorMessage;
              setErrors(newErrors);
            } else {
              delete newErrors[input.name];
              setErrors(newErrors);
            }
            userData[input.name] = input.value;
            setUserdata(userData);
            setErrors(newErrors);
          } else {
            const errorMessage = validateProperty(input);
            if (errorMessage) newErrors[input.name] = errorMessage;
            else delete newErrors[input.name]; 
            if (input.name === "notify_me") {
              userData[input.name] = userData.notify_me === true ? false : true;
              setUserdata(userData);
            }  else {
              userData[input.name] = input.value;
              setUserdata(userData);
            }
            setErrors(newErrors);
          } 
          console.log(userData);
    }

    const handlePwd = ({ currentTarget: input }) => {
        const li_data = { ...li_data };
        const newErrors = { ...errors };
        userData[input.name] = input.value;
    
        if (input.value.length > 7) {
            li_data.length = styles.tick;
        } else {
            li_data.length = "";
        }
        if (input.value.match(/[a-z]/)) {
            li_data.lower = styles.tick;
        } else {
            li_data.lower = "";
        }
        if (input.value.match(/[A-Z]/)) {
            li_data.upper = styles.tick;
        } else {
            li_data.upper = "";
        }
        if (input.value.match(/[0-9]/)) {
            li_data.num = styles.tick;
        } else {
            li_data.num = "";
        }
        if (input.value.match(/[!@#$%^&*()?/]/)) {
            li_data.special = styles.tick;
        } else {
            li_data.special = "";
        }
        if (userData.retypePassword !== "") {
          if (userData.retypePassword !== input.value) {
            let errorMessage = "The passwords you’ve entered does not match.";
            newErrors.retypePassword = errorMessage;
            setErrors(newErrors);
          }
        }
        setUserdata(userData);
        setLidata(li_data);
      };

    const validateProperty = ({ name, value }) => {
        const obj = { [name]: value };
        const Registerschema = { [name]: schema[name] };
        const { error } = Joi.validate(obj, Registerschema);
        return error ? error.details[0].message : null;
      };

    const handlepasswordType = () => {
        if (password_type === "password") {
            setPwdType('text');
            setShowHide(styles.hide_eye);
        } else {
            setPwdType('password');
            setShowHide(styles.show_eye);
        }
      };

    const checkBtnenable = () => {
        const { error } = Joi.validate(userData, schema);
        console.log("sss",error)
        if (!error) {
          return null;
        }
        const errors = {};
        for (let item of error.details) errors[item.path[0]] = item.message;
        return errors;
      };

    const doSubmit = async () => {
      console.log("userData", userData)
    const errors = { ...errors };
    const { error } = Joi.validate(userData, schema);
    if (error) {
      let path = error.details[0].path[0];
      let errormessage = error.details[0].message;
      errors[path] = errormessage;
      setErrors(errors);
    } else {
      setSumbit(true);
      const response = await userService.signupUser(userData);
      if (response) {
        setResponseType(response.data.status);
        setResponseMessage(response.data.message);
        setSumbit(false);
        setUserdata({email:"",password:"",retype_password:"",notify_me:false});
        if (response.data.status === 1) {
          sessionStorage.removeItem("request-sample");
          const result = await authServices.loginWithJwt(
            response.data.data.token
          );
          if (result) {
            router.push('/dashboard')
          }
        }
      }
    }
  };

    return (
      loginCheck?
      <>
        <SeoHead seoData={seoData !== undefined ? seoData : {}} />
      <div className="full-width-container">
        <div className={styles.login_container +" form_register"}>
                  <div className={styles.left + " " +styles.col_6}>
                      <div className={styles.logo_container}>
                      <Link href="/" passHref ><img src="/images/logo.svg" alt="Footprint Logo"  width={127} height={28}/></Link>
                      </div>
                      <div className={styles.form_container +" register"}>
                        <h1>Register</h1>
                        <p>Our Prices Will Not Be Beat</p>
                        <p>Already have an account? <Link href="/login" passHref>Log in</Link></p>
                        <div className='response_container'>
                            <div className={response_type==1?"sucsess":"error"}>
                              <span>{response_message} </span>
                            </div>
                        </div>
                        <div className={styles.form}>
                            <div className={styles.form_group}>
                                <input type="text" name="email" className={styles.form_control} value={userData.email} placeholder='Email' onChange={handleInputchange}/>
                                {errors.email?<ValidationError message={errors.email} />:null}
                            </div>
                            <div className={styles.form_group}>
                                <input type={password_type} name="password" className={styles.form_control} value={userData.password} placeholder='Password' onChange={handlePwd}/>
                                <span className={styles.password_show} >
                                  <i className={showHide} id="pass_status" aria-hidden="true" onClick={handlepasswordType}></i>
                                  <label className={styles.show}>{password_type === "password"? "Show":"Hide"}</label>
                                  </span>
                                  {errors.password?<ValidationError message={errors.password} />:null}
                              </div>
                            <div className={styles.form_group}>
                                <input type="password" name="retype_password" className={styles.form_control} value={userData.retype_password} placeholder='Confirm Password' onChange={handleInputchange} />
                                {errors.retype_password?<ValidationError message={errors.retype_password} />:null}
                            </div>
                            <div className={styles.pass_validate}>
                            <ul>
                                  <li className={lidata.lower}>
                                  One lowercase character
                                  </li>
                                  <li className={lidata.upper}>
                                  One uppercase character
                                  </li>
                                  <li className={lidata.num}>One number</li>
                            </ul>
                            <ul>
                                  <li className={lidata.special}>
                                  One special character
                                  </li>
                                  <li className={lidata.length}>
                                  8 characters minimum
                                  </li>
                              </ul>
                            </div>
                            <div className={styles.checkBox}>
                            <input type="checkbox" name="notify_me" className={styles.check_input} value="true" onChange={handleInputchange}/>
                              <label className={styles.container_checkbox}>
                              I’d like to receive Footprint.io emails about new updates
                              <span className="checkmark"></span>
                              </label>
                              <span className="">
                              <label></label></span>
                              </div>
                            <div className={styles.form_group}>
                                <SubmitButton btn_label="Register Now" className="fp_green_btn" submit_status={submit_status} animation="border" variant="dark" size="sm" submit={doSubmit} Btnenable={checkBtnenable}/>
                            </div>
                        </div>
                      </div>
                      <CopyRight />
                  </div>
                  <div className={styles.right +" " + styles.col_6} >
                      <img src="/images/register.png" data-aos="zoom-in" alt='happy young woman with scratchpad office' className='common_next_img_updated' width={309} height={309}/>
                      <div className={styles.content} data-aos="fdae-in">
                      <h2>We&apos;ll handle your online digital footprint. <br /> Our pricing will not be beat.</h2>
                      <p>Wholesale Development Pricing, White-Labelling, Graphic Design, SEO & PPC. Footprint is the all-in-one digital toolkit your business can count on.</p>
                      </div>
                  </div>
        </div>
      </div>
      </>
      : null
    )
  }
  export default Register