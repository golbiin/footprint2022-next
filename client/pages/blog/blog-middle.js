import React, { useEffect } from 'react'
import Image from 'next/image'
import styles from '../../styles/blog.module.scss'
import AOS from "aos";
function OurBlogMiddle() {
  useEffect(() => {
    AOS.init();
},[])
return (
  <div className={styles.blog_middle_section}>
    <div className='container'>
          <div className={styles.middle_row} >
            
            <div className={styles.col_6+" col-md-6"}>
            <a> <Image alt="bg10" src="/images/blog/10.png" layout="responsive" loading="eager" width={557} height={244}/>   
            </a>
            <span className={styles.blog_date}>06 JAN</span>
            <p className={styles.blog_p}>STEPHEN HAWKING</p>  
            <p className={styles.blog_question}>Graphic Design Importance, Why it&apos;s needed for businesses?</p>
            <p className={styles.blog_text}>Duis vel nibh at velit scelerisque suscipit. Duis lobortis massa imperdiet quam. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Vestibulum volutpat pretium libero.</p>
            </div>
            <div className={styles.col_6+" col-md-6"}>
            <a> <Image alt="bg11" src="/images/blog/22.png" layout="responsive" loading="eager" width={557} height={244}/>   
            </a>
            <span className={styles.blog_date}>06 JAN</span>
            <p className={styles.blog_p}>STEPHEN HAWKING</p>  
            <p className={styles.blog_question}>Graphic Design Importance, Why it&apos;s needed for businesses?</p>
            <p className={styles.blog_text}>Duis vel nibh at velit scelerisque suscipit. Duis lobortis massa imperdiet quam. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Vestibulum volutpat pretium libero.</p>  
            </div>
          </div>
          <div className={styles.middle_row} >
            
            <div className={styles.col_6+" col-md-6"}>
            <a> <Image alt="bg12" src="/images/blog/12.png" layout="responsive" loading="eager" width={550} height={244}/>   
            </a>
            <span className={styles.blog_date}>06 JAN</span>
            <p className={styles.blog_p}>STEPHEN HAWKING</p>  
            <p className={styles.blog_question}>Graphic Design Importance, Why it&apos;s needed for businesses?</p>
            <p className={styles.blog_text}>Duis vel nibh at velit scelerisque suscipit. Duis lobortis massa imperdiet quam. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Vestibulum volutpat pretium libero.</p>
            </div>
            <div className={styles.col_6+" col-md-6"}>
            <a> <Image alt="bg13" src="/images/blog/13.png" layout="responsive" loading="eager" width={550} height={244}/>   
            </a>
            <span className={styles.blog_date}>06 JAN</span>
            <p className={styles.blog_p}>STEPHEN HAWKING</p>  
            <p className={styles.blog_question}>Graphic Design Importance, Why it&apos;s needed for businesses?</p>
            <p className={styles.blog_text}>Duis vel nibh at velit scelerisque suscipit. Duis lobortis massa imperdiet quam. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Vestibulum volutpat pretium libero.</p>  
            </div>
          </div>
    </div>
  </div>
)
}
export default OurBlogMiddle 