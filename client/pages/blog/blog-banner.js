import React, { useEffect } from 'react'
import Image from 'next/image'
import styles from '../../styles/blog.module.scss'
import AOS from "aos";
function BlogBanner() {
    useEffect(() => {
        AOS.init();
    },[])
    return(
        <div className="container">
            <div className={styles.blog_section} data-aos="zoom-in">
                <h1>Sed cursus turpis vitae tortor</h1>
                <p>Nullam tincidunt adipiscing enim. Praesent nonummy mi in odio. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubiliacrae sed aliquam, nisi quis porttitor congue, elit erat euismod orci, ac placerat dolor lectus quis orci. Fusce vel dui. Curabitur suscipit suscipit tellus..</p>
            </div>
        </div>
    )

}
export default BlogBanner