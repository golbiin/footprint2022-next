import Footer from '../../components/common/footer'
import Header from '../../components/common/header'
import BlogBanner from './blog-banner'
import OurBlogPortfolio from './blog-top'
import OurBlogMiddle from './blog-middle'
import OurBlogBottom from './blog-bottom'
import OurBlogLast from './blog-last'
import React, { useState, useEffect, useRef } from "react";
import * as manageBlog from "../../services/blogs";

function Blog() {
        const [smallBlackList, setSmallBlackList] = useState([]);
        const [smallWhiteList, setSmallWhiteList] = useState([]);
        const [largeWhiteList, setLargeWhiteList] = useState([]);
        const getAllBlogs = async (data, pageSection) => {
                try {
                  //let response = await manageBlog.getAllBlogs(data);
                  let response = await manageBlog.getAllBlogs();
                  console.log("response", response);
                  if (response && response.data.data) {
                    console.log("ssssssssssssss", response.data.data);
                    setSmallBlackList(response.data.data.smallBlackList.data);
                    setLargeWhiteList(response.data.data.largeWhiteList.data);
                    setSmallWhiteList(response.data.data.smallWhiteList.data);
                  }
                } catch (error) {
                  console.error("getAllBlogs-error", error);
                }
              };
              useEffect(() => {
                getAllBlogs();
              
              }, []);
    return (<main>
            <Header />
            <BlogBanner />
            <OurBlogPortfolio />
            <OurBlogMiddle />
            <OurBlogBottom />
            <OurBlogLast />
            <Footer />
    </main>)
      }
export default Blog