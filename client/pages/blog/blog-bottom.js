import React, { useEffect } from 'react'
import Image from 'next/image'
import styles from '../../styles/blog.module.scss'
import AOS from "aos";
function OurBlogBottom() {
  useEffect(() => {
    AOS.init();
},[])
return (
    <div className={styles.blog_bottom_section}>
      <div className='container'>
                <div className={styles.bottom_blog_row} >
                  
                  <div className={styles.col_4+" col-md-4"}>
                  <a> <Image alt="bg14" src="/images/blog/14.png" layout="responsive" loading="eager" width={352} height={244}/>   
                  </a>
                  <span className={styles.blog_date}>06 JAN</span>
                  <p className={styles.blog_p}>STEPHEN HAWKING</p>  
                  <p className={styles.blog_question}>Graphic Design Importance, Why it&apos;s needed for businesses?</p>
                  <p className={styles.blog_text}>People are moving towards digital marketing even more. It&apos;s imperative as it visually attracts your followers and also communicates with them.</p>
                  </div>
                  <div className={styles.col_4+" col-md-4"}>
                  <a> <Image alt="bg15" src="/images/blog/15.png" layout="responsive" loading="eager" width={352} height={244}/>   
                  </a>
                  <span className={styles.blog_date}>06 JAN</span>
                  <p className={styles.blog_p}>STEPHEN HAWKING</p>  
                  <p className={styles.blog_question}>Graphic Design Importance, Why it&apos;s needed for businesses?</p>
                  <p className={styles.blog_text}>People are moving towards digital marketing even more. It&apos;s imperative as it visually attracts your followers and also communicates with them.</p>  
                  </div>
                  <div className={styles.col_4+" col-md-4"}>
                  <a> <Image alt="bg16" src="/images/blog/16.png" layout="responsive" loading="eager" width={352} height={244}/>   
                  </a>
                  <span className={styles.blog_date}>06 JAN</span>
                  <p className={styles.blog_p}>STEPHEN HAWKING</p>  
                  <p className={styles.blog_question}>Graphic Design Importance, Why it&apos;s needed for businesses?</p>
                  <p className={styles.blog_text}>People are moving towards digital marketing even more. It&apos;s imperative as it visually attracts your followers and also communicates with them.</p>  
                  </div>
                </div>
                <div className={styles.bottom_blog_row} >
                  
                  <div className={styles.col_4+" col-md-4"}>
                  <a> <Image alt="bg17" src="/images/blog/17.png" layout="responsive" loading="eager" width={352} height={244}/>   
                  </a>
                  <span className={styles.blog_date}>06 JAN</span>
                  <p className={styles.blog_p}>STEPHEN HAWKING</p>  
                  <p className={styles.blog_question}>Graphic Design Importance, Why it&apos;s needed for businesses?</p>
                  <p className={styles.blog_text}>People are moving towards digital marketing even more. It&apos;s imperative as it visually attracts your followers and also communicates with them.</p>
                  </div>
                  <div className={styles.col_4+" col-md-4"}>
                  <a> <Image alt="bg18" src="/images/blog/18.png" layout="responsive" loading="eager" width={352} height={244}/>   
                  </a>
                  <span className={styles.blog_date}>06 JAN</span>
                  <p className={styles.blog_p}>STEPHEN HAWKING</p>  
                  <p className={styles.blog_question}>Graphic Design Importance, Why it&apos;s needed for businesses?</p>
                  <p className={styles.blog_text}>People are moving towards digital marketing even more. It&apos;s imperative as it visually attracts your followers and also communicates with them.</p>  
                  </div>
                  <div className={styles.col_4+" col-md-4"}>
                  <a> <Image alt="bg19" src="/images/blog/19.png" layout="responsive" loading="eager" width={352} height={244}/>   
                  </a>
                  <span className={styles.blog_date}>06 JAN</span>
                  <p className={styles.blog_p}>STEPHEN HAWKING</p>  
                  <p className={styles.blog_question}>Graphic Design Importance, Why it&apos;s needed for businesses?</p>
                  <p className={styles.blog_text}>People are moving towards digital marketing even more. It&apos;s imperative as it visually attracts your followers and also communicates with them.</p>  
                  </div>
                </div>
      </div>
    </div>
)
}
export default OurBlogBottom 