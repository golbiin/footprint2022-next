import React, { useEffect } from 'react'
import Image from 'next/image'
import styles from '../../styles/blog.module.scss'
import AOS from "aos";
function OurBlogPortfolio() {
  useEffect(() => {
    AOS.init();
},[])
return (
 
    <div className={styles.blog_portfolio_section}>
                <div className='container'>
                <h2>Read the blog</h2>
                <p>Nullam tincidunt adipiscing enim praesent nonummy mi in odio.</p>
                </div>
                <div className='container'>
                <div className={styles.portfolio_blog_row} >
                  
                  <div className={styles.col_4+" col-md-4"}>
                  <a> <Image alt="bg1" src="/images/blog/1.png" layout="responsive" loading="eager" width={352} height={244}/>   
                  </a>
                  <span className={styles.blog_date}>06 JAN</span>
                  <p className={styles.blog_p}>STEPHEN HAWKING</p>  
                  <p className={styles.blog_question}>Graphic Design Importance, Why it&apos;s needed for businesses?</p>
                  <p className={styles.blog_text}>People are moving towards digital marketing even more. It&apos;s imperative as it visually attracts your followers and also communicates with them.</p>
                  </div>
                  <div className={styles.col_4+" col-md-4"}>
                  <a> <Image alt="bg2" src="/images/blog/2.png" layout="responsive" loading="eager" width={352} height={244}/>   
                  </a>
                  <span className={styles.blog_date}>06 JAN</span>
                  <p className={styles.blog_p}>STEPHEN HAWKING</p>  
                  <p className={styles.blog_question}>Graphic Design Importance, Why it&apos;s needed for businesses?</p>
                  <p className={styles.blog_text}>People are moving towards digital marketing even more. It&apos;s imperative as it visually attracts your followers and also communicates with them.</p>  
                  </div>
                  <div className={styles.col_4+" col-md-4"}>
                  <a> <Image alt="bg3" src="/images/blog/3.png" layout="responsive" loading="eager" width={352} height={244}/>   
                  </a>
                  <span className={styles.blog_date}>06 JAN</span>
                  <p className={styles.blog_p}>STEPHEN HAWKING</p>  
                  <p className={styles.blog_question}>Graphic Design Importance, Why it&apos;s needed for businesses?</p>
                  <p className={styles.blog_text}>People are moving towards digital marketing even more. It&apos;s imperative as it visually attracts your followers and also communicates with them.</p>  
                  </div>
                </div>
                <div className={styles.portfolio_blog_row} >
                  <div className={styles.col_4+" col-md-4"}>
                  <a> <Image alt="bg4" src="/images/blog/4.png" layout="responsive" loading="eager" width={352} height={244}/>   
                  </a>
                  <span className={styles.blog_date}>06 JAN</span>
                  <p className={styles.blog_p}>STEPHEN HAWKING</p>  
                  <p className={styles.blog_question}>Graphic Design Importance, Why it&apos;s needed for businesses?</p>
                  <p className={styles.blog_text}>People are moving towards digital marketing even more. It&apos;s imperative as it visually attracts your followers and also communicates with them.</p>
                  </div>
                  <div className={styles.col_4+" col-md-4"}>
                  <a> <Image alt="bg5" src="/images/blog/5.png" layout="responsive" loading="eager" width={352} height={244}/>   
                  </a>
                  <span className={styles.blog_date}>06 JAN</span>
                  <p className={styles.blog_p}>STEPHEN HAWKING</p>  
                  <p className={styles.blog_question}>Graphic Design Importance, Why it&apos;s needed for businesses?</p>
                  <p className={styles.blog_text}>People are moving towards digital marketing even more. It&apos;s imperative as it visually attracts your followers and also communicates with them.</p>  
                  </div>
                  <div className={styles.col_4+" col-md-4"}>
                  <a> <Image alt="bg6" src="/images/blog/6.png" layout="responsive" loading="eager" width={352} height={244}/>   
                  </a>
                  <span className={styles.blog_date}>06 JAN</span>
                  <p className={styles.blog_p}>STEPHEN HAWKING</p>  
                  <p className={styles.blog_question}>Graphic Design Importance, Why it&apos;s needed for businesses?</p>
                  <p className={styles.blog_text}>People are moving towards digital marketing even more. It&apos;s imperative as it visually attracts your followers and also communicates with them.</p>  
                  </div>
                </div>
                <div className={styles.portfolio_blog_row} >
                  <div className={styles.col_4+" col-md-4"}>
                  <a> <Image alt="bg7" src="/images/blog/7.png" layout="responsive" loading="eager" width={352} height={244}/>   
                  </a>
                  <span className={styles.blog_date}>06 JAN</span>
                  <p className={styles.blog_p}>STEPHEN HAWKING</p>  
                  <p className={styles.blog_question}>Graphic Design Importance, Why it&apos;s needed for businesses?</p>
                  <p className={styles.blog_text}>People are moving towards digital marketing even more. It&apos;s imperative as it visually attracts your followers and also communicates with them.</p>
                  </div>
                  <div className={styles.col_4+" col-md-4"}>
                  <a> <Image alt="bg8" src="/images/blog/8.png" layout="responsive" loading="eager" width={352} height={244}/>   
                  </a>
                  <span className={styles.blog_date}>06 JAN</span>
                  <p className={styles.blog_p}>STEPHEN HAWKING</p>  
                  <p className={styles.blog_question}>Graphic Design Importance, Why it&apos;s needed for businesses?</p>
                  <p className={styles.blog_text}>People are moving towards digital marketing even more. It&apos;s imperative as it visually attracts your followers and also communicates with them.</p>  
                  </div>
                  <div className={styles.col_4+" col-md-4"}>
                  <a> <Image alt="bg9" src="/images/blog/9.png" layout="responsive" loading="eager" width={352} height={244}/>   
                  </a>
                  <span className={styles.blog_date}>06 JAN</span>
                  <p className={styles.blog_p}>STEPHEN HAWKING</p>  
                  <p className={styles.blog_question}>Graphic Design Importance, Why it&apos;s needed for businesses?</p>
                  <p className={styles.blog_text}>People are moving towards digital marketing even more. It&apos;s imperative as it visually attracts your followers and also communicates with them.</p>  
                  </div>
                </div>
                </div>
    </div>
   
    )
}
export default OurBlogPortfolio 