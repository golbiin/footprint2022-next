import styles from '../../styles/Pricing.module.scss'

function StepsBar(){
    return (
        <div className="container">
            <div className={styles.fp_pricing_steps}>
              <div className={styles.fp_step}>
                  <span className={styles.number}>01</span>
                  <p>Choose Plan</p>
              </div>
               <div c className={styles.fp_step+" "+styles.active}>
                  <span className={styles.number}>02</span>
                  <p>Payment</p>
              </div>
            </div>
        </div>
    )
}

export default StepsBar