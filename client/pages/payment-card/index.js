import { useEffect } from 'react';

import Footer from "../../components/common/footer"
import Header from "../../components/common/header"
import StepsBar from "./steps-bar"
import Checkout from './checkout';
import * as authServices from "../../services/authService";

function Payment(props){
    useEffect(async () => {
        checkForLogin();
        
    },[]);
    
    const checkForLogin=()=>{
        let loginResult = authServices.getCurrentUser();
        console.log("resres", loginResult)    
        if(loginResult === null)
        {    
            window.location.href = '/login'      
        }    
    }
    return (
    <main>
        <Header />
        <StepsBar/>
        <Checkout />
        <Footer />
    </main>)
}
export default Payment