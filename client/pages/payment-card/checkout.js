import React, { useMemo, useState, useEffect, useReducer } from "react";
import { useRouter } from 'next/router'
import { loadStripe } from "@stripe/stripe-js";
import {
  Elements,
  CardNumberElement,
  CardCvcElement,
  CardExpiryElement,
  useElements,
  useStripe
} from "@stripe/react-stripe-js";
import Link from 'next/link'
import styles from '../../styles/PaymentCard.module.scss'
import Spinner from "react-bootstrap/Spinner";
import * as paymentServices from "../../services/paymentServices";
import * as authServices from "../../services/authService";
var moment = require('moment-timezone');


 /* import * as paymentServices from "../../../services/paymentService";
import * as userServices from "../../../services/userService";
import SpinnerBtn from "../../common/spinnerBtn";
import Spinner from "react-bootstrap/Spinner";
import * as authservice from "../../../services/authenticate";
import * as walletServices from "../../../services/walletServices";

 */

const CheckoutForm = props => {
  const router = useRouter();
  console.log("router", router)

  const stripe = useStripe();
  const elements = useElements();
  const [savedCard, setSavedCard] = useState([]);
  const [pricingData, setPricingData] = useState({});
  const [paymentResponse, setPaymentResponse] = useState();
  const [paymentStatus, setPaymentStatus] = useState();
  const [submittingStatus, setsubmittingStatus] = useState(false);
  const [firstName, setFirstName] = useState("");
  const [firstName_err, setfirstNameErr] = useState("");
  const [lastName_err, setlastNameErr] = useState("");
  const [lastName, setLastName] = useState("");
  const [payment_method_id, setPaymentMenthodId] = useState("");
  const [billingData, setBillingData] = useState([]);
  const [userData, setUserData] = useState("");
  const [userEmail, setUserEmail] = useState("");
  const [loader, setLoader] = useState(true);
  const [walletBalance, setWalletBalance] = useState(0);

  const [useWallet, setUseWallet] = useState(false);

  const [baseCurrency, setBaseCurrency] = useState("USDUSD");
  const [rates, setRates] = useState({});
  const [ratesRevert, setRatesRevert] = useState({});


  let pricingpackage = "";
   console.log("savedCard", savedCard)
   useEffect(() => {
    if(Object.keys(router.query).length !== 0) {
        let queryData=router.query;
        setPricingData(queryData);
        console.log("query exist", queryData)
    }
    else {
        console.log("query not exist")
        window.location.href = '/pricing'  
    }
    getsavedCard();
    
},[1]);

 

 
  const getsavedCard = async() => {
    const response = await paymentServices.getsavedCard();
    console.log("response", response)
    if (response && response.data.status === 1) {
      setLoader(false)
     
      if(response.data.data[0].saved !== undefined)
        setSavedCard(response.data.data[0])
    } else {
      setLoader(false)
    }
  }

  const handleChange = async ({ currentTarget: input }) => {
    if (input.name === "first_name") {
      if (input.value === "") {
        setfirstNameErr("Please enter your first name");
      } else {
        setfirstNameErr();
      }
      setFirstName(input.value);
    } else {
      if (input.value === "") {
        setlastNameErr("Please enter your last name");
      } else {
        setlastNameErr();
      }
      setLastName(input.value);
    }
  };
  const handleSubmit = async event => {
    setPaymentResponse("");
    event.preventDefault();
    let user_email = pricingData.user_email ? pricingData.user_email: '';
    console.log("user_email", user_email)
    if (firstName != "" && lastName != "") {
      setsubmittingStatus(true);
      try {
        const cardElementNumber = elements.getElement(CardNumberElement);
        const billingDetails = {
          name: firstName + " " + lastName,
          email: user_email,
          address: {
            city: pricingData.city ? pricingData.city: '',
            line1: pricingData.country ? pricingData.country: '',
            country: "US",
            state: pricingData.state ? pricingData.state: '',
            postal_code: pricingData.postal_code ? pricingData.postal_code: ''
          }
        };

        const paymentMethodResponse= await stripe.createPaymentMethod({
          type: "card",
          card: cardElementNumber,
          billing_details: billingDetails
        });

        const { error, paymentMethod }  = paymentMethodResponse;
        console.log("paymentMethod", paymentMethodResponse);

          if (error) {
          setPaymentResponse(error.message);
          setPaymentStatus(0);
          setsubmittingStatus(false);
        } else {
          const { id } = paymentMethod;
          
          setPaymentMenthodId(id);
           
          const response = await paymentServices.savecard(id, firstName + " " + lastName, user_email);
          console.log("response-savecard", response);
          
          if (response && response.data) {
            if (response.data.status === 1) {
              
              
              if (Object.keys(pricingData).length !== 0) {
                proceedtoPayment(id, response.data.data.customer_id);
              } else {
                
                setPaymentResponse(response.data.message);
                setPaymentStatus(1);
                setsubmittingStatus(false);
                setTimeout((
                  router.reload('/payment/card')
                ), 500);

                
              }            

            } else {
              setPaymentResponse(response.data.message);
              setPaymentStatus(0);
              setsubmittingStatus(false);
            }
          } 
        }  
      } catch (error) {
        console.log("paymentMethod- error", error);
        setPaymentResponse(error.message);
        setPaymentStatus(0);
        setsubmittingStatus(false);
        
      }
    } else {
      if (firstName == "") setfirstNameErr("Please enter your first name");
      if (lastName == "") setlastNameErr("Please enter your last name");
    }
  };
  const handleSubmitSavedCard = async event => {
    setPaymentResponse("");
    event.preventDefault();
    setsubmittingStatus(true);
    try {
       console.log("savedCard.saved.payment_method_id, savedCard.saved.customer_id", savedCard.saved.payment_method_id, savedCard.saved.customer_id)
       proceedtoPaymentSavedCard(savedCard.saved.payment_method_id, savedCard.saved.customer_id);
    } catch (error) {
      setPaymentResponse(error.message);
      setPaymentStatus(0);
      setsubmittingStatus(false);
    }
  };
  const proceedtoPaymentSavedCard = async (id, cid) => {
    try {
      const billingDetails = {
        name: pricingData.company_name,
        email: pricingData.user_email ? pricingData.user_email: '',
        company: pricingData.company_name ? pricingData.company_name: '',
        address: {
          city: pricingData.city ? pricingData.city: '',
          line1: pricingData.country ? pricingData.country: '',
          state: pricingData.state ? pricingData.state: '',
          postal_code: pricingData.postal_code ? pricingData.postal_code: ''
        }
      };
     
      const user = await authServices.getCurrentUser();
  
      if (user) {
        const timezone = Intl.DateTimeFormat().resolvedOptions().timeZone;
        var timemoment = moment();
        var currentTime = timemoment.tz(timezone).format("MM-DD-YYYY HH:mm:ss");
  
        let currency = "USD";
       
  
        const paymentdata = {
          id: id,
          customer_id: cid,
          billingDetails: billingDetails,
          first_name: '',
          last_name: '',
          pricingpackage: pricingData,
          currency: currency,
          charged_by: "user",
          user_id: user._id,
          timezone: timezone,
          currentTime: currentTime,
          amount: pricingData.totalAmount ? pricingData.totalAmount : 0,
        };
        console.log("paymentdata", paymentdata);
  
        const response = await paymentServices.payment(paymentdata);
        console.log("response- paymentdata", response)
        if (response && response.data) {

          if (response.data.status === 1) {
            setPaymentResponse(response.data.message);
            setPaymentStatus(1);
            setsubmittingStatus(false);
            setTimeout((
              window.location.href = '/thank-you'  
            ), 500);
          }
        }  
      }
    } catch (error) {
      setPaymentResponse(error.message);
      setPaymentStatus(0);
      setsubmittingStatus(false);
    }
  }; 
  const proceedtoPayment = async (id, cid) => {
    try {
      const billingDetails = {
        name: firstName + " " + lastName,
        email: pricingData.user_email ? pricingData.user_email: '',
        company: pricingData.company_name ? pricingData.company_name: '',
        address: {
          city: pricingData.city ? pricingData.city: '',
          line1: pricingData.country ? pricingData.country: '',
          state: pricingData.state ? pricingData.state: '',
          postal_code: pricingData.postal_code ? pricingData.postal_code: ''
        }
      };
     
      const user = await authServices.getCurrentUser();
  
      if (user) {
        const timezone = Intl.DateTimeFormat().resolvedOptions().timeZone;
        var timemoment = moment();
        var currentTime = timemoment.tz(timezone).format("MM-DD-YYYY HH:mm:ss");
  
        let currency = "USD";
       
  
        const paymentdata = {
          id: id,
          customer_id: cid,
          billingDetails: billingDetails,
          first_name: firstName,
          last_name: lastName,
          pricingpackage: pricingData,
          currency: currency,
          charged_by: "user",
          user_id: user._id,
          timezone: timezone,
          currentTime: currentTime,
          amount: pricingData.totalAmount ? pricingData.totalAmount : 0,
        };
        console.log("paymentdata", paymentdata);
  
        const response = await paymentServices.payment(paymentdata);
        console.log("response- paymentdata", response)
        if (response && response.data) {

          if (response.data.status === 1) {
            setPaymentResponse(response.data.message);
            setPaymentStatus(1);
            setsubmittingStatus(false);
            setTimeout((
              window.location.href = '/thank-you'  
            ), 500);
          }
        }  
      }
    } catch (error) {
      setPaymentResponse(error.message);
      setPaymentStatus(0);
      setsubmittingStatus(false);
    }
  }; 

  const options = {
    style: {
      base: {
        fontSize: "18px",
        paddingTop: "20px",
        fontFamily: "Inter-Regular",
        color: "#00000",
        "::placeholder": {
          color: "#888888"
        },
        border: "1px solid"
      },
      invalid: {
        color: "red"
      }
    }
  };
  return loader ? (
    <div className="spinner">
        <Spinner animation="grow" variant="success" />  
      <span>Connecting...</span>
    </div>
  ) : (savedCard.length === 0)? (
    <div className="container">
      <form onSubmit={handleSubmit} className={styles.add_card}>
        <div className={styles.card_form + " row"}>
         
          <div className="col-md-12">
              <h3>Update Credit Card</h3>
          </div>
          <div className="col-md-12 response_container">
            {paymentResponse ? (
              <span
                className={paymentStatus === 1 ? "success" : "error"}
              >
                {paymentResponse}
              </span>
            ) : (
              ""
            )}
          </div>
          <div className="col-md-6">
            <label>First Name</label>
            <input
              type="text"
              className="form-control"
              placeholder="First Name"
              name="first_name"
              onChange={handleChange}
            />
            <span className={firstName_err ? "red-validate-error" : ""}>
              {firstName_err}
            </span>
          </div>
          <div className="col-md-6">
            <label>Last Name</label>
            <input
              type="text"
              className="form-control"
              placeholder="Last Name"
              name="last_name"
              onChange={handleChange}
            />
            <span className={lastName_err ? "red-validate-error" : ""}>
              {lastName_err}
            </span>
          </div>
          <div className="col-md-12">
            <label>Credit Card Number</label>
            <div className={styles.card_input} >
              <CardNumberElement options={options} />
            </div>
          </div>
          <div className="col-md-6">
            <label>Expiration Date</label>
            <div className={styles.card_input} >
              <CardExpiryElement options={options} />
            </div>
          </div>
          <div className="col-md-6">
            <label>cvc</label>
            <div className={styles.card_input} >
              <CardCvcElement options={options} />
            </div>
          </div>
          <div className="col-md-12">
            {submittingStatus ? (
               <button
               className="fp_green_btn"
               type="submit"
               disabled
             >
              <Spinner 
              animation="border"
              variant="dark"
              size="sm" 
              />
              Submitting...
              </button>
            ) : (
              <button
                className="fp_green_btn"
                type="submit"
                disabled={!stripe}
              >
                {pricingData && pricingData.totalAmount
                  ?( "Pay $"+ pricingData.totalAmount + " →")
                  : "Submit →"}
                  
              </button>
            )}
          </div>
        </div>
      </form>
      {/* {process.env.customKey} <br/>
      {process.env.customKeyisDev}<br/>
      {process.env.customKeyisStaging} */}
    </div>
  ) : (
    <div className="container">
      <form onSubmit={handleSubmitSavedCard} className={styles.saved_card}>
        <div className={styles.card_form + " row"}>
        


          <div className="col-md-12">
              <h3>Update Credit Card</h3>
          </div>
          <div className="col-md-12 response_container">
            {paymentResponse ? (
              <span
                className={paymentStatus === 1 ? "success" : "error"}
              >
                {paymentResponse}
              </span>
            ) : (
              ""
            )}
          </div>
          <div className="col-md-12">
            <div className={styles.saved_card_view}>
              <div className="card-number">
                <span>xxxxx</span>
                <span>xxxxx</span>
                <span>xxxxx</span>
                <span>
                  {savedCard.length != 0
                    ? savedCard.customer_data.last4
                    : "xxxx"}
                </span>
              </div>
              <div className="card-expire">
                <span>
                  {savedCard.length != 0
                    ? savedCard.customer_data.exp_month
                    : "00"}
                  /
                  {savedCard.length != 0
                    ? savedCard.customer_data.exp_year
                    : "0000"}
                </span>
              </div>
            </div>
           {/*  <Link
              className="remove-card-link"
              to="#"
              onClick={() => removeCrad(savedCard.saved._id)}
            >
              <i className="fa fa-trash" aria-hidden="true"></i> Remove and add new
              card
            </Link> */}
          </div>
          <div className="col-md-12">
            { Object.keys(pricingData).length !== 0? submittingStatus ? (
               <button
               className="fp_green_btn"
               type="submit"
               disabled
             >
              <Spinner 
              animation="border"
              variant="dark"
              size="sm" 
              />
              Submitting...
              </button>
            ) : (
              <button
                className="fp_green_btn"
                type="submit"
                disabled={!stripe}
              >
                {pricingData && pricingData.totalAmount
                  ?( "Pay $"+ pricingData.totalAmount + " →")
                  : "Submit →"}
                  
              </button>
            ) : null}
          </div>
          
        </div>
      </form>
    </div>
  );
};

const stripePromise = loadStripe(process.env.STRIPE_PUBLISHABLE);
const Checkout = props => {
  console.log("ddd", props);
  return (
    <Elements stripe={stripePromise}>
      <CheckoutForm {...props} />
    </Elements>
  );
};

export default Checkout;
