import React, { useState, useEffect } from 'react';
import Switch from "react-switch";
import styles from '../../styles/portfolio.module.scss'
import AOS from "aos";
import "../../node_modules/aos/dist/aos.css";
import * as managePortfolio from "../../services/portfolio";

const Categories = (props) => {
  const [checked, setChecked] = useState(false);
  useEffect(() => {
    AOS.init();
    getAllcategories();
},[])


const [categories, setCategories] = useState([]);
const [languages, setLanguages] = useState([]);
console.log("props.selectedCat", props.selectedCat)



const getAllcategories = async () => {
  let response = await managePortfolio.getAllcats();

  let mainIndex = 0;
  if (response && response.data.data[mainIndex]) {
  
 
    if (response.data.data[mainIndex].sub_categories) {
      var sub_categories = response.data.data[mainIndex].sub_categories;
      var languages = response.data.data[mainIndex].languages;
      let arr = [{ id: 0, value: "All"}];
      sub_categories.map((cat, index)=>{
        
        arr.push.apply(arr, [
          {
            id: cat._id,
            value: cat.sub_cat_name,
          }
        ]);
      })
      let arr2 = [{ id: 0, value: "All"}];
      languages.map((lng, index)=>{
        
        arr2.push.apply(arr2, [
          {
            id: lng._id,
            value: lng.language_name,
          }
        ]);
      })
      console.log("arr", arr)
      console.log("arr22", arr2)
      setCategories(arr);
      setLanguages(arr2);
     // arr.push.apply(arr, sub_categories);
      //arr.push.apply(arr2, languages);
 /*      setCategories(arr);
      setLanguages(arr2); */
    }
  }
};

const listItems = (items, selectedItem, SetSelectedItem) =>{
  return items.map((item, index) => (
    <li key={item.value}>
      <a
        className={selectedItem === item.id ? styles.active : ""}
        onClick={() => SetSelectedItem(item.id, index)}
      >
        {item.value}
      </a>
    </li>
  ))
}
const changeSwitch = (checked) =>{
  setChecked(checked);
  props.SetMainCat(checked? "languages" :"categories")
}
    return (
        <div className={styles.categories_section}>
          <div className={styles.category_left}>
              <h4>{ !checked? "Categories" : "Programming Languages" }</h4>
              <label>
                <Switch onChange={changeSwitch} checked={checked} className={styles.switch_style}/>
              </label>
          </div>
          <ul className={styles.list}>
              {!checked? listItems(categories, props.selectedCat, props.SetSelectedCat) : listItems(languages, props.selectedLng, props.SetSelectedLng)}
          </ul>
        </div>
    );
  
}
export default Categories