import React, { useState, useEffect } from 'react';
import Switch from "react-switch";
import styles from '../../styles/portfolio.module.scss'
import AOS from "aos";
import "../../node_modules/aos/dist/aos.css";
function PaymentSection(){
    useEffect(() => {
        AOS.init();
    },[])
    return(
        <div className="container">
            <div className={styles.payment_section}>
                <div className={styles.Pricing_total}>
                    <h3>$392/m</h3>
                </div>
                <button className={styles.fp_green_btn}>Continue to Payment</button>
            </div>
        </div>

    )
}
export default PaymentSection