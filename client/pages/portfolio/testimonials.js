import Image from 'next/image'
import styles from '../../styles/Home.module.scss'
import Marquee from "react-fast-marquee";

function Testimonials() {
    return ( 
        <div className={styles.testimonial_section}>
                <div className='container'>
                <h2>Listen to others&apos; opinions...</h2>
                </div> 
                <Marquee pauseOnHover={true} speed={50} direction="left">
                <div className={styles.testimonial_box_row}>
                  <div className={styles.col_3}>
                    <div className={styles.top_row}>
                        <Image src="/images/profile1.png" width={66} height={66} alt="profile" loading="eager" />
                        <div className={styles.testimonial_rating}>
                            <h5>Richard Lehun</h5>
                            <div className={styles.rating_star}>
                                <span className={styles.full}></span>
                                <span className={styles.full}></span>
                                <span className={styles.full}></span>
                                <span className={styles.full}></span>
                                <span className={styles.full}></span>
                                <span>5.0</span> 
                            </div>
                        </div> 
                    </div>
                    <div className={styles.bottom_row}>
                        <p>After many frustrating collaborations with web masters we were fortunate enough to meet Michael at Webeteer. Michael and his team have...</p>
                    </div>
                  </div>
                  <div className={styles.col_3}>
                    <div className={styles.top_row}>
                        <Image src="/images/profile2.png" width={66} height={66} alt="profile" loading="eager"/>
                        <div className={styles.testimonial_rating}>
                            <h5>Richard Lehun</h5>
                            <div className={styles.rating_star}>
                                <span className={styles.full}></span>
                                <span className={styles.full}></span>
                                <span className={styles.full}></span>
                                <span className={styles.full}></span>
                                <span className={styles.full}></span>
                                <span>5.0</span> 
                            </div>
                        </div> 
                    </div>
                    <div className={styles.bottom_row}>
                        <p>After many frustrating collaborations with web masters we were fortunate enough to meet Michael at Webeteer. Michael and his team have...</p>
                    </div>
                  </div>
                  <div className={styles.col_3}>
                    <div className={styles.top_row}>
                        <Image src="/images/profile3.png" width={66} height={66} alt="profile" loading="eager"/>
                        <div className={styles.testimonial_rating}>
                            <h5>Richard Lehun</h5>
                            <div className={styles.rating_star}>
                                <span className={styles.full}></span>
                                <span className={styles.full}></span>
                                <span className={styles.full}></span>
                                <span className={styles.full}></span>
                                <span className={styles.full}></span>
                                <span>5.0</span> 
                            </div>
                        </div> 
                    </div>
                    <div className={styles.bottom_row}>
                        <p>After many frustrating collaborations with web masters we were fortunate enough to meet Michael at Webeteer. Michael and his team have...</p>
                    </div>
                  </div>
                  <div className={styles.col_3}>
                    <div className={styles.top_row}>
                        <Image src="/images/profile4.png" width={66} height={66} alt="profile" loading="eager" />
                        <div className={styles.testimonial_rating}>
                            <h5>Richard Lehun</h5>
                            <div className={styles.rating_star}>
                                <span className={styles.full}></span>
                                <span className={styles.full}></span>
                                <span className={styles.full}></span>
                                <span className={styles.full}></span>
                                <span className={styles.full}></span>
                                <span>5.0</span> 
                            </div>
                        </div> 
                    </div>
                    <div className={styles.bottom_row}>
                        <p>After many frustrating collaborations with web masters we were fortunate enough to meet Michael at Webeteer. Michael and his team have...</p>
                    </div>
                  </div>
                  <div className={styles.col_3}>
                    <div className={styles.top_row}>
                        <Image src="/images/profile5.png" width={66} height={66} alt="profile" loading="eager" />
                        <div className={styles.testimonial_rating}>
                            <h5>Richard Lehun</h5>
                            <div className={styles.rating_star}>
                                <span className={styles.full}></span>
                                <span className={styles.full}></span>
                                <span className={styles.full}></span>
                                <span className={styles.full}></span>
                                <span className={styles.full}></span>
                                <span>5.0</span> 
                            </div>
                        </div> 
                    </div>
                    <div className={styles.bottom_row}>
                        <p>After many frustrating collaborations with web masters we were fortunate enough to meet Michael at Webeteer. Michael and his team have...</p>
                    </div>
                  </div>
                </div>
                </Marquee>
                <Marquee pauseOnHover={true} speed={50} direction="right">
                <div className={styles.testimonial_box_row}>
                  <div className={styles.col_3}>
                    <div className={styles.top_row}>
                        <Image src="/images/profile6.png" width={66} height={66} alt="profile" loading="eager" />
                        <div className={styles.testimonial_rating}>
                            <h5>Richard Lehun</h5>
                            <div className={styles.rating_star}>
                                <span className={styles.full}></span>
                                <span className={styles.full}></span>
                                <span className={styles.full}></span>
                                <span className={styles.full}></span>
                                <span className={styles.full}></span>
                                <span>5.0</span>
                            </div>
                        </div> 
                    </div>
                    <div className={styles.bottom_row}>
                        <p>After many frustrating collaborations with web masters we were fortunate enough to meet Michael at Webeteer. Michael and his team have...</p>
                    </div>
                  </div>
                  <div className={styles.col_3}>
                    <div className={styles.top_row}>
                        <Image src="/images/profile1.png" width={66} height={66} alt="profile" loading="eager" />
                        <div className={styles.testimonial_rating}>
                            <h5>Richard Lehun</h5>
                            <div className={styles.rating_star}>
                                <span className={styles.full}></span>
                                <span className={styles.full}></span>
                                <span className={styles.full}></span>
                                <span className={styles.full}></span>
                                <span className={styles.full}></span>
                                <span>5.0</span> 
                            </div>
                        </div> 
                    </div>
                    <div className={styles.bottom_row}>
                        <p>After many frustrating collaborations with web masters we were fortunate enough to meet Michael at Webeteer. Michael and his team have...</p>
                    </div>
                  </div>
                  <div className={styles.col_3}>
                    <div className={styles.top_row}>
                        <Image src="/images/profile2.png" width={66} height={66} alt="profile" loading="eager" />
                        <div className={styles.testimonial_rating}>
                            <h5>Richard Lehun</h5>
                            <div className={styles.rating_star}>
                                <span className={styles.full}></span>
                                <span className={styles.full}></span>
                                <span className={styles.full}></span>
                                <span className={styles.full}></span>
                                <span className={styles.full}></span>
                                <span>5.0</span> 
                            </div>
                        </div> 
                    </div>
                    <div className={styles.bottom_row}>
                        <p>After many frustrating collaborations with web masters we were fortunate enough to meet Michael at Webeteer. Michael and his team have...</p>
                    </div>
                  </div>
                  <div className={styles.col_3}>
                    <div className={styles.top_row}>
                        <Image src="/images/profile3.png" width={66} height={66} alt="profile" loading="eager" />
                        <div className={styles.testimonial_rating}>
                            <h5>Richard Lehun</h5>
                            <div className={styles.rating_star}>
                                <span className={styles.full}></span>
                                <span className={styles.full}></span>
                                <span className={styles.full}></span>
                                <span className={styles.full}></span>
                                <span className={styles.full}></span>
                                <span>5.0</span> 
                            </div>
                        </div> 
                    </div>
                    <div className={styles.bottom_row}>
                        <p>After many frustrating collaborations with web masters we were fortunate enough to meet Michael at Webeteer. Michael and his team have...</p>
                    </div>
                  </div>
                  <div className={styles.col_3}>
                    <div className={styles.top_row}>
                        <Image src="/images/profile4.png" width={66} height={66} alt="profile" loading="eager" />
                        <div className={styles.testimonial_rating}>
                            <h5>Richard Lehun</h5>
                            <div className={styles.rating_star}>
                                <span className={styles.full}></span>
                                <span className={styles.full}></span>
                                <span className={styles.full}></span>
                                <span className={styles.full}></span>
                                <span className={styles.full}></span>
                                <span>5.0</span> 
                            </div>
                        </div> 
                    </div>
                    <div className={styles.bottom_row}>
                        <p>After many frustrating collaborations with web masters we were fortunate enough to meet Michael at Webeteer. Michael and his team have...</p>
                    </div>
                  </div>
                </div>
                </Marquee>
                <a className='fp_green_btn' href='#'>Get Your Free Sample</a>
        </div>
    )
}
export default Testimonials 