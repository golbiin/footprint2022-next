import React, { useEffect } from 'react'
import styles from '../../styles/portfolio.module.scss'
import AOS from "aos";
import "../../node_modules/aos/dist/aos.css";
import Switch from "react-switch";

function Categories() {
    useEffect(() => {
        AOS.init();
    },[])
     
    return (
        <div className={styles.categories_section}>
            <div className='col-md-3'>
                <div className={styles.switch}>
                    <h4>Categories</h4>
                    <label>
        <span>Switch with default style</span>
        <Switch onChange={() => console.log('sss')} checked={true} />
      </label>
                </div>
               <ul className={styles.list}>
                    <li>All</li>
                    <li>CMS</li>
                    <li>Custom HTML</li>
                    <li>Ecommerce</li>
               </ul>
            </div>
        </div>
    )
}
export default Categories