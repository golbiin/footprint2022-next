import { useState } from 'react';
import React, { useEffect } from 'react';
import Image from 'next/image';
import * as managePortfolio from "../../services/portfolio";
import styles from '../../styles/portfolio.module.scss'
import AOS from "aos";
import "../../node_modules/aos/dist/aos.css";
import Spinner from "react-bootstrap/Spinner";
function CategoryImageSection(props){
    const [portfolios, SetPortfolios] = useState([]);
    const [loader, setLoader] = useState(true);
    const [limit, setLimit] = useState(9);
    const [total, setTotal] = useState(0);

    useEffect(() => {
        AOS.init();
        getPortfolios();
        setLimit(9);
    },[1])

 
    const loadMore = () => {
 
        setLimit(limit + 9);
      };
    const getPortfolios = async () => {
        let response = await managePortfolio.getPortfolios();
        console.log("response - getPortfolios", response);
        if (response && response.data) {
          
            console.log("response.data.data", response.data.data)
            SetPortfolios(response.data.data);
            setTotal(response.data.data.length);
            setLoader(false);
        }
      };
    const filterTotal = () =>{
        
      let filterItems = portfolios;
      if(props.mainCat ===  "categories")
        filterItems = portfolios.filter(portfolio => portfolio.sub_category.some(item => item.id === props.selectedCat && item.status || props.selectedCat === 0 ));
      else
        filterItems = portfolios.filter(portfolio => portfolio.languages.some(item => item.id === props.selectedLng && item.status || props.selectedLng === 0 ));

      return filterItems.length;
    }
    
    const filterPortfolio = () =>{
        
      let filterItems = portfolios;
      if(props.mainCat ===  "categories")
        filterItems = portfolios.filter(portfolio => portfolio.sub_category.some(item => item.id === props.selectedCat && item.status || props.selectedCat === 0 ));
      else
        filterItems = portfolios.filter(portfolio => portfolio.languages.some(item => item.id === props.selectedLng && item.status || props.selectedLng === 0 ));
 
        
      return filterItems.slice(0, limit).map((portfolio, index) => (
        <div key={portfolio._id} className={styles.image_col + " col-xl-4  col-lg-6 col-md-6"}>
            <a target="_blank" rel="noopener noreferrer" href={portfolio.url}>
                <div className={styles.image_wrap}>
                <img  src={portfolio.portfolio_image} className={styles.portfolio_image} width={348} height={495} alt="profile" loading="eager" />
                </div>
            </a>
        </div> 
      ));
    }

    return(
        <div className={styles.image_section}>
                <div className={styles.image_list + " row"}>
                {
                    loader ? 
                    <div className="spinner">
                        <Spinner animation="grow" variant="success" />  
                            <span>Connecting...</span>
                    </div>
                    : filterPortfolio()}
                  {portfolios ? (
                        total != 0 && limit < filterTotal() ? (
                            <button className="fp_green_btn portfolio_get" onClick={loadMore}>
                            Load More
                            </button>
                        ) : (
                            ""
                        )
                        ) : (
                        ""
                        )}
                </div>
         </div>
    );
}
export default CategoryImageSection
