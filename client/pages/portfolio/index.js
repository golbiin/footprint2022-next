import { useState } from 'react';
import Footer from '../../components/common/footer'
import Header from '../../components/common/header'
import Banner from './banner'
import Categories from './categories'
import CategoryImageSection from './categoryImageSection'
import styles from '../../styles/portfolio.module.scss'
 
 
function Home() {

    const [mainCat, SetMainCat] = useState("categories");
    const [selectedCat, SetSelectedCat] = useState(0);
    const [selectedLng, SetSelectedLng] = useState(0);
    const seoData = {
        title: "Our Work | Footprint.io",
        description: "Footprint simplifies Software and Digital Marketing. We develop and refine software solutions for companies across North America and worldwide."
    }
    return (<main>
        <Header seoData={seoData}/>
        <div className={styles.custom_row+' row'}>
            <div className='col-md-3'>
            
            </div>
            <div className='col-md-9'>
                 <Banner />
            </div>
        </div>
        <div className={styles.custom_row+' row'}>
            <div className='col-md-3'>
                <Categories selectedCat={selectedCat} SetSelectedCat={SetSelectedCat} selectedLng={selectedLng} SetSelectedLng={SetSelectedLng} SetMainCat={SetMainCat}/>
            </div>
            <div className='col-md-9'>
                <CategoryImageSection selectedCat={selectedCat} selectedLng={selectedLng} mainCat ={mainCat}/>
            </div>
        </div>
         <Footer /> 
      </main>)
  }
  export default Home