import React, { useEffect } from 'react'
import styles from '../../styles/portfolio.module.scss'
import AOS from "aos";
import "../../node_modules/aos/dist/aos.css";
function Banner() {
    useEffect(() => {
        AOS.init();
    },[])
    return (
        <div className="container">
            <div className={styles.banner_section}>
                <div className="col-md-3 left">
                </div>
                <div className="col-md-9">
                    <h1 data-aos="fade-up">Our Portfolio</h1>
                    <h5 data-aos="fade-up">Set your eyes on 400 websites we&apos;ve been grateful to be apart of.</h5>
                </div>
            </div>
        </div>
         
    )
}
export default Banner