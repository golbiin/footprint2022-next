import React, { useEffect ,useState} from 'react'
import { Modal, Button } from "react-bootstrap";
import ValidationError from '../../components/common/validation-error';
import styles from '../../styles/Login.module.scss'
import SubmitButton from '../../components/submit-button';

function ForgotPassword(props){
    console.log('props.forgot_submit_status', props);

    
    return(
        <Modal
          show={props.showForgot?props.showForgot:false}
          className={styles.custom_modal}
          onHide={props.handleClose}
          animation={false}
        >
          <div className={styles.modal_content}>
          <Modal.Header closeButton className={styles.modal_header}>
            <Modal.Title className={styles.modal_title}><h2>Forgot your Password?</h2></Modal.Title>
          </Modal.Header>
          <Modal.Body className={styles.modal_body}>

          <div className='response_container'>
              <div className={props.forgot_message_type?props.forgot_message_type==1?"sucsess":"error":""}>
              <span>{props.forgot_message?props.forgot_message:""}</span>
              </div>
             </div>

             { <span
              className={
                props.forgot_message_type?
                props.forgot_message_type === "1"
                  ? "res-sucess"
                  : "res-error":""
              }
            >
             
            </span> }

            <p>We&apos;ll email you a link to make a brand new password.</p>
            <input
              type="email"
              className="form-control"
              placeholder="Enter your email"
              name="forgot_email" 
              onChange={props.handleForgotpwd}
              value={props.forgotpassword?props.forgotpassword.forgot_email:""}
            />
           {props.errors?props.errors.forgot_email?<ValidationError message={props.errors.forgot_email} />:null:""} 
           
          </Modal.Body>
          <Modal.Footer className={styles.modal_footer}>
          <SubmitButton btn_label="Submit" className="fp_white_btn" submit_status={props.forgot_submit_status?props.forgot_submit_status:false} animation="border" variant="dark" size="sm" submit={props.submitFn} Btnenable={props.checkForgotBtnenable}/>
           
          </Modal.Footer>
          </div>
         
        </Modal>
    )
}
export default ForgotPassword;