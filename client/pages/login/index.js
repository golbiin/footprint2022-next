import React, { useEffect ,useState} from 'react'
import { useRouter } from 'next/router'
import styles from '../../styles/Login.module.scss'
import Image from 'next/image'
import AOS from "aos";
import Link from 'next/link'
import SubmitButton from '../../components/submit-button';
import CopyRight from '../../components/common/copyright';
import Joi from "joi-browser";
import ValidationError from '../../components/common/validation-error';
import * as authServices from "../../services/authService";
import ForgetPassword from './forgotpassword';
import SeoHead from './../../components/common/seoHead';


function Login() {
  const seoData = {
      title: "Login to add tasks, projects and view daily updates | Footprint.io",
      description: "You are moments away from accessing a vast pool of highly qualified Artists and Developers. If you don’t have an account, create one today."
  }
    const [loginData,setLoginData]=useState({email:"",password:""});
    const [leadsData,setLeadsData]=useState({});
    const [errors,setErrors]=useState([]);
    const [submit_status,setSumbit]=useState(false);
    const [response_type,setResponseType]=useState("");
    const [response_message,setResponseMessage]=useState("");
    const [verification,setVerification]=useState("");
    const [verify_email,setVerifyEmail]=useState("");
    const [show_forgot,setShowForgot]=useState("");

    const [forgotpassword,setForgotpassword]=useState({forgot_email:""});
    const [forgot_message,setForgotMessage]=useState("");
    const [forgot_message_type,setForgotType]=useState("");
    const [forgot_submit_status,setForgotSubmitStatus]=useState(false);
    const [loginCheck, SetLoginCheck] = useState(false);

    useEffect(() => {
      let res=authServices.getCurrentUser();
        if(res) {
            router.push('/dashboard')
        }
        else{
          SetLoginCheck(true);
        }
      AOS.init();
      if(window.sessionStorage.getItem("request-sample") !== null){     
        const data = JSON.parse(window.sessionStorage.getItem("request-sample"));  
        setLeadsData(data);
      } 
  },[]);

    const schema = {
        email: Joi.string()
          .required()
          .email()
          .error(() => {
            return {
              message:
                "The email that you’ve entered is invalid. Please enter a properly formatted email."
            };
          }),
        password: Joi.string()
          .required()
          .min(8)
          .label("Password")
      };
    
    const forgotSchema = {
      forgot_email: Joi.string()
        .required()
        .email()
        .error(() => {
          return {
            message:
              "The email that you’ve entered is invalid. Please enter a properly formatted email."
          };
        })
    };

    const router = useRouter()

    const handleChange = async ({ currentTarget: input }) => {
        const errors = { ...errors };
        const errorMessage = validateProperty(input);
        if (errorMessage) errors[input.name] = errorMessage;
        else delete errors[input.name];
        loginData[input.name] = input.value;
        setErrors(errors);
        setLoginData(loginData);
      };

    const  handleForgotpwd = ({ currentTarget: input }) => {
      const errors = { ...errors };
      const forgotpassword = { ...forgotpassword };
      const errorMessage = validateForgotForm(input);
      if (errorMessage) errors[input.name] = errorMessage;
      else delete errors[input.name];
      forgotpassword[input.name] = input.value;

      setErrors(errors);
      setForgotpassword(forgotpassword);
    };

    const checkBtnenable = () => {
        const { error } = Joi.validate(loginData, schema);
        if (!error) {
          return null;
        }
        const errors = {};
        for (let item of error.details) errors[item.path[0]] = item.message;
        return errors;
      };
      
    const checkForgotBtnenable = () => {
        const { error } = Joi.validate(forgotpassword, forgotSchema);
        if (!error) {
          return null;
        }
        const errors = {};
        for (let item of error.details) errors[item.path[0]] = item.message;
        return errors;
      };

    const authenticateUser = async () => {
        const errors = { ...errors };
        let result = Joi.validate(loginData,schema);
    
        if (result.error) {
          let path = result.error.details[0].path[0];
          let errormessage = result.error.details[0].message;
          errors[path] = errormessage;
          setErrors(errors);
        } else {
            const response = await authServices.authenticate(loginData, leadsData);
            if (response) {
              sessionStorage.removeItem("request-sample");
              SessionManage(response);
            }
        }
      };

    const validateProperty = ({ name, value }) => {
        const obj = { [name]: value };
        const lschema = { [name]: schema[name] };
        const { error } = Joi.validate(obj, lschema);
        return error ? error.details[0].message : null;
      };

    const validateForgotForm = ({ name, value }) => {
        const obj = { [name]: value };
        const fSchema = { [name]: forgotSchema[name] };
        const { error } = Joi.validate(obj, fSchema);
        return error ? error.details[0].message : null;
      };

    const forgotPasswordSubmit = async () => {
      let forgotPassword = { ...forgotpassword };
      const errors = { ...errors };
  
      let result = Joi.validate(forgotPassword, forgotSchema);
      if (result.error) {
        let path = result.error.details[0].path[0];
        let errormessage = result.error.details[0].message;
        errors[path] = errormessage;
        setErrors(errors);
      } else {
        try {
          setForgotSubmitStatus(true);
          const response = await authServices.forgotPassword(forgotPassword);
          console.log("responseresponse", response)
          if (response) {
            setForgotSubmitStatus(false);
            if (response.data.status === 1) {
              setForgotMessage(response.data.message);
              setForgotType(response.data.status);
            } else {
              delete errors["forgot_email"];
              forgotPassword.email= null;
              setForgotpassword(forgotPassword);
              setForgotMessage(response.data.message);
              setForgotType(response.data.status);
              setErrors(errors);
            }
            
          }
        } catch (err) {}
      }
    };

    const SessionManage = async response => {
        loginData.email = "";
        loginData.password = "";
        setLoginData(loginData);
        if (response.data.status === 0) {
            setResponseType(response.data.status);
            setResponseMessage(response.data.message);
            setSumbit(false);
            setVerification(true);
        } else if (response.data.status === 2) {
            setResponseType(response.data.status);
            setResponseMessage(response.data.message);
            setSumbit(false);
            setVerification(false);
            setVerifyEmail(response.data.data.email);
        } else if (response.data.status === 3) {
            setResponseType(response.data.status);
            setResponseMessage(response.data.message);
            setSumbit(false);
            setVerification(true);
            const token = response.data.data["token"];
            const result = await authServices.loginWithJwt(
            token
          );
        } else {
            setResponseType(response.data.status);
            setResponseMessage(response.data.message);
            setSumbit(false);
            const token = response.data.data["token"];
            const result = await authServices.loginWithJwt(
            token,
          );
          if (result) {
            setResponseType(response.data.status);
            setResponseMessage(response.data.message);
            setSumbit(false);
            router.push('/dashboard')
          }
        }
    };

    const handleClose = () => {
      setShowForgot(false);
    };
    const  handleShow = () => {
      setShowForgot(true);
    };
    const resendVerifymail = async () => {
      setResponseType(1);
      setResponseMessage("Please wait...");
      setVerification(true);
      const response = await authServices.resendVerification(verify_email);
      if (response) {
        setResponseType(response.data.status);
        setResponseMessage(response.data.message);
        setVerification(response.data.status);
      }
    };
      



    return (
      loginCheck?
      <>
        <SeoHead seoData={seoData !== undefined ? seoData : {}} />
        <div className="full-width-container">
          <ForgetPassword errors={errors} handleClose={handleClose} showForgot={show_forgot} checkForgotBtnenable={checkForgotBtnenable} handleForgotpwd={handleForgotpwd} submitFn={forgotPasswordSubmit} forgotpassword={forgotpassword} forgot_message={forgot_message} forgot_message_type={forgot_message_type} forgot_submit_status={forgot_submit_status} />
          <div className={styles.login_container}>
                    <div className={styles.left + " " +styles.col_6}>
                        <div className={styles.logo_container}>
                        <Link href="/" passHref><img src="/images/logo.svg" alt="Footprint Logo"  width={127} height={28}/></Link>
                        </div>
                        <div className={styles.form_container}>
                          <h1>Login</h1>
                          <p>Don’t have an account? <Link href='/register' passHref>Create one here</Link></p>
                          <div className='response_container'>
                              <div className={response_type==1?"sucsess":"error"}>
                                <span>{response_message}</span> {verification===false?<span className={styles.verify_now}><a  onClick={resendVerifymail}>verify now.</a></span>:""} 
                              </div>
                          </div>
                          <div className={styles.form}> 
                              <div className={styles.form_group}>
                                  <input type="text" name="email" className={styles.form_control} placeholder='Email' onChange={handleChange}/>
                                  {errors.email?<ValidationError message={errors.email} />:null}
                              </div>
                              <div className={styles.form_group}>
                                  <input type="password" name="password" className={styles.form_control} placeholder='Password' onChange={handleChange}/>
                                  {errors.password?<ValidationError message={errors.password} />:null}
                              </div>
                              <div className={styles.form_group}>
                              <SubmitButton btn_label="Login" className="fp_green_btn" submit_status={submit_status} animation="border" variant="dark" size="sm" submit={authenticateUser} Btnenable={checkBtnenable}/>
                              </div>
                          </div>
                          <div className={styles.info_btm}><a onClick={handleShow}> Forgot Password? </a></div>
                        </div>
                        <CopyRight />
                    </div>
                    <div className={styles.right +" " + styles.col_6} >
                        <img src="/images/login.png" className='common_next_img_updated' data-aos="zoom-in" alt='happy young woman with scratchpad office' width={309} height={309}/>
                        <div className={styles.content} data-aos="fdae-in">
                        <h2>We&apos;ll handle your online digital footprint. <br /> Our pricing will not be beat.</h2>
                        <p>Wholesale Development Pricing, White-Labelling, Graphic Design, SEO & PPC. Footprint is the all-in-one digital toolkit your business can count on.</p>
                        </div>
                    </div>
          </div>
        </div>
    </> : null
    )
  }
  export default Login