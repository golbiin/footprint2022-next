import Footer from "../../components/common/footer"
import Header from "../../components/common/header"
import styles from '../../styles/Cancellation.module.scss'
function PrivacyPolicy() {
    return(
        <div>
            <Header />
            <div className="container">
                <div className={styles.cancellation_cotainer}>
                <h1>Privacy Policy</h1>
                <p className={styles.privacy}>Footprint and its professionals shall use information provided by a Client in relation to this agreement in accordance with the Canada Personal Information Protection and Electronic Documents Act and Ontario Freedom of Information and Protection of Privacy Act, and also for the following purposes: to identify the Client in communications from Footprint to them; and for Footprint to contact the Client from time to time to offer them services or products which may be of interest to or benefit the Client.
                </p>
                </div>
            </div>
            <Footer />
        </div>
    )
}

export default PrivacyPolicy
