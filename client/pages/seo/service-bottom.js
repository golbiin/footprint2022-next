import React, { useEffect } from 'react'
import Image from 'next/image'
import styles from '../../styles/services.module.scss'
import AOS from "aos";
function ServiceBottom() {
    useEffect(() => {
        AOS.init();
    },[])
    return(
        <div className="container">
             <div className={styles.servicebottom_section+ " services_bottom"} data-aos="zoom-in">
                <h2>Search Engine Optimization</h2>
                <p style={{maxWidth: "900px"}}>No matter how much you&apos;ve spent beautifying your website, if no one&apos;s visiting it, it will all be in vain. SEO is what you need to gain the traction and traffic you deserve. And we will most definitely help you get there.  </p> 
            
                <div className={styles.service_bottom+ " bottom_service"} data-aos="zoom-in">
                    <div className="row" data-aos="zoom-in">
                    <div className={styles.col_6+" col-md-6"+" service_two center"}>
                        <img src="/images/service_sectiontwo.png" className='common_next_img_updated' width={500} height={523} alt="duis" loading="eager"/>
                    </div>
                    <div className={styles.col_6+" col-md-6"}>
                            <div className={styles.service_sectiontwo}>
                                <ul>
                                    <li>
                                        <h5>Brand Awareness</h5>
                                        Brand awareness is all about making the public recognize, remember, and differentiate you. We will be creating the list of keywords to rank for and help you fortify your brand awareness by making sure that you&apos;re found and discovered. 
                                    </li>
                                    <li>
                                        <h5>Local Search Domination</h5>
                                        Google takes the searchers location into account when generating results. Harnessing your SEO strategy enables more potential customers within your area to discover your business. Our team understands how local SEO works, and we&apos;ll help you make the most of it.
                                    </li>
                                    <li>
                                        <h5>User Engagement</h5>
                                        SEO is about creating useful content that satisfies searchers queries. The more engaged customers are with the content produced, the higher the chances of conversion, customer loyalty, and long-term revenue. On top of content quality, there are many other optimizations we can help you top off.
                                    </li>
                                </ul>
                            </div>
                        </div>
                    
                    </div>
                </div>
            </div>
         </div>
    )

}
export default ServiceBottom