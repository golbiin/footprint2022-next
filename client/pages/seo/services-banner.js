import React, { useEffect } from 'react'
import Image from 'next/image'
import styles from '../../styles/services.module.scss'
import AOS from "aos";
function ServiceBanner() {
    useEffect(() => {
        AOS.init();
    },[])
    return(
        <div className="container">
            <div className={styles.service_section} data-aos="zoom-in">
             <h1>Why Choose Footprint.io SEO Services</h1>
                
                <p style={{maxWidth: "1200px"}}>
                Being visible in search engines is what your website needs to capture its target audience and dominate the space. And there are a whole lot of factors that dictate your search engine rankings. It takes a highly experienced SEO team and consistent efforts to make it to the top. Footprint.io is equipped and ready to be your SEO guide. From on-page to off-page SEO, we know how it works and we know how to win.
                </p>
            </div>
        </div>
    )

}
export default ServiceBanner