import React, { useEffect } from 'react'
import Image from 'next/image'
import styles from '../../styles/services.module.scss'
import AOS from "aos";
function ServiceTop() {
    useEffect(() => {
        AOS.init();
    },[])
    return(
        <div className="container">
            <div className={styles.service_topsection + " service_top"} data-aos="zoom-in">
                <div className="row first" data-aos="zoom-in">
                   <div className={styles.col_6+" col-md-6"+" brochure_sectionone"}>
                        <div className={styles.service_sectionone}>
                            
                            <ul>
                                <li>
                                    <h5>Solid Expertise</h5>
                                    
                                    Our years of experience in the industry has enabled us to get to know Google algorithms and search engine ranking factors. After having tried and tested different approaches, we&apos;re now happily and proudly helping clients win the SEO game. We have the expertise you need to thrive and succeed. 
                                </li>
                                <li>
                                    <h5>Save Time & Resources</h5>
                                    
                                    It might be tempting to experiment all by yourself and see if there&apos;s the cheapest, fastest way of beating the SEO algorithm. In the end, you might just end up wasting time and resources trying to search for answers. Footprint.io helps you achieve the results you want while keeping the entire SEO approach clean and sustainable.
                                </li>  
                                <li>
                                    <h5>Regular Check-ins</h5>
                                    
                                    We will be sending you updates regularly and share all the information you need about each campaign. We will answer all of your questions and work alongside you from day one onwards. 
                                </li>
                               
                            </ul>
                        </div>
                       
                  </div>
                <div className={styles.col_6+" col-md-6"+" service_one center"}>
                    <img src="/images/service_sectionone.png" className='common_next_img_updated' width={500} height={523} alt="duis" loading="eager"/>
                </div>
                </div>
            </div>
        </div>
    )

}
export default ServiceTop