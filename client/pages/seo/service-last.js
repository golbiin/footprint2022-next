import React, { useEffect } from 'react'
import Image from 'next/image'
import styles from '../../styles/services.module.scss'
import AOS from "aos";
function ServiceLast() {
    useEffect(() => {
        AOS.init();
    },[])
    return(
        <div className="container">
            <div className={styles.service_last+ " services_last"} data-aos="zoom-in">
                    <div className="row last" data-aos="zoom-in">
                        <div className={styles.col_6+" col-md-6"+" brochure_sectionlast"}>
                                <div className={styles.service_sectionthree}>
                                    <ul>
                                        <li>
                                            <h5> High Conversion</h5>
                                            Footprint.io helps you improve your site&apos;s visibility on search engines. We&apos;ll look at your site and help you address all the issues that are keeping you from ranking. We&apos;ll amplify your traffic to increase the chances of conversion and optimize your website to keep visitors coming back for more. 
                                        </li>
                                          <li>
                                            <h5>Builds Reputation </h5>
                                            People perceive high-ranking websites as reputable ones. The goal is to be on page one as not everyone bothers to open page two. Making it to the first page while providing visitors with quality content and an optimized user experience will definitely help you stand out. 
                                        </li>
                                        <li>
                                            <h5>Cost-Effective</h5>
                                            SEO generates the highest ROI among all digital marketing methods. It may take time to achieve results, but as long as you&apos;re investing in the right SEO company that generates leads and does the job, you&apos;ll be set up for success. Footprint.io wishes to be your trusty SEO partner. Allow us to make it happen for you.
                                        </li> 
                                    </ul>
                                </div>
                        </div>
                        <div className={styles.col_6+" col-md-6"+" service_three center"}>
                            <img src="/images/service_sectionthree.png" className='common_next_img_updated' width={500} height={523} alt="duis" loading="eager"/>
                        </div>
                    </div>
                   
            </div>
        </div>
        )

    }
    export default ServiceLast