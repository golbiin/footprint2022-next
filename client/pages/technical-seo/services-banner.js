import React, { useEffect } from 'react'
import Image from 'next/image'
import styles from '../../styles/services.module.scss'
import AOS from "aos";
function ServiceBanner() {
    useEffect(() => {
        AOS.init();
    },[])
    return(
        <div className="container">
            <div className={styles.service_section} data-aos="zoom-in">
             <h1>Why Choose Our Technical SEO Services</h1>
                
                <p style={{maxWidth: "900px"}}>
                Technical SEO is all about optimizing a website&apos;s infrastructure, so it can be crawled and indexed easily by search engines. Footprint.io knows the technical components and requirements to improve your site&apos;s discoverability. All you have to do is allow us to make your top rankings come to life. </p>
            </div>
        </div>
    )

}
export default ServiceBanner