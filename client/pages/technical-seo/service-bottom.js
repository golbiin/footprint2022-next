import React, { useEffect } from 'react'
import Image from 'next/image'
import styles from '../../styles/services.module.scss'
import AOS from "aos";
function ServiceBottom() {
    useEffect(() => {
        AOS.init();
    },[])
    return(
        <div className="container">
             <div className={styles.servicebottom_section+ " services_bottom"} data-aos="zoom-in">
                <h2>Technical SEO</h2>
                <p style={{maxWidth: "900px"}}>Getting your SEO campaign wrong is downright bad for your business. Thus, you have to make sure that the technical aspects are covered well. Footprint.io is here to fill the gap.</p> 
            
                <div className={styles.service_bottom+ " bottom_service"} data-aos="zoom-in">
                    <div className="row" data-aos="zoom-in">
                    <div className={styles.col_6+" col-md-6"+" service_two center"}>
                        <img src="/images/service_sectiontwo.png" className='common_next_img_updated' width={500} height={523} alt="duis" loading="eager"/>
                    </div>
                    <div className={styles.col_6+" col-md-6"}>
                            <div className={styles.service_sectiontwo}>
                                <ul>
                                    <li>
                                        <h5>Improve Indexing</h5>
                                        We&apos;ll make it easy for search engines to index, crawl, and rank your website. We&apos;ll cover the technical aspects like laying our search-engine-friendly URL structures, adding structured data, including robots.txt files, setting up sitemaps, and more. 
                                    </li>
                                    <li>
                                        <h5>Enhance User Experience</h5>
                                        We&apos;ll also make sure that your website is winning in terms of performance and user experience. We&apos;ll make it easy for users to navigate your website and easily find the information they need. 
                                    </li>
                                    <li>
                                        <h5>Boost Site Security</h5>
                                        Your website&apos;s security can also affect your rankings. We&apos;ll set up your application protocol and make sure it&apos;s HTTPS. Google rewards websites with secure information transfer. Especially if you&apos;re running an ecommerce site where users set up their billing address, phone number, credit card data, and payment information, you need to be all the more cautious. 
                                    </li>
                                </ul>
                            </div>
                        </div>
                    
                    </div>
                </div>
            </div>
         </div>
    )

}
export default ServiceBottom