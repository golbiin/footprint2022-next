import React, { useEffect } from 'react'
import Image from 'next/image'
import styles from '../../styles/services.module.scss'
import AOS from "aos";
function ServiceLast() {
    useEffect(() => {
        AOS.init();
    },[])
    return(
        <div className="container">
            <div className={styles.service_last+ " services_last"} data-aos="zoom-in">
                    <div className="row last" data-aos="zoom-in">
                        <div className={styles.col_6+" col-md-6"+" brochure_sectionlast"}>
                                <div className={styles.service_sectionthree}>
                                    <ul>
                                        <li>
                                            <h5>Improve Site Speed</h5>
                                            Load time is another key technical SEO metric. Site speed improves user experience and can lead to higher conversion rates. The ideal page load time is less than 3 seconds. We know how to help your site achieve blazing fast speed.
                                        </li>
                                          <li>
                                            <h5>Responsive Design</h5>
                                            We&apos;ll help you pass Google&apos;s Mobile-Friendly Test. More users are now accessing web materials using their handheld devices. We have an awesome team of experts that can help you achieve a user-friendly, responsive design that your visitors will appreciate.
                                        </li>
                                        <li>
                                            <h5>Increase Search Engine Ranking</h5>
                                            To win against the competitors, you need to go beyond the basics. Our technical SEO team will cover all technical aspects of your site which affect your SEO campaign. Together, let&apos;s make Google and other search engines crawl, index, and rank your website to the first page.
                                        </li> 
                                    </ul>
                                </div>
                        </div>
                        <div className={styles.col_6+" col-md-6"+" service_three center"}>
                            <img src="/images/service_sectionthree.png" className='common_next_img_updated' width={500} height={523} alt="duis" loading="eager"/>
                        </div>
                    </div>
                   
            </div>
        </div>
        )

    }
    export default ServiceLast