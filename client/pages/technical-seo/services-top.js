import React, { useEffect } from 'react'
import Image from 'next/image'
import styles from '../../styles/services.module.scss'
import AOS from "aos";
function ServiceTop() {
    useEffect(() => {
        AOS.init();
    },[])
    return(
        <div className="container">
            <div className={styles.service_topsection + " service_top"} data-aos="zoom-in">
                <div className="row first" data-aos="zoom-in">
                   <div className={styles.col_6+" col-md-6"+" brochure_sectionone"}>
                        <div className={styles.service_sectionone}>
                            
                            <ul>
                                <li>
                                    <h5>Technical Knowledge</h5>
                                    
                                    We take care of everything—be it page speed, site maps, schema, URL structure, navigation, site responsiveness, and more. The goal is to make search engines discover that your website is of high value. Together, we can create a strong technical foundation for your website&apos;s SEO campaign.
                                </li>
                                <li>
                                    <h5>Years of Experience</h5>
                                    
                                    We take pride in helping multiple clients do better at their SEO game. We do a thorough technical SEO audit and leave no stone unturned in terms of optimizing your website and increasing your SERP rankings.  
                                </li>  
                                <li>
                                    <h5>Solid Results</h5>
                                    
                                    We support our clients businesses like they&apos;re ours. Your success is our victory as well, and whenever we successfully rank websites on search engines, it&apos;s a win that pushes us to do even better and help even more businesses online.
                                </li>
                               
                            </ul>
                        </div>
                       
                  </div>
                <div className={styles.col_6+" col-md-6"+" service_one center"}>
                    <img src="/images/service_sectionone.png" className='common_next_img_updated' width={500} height={523} alt="duis" loading="eager"/>
                </div>
                </div>
            </div>
        </div>
    )

}
export default ServiceTop