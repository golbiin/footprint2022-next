import React, { useEffect } from 'react'
import Image from 'next/image'
import styles from '../../styles/services.module.scss'
import "react-responsive-carousel/lib/styles/carousel.min.css";// requires a loader
import { Carousel } from 'react-responsive-carousel';
import ReactDOM from 'react-dom';
import AOS from "aos";
function ServiceTestimony() {
    useEffect(() => {
        AOS.init();
    },[])
    return(
        <div className={styles.service_testimony + " testimony"} data-aos="zoom-in">
            <div className="container services-testimony">
            <Carousel  showArrows={false} showThumbs={false} showStatus={false}>
                     
                    <div className='row testim'>
                        <div className={styles.col_9+" col-md-9"}>
                            <p className={styles.testimony_para}>“Michael and his team have brought my vision to life through building on online home for my community. I highly recommend Footprint for web design and development!” </p>
                            <h6>CNY Fertility</h6>
                            <div className={styles.content_details}>
                                <h3>Erin McCollough,</h3>
                                <h5>Chief Marketing Officer</h5>
                            </div>
                        </div>
                        <div className="col-md-3">
                           <Image src="/images/testimony/2.png" width={343} height={343} alt="duis" loading="eager"/>
                            
                        </div>
                       
                    </div>
                    <div className='row testim'>
                        <div className={styles.col_9+" col-md-9"}>
                            <p className={styles.testimony_para}>“Terrific team here, great response time and diligent service. Love the new site - thank you!!” </p>
                            <h6>SOLEIMAN LAW </h6>
                            <div className={styles.content_details}>
                                <h3>Matthew Soleimanpour,</h3>
                                <h5>Managing Partner</h5>
                            </div>
                        </div>
                        <div className=" col-md-3">
                           <Image src="/images/testimony/1.png" width={343} height={343} alt="duis" loading="eager"/>
                            
                        </div>
                       
                    </div> 
                    <div className='row testim'>
                        <div className={styles.col_9+" col-md-9"}>
                            <p className={styles.testimony_para}>“Great service all around. Professional. Quality work, delivered on time. 100% recommended.” </p>
                            <h6>Nicolás Muiño Diseño Gráfico</h6>
                            <div className={styles.content_details}>
                                <h3>Nicolás Muiño,</h3>
                                <h5>Digital Designer</h5>
                            </div>
                        </div>
                        <div className = "col-md-3">
                           <Image src="/images/testimony/3.png" width={343} height={343} alt="duis" loading="eager"/>
                            
                        </div>
                       
                    </div>
                    <div className='row testim'>
                        <div className={styles.col_9+" col-md-9"}>
                            <p className={styles.testimony_para}>“Michael was great to work with. Very helpful, quick to respond and professional. Would definitely have him build me another site in the future.” </p>
                            <h6>Blue Aspect Properties</h6>
                            <div className={styles.content_details}>
                                <h3>Jarrett Vaughan,</h3>
                                <h5>Managing Director</h5>
                            </div>
                        </div>
                        <div className= "col-md-3">
                           <Image src="/images/testimony/4.png" width={343} height={343} alt="duis" loading="eager"/>
                            
                        </div>
                       
                    </div>  
            </Carousel>
            </div>
        </div>
    );
    }

    export default ServiceTestimony