import React, { useEffect } from 'react'
import Image from 'next/image'
import styles from '../../styles/services.module.scss'
import AOS from "aos";
import Link from 'next/link';
function ServiceFeatures() {
    useEffect(() => {
        AOS.init();
    },[])
    return(
        <div className="container">
           <div className={styles.features_box} data-aos="zoom-in">
            <div className="row" data-aos="zoom-in">
                <div className={styles.col_6+" col-md-6"}>
                 <p>Upload your tasks and watch them get completed in real-time.</p> 
                </div>
                <div className={styles.dwnldfirst+" col-md-6"}>
                <Link href="/register" className="dwnld_btn">Register Now</Link>
                </div>
            </div> 
           </div>
        </div>
         )

        }
 export default ServiceFeatures