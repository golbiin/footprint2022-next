import React, { useEffect } from 'react'
import Image from 'next/image'
import styles from '../../styles/services.module.scss'
import AOS from "aos";
function ServiceTop() {
    useEffect(() => {
        AOS.init();
    },[])
    return(
        <div className="container">
            <div className={styles.service_topsection + " service_top"} data-aos="zoom-in">
                <div className="row first" data-aos="zoom-in">
                   <div className={styles.col_6+" col-md-6"}>
                        <div className={styles.service_sectionone}>
                            
                            <ul>
                                <li>
                                    <h5>Improved User Experience</h5>
                                    
                                    Nobody would want to browse a desktop website on their mobile phone. If you want to improve user experience and leave a positive impression, always go for creating a mobile website for your brand.
                                </li>
                                <li>
                                    <h5>Competitive Advantage</h5>
                                    
                                    If your visitor exits your page because it&apos;s not mobile-friendly, that&apos;s a loss and an opportunity you&apos;re handing over to your competitors. Make sure that you capture your users&apos; attention while you have it.
                                </li>
                                <li>
                                    <h5>Cost-effective</h5>
                                    
                                    As compared to creating a mobile app, a mobile website is way cheaper. Footprint.io will help you create the mobile website you need without breaking the bank. 
                                </li>
                               
                            </ul>
                        </div>
                       
                  </div>
                <div className={styles.col_6+" col-md-6"+" service_one"}>
                    <img src="/images/service_sectionone.png"  width={500} height={523} className='common_next_img_updated' alt="duis" loading="eager"/>
                </div>
                </div>
            </div>
        </div>
    )

}
export default ServiceTop