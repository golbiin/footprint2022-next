import React, { useEffect } from 'react'
import Image from 'next/image'
import styles from '../../styles/services.module.scss'
import AOS from "aos";
function ServiceBanner() {
    useEffect(() => {
        AOS.init();
    },[])
    return(
        <div className="container">
            <div className={styles.service_section} data-aos="zoom-in">
             <h1>Mobile Websites</h1>
                <p>Mobile internet usage is stronger than ever. Make sure that your website is optimized for mobile navigation. Footprint.io has the expertise you need to make the smart transition.</p>
            </div>
        </div>
    )

}
export default ServiceBanner