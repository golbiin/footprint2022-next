import React, { useEffect } from 'react'
import Image from 'next/image'
import styles from '../../styles/services.module.scss'
import AOS from "aos";
function ServiceLast() {
    useEffect(() => {
        AOS.init();
    },[])
    return(
        <div className="container">
            <div className={styles.service_last+ " services_last"} data-aos="zoom-in">
                    <div className="row last" data-aos="zoom-in">
                        <div className={styles.col_6+" col-md-6"}>
                                <div className={styles.service_sectionthree}>
                                    <ul>
                                        <li>
                                            <h5>Increased Ad Views</h5>
                                            Using AMP, the HTML is programmed in a manner that boosts the overall usability of images and banners which lead to a better ad viewability rate and increases the chances of monetizing content. 
                                        </li>
                                        <li>
                                            <h5>Higher Click-through Rates</h5>
                                            AMP pages are most likely showcased at the top of all mobile search results, which naturally increases click-through rates and eventually, conversions.

                                        </li>
                                        <li>
                                            <h5>AMP Visitor Analytics</h5>
                                            It&apos;s always handy to be able to track visitor behavior to gather data and make informed decisions and adjustments. AMP takes this into account and you can track visitor count, conversions, clicks, and more.
                                        </li>
                                    </ul>
                                </div>
                        </div>
                        <div className={styles.col_6+" col-md-6"+" service_three"}>
                            <img src="/images/service_sectionthree.png" className='common_next_img_updated' width={500} height={523} alt="duis" loading="eager"/>
                        </div>
                    </div>
                   
            </div>
        </div>
        )

    }
    export default ServiceLast