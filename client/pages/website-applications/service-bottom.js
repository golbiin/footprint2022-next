import React, { useEffect } from 'react'
import Image from 'next/image'
import styles from '../../styles/services.module.scss'
import AOS from "aos";
function ServiceBottom() {
    useEffect(() => {
        AOS.init();
    },[])
    return(
        <div className="container">
             <div className={styles.servicebottom_section+ " services_bottom"} data-aos="zoom-in">
                <h2>Why Choose Footprint.io</h2>
                <p>Footprint.io has helped clients across numerous industries create efficient, functional, and cost-effective web apps for their business needs. We are more than happy to help you maximize the benefits and productivity web apps have to offer.</p> 
            
                <div className={styles.service_bottom+ " bottom_service"} data-aos="zoom-in">
                    <div className="row" data-aos="zoom-in">
                    <div className={styles.col_6+" col-md-6"+" service_two"}>
                        <img src="/images/service_sectiontwo.png" className='common_next_img_updated' width={500} height={523} alt="duis" loading="eager"/>
                    </div>
                    <div className={styles.col_6+" col-md-6"}>
                            <div className={styles.service_sectiontwo}>
                                <ul>
                                    <li>
                                        <h5>Excellent UX and UI Design</h5>
                                        Our UI experts understand the importance of appealing interfaces and intuitive navigation. Our responsive, high-performing web apps guarantee smooth web experiences across all devices. Our goal is high user satisfaction and improved conversion rate.
                                    </li>
                                    <li>
                                        <h5>Great Customer Support</h5>
                                        Our systematic approach to project management and our strong dedication to solid customer support set us apart. We will be with you from day one onwards, keeping you updated and engaged every step of the way. 
                                    </li>
                                    <li>
                                        <h5>Extensive Experience</h5>
                                        We can tackle web application projects of any complexity. We have worked with clients from various industries, for various purposes. Our years of experience enable us to make the best happen.
                                    </li>
                                </ul>
                            </div>
                        </div>
                    
                    </div>
                </div>
            </div>
         </div>
    )

}
export default ServiceBottom