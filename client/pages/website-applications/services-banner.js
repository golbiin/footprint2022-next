import React, { useEffect } from 'react'
import Image from 'next/image'
import styles from '../../styles/services.module.scss'
import AOS from "aos";
function ServiceBanner() {
    useEffect(() => {
        AOS.init();
    },[])
    return(
        <div className="container">
            <div className={styles.service_section} data-aos="zoom-in">
             <h1>Web Apps</h1>
                <p style={{maxWidth: '900px'}}>Web apps enable you to capture and interact with your target audience easily. It also streamlines your business to get more done with less time and effort and with better accuracy. Having all of your data in one place also helps you receive real-time updates and saves everyone time and energy. Here are more perks web apps bring about.</p>
            </div>
        </div>
    )

}
export default ServiceBanner