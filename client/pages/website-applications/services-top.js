import React, { useEffect } from 'react'
import Image from 'next/image'
import styles from '../../styles/services.module.scss'
import AOS from "aos";
function ServiceTop() {
    useEffect(() => {
        AOS.init();
    },[])
    return(
        <div className="container">
            <div className={styles.service_topsection + " service_top"} data-aos="zoom-in">
                <div className="row first" data-aos="zoom-in">
                   <div className={styles.col_6+" col-md-6"}>
                        <div className={styles.service_sectionone}>
                            
                            <ul>
                                <li>
                                    <h5>Easy 24/7 Accessibility</h5>
                                    
                                    As long as you have an internet connection, you can access a web app 24/7. And as long as the browser is compatible, web apps can run on any platform regardless of the device or operating system. 
                                </li>
                                <li>
                                    <h5>Low-maintenance</h5>
                                    
                                    Web apps don&apos;t need a lot of maintenance and management from your side and also require really low technical requirements from the user&apos;s end. The updates and patches are also remotely rolled out to every device, making it even more efficient.
                                </li>
                                <li>
                                    <h5>Cost-effective</h5>
                                    
                                    Web apps reduce cost for both the business and the end-user. Web apps generally have shorter development cycles, and you don&apos;t need a large team to create them. End users can also readily access them anytime, anywhere as long as there&apos;s a working device and internet connection. 
                                </li>
                               
                            </ul>
                        </div>
                       
                  </div>
                <div className={styles.col_6+" col-md-6"+" service_one"}>
                    <img src="/images/service_sectionone.png"  width={500} height={523} className='common_next_img_updated' alt="duis" loading="eager"/>
                </div>
                </div>
            </div>
        </div>
    )

}
export default ServiceTop