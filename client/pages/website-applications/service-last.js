import React, { useEffect } from 'react'
import Image from 'next/image'
import styles from '../../styles/services.module.scss'
import AOS from "aos";
function ServiceLast() {
    useEffect(() => {
        AOS.init();
    },[])
    return(
        <div className="container">
            <div className={styles.service_last+ " services_last"} data-aos="zoom-in">
                    <div className="row last" data-aos="zoom-in">
                        <div className={styles.col_6+" col-md-6"}>
                                <div className={styles.service_sectionthree}>
                                    <ul>
                                        <li>
                                            <h5>Quality assurance</h5>
                                            We do thorough testing to ensure the security, sustainability, and speed of your web app. Our top-notch Quality Management System has enabled us to capture clients trust for years. 
                                        </li>
                                        <li>
                                            <h5>Integration</h5>
                                            We can help you achieve utmost automation and streamlining, and our integration experts will ensure that deployments will go smoothly without impeding the functionality of your web app. 

                                        </li>
                                        <li>
                                            <h5>Optimization and evolution</h5>
                                            Our support will be ongoing. We will constantly optimize and improve your web app to integrate updated and relevant functionalities. We will also take care of urgent fixes and propose application updates periodically.
                                        </li>
                                    </ul>
                                </div>
                        </div>
                        <div className={styles.col_6+" col-md-6"+" service_three"}>
                            <img src="/images/service_sectionthree.png" className='common_next_img_updated' width={500} height={523} alt="duis" loading="eager"/>
                        </div>
                    </div>
                   
            </div>
        </div>
        )

    }
    export default ServiceLast