import React, { useEffect,useState } from 'react'
import styles from '../../styles/Pricing.module.scss'
import AOS from "aos";
import "rsuite/dist/rsuite.min.css";
import { Slider, RangeSlider } from 'rsuite';

function ExtensionScale(props){

    useEffect(() => {
        AOS.init();
    },[])

    return (
        <div className="container">
            <div className={styles.extension_scale_section} data-aos="zoom-in">
                    <p>How many plugins/extensions do you currently have installed? &nbsp;&nbsp;{props.selectedPlans?props.selectedPlans.sliderNumber:0} Plugins</p>
                    <div className={styles.range_slider}>
                    <Slider
                        barClassName={styles.fp_slider_bar+" "}
                        handleClassName={styles.fb_slider_handle+" "}
                        defaultValue={0}
                        value={props.selectedPlans && props.selectedPlans.sliderValue ? props.selectedPlans.sliderValue : 0}
                        min={0}
                        step={5}
                        max={50}
                        graduated
                        progress
                        renderMark={mark => {
                            return mark;
                        }}
                        onChange={value => {
                            let selectedPlans = { ...props.selectedPlans };
                            selectedPlans.sliderNumber = value;
                            selectedPlans.sliderValue = value;
                            props.setSelectedPlans( selectedPlans );
                        }}
                        />
                    </div>
            </div>
        </div>
    )
}


export default ExtensionScale