import React, { useEffect, useState } from 'react'
import { useRouter } from 'next/router'
import styles from '../../styles/Pricing.module.scss'
import AOS from "aos";
import * as authServices from "../../services/authService";
 
function ContinuePayment(props){
    const router = useRouter();
    useEffect(() => {
        AOS.init();
    },[]);

   const [validCheck, setValidCheck]=useState(false); 

   const totalValue = () => {
       let totalAmount = props.selectedPlans.planValue + props.selectedPlans.categoryValue + props.selectedPlans.sliderValue;
       return parseInt(totalAmount);
   } 
   const continueNext = () =>{
       var total =  totalValue();
       let totalAmount = {totalAmount : parseInt(total) };
       let keep_local_storage={...props.selectedPlans, ...totalAmount};
       let res=authServices.createCart(keep_local_storage);
       if(res) {
        if(props.selectedPlans.plan !== '')
        router.push({
            pathname: '/payment',
            query: {...props.selectedPlans, ...totalAmount},
        }, 'payment');
        else
        setValidCheck(true);
       }
      
   }
 

    return (
        <div className={styles.payment_continue_section} data-aos="zoom-in">
            <div className='container'>
                <p className={props.selectedPlans && props.selectedPlans.plan === '' && validCheck ? styles.valid_error : styles.valid_error + " hide" }>Please Select a Plan</p>

            </div>
           <div className={styles.section_start+ " container"}>
                <div className={styles.total}><h2>${props.selectedPlans ? totalValue() : 0}/m</h2></div>
                <button className='fp_green_btn' onClick={()=>continueNext()}>Continue to Payment</button>
            </div>
        </div>
    )
}

export default ContinuePayment