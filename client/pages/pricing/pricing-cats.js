import React, { useEffect } from 'react'
import styles from '../../styles/Pricing.module.scss'
import AOS from "aos";
function PricingCat(props){
    useEffect(() => {
        AOS.init();
    },[])
    const handleSelect = (category, amount) => {
        let selectedPlans = { ...props.selectedPlans };
        selectedPlans.category = category;
        selectedPlans.categoryValue = amount;
        props.setSelectedPlans( selectedPlans );
    }
    return (
        <div className="container">
            <div className={styles.fp_pricing_cats_section} data-aos="zoom-in">
                    <h2>We’ll be here for you when you need it.</h2>
                    <p>Please select the Content Management System you’re currently using.</p>
                    <ul className={styles.categories}>
                        <li className={props.selectedPlans && props.selectedPlans.category === 'Wordpress' ? styles.selected : '' } onClick={() => handleSelect('Wordpress', 10)}>Wordpress</li>
                        <li className={props.selectedPlans && props.selectedPlans.category === 'Magento' ? styles.selected : '' } onClick={() => handleSelect('Magento', 15)}>Magento</li>
                        <li className={props.selectedPlans && props.selectedPlans.category === 'Joomla' ? styles.selected : '' } onClick={() => handleSelect('Joomla', 20)} >Joomla</li>
                        <li className={props.selectedPlans && props.selectedPlans.category === 'Shoppify' ? styles.selected : '' } onClick={() => handleSelect('Shoppify', 10)}>Shopify</li>
                        <li className={props.selectedPlans && props.selectedPlans.category === 'Other' ? styles.selected : '' } onClick={() => handleSelect('Other', 15)}>Other</li>
                    </ul>
            </div>
        </div>
    )
}

export default PricingCat