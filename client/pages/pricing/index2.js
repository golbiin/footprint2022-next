import Footer from "../../components/common/footer"
import Header from "../../components/common/header"
import ContinuePayment from "./continue-payment"
import ExtensionScale from "./extension-scale"
import PricingBanner from "./pricing-banner"
import PricingCat from "./pricing-cats"
import PricingPlan from "./pricing-plan"
import StepsBar from "./steps-bar"
import { useState,useEffect } from 'react';
import * as authServices from "../../services/authService";

const Pricing = () =>{
    const [selectedPlans, setSelectedPlans] = useState({plan:"",planDescription:"", planValue:0, category: "", categoryValue:0, sliderNumber: 0, sliderValue: 0});
    useEffect(async() => {
        let cartData= await authServices.getCart();
        if(cartData){
        setSelectedPlans(cartData);
        }
        console.log("cartData", cartData)
    },[]);
    return (
    <main>
        <Header />
        <StepsBar/>
        <PricingBanner />
        <PricingPlan selectedPlans={selectedPlans} setSelectedPlans={setSelectedPlans}/>
        <PricingCat selectedPlans={selectedPlans} setSelectedPlans={setSelectedPlans}/>
        <ExtensionScale selectedPlans={selectedPlans} setSelectedPlans={setSelectedPlans}/>
        <ContinuePayment selectedPlans={selectedPlans}/>
        <Footer />
    </main>)
}
export default Pricing