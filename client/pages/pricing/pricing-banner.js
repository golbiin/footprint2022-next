import React, { useEffect } from 'react'
import styles from '../../styles/Pricing.module.scss'
import AOS from "aos";
import ReactTooltip from 'react-tooltip';

function PricingBanner(){
    useEffect(() => {
        AOS.init();
    },[])
    return (
        <div className="container">
            <div className={styles.fp_pricing_banner_section} data-aos="zoom-in">
                    <h1>24/7 Monitoring and Prevention from Hacking & Website Vulnerabilities</h1>
                    <div className={styles.yellow_round} data-aos="fade-in">
                       <span>
                       30 Days <br />MONEY-BACK <br />100%<br />GUARANTEE
                       </span>
                    </div>
                    <div className={styles.check_data}>
                        <div className={styles.checks} data-aos="zoom-in">
                            <p>Prevent Hacking and Vulnerabilities</p>
                        </div>
                        <div className={styles.checks} data-aos="zoom-in">
                            <p>Update Core CMS and Plugins </p>
                            <p>Automated Backups</p>
                        </div>
                        <div className={styles.checks} data-aos="zoom-in">
                            <p>24/7 Support</p>
                            <p>Essurance <span className={styles.tooltip} data-tip="In the event of a breach, we will cover labour required to get site operational again.">?</span></p>
                            <ReactTooltip backgroundColor="#00C174" borderColor="#00C174" place="right"/>
                        </div>
                    </div>
            </div>
        </div>
    )
}

export default PricingBanner