import styles from '../../styles/Pricing.module.scss'
const PricingPlan = (props) => {
    const handleSelect = (plan, amount, description) => {
        let selectedPlans = { ...props.selectedPlans };
        selectedPlans.plan = plan;
        selectedPlans.planValue = amount;
        selectedPlans.planDescription = description;
        props.setSelectedPlans( selectedPlans );
    }
    console.log("propssssss", props)
    return (
        <div className="container">
            <div className={styles.fp_pricing_plans_section} >
                   <div className={props.selectedPlans && props.selectedPlans.plan === '1-y' ? styles.plan_box + " " + styles.selected : styles.plan_box } onClick={() => handleSelect('1-y', 49,'1-Year plan($49/mo)')}>
                       <span className={styles.round}></span>
                       <h2>1 Year Plan</h2>
                       <div className={styles.price}>
                           <span className={styles.currancy}>$</span>
                           <span className={styles.amount}>49</span>
                           <span className={styles.validity}>/mo</span>
                       </div>
                       <div className={styles.description}>
                        <p>All the features you need to keep your personal.</p>
                       </div>
                       <button className='fp_yellow_btn'>Save 30% </button>
                   </div>
                   <div className={props.selectedPlans && props.selectedPlans.plan === '2-y' ? styles.plan_box + " " + styles.selected : styles.plan_box }  onClick={() => handleSelect('2-y', 28,'2-Year plan($28/mo) + 3 Months FREE')}>
                       <span className={styles.round}></span>
                       <h2>2 Year Plan</h2>
                       <div className={styles.price}>
                           <span className={styles.currancy}>$</span>
                           <span className={styles.amount}>28</span>
                           <span className={styles.validity}>/mo</span>
                       </div>
                       <div className={styles.description}>
                        <h3>+3 Month Free</h3> 
                       </div>
                       <button className='fp_yellow_btn'>Save 60% </button>
                   </div>
                   <div className={props.selectedPlans && props.selectedPlans.plan === '1-m' ? styles.plan_box + " " + styles.selected : styles.plan_box }  onClick={() => handleSelect('1-m', 56, 'Monthly plan($56/mo)')}>
                       <span className={styles.round}></span>
                       <h2>Monthly Plan</h2>
                       <div className={styles.price}>
                           <span className={styles.currancy}>$</span>
                           <span className={styles.amount}>56</span>
                           <span className={styles.validity}>/mo</span>
                       </div>
                       <div className={styles.description}>
                           <p>All the features you need to keep your personal.</p>
                       </div>
                       <button className='fp_yellow_btn'>Save 20% </button>
                   </div>
            </div>
        </div>
    )
}

export default PricingPlan