import React, { useEffect } from 'react'
import styles from '../../styles/Payment.module.scss'
import AOS from "aos";
import Image from 'next/image';
import ValidationError from '../../components/common/validation-error';

function OrderSummary(props){
    useEffect(() => {
        AOS.init();
    },[])

const handleInputchange=({ currentTarget: input })=> {
      props.handleInputchange(input);
    }

    return (
        <div className="container">
            <div className={styles.fp_order_summary} data-aos="zoom-in">
                <h2>Order summary</h2>
                  <div className={styles.summary_box}>
                      <div className={styles.summary}>
                          <div className={styles.left_to}>
                            <Image src="/images/summary2.png" data-aos="zoom-in" alt='summary' width={82} height={79}/>
                            <div className={styles.plan_details}>
                              <h5>{props.plans?props.plans.planDescription:""}</h5>
                               
                            </div>
                          </div>
                          <div className={styles.right_to}>
                             <div className={styles.amount_details}>
                                 <p className={styles.offer}>${props.plans?props.plans.planValue:""}</p>
                                 <p className={styles.orginal}><strike>$70</strike></p>
                             </div>
                          </div>
                    </div>
                    <div className={styles.order_form}>
                        <div className={styles.col_10}>
                            <div className={styles.form_field}>
                                <label className={styles.form_label}>Company Name</label>
                                <input type="text" name="company_name" className={styles.fields} onChange={handleInputchange} value={props.paymentData?props.paymentData.company_name:""}/>
                                {props.errors?props.errors.company_name?<ValidationError message={props.errors.company_name} />:"":null}
                            </div>
                            <div className={styles.form_field}>
                                <label className={styles.form_label}>Country  </label>
                                <input type="text" name="country" className={styles.fields}  onChange={handleInputchange} value={props.paymentData?props.paymentData.country:""}/>
                                {props.errors?props.errors.country?<ValidationError message={props.errors.country} />:"":null}
                            </div>
                            <div className={styles.form_field}>
                                <label className={styles.form_label}>State / Province</label>
                                <input type="text" name="state" className={styles.fields}  onChange={handleInputchange} value={props.paymentData?props.paymentData.state:""}/>
                                {props.errors?props.errors.state?<ValidationError message={props.errors.state} />:"":null}
                            </div>
                            <div className={styles.form_field}>
                                <label className={styles.form_label}>Phone Number</label>
                                <input type="text" name="phone" className={styles.fields}  onChange={handleInputchange} value={props.paymentData?props.paymentData.phone:""}/>
                                {props.errors?props.errors.phone?<ValidationError message={props.errors.phone} />:"":null}
                            </div>
                        </div>
                    </div>
                    <div className={styles.total_section}>
                        <h5 className={styles.total_title}>Total</h5>
                        <p className={styles.total_amount}>${props.total?props.total:""}*</p>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default OrderSummary