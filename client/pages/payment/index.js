import React, { useEffect, useState } from 'react'
import Footer from "../../components/common/footer"
import Header from "../../components/common/header"
import OrderSummary from "./order-summary.js"
import PaymentBanner from "./payment-banner"
import PaymentMethods from "./payment-methods"
import AccountGeneration from "./account-generation"
import StepsBar from "./steps-bar"
import { useRouter } from "next/router"
import AOS from "aos";
import Joi, { errors } from "joi-browser";
import * as authServices from "../../services/authService";
import * as userService from "../../services/userUservice";
import * as paymentService from "../../services/paymentServices";
import styles from '../../styles/Payment.module.scss'
var moment = require('moment-timezone');

const timezone = Intl.DateTimeFormat().resolvedOptions().timeZone;
var timemoment = moment();
var currentTime = timemoment.tz(timezone).format("MM-DD-YYYY HH:mm:ss");

function Payment(){
    const router = useRouter();
    const [selectedPlans,setSelectedPlan]=useState();
    const [totalAMount,setTotal]=useState();
    const [PaymentData,setPaymentData]=useState({method:"",company_name:"",country:"",state:"",phone:"",user_id:"",user_email:""});
    const [UserData,setUserData]=useState({email:"",password:"",retype_password:""});
    const[errors,setErrors]=useState([]);
    const [lidata,setLidata]=useState({length:"",lower:"",upper:"",num:"",special:"",});
    const [submit_status,setSumbit]=useState(false);
    const [response_type,setResponseType]=useState("");
    const [response_message,setResponseMessage]=useState("");
    
    const paymentSchema = {
        method: Joi.string()
          .required()
          .error(() => {
            return {
              message:
                "Please select a payment method."
            };
          }),
          company_name: Joi.string()
          .required()
          .error(() => {
            return {
              message: "Please enter company name."
            };
          }),
          country: Joi.string()
          .required()
          .error(() => {
            return {
              message: "Please enter country name."
            };
          }),
          state: Joi.string()
          .required()
          .error(() => {
            return {
              message: "Please enter state."
            };
          }),
          phone: Joi.string()
          .required()
          .error(() => {
            return {
              message: "Please select a phone."
            };
          }),
          user_id: Joi.string()
          .required()
          .error(() => {
            return {
              message: "Please sign up or login."
            };
          }),
          user_email: Joi.string()
          .email()
          .required()
          .error(() => {
            return {
              message: "Please enter user email."
            };
          }),
      };

    const schema = {
        email: Joi.string()
          .required()
          .email()
          .error(() => {
            return {
              message:
                "The email that you’ve entered is invalid. Please enter a properly formatted email."
            };
          }),
        password: Joi.string()
          .required()
          .label("Password"),
          retype_password: Joi.any()
          .valid(Joi.ref("password"))
          .required()
          .options({ language: { any: { allowOnly: "must match password" } } })
          .error(() => {
            return {
              message: "The passwords you’ve entered does not match."
            };
          }),
      };

      

    useEffect(async() => {
        AOS.init();
        let cartData= await authServices.getCart();
        console.log("cartData", cartData)
        if(cartData === null)
          router.push('/pricing')
        if(cartData){
          setSelectedPlan(cartData);
          setTotal(cartData.totalAmount);
          PaymentData.company_name=cartData.company_name?cartData.company_name:"";
          PaymentData.country=cartData.country?cartData.country:"";
          PaymentData.state=cartData.state?cartData.state:"";
          PaymentData.phone=cartData.phone?cartData.phone:"";
          setPaymentData(PaymentData);
        }
        checkForLogin();
    },[]);

    const checkForLogin=()=>{
        let res=authServices.getCurrentUser();
        if(res) {
            PaymentData.user_email=res.email;
            PaymentData.user_id=res._id;
            setPaymentData(PaymentData);
        }
    }

    //paymentchanges
    const handleInputchange=(input)=>{
        const errors = { ...errors };
        const errorMessage = validateProperty(input);
        if (errorMessage) errors[input.name] = errorMessage;
        else delete errors[input.name];
        PaymentData[input.name] = input.value;
        setErrors(errors);
        setPaymentData(PaymentData);
    }

    const validateProperty = ({ name, value }) => {
        const obj = { [name]: value };
        const schema = { [name]: paymentSchema[name] };
        const { error } = Joi.validate(obj, schema);
        return error ? error.details[0].message : null;
    };




    //account changes
    const handleAccountChange=(input)=> {
        const errors = { ...errors };
        setResponseMessage("");
        setResponseType(1);
        if (input.name === "retype_password") {
            let password = UserData.password;
            if (password !== input.value) {
              let errorMessage = "The passwords you’ve entered does not match.";
              errors[input.name] = errorMessage;
              setErrors(errors);
            } else {
              delete errors[input.name];
              setErrors(errors);
            }
            UserData[input.name] = input.value;
            setUserData(UserData);
            setErrors(errors);
          } else {
            const errorMessage = validateAccountProperty(input);
            if (errorMessage) errors[input.name] = errorMessage;
            else delete errors[input.name]; 
            UserData[input.name] = input.value;
            setUserData(UserData);
            setErrors(errors);
          } 
    }



    const handlePwd = ({ currentTarget: input }) => {
      const li_data = { ...li_data };
      const newErrors = { ...errors };
      UserData[input.name] = input.value;
  
      if (input.value.length > 7) {
          li_data.length = styles.tick;
      } else {
          li_data.length = "";
      }
      if (input.value.match(/[a-z]/)) {
          li_data.lower = styles.tick;
      } else {
          li_data.lower = "";
      }
      if (input.value.match(/[A-Z]/)) {
          li_data.upper = styles.tick;
      } else {
          li_data.upper = "";
      }
      if (input.value.match(/[0-9]/)) {
          li_data.num = styles.tick;
      } else {
          li_data.num = "";
      }
      if (input.value.match(/[!@#$%^&*()?/]/)) {
          li_data.special = styles.tick;
      } else {
          li_data.special = "";
      }
      if (UserData.retypePassword !== "") {
        if (UserData.retypePassword !== input.value) {
          let errorMessage = "The passwords you’ve entered does not match.";
          newErrors.retypePassword = errorMessage;
          setErrors(newErrors);
        }
      }
      setUserData(UserData);
      setLidata(li_data);
    };
    
    const validateAccountProperty = ({ name, value }) => {
        const obj = { [name]: value };
        const lschema = { [name]: schema[name] };
        const { error } = Joi.validate(obj, lschema);
        return error ? error.details[0].message : null;
    };

    const submitPay=async()=>{ 
       const errors={...errors}
        //setSumbit(true);
        if(!authServices.getCurrentUser()) {
            const errors = { ...errors };
            const { error } = Joi.validate(UserData, schema);
            if (error) {
              let path = error.details[0].path[0];
              let errormessage = error.details[0].message;
              errors[path] = errormessage;
              setErrors(errors);
            } else {
              setSumbit(true);
              const response = await userService.signupUser(UserData );
              if (response) {
                setResponseType(response.data.status);
                setResponseMessage(response.data.message);
                setSumbit(false);
                setUserData({email:"",password:"",retype_password:""});
                if(response.data.status==1) {
                    const result = await authServices.loginWithJwt(response.data.data.token);
                    if(result){
                      let user= authServices.getCurrentUser();
                      PaymentData.user_id=user._id;
                      PaymentData.user_email=user.email;
                      setPaymentData(PaymentData);
                    }
                    const { error } = Joi.validate(PaymentData, paymentSchema);
                    if (error) {
                        let path = error.details[0].path[0];
                        let errormessage = error.details[0].message;
                        errors[path] = errormessage;
                        setErrors(errors);
                      } else {
                      
                         if(PaymentData.method==="credit")
                         {
                            let newString={...PaymentData,...selectedPlans}
                            let res=authServices.createCart(newString);
                            if(res){
                              router.push({
                                pathname: '/payment/card',
                                query: newString,
                              },'/payment/card');
                            }
                         }
                         else if(PaymentData.method==="cheque") {
                          let newString={...PaymentData,...selectedPlans}
                          router.push({
                            pathname: '/thank-you',
                          });
                          }
                       }
                    }
                 }
              }
         }
    else {     
            const { error } = Joi.validate(PaymentData, paymentSchema);
            if (error) {
                let path = error.details[0].path[0];
                let errormessage = error.details[0].message;
                errors[path] = errormessage;
                setErrors(errors);
              } else {
                let newString={...PaymentData,...selectedPlans}    
                if(PaymentData.method==="credit")
                {
                  let res=authServices.createCart(newString);
                  if(res){
                    router.push({
                      pathname: '/payment/card',
                      query: newString,
                    },'/payment/card');
                  } 
                }
                else if(PaymentData.method==="cheque") {
                  let timeData={
                    timezone:timezone,
                    timemoment:timemoment,
                    currentTime:currentTime
                  }
                  const response = await paymentService.paymentBycheque(newString,timeData);
                  if(response.data.status==1){
                    router.push({
                      pathname: '/thank-you',
                    });
                  }
                  else{
                    setResponseType(response.data.status);
                    setResponseMessage(response.data.message);
                    setSumbit(false);
                  }
                 }
              }
        }
    }
console.log("selectedPlansselectedPlans", selectedPlans)
    return (
        
    <main>
        <Header />
        <StepsBar/>
        <PaymentBanner/>
        <PaymentMethods PaymentData={PaymentData} setPaymentData={setPaymentData} errors={errors} setErrors={setErrors}/>
        <OrderSummary plans={selectedPlans} total={totalAMount} paymentData={PaymentData} handleInputchange={handleInputchange} errors={errors}/>
        <AccountGeneration UserData={UserData} method={PaymentData.method} handleAccountChange={handleAccountChange} handlePwd ={handlePwd}  errors={errors}  submitPay={submitPay}  submit_status={submit_status} response_type={response_type} response_message={response_message} lidata={lidata}/>
       
        <Footer />
    </main>)
}
export default Payment 