import Image from 'next/image'
import Link from 'next/link'
import styles from '../../styles/Pricing.module.scss'

function StepsBar(){
    return (
        <div className={styles.control_buttons_outer +" container" }>
            <div className={styles.control_buttons}>
                <Link href='/pricing' passHref>
                    <Image src="/images/back-arrow.png" className={styles.back_arrow}  alt='back arrow' width={30} height={30}/>
                </Link>
            </div>
             
            <div className={styles.fp_pricing_steps}>
                <div className={styles.fp_step}>
                
                    <Link href='/pricing' passHref>
                        <div className={styles.fp_step_link}>
                        <span className={styles.number}>01</span>
                        <p>Choose Plan</p>
                        </div>
                    </Link>
                </div>
                <div  className={styles.fp_step+" "+styles.active}>
                    <span className={styles.number}>02</span>
                    <p>Payment</p>
                </div>
            </div>
        </div>
    )
}

export default StepsBar