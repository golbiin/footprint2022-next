import React, { useEffect } from 'react'
import styles from '../../styles/Pricing.module.scss'
import AOS from "aos";


function PaymentBanner(){
    useEffect(() => {
        AOS.init();
    },[])
    return (
        <div className="container">
            <div className={styles.fp_pricing_banner_section} data-aos="zoom-in">
                    <h1>Select a payment method</h1>
                    <div className={styles.yellow_round+" "+styles.payment_round} data-aos="fade-in">
                       <span>
                       30 Days <br />MONEY-BACK <br />100%<br />GUARANTEE
                       </span>
                    </div>
            </div>
        </div>
    )
}

export default PaymentBanner