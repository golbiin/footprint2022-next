import React, { useEffect } from 'react'
import styles from '../../styles/Payment.module.scss'
import AOS from "aos";
import Image from 'next/image';
import ValidationError from '../../components/common/validation-error';

function PaymentMethods(props){
    useEffect(() => {
        AOS.init();
    },[])
    const handleSelect = (method) => {
        let paymentData = { ...props.PaymentData };
        let errors={...props.errors}
        paymentData.method = method;
        props.setPaymentData( paymentData );
        if(errors.method) {
            delete errors.method;
            props.setErrors(errors);
        }

    }
    return (
        <div className="container">
            <div className={styles.fp_payment_methods} data-aos="zoom-in">
                  <div  className={props.PaymentData && props.PaymentData.method === 'credit' ? styles.payment_method_box + " " + styles.selected : styles.payment_method_box } onClick={()=>handleSelect("credit")}>
                      <span className={styles.round_box}></span>
                      <h5>Credit card</h5>
                      <Image src="/images/card.png" data-aos="zoom-in" alt='credit card' width={200} height={24}/>
                  </div>
 
                  <div  className={props.PaymentData && props.PaymentData.method === 'cheque' ? styles.payment_method_box + " " + styles.selected : styles.payment_method_box } onClick={()=>handleSelect("cheque")}>
                  <span className={styles.round_box}></span>
                      <h5>Cheque</h5>
                  </div>
            </div>
            <div className={styles.validation_container}>
            {props.errors?props.errors.method?<ValidationError message={props.errors.method} />:"":""}
            </div>
        </div>
    )
}

export default PaymentMethods