import React, { useEffect,useState } from 'react'
import styles from '../../styles/Payment.module.scss'
import AOS from "aos";
import Joi from "joi-browser";
import ValidationError from '../../components/common/validation-error';
import Image from 'next/image';
import { useRouter } from "next/router"
import * as authServices from "../../services/authService";
import SubmitButton from '../../components/submit-button';
import Link from 'next/link';


function AccountGeneration(props){
    const [password_type,setPwdType]=useState("password");
    const [showHide,setShowHide]=useState(styles.show_eye);
    useEffect(() => {
        AOS.init();
    },[])

    const handleInputchange=({ currentTarget: input })=> {
       props.handleAccountChange(input);
    }
    const checkBtnenable = () => {
        return null;
    }

    const handlepasswordType = () => {
        if (password_type === "password") {
            setPwdType('text');
            setShowHide(styles.hide_eye);
        } else {
            setPwdType('password');
            setShowHide(styles.show_eye);
        }
      };
   
    return (
        <div className="container">
            <div className={styles.account_creation}>
              {!authServices.getCurrentUser()?<React.Fragment>
                <h4 className={styles.title}>Create account</h4>
                <div className={styles.account_form}>
                    <div className={styles.form_field}>
                        <label className={styles.form_label}>Your email address</label>
                        <input type="text" name="email" className={styles.fields} onChange={handleInputchange} value={props.UserData?props.UserData.email:""}/>
                        {props.errors?props.errors.email?<ValidationError message={props.errors.email} />:null:""}
                    </div>
                    <div className={styles.form_field}>
                        <label className={styles.form_label}>Password </label>
                        <div className={styles.showpassgroup}>
                            <input type={password_type} name="password" className={styles.fields} onChange={props.handlePwd} value={props.UserData?props.UserData.password:""}/>
                            <span className={styles.password_show} onClick={handlepasswordType}>
                                <i className={showHide} id="pass_status" aria-hidden="true" ></i>
                                <label className={styles.show}>{password_type === "password"? "Show":"Hide"}</label>
                            </span>
                        </div>
                        {props.errors?props.errors.password?<ValidationError message={props.errors.password} />:null:""}
                    </div>
                    <div className={styles.form_field}>
                        <label className={styles.form_label}>Re-Type Password</label>
                        <input type="password" name="retype_password"className={styles.fields} onChange={handleInputchange} value={props.UserData?props.UserData.retype_password:""}/>
                        
                        {props.errors?props.errors.retype_password?<ValidationError message={props.errors.retype_password} />:null:""}
                    </div>
                    <div className={styles.pass_validate}>
                        <ul>
                            <li className={props.lidata? props.lidata.lower: ''}>
                            One lowercase character
                            </li>
                            <li className={props.lidata? props.lidata.upper : ''}>
                            One uppercase character
                            </li>
                            <li className={props.lidata? props.lidata.num : ''}>One number</li>
                        </ul>
                        <ul>
                            <li className={props.lidata ? props.lidata.special :''}>
                            One special character
                            </li>
                            <li className={props.lidata? props.lidata.length : ''}>
                            8 characters minimum
                            </li>
                        </ul>
                    </div>
                </div>
                </React.Fragment>
                :""}
                <div className={styles.account_info}>
                    <p>We do not share your information and will contact you only as needed to provide our service.</p>
                </div>
                <div className={styles.account_terms}>
                    <p>By submitting this form, you agree to our terms of service and <Link href="privacy-policy"><span>privacy policy.</span></Link></p>
                    <p>See offer details and <Link href="cancellation-policy"><span>cancellation terms</span></Link> here.</p>
                    <p>If you do not wish to receive information about Footprint.io&apos;s services, please opt-out of notifications here.</p>
                </div>
               
                {props.method=="cheque"?
                 <div className={styles.account_button}>
                    <SubmitButton btn_label="Submit" className="fp_green_btn" submit_status={props.submit_status} animation="border" variant="dark" size="sm" submit={props.submitPay} Btnenable={checkBtnenable}/>
                 </div>: <div className={styles.account_button}>
                    <SubmitButton btn_label="Proceed to Payment" className="fp_green_btn" submit_status={props.submit_status} animation="border" variant="dark" size="sm" submit={props.submitPay} Btnenable={checkBtnenable}/>
                </div>
                }
                {props.response_type==0?
                    <div className='response_container'>
                    <div className={props.response_type?props.response_type===0?"error-bordered":"success":"error-bordered"}>
                        <span>{props.response_message?props.response_message:""}</span>
                    </div>
                    </div>:""
                }
            </div>
        </div>
    )
}
export default AccountGeneration