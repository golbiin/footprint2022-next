import React from "react"
import Header from "./../components/common/header"
import Link from "next/link";
import styles from "../styles/notfound.module.scss"
function notFound(){
    return (
    <main>
   
   
      <Header seoHead={{ title: "Oops! Page Not Found - Outsource Me" }}/>
      
      <div id={styles.not_found}>
        <div className={styles.oops_notfound}>
          <h1>Oops!</h1>
          <h3>404 - PAGE NOT FOUND</h3>
          <p>
            The page you are looking for might have been removed, had its name
            changed or is temporarily unavailable.
          </p>
          <Link href ="/home"><a className ={styles.fp_white_btn} >Go back to the Homepage</a></Link>
          
        </div>
      </div>
  
    
    
</main>)
}
export default notFound