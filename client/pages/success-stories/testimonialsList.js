import { useEffect, useState } from 'react';
import styles from '../../styles/Testimonials.module.scss'
import * as fetchSiteData from "../../services/fetchSiteData";
import Spinner from "react-bootstrap/Spinner";
import SectionOne from './section-one';
import SectionTwo from './section-two';
import SectionThree from './section-three';

function TestimonialList() {
    const [loader, setLoader] = useState(true);
    const [reviewsFirst, setReviewsFirst] = useState([]);
    const [reviewsSecond, setReviewsSecond] = useState([]);
    useEffect(() => {
        getReviews();    
           
      },[1]);

      const getReviews = async() => {
        const response = await fetchSiteData.get_reviews_all();
        console.log("response", response)
        if (response && response.data.status === 1) {
          setLoader(false);
          let reviewsLoad = [];
          if(response.data.data.length > 0){
            reviewsLoad = response.data.data;
           setReviewsFirst(reviewsLoad.slice(0, 12))
           setReviewsSecond(reviewsLoad.slice(12, 500))
       
        }
            
        } else {
          setLoader(false)
        }
      }

      const showRating = (rating) => {

        return (rating === 3)?
        <>
       <span className={styles.full}></span>
       <span className={styles.full}></span>
       <span className={styles.full}></span>
       </>
       :(rating === 4)? <>
       <span className={styles.full}></span>
       <span className={styles.full}></span>
       <span className={styles.full}></span>
       <span className={styles.full}></span>
       </>
       : <>
       <span className={styles.full}></span>
       <span className={styles.full}></span>
       <span className={styles.full}></span>
       <span className={styles.full}></span>
       <span className={styles.full}></span>
       </>;
    }
                 
    return ( 
        <div className={styles.testimonial_section}>
                {
                    loader ? 
                    <div className="spinner">
                        <Spinner animation="grow" variant="success" />  
                            <span>Connecting...</span>
                    </div>
                    :<>
                    <SectionOne  reviewsFirst={reviewsFirst} showRating={showRating}/>
                    <SectionTwo  reviewsFirst={reviewsFirst} showRating={showRating}/>
                    <SectionThree  reviewsSecond={reviewsSecond} showRating={showRating}/>
                </>
                }
        </div>
    )
}
export default TestimonialList 