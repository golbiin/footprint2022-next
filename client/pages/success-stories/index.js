import React, { useEffect } from 'react'
import Header from '../../components/common/header';
import Footer from '../../components/common/footer';
import PageTop from './page-top';
import TestimonialList from './testimonialsList';


function Testimonials() {
    const seoData = {
        title: "Success Stories | Footprint.io",
        description: "Over 400 customers have been raving about Footprint.io. Try us before you hire us."
    }
    return (
        <div>
            <Header seoData={seoData} />
             <PageTop />
             <TestimonialList />
            <Footer />
        </div>
    )
}
export default Testimonials