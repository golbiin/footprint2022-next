import React, { useEffect, useState } from 'react'
import styles from '../../styles/Testimonials.module.scss'
import AOS from "aos";
import ReactPlayer from 'react-player'
import ReactModal from 'react-modal';
function SectionOne(props) {

    const [showModal, setShowModal] = useState(false);
    const [isOpen, setIsOpen] = useState(false);
    const [loadVideo, setLoadVideo] = useState(0);
    const [videoList, setVideoList] = useState([
        {
            imgUrl: '/images/video-thumb-1.png',
            videoUrl: 'https://outsourceme.s3.amazonaws.com/portfolio/1645597438460_1645597438396-IMG-4661.mov'      
        },
        {
            imgUrl: '/images/video-thumb-2.png',
            videoUrl: 'https://outsourceme.s3.amazonaws.com/portfolio/1645431446465_1645431446297-VIDEO-2019-09-12-14-01-25.mp4'
        },
        {
            imgUrl: '/images/video-thumb-3.png',
            videoUrl: 'https://outsourceme.s3.amazonaws.com/portfolio/1645435942011_1645435941935-Webeteer+Kwik+Look+Testimonial+060919+%282%29.mp4'
        },
    ]);
    useEffect(() => {
        AOS.init();
    },[]);

    const handleOpenModal = () => {
        setShowModal(true);
    }
      
    const handleCloseModal = () => {
        setShowModal(false);
    }
    const showReview = (review) =>{
        var rating = parseInt(review.rating);
        return(
            <div className={styles.col_3 + " col-md-3"} key={review._id}>
            <div className={styles.top_row}>
            
                <img src={review.user_image === "" ? "/images/user.png" :  review.user_image}  className={styles.user_img} width={66} height={66} alt="profile" loading="eager" />
            
                <div className={styles.testimonial_rating}>
                    <h5>{review.user_name}</h5>
                    <div className={styles.rating_star}>
                    {
                            props.showRating(rating)
                        }
                        <span>{rating}.0</span> 
                    </div>
                </div> 
            </div>
            <div className={styles.bottom_row}>
                <p>{review.testimony.substr(0, 150)}{review.testimony.length > 150? '...' : null}</p>
            </div>
        </div>
        )
    }
    const openVideo = (id) =>{
        setLoadVideo(id)
        handleOpenModal()
        setIsOpen(true)
    }
    const showVideo = (id) =>{
        return(
            <div className={styles.col_3_img + " col-md-3"}>
                <i onClick={() =>openVideo(id)} className={styles.video_icon}></i>
                <img src={videoList[id].imgUrl} alt="profile"/>
            </div>
        );
    }
    const customStyles = {
        content: {
          top: '50%',
          left: '50%',
          right: 'auto',
          bottom: 'auto',
          marginRight: '-50%',
          transform: 'translate(-50%, -50%)',
        },
      };
    return(
        <div className={styles.sectionOne}>
            <div className="container">
                <ReactModal 
                isOpen={showModal}
                contentLabel="Minimal Modal Example"
                onRequestClose={handleCloseModal}
                style={customStyles}
                >
                <ReactPlayer playing={isOpen} controls={true}  url={videoList[loadVideo].videoUrl}/>
                </ReactModal>
                <div className={styles.testimonial_box_row +" row"}>
                        {props.reviewsFirst !== undefined && props.reviewsFirst.length > 0 ? 
                        <>
                            {showReview(props.reviewsFirst[0])}
                            {showVideo(0)}                  
                            {showReview(props.reviewsFirst[1])}
                            {showVideo(1)}
                            {showReview(props.reviewsFirst[2])}
                            {showVideo(2)}
                             
                        </>
                        :
                        null
                        }
                    
                    </div>
            </div>
        </div>
    )

}
export default SectionOne