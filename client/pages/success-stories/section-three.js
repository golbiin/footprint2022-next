import React, { useEffect, useState } from 'react'
import styles from '../../styles/Testimonials.module.scss'
import AOS from "aos";
function SectionThree(props) {
    const [loadLimit, SetLoadLimit] = useState(78);
    useEffect(() => {
        AOS.init();
    },[])
    const showReview = (review) =>{
     
        
        var rating = parseInt(review.rating);
        return(
            <div className={styles.col_3 + " col-md-3"} key={review._id}>
            <div className={styles.top_row}>
            
                <img src={review.user_image === "" ? "/images/user.png" :  review.user_image}  className={styles.user_img} width={66} height={66} alt="profile" loading="eager" />
            
                <div className={styles.testimonial_rating}>
                    <h5>{review.user_name}</h5>
                    <div className={styles.rating_star}>
                    {
                            props.showRating(rating)
                        }
                        <span>{rating}.0</span> 
                    </div>
                </div> 
            </div>
            <div className={styles.bottom_row}>
                <p>{review.testimony.substr(0, 150)}{review.testimony.length > 150? '...' : null}</p>
            </div>
        </div>
        )
    }
//    console.log("loadLimit", loadLimit)
    return(
        <div className={styles.sectionThree}>
            <div className="container">
                <div className={styles.testimonial_box_row +" row"}>
                        {props.reviewsSecond !== undefined && props.reviewsSecond.length > 0 ? 
                          props.reviewsSecond.slice(0, loadLimit).map((review, index) => {
                            return showReview(review)  
                        })
                        :
                        null
                        }
                    </div>
                    <div className='row'>
                        <button onClick={()=> SetLoadLimit(loadLimit + 18)} className={styles.loadMore}>Load More</button>
                    </div>
            </div>
        </div>
    )

}
export default SectionThree