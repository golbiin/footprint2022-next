import React, { useEffect } from 'react'
import styles from '../../styles/Testimonials.module.scss'
import AOS from "aos";
function PageTop() {
    useEffect(() => {
        AOS.init();
    },[])
    return(
        <div className="container">
            <div className={styles.banner_section} data-aos="zoom-in">
                <h1>Success Stories</h1>
                <p>Over 400+ happy clients.</p>
            </div>
        </div>
    )

}
export default PageTop