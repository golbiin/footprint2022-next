import React, { useEffect ,useState} from 'react'
import styles from '../../styles/Dashboard.module.scss'
import * as authServices from "../../services/authService";
import { useRouter } from 'next/router'
import Sidebar from '../../components/common/sidebar';
import MiddleBox from './middle-box';
import DashboardLayout from './../../components/common/dashboardLayout';


function BillingDetails() {
    const router = useRouter()
    
    useEffect(() => {
        let res=authServices.getCurrentUser();
        if(!res) {
            router.push('/login')
        }
        
    },[])
    const seoData = {
        title: "Billing Information | Footprint.io"
    }
    return(
        <DashboardLayout seoData={seoData}>
            <Sidebar />    
            <MiddleBox />
        </DashboardLayout>
    )
}

export default BillingDetails