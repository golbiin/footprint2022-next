
import bstyles from '../../styles/Dashboardbilling.module.scss'
import Joi from "joi-browser";
import React, { useEffect ,useState} from 'react'
import * as userService from "../../services/userUservice";
import SubmitButton from '../../components/submit-button';
import ValidationError from '../../components/common/validation-error';
const pattern = /[.]/g;
function MiddleBox() {
    const [billing,setBilling]=useState({first_name: "", last_name: "", work_email: "", company: "", website: "",  office_phone: "", contact_address: "", contact_address_two: "",country: "",city: "", zip: "", state: "",calling: ""});
    const [submit_status,setSubmitStatus]=useState(false);
    const [response_message,setResMsg]=useState(false); 
    const [response_type,setResType]=useState("");
    const [errors,setErrors]=useState([]);
    const [loading,setLoading]=useState(true);

    const  schema = {
        first_name: Joi.string()
          .required()
          .error(() => {
            return {
              message: "Please enter your first name"
            };
          }),
        last_name: Joi.string()
          .required()
          .error(() => {
            return {
              message: "Please enter your last name"
            };
          }),
        work_email: Joi.string()
          .required()
          .email()
          .error(() => {
            return {
              message:
                "The email that you’ve entered is invalid. Please enter a properly formatted email."
            };
          }),
        company: Joi.string()
          .required()
          .error(() => {
            return {
              message: "Please enter your company"
            };
          }),
        office_phone: Joi.string()
          .required()
          .error(() => {
            return {
              message: "Please enter your office phone number"
            };
          }),
        contact_address: Joi.string()
          .required()
          .error(() => {
            return {
              message: "Please enter your contact address"
            };
          }),
    
        country: Joi.string()
          .required()
          .error(() => {
            return {
              message: "Please select your country"
            };
          }),
        city: Joi.string()
          .required()
          .error(() => {
            return {
              message: "Please select your city"
            };
          }),
        zip: Joi.string()
          .required()
          .error(() => {
            return {
              message: "Please enter your zip"
            };
          }),
        state: Joi.string()
          .required()
          .error(() => {
            return {
              message: "Please enter your zip"
            };
          }),
        website: Joi.string()
          .regex(pattern)
          .optional()
          .error(() => {
            return {
              message: "Website must be a valid url"
            };
          }),
        calling: Joi.string()
          .required()
          .error(() => {
            return {
              message: "Please select one option"
            };
          }),
        contact_address_two: Joi.optional(),
        login_email: Joi.optional()
      };

    const checkBtnenable = () => {
      return null;
        const { error } = Joi.validate(billing, schema);
        if (!error) {
          return null;
        }
        const errors = {};
        for (let item of error.details) errors[item.path[0]] = item.message;
        
        return errors;
      };

     const handleChange = async ({ currentTarget: input }) => {
        const n_errors = { ...errors };
        const errorMessage = validateProperty(input);
        if (errorMessage) n_errors[input.name] = errorMessage;
        else delete n_errors[input.name];
        billing[input.name] = input.value;
        setErrors(n_errors);
        setBilling(billing);
      };
    
    const  validateProperty = ({ name, value }) => {
        const obj = { [name]: value };
        const bschema = { [name]: schema[name] };
        const { error } = Joi.validate(obj, bschema);
        return error ? error.details[0].message : null;
      };

    const doSubmit=async()=>{
        setSubmitStatus(true);
        const n_errors = { ...errors };
        const { error } = Joi.validate(billing, schema);
        console.log("error", error)
        if (error) {
          let path = error.details[0].path[0];
          let errormessage = error.details[0].message;
          n_errors[path] = errormessage;
          setErrors(n_errors);
          setSubmitStatus(false);
        } else {
          const response = await userService.updateBillingAddress(billing);
          if (response && response.data) {
             setSubmitStatus(false);
             setResMsg(response.data.message);
             setResType(response.data.status)
          }
        }
    }

    const  getUserdetails=async()=>{

        const response = await userService.geUserProfile();
        if (response && response.data) {
          if (response.data.status === 1) {
            let n_billing = { ...billing };
            n_billing = response.data.data.billing
              ? response.data.data.billing
              : billing;
              setBilling(n_billing);
              setLoading(false);
          } else {
            setLoading(false);
          }
        }

    }

    useEffect(() => {
       getUserdetails();
    },[])
 
    return(
        <div className={bstyles.middle_box_col_12 + " col"} >
            <div className={bstyles.inner_container}>
                <div className={bstyles.title}>Billing Details</div>
                <div className={bstyles.content}>
                {loading ? (
                    <div className="row">
                      <div className="col-md-12">
                        <div className={bstyles.form+ " "+ "row"}>
                          <div className="spinner">
                            {/* <Spinner animation="grow" variant="warning" /> */}
                            <span>Connecting...</span>
                          </div>
                        </div>
                      </div>
                    </div>
              
                ) : (
                  <div className={bstyles.form+ " "+ "row"}>
                    <div className='response_container'>
                        <div className={response_type?response_type==1?"sucsess":"error":""}>
                         <span>{response_message}</span>
                        </div>
                    </div>
                 
                    <div className={bstyles.form_group+ " "+ "col-md-4"}>
                      <label className={bstyles.label_top}>First Name</label>
                      <input
                        type="text"
                        className="form-control"
                        placeholder="First Name"
                        name="first_name"
                        onChange={handleChange}
                        value={billing.first_name}
                      />
                       {errors.first_name?<ValidationError message={errors.first_name} />:null} 
                    </div>
                    <div className={bstyles.form_group+ " "+ "col-md-4"}>
                      <label  className={bstyles.label_top}>Last Name</label>
                      <input
                        type="text"
                        className="form-control"
                        placeholder="Last Name"
                        name="last_name"
                        onChange={handleChange}
                        value={billing.last_name}
                      />
                       {errors.last_name?<ValidationError message={errors.last_name} />:null} 
                    </div>
                    <div className={bstyles.form_group+ " "+ "col-md-4"}>
                      <label  className={bstyles.label_top}>Email address (at work)</label>
                      <input
                        type="email"
                        className="form-control"
                        placeholder="Email address"
                        name="work_email"
                        onChange={handleChange}
                        value={billing.work_email}
                      />
                     {errors.email?<ValidationError message={errors.email} />:null} 
                    </div>
                    <div className={bstyles.form_group+ " "+ "col-md-4"}>
                      <label  className={bstyles.label_top}>Company / Organization</label>
                      <input
                        type="text"
                        className="form-control"
                        placeholder="Company / Organization"
                        name="company"
                        onChange={handleChange}
                        value={billing.company}
                      />
                       {errors.company?<ValidationError message={errors.company} />:null} 
                    </div>
                    <div className={bstyles.form_group+ " "+ "col-md-4"}>
                      <label  className={bstyles.label_top}>Website URL</label>
                      <input
                        type="text"
                        className="form-control"
                        placeholder="Website URL"
                        name="website"
                        onChange={handleChange}
                        value={billing.website}
                      />
                     {errors.website?<ValidationError message={errors.website} />:null} 
                    </div>
                    <div className={bstyles.form_group+ " "+ "col-md-4"}>
                      <label  className={bstyles.label_top}>Office Phone</label>
                      <input
                        type="text"
                        className="form-control"
                        placeholder="Office Phone"
                        name="office_phone"
                        onChange={handleChange}
                        value={billing.office_phone}
                      />
                       {errors.office_phone?<ValidationError message={errors.office_phone} />:null} 
                    </div>
                    <div className={bstyles.form_group+ " "+ "col-md-6"}>
                      <label  className={bstyles.label_top}>Contact Address</label>
                      <input
                        type="text"
                        className="form-control"
                        placeholder="Contact Address"
                        name="contact_address"
                        onChange={handleChange}
                        value={billing.contact_address}
                      />
                    {errors.contact_address?<ValidationError message={errors.contact_address} />:null} 
                    </div>
                    <div className={bstyles.form_group+ " "+ "col-md-6"}>
                      <label  className={bstyles.label_top}>Contact Address 2</label>
                      <input
                        type="text"
                        className="form-control"
                        placeholder="Contact Address 2"
                        name="contact_address_two"
                        onChange={handleChange}
                        value={billing.contact_address_two}
                      />
                     {errors.contact_address_two?<ValidationError message={errors.contact_address_two} />:null} 
                    </div>
                    <div className={bstyles.form_group+ " "+ "col-md-3"}>
                      <label  className={bstyles.label_top}>Country</label>
                      <select
                        name="country"
                        onChange={handleChange}
                        defaultValue={billing.country}
                      >
                        <option value="">--Select--</option>
                        <option value="US">USA</option>
                        <option value="AU">Australia</option>
                        <option value="CA">Canada</option>
                      </select>
                      {errors.country?<ValidationError message={errors.country} />:null} 
                    </div>
                    <div className={bstyles.form_group+ " "+ "col-md-3"}>
                      <label  className={bstyles.label_top}>City</label>
                      <input
                        type="text"
                        className="form-control"
                        placeholder="City"
                        name="city"
                        onChange={handleChange}
                        value={billing.city}
                      />
                    {errors.city?<ValidationError message={errors.city} />:null} 
                    </div>
                    <div className={bstyles.form_group+ " "+ "col-md-3"}>
                      <label  className={bstyles.label_top}>ZIP Code</label>
                      <input
                        type="text"
                        className="form-control"
                        placeholder="ZIP Code"
                        name="zip"
                        onChange={handleChange}
                        value={billing.zip}
                      />
                      {errors.zip?<ValidationError message={errors.zip} />:null} 
                    </div>
                    <div className={bstyles.form_group+ " "+ "col-md-3"}>
                      <label  className={bstyles.label_top}>State</label>
                      <input
                        type="text"
                        className="form-control"
                        placeholder="State"
                        name="state"
                        onChange={handleChange}
                        value={billing.state}
                      />
                    {errors.state?<ValidationError message={errors.state} />:null} 
                    </div>
                    <div className="col-md-12">
                      <label  className={bstyles.label_top}>
                        Can we call you with special offers or services that may
                        be of interest?
                      </label>
                      <div className={bstyles.radio_buttons}>
                        <div className={bstyles.radio}>
                          <input
                            type="radio"
                            id="inlineRadio1"
                            value="yes"
                            name="calling"
                            checked={billing.calling === "yes" ? true : false}
                            onChange={handleChange}
                          />
                          <span htmlFor="inlineRadio1">Yes</span>
                        </div>
                        <div className={bstyles.radio}>
                          <input
                            type="radio"
                            name="calling"
                            id="inlineRadio2"
                            value="no"
                            checked={billing.calling === "no" ? true : false}
                            onChange={handleChange}
                          />
                          <span htmlFor="inlineRadio2">No</span>
                        </div>
                      </div>
                      {errors.calling?<ValidationError message={errors.calling} />:null} 
                    </div>
                    <div className="col-md-12 btns">
                    <SubmitButton btn_label="Submit" className="fp_green_btn" submit_status={submit_status} animation="border" variant="dark" size="sm" submit={doSubmit} Btnenable={checkBtnenable}/>
                    </div>
                  </div>
                )}
                </div>
            </div>
        </div>

    )
}

export default MiddleBox