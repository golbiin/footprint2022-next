import React, { useEffect, useState } from 'react';
import { useRouter } from 'next/router'
import styles from '../../styles/Verify.module.scss'
import Link from 'next/link'
import * as userService from "../../services/userUservice";
import Footer from "../../components/common/footer"
import Header from "../../components/common/header"
import Spinner from "react-bootstrap/Spinner";

function Verify() {
    const router=useRouter();
    const {id} = router.query
    const[message,setMessage]=useState("");
    const[message_type,setMessageType]=useState("");
    const[spinning,setSpinning]=useState(true);
    const VerifyUSer=async()=>{
        console.log("AAAAA",router.query.id);
        const response = await userService.verify(router.query.id);
        if (response) {
            setMessage(response.data.message); 
            setMessageType(response.data.status);
            setSpinning(false); 
        }
    }
    useEffect(() => {
        if(!id) {
            return;
          }
          else {
            VerifyUSer();
          } 
    },[id])

    return (
        <div >
        <Header />
        <div className="container ">
          <div className={styles.verify}>
            {spinning === true ? (
              <React.Fragment>
                <Spinner animation="border" variant="dark" size="lg" />
                <div className={styles.verifying}>Account verifying, Please wait..</div>
              </React.Fragment>
            ) : (
              <React.Fragment>
                
                <p className={message_type==1?styles.success:styles.error} style={{ marginTop: "35px" }}>
                  {message}
                  {message_type === 1 ? (
                    <React.Fragment>
                      <span>
                        <Link
                          href="/login"
                          style={{ color: "#fff", paddingLeft: "5px" }}
                          passHref
                        >
                          Login
                        </Link>
                      </span>
                    </React.Fragment>
                  ) : (
                    ""
                  )}
                </p>
              </React.Fragment>
            )}
          </div>
        </div>
        <Footer />
      </div>
    )
  }
  
  export default Verify