import React, { useEffect } from 'react'
import Image from 'next/image'
import styles from '../../styles/services.module.scss'
import AOS from "aos";
function ServiceBottom() {
    useEffect(() => {
        AOS.init();
    },[])
    return(
        <div className="container">
             <div className={styles.servicebottom_section+ " services_bottom"} data-aos="zoom-in">
                <h2>Why Choose Footprint.io</h2>
                <p style={{maxWidth: "900px"}}>Native app development delivers the best performance amongst all other development approaches. They bring about optimum performance, maximum safety, great US/UI design, and more. If you&apos;re looking for qualified Native iOS developers, you&apos;ve come to the right place.</p> 
            
                <div className={styles.service_bottom+ " bottom_service"} data-aos="zoom-in">
                    <div className="row" data-aos="zoom-in">
                    <div className={styles.col_6+" col-md-6"+" service_two center"}>
                        <img src="/images/service_sectiontwo.png" className='common_next_img_updated' width={500} height={523} alt="duis" loading="eager"/>
                    </div>
                    <div className={styles.col_6+" col-md-6"}>
                            <div className={styles.service_sectiontwo}>
                                <ul>
                                    <li>
                                        <h5>In-depth Knowledge </h5>
                                        We&apos;re all about creating user-friendly apps in slick designs. We use modern programming languages and have a solid track record of successful projects for various industries. From UI/UX Design to backend development, we&apos;ll make sure that your mobile app will not disappoint.  
                                    </li>
                                    <li>
                                        <h5>Cross-platform Development</h5>
                                        We aren&apos;t just adept in a single platform, but we are highly versatile in all operating systems. We can make an excellent app for any kind of device. 
                                    </li>
                                    <li>
                                        <h5>Strong Background</h5>
                                        From Java to Swift to Kotlin, we have solid experience as to using different native languages and for various device sizes. We specialize in native programming languages as we understand how it&apos;s the best option for excellent speed, user experience, performance, and scalability.
                                    </li>
                                </ul>
                            </div>
                        </div>
                    
                    </div>
                </div>
            </div>
         </div>
    )

}
export default ServiceBottom