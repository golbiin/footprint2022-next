import React, { useEffect } from 'react'
import Image from 'next/image'
import styles from '../../styles/services.module.scss'
import AOS from "aos";
function ServiceLast() {
    useEffect(() => {
        AOS.init();
    },[])
    return(
        <div className="container">
            <div className={styles.service_last+ " services_last"} data-aos="zoom-in">
                    <div className="row last" data-aos="zoom-in">
                        <div className={styles.col_6+" col-md-6"+" brochure_sectionlast"}>
                                <div className={styles.service_sectionthree}>
                                    <ul>
                                        <li>
                                            <h5>UI/ UX Design Experts</h5>
                                            We enjoy creating the best user interface for any type of project. We know which screen is perfect, what resolution, which effects, pixel, icons, and design elements that will create the perfect, most user-friendly design. 
                                        </li>
                                          <li>
                                            <h5>Timely Delivery</h5>
                                            We hate delays as much as you do, so we make sure to respect and stick to your timetables. We have perfected our project management flow, and that makes us equipped and able to deliver on time and on budget. 
                                        </li>
                                        <li>
                                            <h5>Smooth Project Execution</h5>
                                            From start to finish, we will keep you updated and will work with you till we achieve the best mobile app you&apos;re looking for. This is made possible with the most brilliant team of app developers we have at Footprint.io. With their problem-solving skills, passion, and solid experience and knowledge, nothing is impossible. 
                                        </li> 
                                    </ul>
                                </div>
                        </div>
                        <div className={styles.col_6+" col-md-6"+" service_three center"}>
                            <img src="/images/service_sectionthree.png" className='common_next_img_updated' width={500} height={523} alt="duis" loading="eager"/>
                        </div>
                    </div>
                   
            </div>
        </div>
        )

    }
    export default ServiceLast