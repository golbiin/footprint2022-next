import React, { useEffect } from 'react'
import Image from 'next/image'
import styles from '../../styles/services.module.scss'
import AOS from "aos";
function ServiceTop() {
    useEffect(() => {
        AOS.init();
    },[])
    return(
        <div className="container">
            <div className={styles.service_topsection + " service_top"} data-aos="zoom-in">
                <div className="row first" data-aos="zoom-in">
                   <div className={styles.col_6+" col-md-6"+" brochure_sectionone"}>
                        <div className={styles.service_sectionone}>
                            
                            <ul>
                                <li>
                                    <h5>Optimum Performance</h5>
                                    
                                    Native iOS development offers high-level operational efficiency. Native mobile apps are exceedingly fast and responsive. Footprint.io can help you create the best native iOS app using core programming languages and APIs.
                                </li>
                                <li>
                                    <h5>Maximum Security</h5>
                                    
                                    With native apps, users can enjoy high-end data protection since everything is coded into the app infrastructure, encrypted as well as obfuscated.
                                </li>  
                                <li>
                                    <h5>Great UX/ UI Design</h5>
                                    
                                    Native apps simply offer the best user experience since you can maximize the device capabilities, providing functionalities which are exclusive to the OS. We can help you create native apps that are extremely natural and makes users easily learn and love your app. 
                                </li>
                               
                            </ul>
                        </div>
                       
                  </div>
                <div className={styles.col_6+" col-md-6"+" service_one center"}>
                    <img src="/images/service_sectionone.png" className='common_next_img_updated' width={500} height={523} alt="duis" loading="eager"/>
                </div>
                </div>
            </div>
        </div>
    )

}
export default ServiceTop