import React, { useEffect } from 'react'
import Image from 'next/image'
import styles from '../../styles/services.module.scss'
import AOS from "aos";
function ServiceBanner() {
    useEffect(() => {
        AOS.init();
    },[])
    return(
        <div className="container">
            <div className={styles.service_section} data-aos="zoom-in">
             <h1>Native iOS Development</h1>
                
                <p style={{maxWidth: "900px"}}>
                Native app development sure has a lot of advantages, but the process is definitely not simple, and you need the most experienced Native iOS developers to make it all possible. With the right team backing you up, you can make the most of the benefits of native iOS development.</p>
            </div>
        </div>
    )

}
export default ServiceBanner