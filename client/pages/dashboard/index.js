import React from 'react'
import Sidebar from '../../components/common/sidebar';
import MiddleBox from './middle-box';
import RightBox from './right-box';
import DashboardLayout from './../../components/common/dashboardLayout';


function Dashboard() {
    const seoData = {
        title: "Dashboard | Footprint.io"
    }
    return(
        <DashboardLayout seoData={seoData}>
            <Sidebar />
            <MiddleBox />
            <RightBox />
        </DashboardLayout>
    )
}

export default Dashboard