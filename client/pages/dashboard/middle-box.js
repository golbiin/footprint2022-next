import styles from '../../styles/Dashboard.module.scss'
import { useRouter } from 'next/router'
import Joi from "joi-browser";
import Image from 'next/image'
import React, { useEffect ,useState} from 'react'
import { Modal, Button } from "react-bootstrap";
import * as userService from "../../services/userUservice";
import SubmitButton from '../../components/submit-button';
import Moment from "react-moment";

function MiddleBox() {
    const router = useRouter();
    const [showModal,setShowModal]=useState(false);
    const [errors,setErrors]=useState([]);
    const [websiteError,setWebsiteError]=useState(false);
    const [projectData,setProjectData]=useState({project_name:"",website:""});
    const [response_message,setResponseMessage]=useState("");
    const [response_type,setResponseType]=useState("");
    const [submit_status,setSumbit]=useState(false);
    const [projectsList,setProjectsList]=useState([]);
    const [workingHours,setWorkingHours]=useState([]);
    const [currentProject,setCurrentProject]=useState("");
    const [openDetailsId, setOpenDetailsId]=useState("");
    useEffect(() => {
      getProjects();
      getWorkingDetailsList();
      
    },[1]);

 
    
const getWorkingDetailsList = async () => {
 
  
      const responseDetails = await userService.getWorkingDetailsList();
      console.log("responseDetails", responseDetails);
  
       if (responseDetails.data.status === 1) {
        setWorkingHours(responseDetails.data.data)
       } else {
       }
    };

  const getProjects = async() => {
    const response = await userService.getProjects();
    console.log("response", response)
    if (response && response.data.status === 1) {
      //setLoader(false)
      setProjectsList(response.data.data);
     
      
    } else {
     // setLoader(false)
    }
  }
    const handleClose = (val) => {
        setShowModal(val);
      };
    const schema = {
        project_name: Joi.string()
        .required()
            .error(() => {
            return {
                message:
                "Please Enter Project Name"
            };
        }),
        website: Joi.any().optional(),
    };
    const validateProperty = ({ name, value }) => {
        const obj = { [name]: value };
        const Registerschema = { [name]: schema[name] };
        const { error } = Joi.validate(obj, Registerschema);
        return error ? error.details[0].message : null;
      };
      const handleInputchange=({ currentTarget: input })=> {
        let newErrors = { ...errors };
        setResponseMessage("");

        if(input.name=="website"){
           
          var pattern = new RegExp('^(https?:\\/\\/)?'+ // protocol
          '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|'+ // domain name
          '((\\d{1,3}\\.){3}\\d{1,3}))'+ // OR ip (v4) address
          '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*'+ // port and path
          '(\\?[;&a-z\\d%_.~+=-]*)?'+ // query string
          '(\\#[-a-z\\d_]*)?$','i'); // fragment locator
          
          if(!!pattern.test(input.value)){
            delete newErrors[input.name]; 
            console.log("SSSSSSSS");
            setErrors(newErrors);
          }
          else{
            newErrors[input.name]="Please enter a valid url."
            setErrors(newErrors);
            console.log("SSS","S", newErrors);
          }
        }

        const errorMessage = validateProperty(input);
        if (errorMessage) newErrors[input.name] = errorMessage;
        else delete newErrors[input.name]; 

        projectData[input.name] = input.value;
        setProjectData(projectData);
        setErrors(newErrors);
        console.log(errors);
    }  
    const handleWebsitechange=({ currentTarget: input })=> {
      let newErrors = { ...errors };
      setResponseMessage("");
        var pattern = new RegExp('^(https?:\\/\\/)?'+ // protocol
        '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|'+ // domain name
        '((\\d{1,3}\\.){3}\\d{1,3}))'+ // OR ip (v4) address
        '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*'+ // port and path
        '(\\?[;&a-z\\d%_.~+=-]*)?'+ // query string
        '(\\#[-a-z\\d_]*)?$','i'); // fragment locator
        if(input.value === ''){
          setWebsiteError(false);
        }
        else if(!!pattern.test(input.value)){
          delete newErrors[input.name]; 
          console.log("SSSSSSSS", newErrors);
          setErrors(newErrors);
          setWebsiteError(false);
        }
        else{
          newErrors[input.name]="Please enter a valid url."
          setErrors(newErrors);
          console.log("SSS","S", newErrors);
          setWebsiteError(true)
        }
        projectData[input.name] = input.value;
        setProjectData(projectData);
     //   setErrors(newErrors);
      }
     
    const checkBtnenable = () => {
      console.log("websiteError-btnnn",websiteError)
        const { error } = Joi.validate(projectData, schema);
        console.log("sss",error)
        if(websiteError){
          return true;
        }
        if (!error) {
          return null;
        }
        const errors = {};
        for (let item of error.details) errors[item.path[0]] = item.message;
        console.log("errors222",errors)
        return errors;
      };

    const doSubmit = async () => {
      console.log("projectData", projectData)
      console.log("websiteError",websiteError)
    const errors = { ...errors };
    const { error } = Joi.validate(projectData, schema);
    if (error) {
      let path = error.details[0].path[0];
      let errormessage = error.details[0].message;
      errors[path] = errormessage;
      setErrors(errors);
    } else {
      setSumbit(true);
      const response = await userService.createProject(projectData);
      if (response) {
        setResponseType(response.data.status);
        setResponseMessage(response.data.message);
        setSumbit(false);
        setProjectData({project_name:"",website:""});
         setTimeout(() => {
          setShowModal(false);
          setResponseType("");
          setResponseMessage("");
 
          setProjectsList([response.data.data.project, ...projectsList]);
        }, 1500);
      } 
    }
  };

  const readWorkingDetails = async (id) => {
    const response = await userService.readWorkingDetails(id);
    if (response) {
      console.log("response", response)
    }
  };
  const openDetails = (id, read_post) => {

    setOpenDetailsId(openDetailsId === id  ? '' : id);
    if(!read_post) 
      readWorkingDetails(id);
  } 
  console.log("projectsList", projectsList)
    return (
    <div className={styles.middle_box+" "+ "col"}>
      <div className={styles.inner_container}>
          <div className={styles.title}>Dashboard</div>
          <div className={styles.content}>
            
              <div className={styles.projects+ " row"}>
                  <div className={styles.project_box+"  col-md-4 "+styles.add_new} onClick={()=>handleClose(!showModal)}>
                        <Image src="/images/add_new.png" width="33" height="33"/>
                         <p>Add project</p>
                  </div>
                {
                  projectsList && projectsList.length > 0 ?
                  projectsList.map((project, index) => {
                    let count=0;
                  return(
                    <div key={project._id} className={styles.project_box + " col-md-4"} onClick={()=>setCurrentProject(project._id)}>
                    <h3>{project.project_name}</h3>
                    <div className={styles.updates}>
                        {
                        workingHours && workingHours.length > 0 ?
                        workingHours.map((workingHoursEach, index) =>{
                        if(workingHoursEach.project_id==project._id && !workingHoursEach.read_post){
                          count++
                        }
                     }):""}
                    <span className={styles.count}>{count}</span>
                      {count>1? <p>Updates</p>:<p>Update</p>}
                    </div>
                  </div>
                  ) 
                }) : <div className={styles.noProject}>No Projects Added</div>
                }
              
                 
              </div>
              <div className={styles.title}>Daily Updates</div>
              <div className={styles.activities}>
              {
                  workingHours && workingHours.length > 0 ?
                  workingHours.map((workingHoursEach, index) => (
                    currentProject?workingHoursEach.project_id==currentProject? 
                     <div className={styles.each_row} key={workingHoursEach._id}>
                    <p className={styles.report_date}>Account Change  -   <Moment format="MMM DD">
                                {workingHoursEach.report_date}
                              </Moment> {/* -  Dec 14 */}</p>
                    
                              <div className={styles.hours_title} onClick={()=>openDetails(workingHoursEach._id, workingHoursEach.read_post)}>
                    <h4>{workingHoursEach.project[0] &&
                                  workingHoursEach.project[0].project_name
                                    ? workingHoursEach.project[0].project_name
                                    : "NA"}</h4>
                    <h3>{workingHoursEach.project[0] &&
                                  workingHoursEach.post_title
                                    ? " - " + workingHoursEach.post_title
                                    : "NA"}</h3>
                                   
                                    </div>
                            <div className={openDetailsId === workingHoursEach._id ? styles.hours_details + " " + styles.show :  styles.hours_details}>
                              <p>Time: {workingHoursEach.working_hours} Hours</p>
                          <p
                              dangerouslySetInnerHTML={{
                                __html: workingHoursEach.project[0] &&
                                workingHoursEach.post_details
                                  ? workingHoursEach.post_details
                                  : "NA"
                              }}
                            />

                          </div>
                    </div>
                    :"":   <div className={styles.each_row} key={workingHoursEach._id} >
                    <p className={styles.report_date}>Account Change  -   <Moment format="MMM DD">
                                {workingHoursEach.report_date}
                              </Moment> {/* -  Dec 14 */}</p>
                              <div className={styles.hours_title} onClick={()=>openDetails(workingHoursEach._id, workingHoursEach.read_post)}>
                    <h4>{workingHoursEach.project[0] &&
                                  workingHoursEach.project[0].project_name
                                    ? workingHoursEach.project[0].project_name
                                    : "NA"}</h4>
                    <h3>{workingHoursEach.project[0] &&
                                  workingHoursEach.post_title
                                    ? " - " + workingHoursEach.post_title
                                    : "NA"}</h3>
                          </div>
                          <div className={openDetailsId === workingHoursEach._id ? styles.hours_details + " " + styles.show :  styles.hours_details}>
                          <p>Time: {workingHoursEach.working_hours} Hours</p>
                          <p
                              dangerouslySetInnerHTML={{
                                __html: workingHoursEach.project[0] &&
                                workingHoursEach.post_details
                                  ? workingHoursEach.post_details
                                  : "NA"
                              }}
                            />

                          </div>
                    </div>
                  )) : null
                }
                
                  <div className={styles.end_of_feed}>You&apos;ve reached the end of the feed</div>
              </div>
          </div>
      </div>
      <Modal
          show={showModal}
          className={styles.custom_modal}
          onHide={handleClose}
          animation={false}
        >
          <div className={styles.modal_content}>
          <Modal.Header closeButton className={styles.modal_header}>
            <Modal.Title className={styles.modal_title}><h2>Add A Project</h2></Modal.Title>
          </Modal.Header>
          <Modal.Body className={styles.modal_body}>
              <div className='response_container'>
                <div className={response_type==1?"sucsess":"error"}>
                    <span>{response_message} </span>
                </div>
              </div>
              <div className={styles.form}>
                 <div className={styles.form_group}>
                    <label className={styles.project}>Project Name</label>
                    <input type="text" name='project_name' className={styles.form_control} placeholder='Enter Project Name' value={projectData.project_name}  onChange={handleInputchange}/>
                    {errors.project_name?<span className='red-validate-error'>{errors.project_name}</span>:null}
                 </div>
                 <div className={styles.form_group}>
                    <label className={styles.website}>Website URL</label>
                    <input type="text" name='website' className={styles.form_control} placeholder='www.sample.com' value={projectData.website}  onChange={handleWebsitechange}/>
                    {errors.website?<span className='red-validate-error'>{errors.website}</span>:null}
                 </div>
                
              </div>
          </Modal.Body>
          <Modal.Footer className={styles.modal_footer}>
          <SubmitButton btn_label="Submit" className="fp_green_btn" submit_status={submit_status} animation="border" variant="dark" size="sm" submit={doSubmit} Btnenable={checkBtnenable}/>
          </Modal.Footer>
          </div>
         
        </Modal>
    </div>
    )
}
export default MiddleBox