
import styles from '../../styles/Dashboard.module.scss'
 
import Joi from "joi-browser";
import Image from 'next/image'
import React, { useEffect ,useState} from 'react'
import { Modal, Button } from "react-bootstrap";
import * as paymentService from "../../services/paymentServices";
import SubmitButton from '../../components/submit-button';
import Moment from "react-moment";

function RightBox() {
    const [subscriptionList,setSubscriptionList]=useState([]);

    useEffect(() => {
        getSubscriptions();
      
    },[1]);


  const getSubscriptions = async() => {
    const response = await paymentService.getSubscriptions();
    console.log("response", response)
    if (response && response.data.status === 1) {
      //setLoader(false)
      setSubscriptionList(response.data.data);
     
      
    } else {
     // setLoader(false)
    }
  }
 
   

   
 
  console.log("subscriptionList", subscriptionList)

    return (
        <div className={styles.right_box+" "+ "col"}>
         <div className={styles.inner_container}>
         <div className={styles.title}>Billing</div>
          <div className={styles.content}>
              {subscriptionList.length ? <>
               <div className={styles.each}>
                   <div className={styles.heading}>{subscriptionList.length}</div>
                   <div className={styles.sub_contnet}>you have currently {subscriptionList.length} plan.</div>
               </div>
               <div className={styles.each}>
                   <div className={styles.heading}>Payment</div>
                   <div className={styles.sub_contnet}>Next payment is due <Moment format="MMMM DD">{subscriptionList[0].expire_on}</Moment>
                   </div>
               </div>
               <div className={styles.each}>
                   <div className={styles.heading}>Plan</div>
                   <div className={styles.sub_contnet}>
                        {Object.keys(subscriptionList[0].plan).length !== 0 ? subscriptionList[0].plan.planDescription : null} 
                        <p>
                        {Object.keys(subscriptionList[0].plan).length !== 0 ? "Total $" + subscriptionList[0].plan.totalAmount + "/Mo": null} 
                        </p>

                   </div>
               </div>
             </>
             : null }
                <div className={styles.concern}>
                    <p>Have a problem or concern?</p>
                    <a className='fp_green_btn'>Chat With Us</a>
                </div>
               
          </div>
         </div>
        </div>
    )
}
export default RightBox