import React, { useEffect } from 'react'
import Image from 'next/image'
import styles from '../../styles/About.module.scss'
import AOS from "aos";

function OurJourney() {
    useEffect(() => {
        AOS.init();
    },[])
    return(
        <div className={styles.joureny_section}>
            <div className="container" data-aos="zoom-in">
             <h3>We&apos;ve developed Websites and Apps using the Worlds Most Popular Platforms.</h3>
                {/* <div className={styles.journey_wrapper+' row'}>
                    <div className={styles.col_right+ " col-md-6"} data-aos="zoom-in">
                    <Image src="/images/journey.png" width={459} height={459} alt="journey" />
                    </div>
                    <div className={styles.col_left+ " col-md-6"} data-aos="zoom-in">
                        <h3>Our Journey</h3>
                        <ul className={styles.journey_list}>
                            <li><span>2015</span>Suspendisse faucibus nunc et pellentesque egestas</li>
                            <li><span>2016</span>Donec venenatis vulputate lorem proin viverra</li>
                            <li><span>2017</span>Pellentesque habitant morbi tristique senectus et netus</li>
                            <li><span>2018</span>Phasellus tempus nam quam nunc blandit</li>
                            <li><span>2019</span>onec elit libero sodales nec volutp</li>
                            <li><span>2020</span>Ut a nisl id ante tempus hendrerit nunc nonummy metus</li>
                        </ul>
                    </div>
                </div> */}
                <div className={styles.client_logos} data-aos="zoom-in">
                
                 <div className={styles.each_logo}><Image src="/images/logos/1.png"  width={212} height={44} alt="stripe" loading="eager" layout="fixed" /></div>
                 <div className={styles.each_logo}><Image src="/images/logos/2.png"  width={53} height={63} alt="gusto" loading="eager" layout="fixed"/></div>
                 <div className={styles.each_logo}><Image src="/images/logos/3.png"  width={83} height={71} alt="square" loading="eager" layout="fixed" /></div>
                 <div className={styles.each_logo}><Image src="/images/logos/4.png"  width={136} height={32} alt="bigcommerce" loading="eager" layout="fixed"/></div>
                 <div className={styles.each_logo}><Image src="/images/logos/5.png"  width={58} height={61} alt="shopify" loading="eager" layout="fixed" /></div>
                 <div className={styles.each_logo}><Image src="/images/logos/6.png"  width={144} height={71} alt="fresh books" loading="eager" layout="fixed"/></div>
       
                 <div className={styles.each_logo}><Image src="/images/logos/8.png"   width={157} height={69}  alt="React Js" loading="eager" layout="fixed"/></div>
             
                </div>
            </div>
        </div>
    )

}
export default  OurJourney
