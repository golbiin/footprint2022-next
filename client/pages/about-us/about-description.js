import React, { useEffect } from 'react'
import Image from 'next/image'
import styles from '../../styles/About.module.scss'
import AOS from "aos";

function AboutDescription() {
    useEffect(() => {
        AOS.init();
    },[])
    return(
        <div className="container">
            <div className={styles.description_section +" row"} >
                <div className={styles.col_left+ " col-md-6"} data-aos="zoom-in">
                     <h3>Website Development and Digital Marketing shouldn&apos;t be complicated</h3>
                     <p>Footprint simplifies software and digital marketing. Our dream, however, expands beyond that. We build the software your business requires to make an impact and then help you carve a footprint on the internet for business</p>
                </div>
                <div className={styles.col_right+ " col-md-6"} data-aos="zoom-in">
                <img src="/images/about-1.png" className='common_next_img_updated' width={564} height={522} alt="about" />
                </div>
            </div>
        </div>
    )

}
export default AboutDescription