import React, { useEffect } from 'react'
import Image from 'next/image'
import styles from '../../styles/About.module.scss'
import AOS from "aos";
import CountUp from 'react-countup';
import VisibilitySensor from 'react-visibility-sensor';

function CountDown() {
    useEffect(() => {
        AOS.init();
    },[])
    return(
        <div className="container">
            <div className={styles.countdown_section}>
                <div className={styles.col_3}  data-aos="zoom-in">
                    <div className={styles.number_div} >
                    <VisibilitySensor partialVisibility offset={{ bottom: 200 }}>
                        {({ isVisible }) => (
                        <div style={{ height: 50 }}>
                            {isVisible ? <CountUp start={1990} end={2015}  duration={1.5} /> : null}
                        </div>
                        )}
                    </VisibilitySensor>
                    
                    </div>
                    <div className={styles.count_label}>Year Founded</div>
                </div>
                <div className={styles.col_3}  data-aos="zoom-in">
                    <div className={styles.number_div}>
                    <VisibilitySensor partialVisibility offset={{ bottom: 200 }}>
                        {({ isVisible }) => (
                        <div style={{ height: 50 }}>
                            {isVisible ? <><CountUp start={0} end={480}  duration={1.5} />+</> : null}
                        </div>
                        )}
                    </VisibilitySensor>
                        
                        </div>
                    <div className={styles.count_label}>Websites Built</div>
                </div>
                <div className={styles.col_3}  data-aos="zoom-in">
                    <div className={styles.number_div}>
                    <VisibilitySensor partialVisibility offset={{ bottom: 200 }}>
                        {({ isVisible }) => (
                        <div style={{ height: 50 }}>
                            {isVisible ? <><CountUp start={0} end={400}  duration={1.5} />+</> : null}
                        </div>
                        )}
                    </VisibilitySensor>
                        </div>
                    <div className={styles.count_label}> Testimonials</div>
                </div>
                <div className={styles.col_3}  data-aos="zoom-in">
                    <div className={styles.number_div}>
                    <VisibilitySensor partialVisibility offset={{ bottom: 200 }}>
                        {({ isVisible }) => (
                        <div style={{ height: 50 }}>
                            {isVisible ? <><CountUp start={0} end={1000}  duration={1.5} />~</> : null}
                        </div>
                        )}
                    </VisibilitySensor>
                        </div>
                    <div className={styles.count_label}> Happy  Clients</div>
                </div>
            </div>
        </div>
    )

}
export default CountDown