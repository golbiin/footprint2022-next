import React, { useEffect } from 'react'
import Image from 'next/image'
import styles from '../../styles/About.module.scss'
import AOS from "aos";
import Link from 'next/link';
function WhoWeAre() {
    useEffect(() => {
        AOS.init();
    },[])
    return(
        <div className="container">
            <div className={styles.who_we_are_section +" row"} data-aos="zoom-in" >
                <h3>Who we help</h3>
                <p>From Entrepreneurs to Larger Businesses, Footprint.io supports thousands of small businesses across the World. 
                    <br></br>View what some of them have to say about Footprint.io
</p>
                <p> <Link href='/success-stories'><a className='fp_green_btn'>View Success Stories</a></Link></p> 
                <img src="/images/who-we-are.png" className={styles.who_we_are_img} width={363} height={376} alt="who we are" />
            </div>
        </div>
    )

}
export default WhoWeAre