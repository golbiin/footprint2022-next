import React, { useEffect } from 'react'
import Header from '../../components/common/header';
import Footer from '../../components/common/footer';
import AboutBanner from './about-banner';
import AboutDescription from './about-description';
import CountDown from './countdown';
import OurJourney from './our-journey';
import WhoWeAre from './who-we-are';
import AboutBottom from './about-bottom';

function About() {
    const seoData = {
        title: "About | Footprint.io",
        description: "Footprint.io builds the software your business requires to make an impact and then help you carve a Footprint on the internet."
    }
    return (
        <div>
            <Header seoData={seoData}/>
            <AboutBanner/>
            <AboutDescription />
            <CountDown />
            <OurJourney />
            <WhoWeAre />
            <AboutBottom />
            
            <Footer />
        </div>
    )
}
export default About