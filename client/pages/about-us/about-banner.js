import React, { useEffect } from 'react'
import Image from 'next/image'
import styles from '../../styles/About.module.scss'
import AOS from "aos";
function AboutBanner() {
    useEffect(() => {
        AOS.init();
    },[])
    return(
        <div className="container">
            <div className={styles.banner_section} data-aos="zoom-in">
                <h1>About Us</h1>
                <p>You have the vision. We have your dream team to execute it.</p>
            </div>
        </div>
    )

}
export default AboutBanner