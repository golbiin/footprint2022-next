import React, { useEffect } from 'react'
import Image from 'next/image'
import styles from '../../styles/About.module.scss'
import AOS from "aos";
import Link from 'next/link';

function AboutBottom() {
    useEffect(() => {
        AOS.init();
    },[])
    return(
        <div className={styles.bottom_bg_section} >
        <div className="container">
                <div className={styles.col_left} data-aos="zoom-in">
                     <h5>“I&apos;m a visual person, and if I was in the market for a new Website and/or App, I&apos;d like to see what it could look like before I commit to a company. That&apos;s why we offer a complimentary design to show our quality before you commit to Footprint.io.”</h5>
                     <p> Michael Teofilo</p>
                     <p>Director</p>
                     <Link href="/request-sample"><a className={styles.white_btn}>Get A Sample</a></Link>
                </div>
            </div>
        </div>
    )

}
export default AboutBottom