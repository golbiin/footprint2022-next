import React, { useEffect,useState } from 'react'
import styles from '../../styles/Contact.module.scss'
import AOS from "aos";
import { useRouter } from 'next/router'

function StartForm() {
    useEffect(() => {
        AOS.init();
    },[])
    const router = useRouter();
    const [topic,settopic]=useState("");
    const [error,setError]=useState("");
    const handleChange = async ({ currentTarget: input }) => {
        settopic(input.value)
    };
    
    const gotoNext = () => {
        if (topic != "") {
            window.sessionStorage.setItem("contact-topic", topic);
            router.push({
                pathname: '/contact',
                query: {topic:topic},
            }, 'contact');
        } else {
            setError("Please select a topic.")
        }
      };
    return(
        <div className="container">
            <div className={styles.form_section} data-aos="zoom-in">
               <select className={styles.form_control} onChange={handleChange}>
                    <option value="">Start by selecting a topic</option>
                    <option value="Sales">Sales</option>
                    <option value="General">General</option>
                    <option value="Billing">Billing</option>
                    <option value="Technical Support">Technical Support</option>
                    <option value="Other">Other</option>
                </select>
                <a className='fp_green_btn' onClick={gotoNext}>NEXT</a>
                {error ? (
                    <span className="red-validate-error" style={{ marginTop: "10px" }}>
                      {error}
                    </span>
                  ) : (
                    ""
                  )}
            </div>
        </div>
    )

}
export default StartForm