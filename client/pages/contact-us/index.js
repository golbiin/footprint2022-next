import React, { useEffect } from 'react'
import Header from '../../components/common/header';
import Footer from '../../components/common/footer';
import ContactTop from './contact-top';
import StartForm from './contact-start-form';
import BottomBg from './contact-bottom-bgsection';

function Contact() {
    const seoData = {
        title: "Contact Us | Footprint.io",
        description: "Get immediate support. We’d love to hear from you. An agent is always available for issues, concerns, or questions."
    }
    return (
        <div>
            <Header seoData={seoData}/>
             <ContactTop />
             <StartForm />
             <BottomBg />
            <Footer />
        </div>
    )
}
export default Contact