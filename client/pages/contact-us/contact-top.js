import React, { useEffect } from 'react'
import styles from '../../styles/Contact.module.scss'
import AOS from "aos";
function ContactTop() {
    useEffect(() => {
        AOS.init();
    },[])
    return(
        <div className="container">
            <div className={styles.banner_section} data-aos="zoom-in">
                <h1>Contact Us</h1>
                <p>Please start by selecting one of the topics from the dropdown list below.</p>
                <p>You&apos;re welcome to <a >chat with us</a> for a faster response.</p>
            </div>
        </div>
    )

}
export default ContactTop