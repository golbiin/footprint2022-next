import React, { useEffect } from 'react'
import styles from '../../styles/Contact.module.scss'
import AOS from "aos";
function BottomBg() {
    useEffect(() => {
        AOS.init();
    },[])
    return(
        <div className={styles.bottom_section} >
            <div className="container">
             <div className={styles.contact_info_box}>
                 
                 <h2>Say Hello</h2>
                 <a href="mailto:hello@footprint.io">hello@footprint.io</a>
                 <a href="to:18335034420">1-833-203-4420</a>
                 <a className={styles.chat_link}>Chat With Us</a>
                 </div>
             </div>
         
        </div>
    )

}
export default BottomBg