import React, { useEffect } from 'react'
import Image from 'next/image'
import styles from '../../styles/services.module.scss'
import AOS from "aos";
function ServiceTop() {
    useEffect(() => {
        AOS.init();
    },[])
    return(
        <div className="container">
            <div className={styles.service_topsection + " service_top"} data-aos="zoom-in">
                <div className="row first" data-aos="zoom-in">
                   <div className={styles.col_6+" col-md-6"}>
                        <div className={styles.service_sectionone}>
                            
                            <ul>
                                <li>
                                    <h5>Engaging Designs</h5>
                                    
                                    It all stems from the design. Leave an incredible first impression and get that WOW! factor when someone lands on your website. Make customers want to purchase from you.
                                </li>
                                <li>
                                    <h5>All Types of Content Management Systems</h5>
                                    
                                    If the CMS is out there, we&apos;ve likely built on it. Our team will not only set it up but also train you to effectively utilize its tools to help you save time and money during the development process.
                                </li>
                                <li>
                                    <h5>Coding Isn&apos;t a Problem</h5>
                                    
                                    Footprint.io is a development agency. Our in-house developers can create and adapt any website/software to your company&apos;s goal.
                                </li>
                               
                            </ul>
                        </div>
                       
                  </div>
                <div className={styles.col_6+" col-md-6"+" service_one"}>
                    <img src="/images/service_sectionone.png" className='common_next_img_updated' width={500} height={523} alt="duis" loading="eager"/>
                </div>
                </div>
            </div>
        </div>
    )

}
export default ServiceTop