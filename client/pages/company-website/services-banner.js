import React, { useEffect } from 'react'
import Image from 'next/image'
import styles from '../../styles/services.module.scss'
import AOS from "aos";
function ServiceBanner() {
    useEffect(() => {
        AOS.init();
    },[])
    return(
        <div className="container">
            <div className={styles.service_section} data-aos="zoom-in">
             <h1>Custom Websites</h1>
                <p>Don&apos;t be limited by your geographic location. Instead, target the masses by offering your products & services for sale on the internet. It&apos;s easier than you think.</p>
            </div>
        </div>
    )

}
export default ServiceBanner