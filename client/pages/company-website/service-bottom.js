import React, { useEffect } from 'react'
import Image from 'next/image'
import styles from '../../styles/services.module.scss'
import AOS from "aos";
function ServiceBottom() {
    useEffect(() => {
        AOS.init();
    },[])
    return(
        <div className="container">
             <div className={styles.servicebottom_section+ " services_bottom"} data-aos="zoom-in">
                <h2>Mobile & Tablet Responsive</h2>
                <p>Whether customers are browsing from a computer, mobile, or tablet device, they&apos;ll view a perfectly responsive page, specifically optimized for that device</p> 
            
                <div className={styles.service_bottom+ " bottom_service"} data-aos="zoom-in">
                    <div className="row" data-aos="zoom-in">
                    <div className={styles.col_6+" col-md-6"+" service_two"}>
                        <img src="/images/service_sectiontwo.png" className='common_next_img_updated' width={500} height={523} alt="duis" loading="eager"/>
                    </div>
                    <div className={styles.col_6+" col-md-6"}>
                            <div className={styles.service_sectiontwo}>
                                <ul>
                                    <li>
                                        <h5>Appear in Google Shopping & Amazon</h5>
                                        Building a website to list your products is the initial step. But being found and ranking higher amongst your competition is what counts.
                                    </li>
                                    <li>
                                        <h5>Build Brand Recognition</h5>
                                        Utilize Footprint.io&apos;s proven digital marketing strategy to stand above the sea of competition. We combine customer reviews and social media following to make your listing stand out.
                                    </li>
                                    <li>
                                        <h5>Get Found</h5>
                                        Having a website isn&apos;t enough. Tap into our successful online marketing resources, which combine Search Engine Optimization, Google My Business, Google Shopping, Amazon, and Social Media to drive traffic and results.
                                    </li>
                                </ul>
                            </div>
                        </div>
                    
                    </div>
                </div>
            </div>
         </div>
    )

}
export default ServiceBottom