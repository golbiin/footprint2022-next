import React, { useEffect ,useState} from 'react'
import styles from '../../styles/Dashboard.module.scss'
import Spinner from "react-bootstrap/Spinner";
import * as authServices from "../../services/authService";
 
import { useRouter } from 'next/router'
import { loadStripe } from "@stripe/stripe-js";
import {
  Elements,
  CardNumberElement,
  CardCvcElement,
  CardExpiryElement,
  useElements,
  useStripe
} from "@stripe/react-stripe-js";
import * as paymentServices from "../../services/paymentServices";

function CardForm() {
    const router = useRouter();
    const stripe = useStripe();
    const elements = useElements();
 
    const [savedCard, setSavedCard] = useState([]);
    const [loader, setLoader] = useState(true);
    const [paymentResponse, setPaymentResponse] = useState();
    const [paymentStatus, setPaymentStatus] = useState();
    const [submittingStatus, setsubmittingStatus] = useState(false);
    const [firstName, setFirstName] = useState("");
    const [firstName_err, setfirstNameErr] = useState("");
    const [lastName_err, setlastNameErr] = useState("");
    const [lastName, setLastName] = useState("");
    const [userEmail, setUserEmail] = useState("");

    useEffect(() => {
      let res=authServices.getCurrentUser();
         if(!res) {
            router.push('/login')
        }
        else{
          setUserEmail(res.email)
        }
      getsavedCard();
       
    },[1]);
  
    const getsavedCard = async() => {
      const response = await paymentServices.getsavedCard();
      console.log("response", response)
      if (response && response.data.status === 1) {
        setLoader(false)
       
        if(response.data.data[0].saved !== undefined)
          setSavedCard(response.data.data[0])
      } else {
        setLoader(false)
      }
    }








    const handleSubmit = async event => {
      setPaymentResponse("");
      event.preventDefault();
      console.log("userEmail", userEmail)
      if (firstName != "" && lastName != "") {
        setsubmittingStatus(true);
        try {
          const cardElementNumber = elements.getElement(CardNumberElement);
          const billingDetails = {
            name: firstName + " " + lastName,
            email: userEmail,
            address: {
              city: '',
              line1: '',
              country: "US",
              state: '',
              postal_code: ''
            }
          };
  
          const paymentMethodResponse= await stripe.createPaymentMethod({
            type: "card",
            card: cardElementNumber,
            billing_details: billingDetails
          });
  
          const { error, paymentMethod }  = paymentMethodResponse;
          console.log("paymentMethod", paymentMethodResponse);
  
            if (error) {
            setPaymentResponse(error.message);
            setPaymentStatus(0);
            setsubmittingStatus(false);
          } else {
            const { id } = paymentMethod;
            
            
             
            const response = await paymentServices.savecard(id, firstName + " " + lastName, userEmail);
            console.log("response-savecard", response);
            
            if (response && response.data) {
              if (response.data.status === 1) {
                
                
               
                  
                  setPaymentResponse(response.data.message);
                  setPaymentStatus(1);
                  setsubmittingStatus(false);
                  setTimeout((
                    router.reload(window.location.pathname)
                  ), 500);
  
                  
                            
  
              } else {
                setPaymentResponse(response.data.message);
                setPaymentStatus(0);
                setsubmittingStatus(false);
              }
            } 
          }  
        } catch (error) {
          console.log("paymentMethod- error", error);
          setPaymentResponse(error.message);
          setPaymentStatus(0);
          setsubmittingStatus(false);
          
        }
      } else {
        if (firstName == "") setfirstNameErr("Please enter your first name");
        if (lastName == "") setlastNameErr("Please enter your last name");
      }
    };
    
     
  
    const handleChange = async ({ currentTarget: input }) => {
      if (input.name === "first_name") {
        if (input.value === "") {
          setfirstNameErr("Please enter your first name");
        } else {
          setfirstNameErr();
        }
        setFirstName(input.value);
      } else {
        if (input.value === "") {
          setlastNameErr("Please enter your last name");
        } else {
          setlastNameErr();
        }
        setLastName(input.value);
      }
    };

    const removeCrad = async id => {
      setLoader(true);
     // console.log(pricingpackage);
      const response = await paymentServices.removeCard(id);
      if (response && response.data) {
       router.reload(window.location.pathname)

      }
    };
    const options = {
      style: {
        base: {
          fontSize: "18px",
          paddingTop: "20px",
          fontFamily: "Inter-Regular",
          color: "#00000",
          "::placeholder": {
            color: "#888888"
          },
          border: "1px solid"
        },
        invalid: {
          color: "red"
        }
      }
    };
    return (
    <div className={styles.middle_box+" "+ "col"}>
      <div className={styles.inner_container}>
          <div className={styles.title}> Update Credit Card</div>
          <div className={styles.content}>
            <div className={styles.payment_method}>
             {
               loader ? (
                <div className="spinner">
                    <Spinner animation="grow" variant="success" />  
                  <span>Connecting...</span>
                </div>
              ): (savedCard.length === 0)? (
                <div className="container">
                  <form onSubmit={handleSubmit} className={styles.add_card}>
                    <div className={styles.card_form + " row"}>
                     
                       
                      <div className="col-md-12 response_container">
                        {paymentResponse ? (
                          <span
                            className={paymentStatus === 1 ? "success" : "error"}
                          >
                            {paymentResponse}
                          </span>
                        ) : (
                          ""
                        )}
                      </div>
                      <div className="col-md-6 payment_card">
                        <label>First Name</label>
                        <input
                          type="text"
                          className="form-control"
                          placeholder="First Name"
                          name="first_name"
                          onChange={handleChange}
                        />
                        <span className={firstName_err ? "red-validate-error" : ""}>
                          {firstName_err}
                        </span>
                      </div>
                      <div className="col-md-6 payment_card">
                        <label>Last Name</label>
                        <input
                          type="text"
                          className="form-control"
                          placeholder="Last Name"
                          name="last_name"
                          onChange={handleChange}
                        />
                        <span className={lastName_err ? "red-validate-error" : ""}>
                          {lastName_err}
                        </span>
                      </div>
                      <div className="col-md-12">
                        <label>Credit Card Number</label>
                        <div className={styles.card_input} >
                          <CardNumberElement options={options} />
                        </div>
                      </div>
                      <div className="col-md-6 payment_card">
                        <label>Expiration Date</label>
                        <div className={styles.card_input} >
                          <CardExpiryElement options={options} />
                        </div>
                      </div>
                      <div className="col-md-6 payment_card">
                        <label>cvc</label>
                        <div className={styles.card_input} >
                          <CardCvcElement options={options} />
                        </div>
                      </div>
                      <div className="col-md-12">
                        {submittingStatus ? (
                           <button
                           className="fp_green_btn"
                           type="submit"
                           disabled
                         >
                          <Spinner 
                          animation="border"
                          variant="dark"
                          size="sm" 
                          />
                          Submitting...
                          </button>
                        ) : (
                          <button
                            className="fp_green_btn"
                            type="submit"
                            disabled={!stripe}
                          >
                            Submit →
                              
                          </button>
                        )}
                      </div>
                    </div>
                  </form>
                 
                </div>
              ) : (
                <div className="container">
                  <form className={styles.saved_card}>
                    <div className={styles.card_form + " row"}>
                    
             
                      <div className="col-md-12 response_container">
                        {paymentResponse ? (
                          <span
                            className={paymentStatus === 1 ? "success" : "error"}
                          >
                            {paymentResponse}
                          </span>
                        ) : (
                          ""
                        )}
                      </div>
                      <div className="col-md-12">
                        <div className={styles.saved_card_view}>
                          <div className="card-number">
                            <span>xxxxx</span>
                            <span>xxxxx</span>
                            <span>xxxxx</span>
                            <span>
                              {savedCard.length != 0
                                ? savedCard.customer_data.last4
                                : "xxxx"}
                            </span>
                          </div>
                          <div className="card-expire">
                            <span>
                              {savedCard.length != 0
                                ? savedCard.customer_data.exp_month
                                : "00"}
                              /
                              {savedCard.length != 0
                                ? savedCard.customer_data.exp_year
                                : "0000"}
                            </span>
                          </div>
                        </div>
                          <a
                          className={styles.remove_card_link}
                          to="#"
                          onClick={() => removeCrad(savedCard.saved._id)}
                        >
                          <i className="fa fa-trash" aria-hidden="true"></i> Remove and add new
                          card
                        </a> 
                      </div>
                      
                      
                    </div>
                  </form>
                </div>
              )
             }
              
            </div>
          </div>
      </div>
      
    </div>
    )
}
const stripePromise = loadStripe(process.env.STRIPE_PUBLISHABLE);
const MiddleBox = props => {
  console.log("ddd", props);
  return (
    <Elements stripe={stripePromise}>
      <CardForm {...props} />
    </Elements>
  );
};
export default MiddleBox