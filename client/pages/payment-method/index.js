import React, { useEffect ,useState} from 'react'
import styles from '../../styles/Dashboard.module.scss'

import { useRouter } from 'next/router'
import Sidebar from '../../components/common/sidebar';
import MiddleBox from './middle-box';
import RightBox from './../dashboard/right-box';
import DashboardLayout from './../../components/common/dashboardLayout';

function Dashboard() {
    const router = useRouter()
    
    const seoData = {
        title: "Payment Methods | Footprint.io"
    }
    return(
        <DashboardLayout seoData={seoData}>
            <Sidebar />
            <MiddleBox />
            <RightBox />
        </DashboardLayout>
    )
}

export default Dashboard