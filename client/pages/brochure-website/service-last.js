import React, { useEffect } from 'react'
import Image from 'next/image'
import styles from '../../styles/services.module.scss'
import AOS from "aos";
function ServiceLast() {
    useEffect(() => {
        AOS.init();
    },[])
    return(
        <div className="container">
            <div className={styles.service_last+ " services_last"} data-aos="zoom-in">
                    <div className="row last" data-aos="zoom-in">
                        <div className={styles.col_6+" col-md-6"+" brochure_sectionlast"}>
                                <div className={styles.service_sectionthree}>
                                    <ul>
                                        <li>
                                            <h5>Get Noticed</h5>
                                            Drafting unique, engaging content & video through advertisements will draw the user&apos;s eye to your listing and website. We&apos;ll teach you how to create funnels that properly drive traffic and increase engagement. 
                                        </li>
                                        {/* <li>
                                            <h5>Keep Customers Engaged</h5>
                                            We&apos;ll teach you how to properly integrate CRMs to keep customers engaged with your brand and company updates. All in the effort to turn more sales and increase exposure.
                                        </li>
                                        <li>
                                            <h5>Keep Customers Coming Back</h5>
                                            We&apos;ve worked with many companies and have implemented many different marketing strategies; we know which ones work and which don&apos;t. 
                                        </li> */}
                                    </ul>
                                </div>
                        </div>
                        <div className={styles.col_6+" col-md-6"+" service_three"}>
                            <img src="/images/service_sectionthree.png" className='common_next_img_updated' width={500} height={523} alt="duis" loading="eager"/>
                        </div>
                    </div>
                   
            </div>
        </div>
        )

    }
    export default ServiceLast