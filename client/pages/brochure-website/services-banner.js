import React, { useEffect } from 'react'
import Image from 'next/image'
import styles from '../../styles/services.module.scss'
import AOS from "aos";
function ServiceBanner() {
    useEffect(() => {
        AOS.init();
    },[])
    return(
        <div className="container">
            <div className={styles.service_section} data-aos="zoom-in">
             <h1>Brochure Websites</h1>
                
                <p>Sometimes building a website for your company through a Website Builder isn&apos;t enough. Businesses often don&apos;t realize that your customers judge your business based on your website, as it&apos;s their first impression.</p>
            </div>
        </div>
    )

}
export default ServiceBanner