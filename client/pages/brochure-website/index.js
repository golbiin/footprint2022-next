import Footer from '../../components/common/footer'
import Header from '../../components/common/header'
import ServiceBanner from './services-banner'
import ServiceTop from './services-top'
import ServiceBottom from './service-bottom'
import ServiceLast from './service-last'
import ServiceFeatures from './services-features'
import ServiceTestimony from './service-testimony'

function Services() {
        const seoData = {
                title: "Brochure & Company Websites | Footprint.io",
                description: "Finding a place for your business on the web. We can help drive traffic to your website."
                }
    return (
    <main>
            <Header seoData={seoData}/>
            <ServiceBanner />
            <ServiceTop />
            <ServiceBottom />
            <ServiceLast />
            <ServiceFeatures />
            <ServiceTestimony />
            
            <Footer />
    </main>)
      }
export default Services