import React, { useEffect,useState } from 'react'
import Footer from '../../components/common/footer'
import Header from '../../components/common/header'
import Banner from './banner';
import FormSection from './formSection';
import AOS from "aos";
import Joi from "joi-browser";
import { useRouter } from 'next/router'
import * as authService from "../../services/authService";
function ResetPassword(){
    const router=useRouter();
    const {id} = router.query;
    const [resetpassword,setResetpassword]=useState({new_password:"",re_password:""});
    const [reset_message,setResetMessage]=useState("");
    const [reset_message_type,setResetType]=useState("");
    const [reset_submit_status,setResetSubmitStatus]=useState(false);
    const [errors,setErrors]=useState([]);

    const resetSchema = {
        new_password: Joi.string()
          .required()
          .min(8)
          .regex(
            /^(?=.*?[A-Z])(?=(.*[a-z]){1,})(?=(.*[\d]){1,})(?=(.*[\W]){1,})(?!.*\s).{8,}$/
          )
          .label("Password"),
          re_password: Joi.any()
          .valid(Joi.ref("new_password"))
          .required()
          .options({ language: { any: { allowOnly: "must match password" } } })
          .error(() => {
            return {
              message: "The passwords you’ve entered does not match."
            };
          })
      };

    const  handleChange = ({ currentTarget: input }) => {
        const errors = { ...errors };
        setResetMessage("");
        resetpassword[input.name] = input.value;
        if (input.name === "re_password") {
          const password = resetpassword.new_password;
            if (password !== input.value) {
              const errorMessage = "The passwords you’ve entered does not match.";
              errors[input.name] = errorMessage;
              setErrors(errors);
            } else {
              delete errors[input.name];
              setErrors(errors);
            }
            resetpassword[input.name] = input.value;
            setResetpassword(resetpassword);
            setErrors(errors);
          } else {
            const errorMessage = validateResetForm(input);
            if (errorMessage) errors[input.name] = errorMessage;
            else delete errors[input.name]; 
            setErrors(errors);
          } 
          setResetpassword(resetpassword);
    };

    
    const doSubmit = async () => {
      const errors = { ...errors };
      setResetSubmitStatus(true);
      let result = Joi.validate(resetpassword, resetSchema);
      if (result.error) {
        let path = result.error.details[0].path[0];
        let errormessage = result.error.details[0].message;
        errors[path] = errormessage;
       setErrors(errors);
       setResetSubmitStatus(false);
      } else {
        try {
          const response = await authService.resetPassword(
            resetpassword,
            id
          );
          if (response) {
              resetpassword.new_password= "";
              resetpassword.re_password = "";
              setResetpassword(resetpassword);
              setResetMessage(response.data.message);
              setResetSubmitStatus(false)
              setResetType(response.data.status);
          }
        } catch (err) {
          console.log(err);
        }
      }
    };
    const validateResetForm = ({ name, value }) => {
        const obj = { [name]: value };
        const RSchema = { [name]: resetSchema[name] };
        const { error } = Joi.validate(obj, RSchema);
        return error ? error.details[0].message : null;
    };

    const Btnenable = () => {
            const { error } = Joi.validate(resetpassword, resetSchema);
            console.log
            if (!error) {
              return null;
            }
            const errors = {};
            for (let item of error.details) errors[item.path[0]] = item.message;
            return errors;
    };

    useEffect(() => {
            AOS.init();
        },[])
    
    return(
        <>
        <Header />
        <Banner />
        <FormSection errors={errors} doSubmit={doSubmit}  resetSchema={resetSchema} handleChange={handleChange} validateResetForm={validateResetForm} Btnenable={Btnenable} resetpassword={resetpassword} reset_message={reset_message} reset_message_type={reset_message_type} reset_submit_status={reset_submit_status}/>
        <Footer />
        </>
    );
}
export default ResetPassword;