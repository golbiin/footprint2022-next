import React, { useEffect } from 'react'
import AOS from "aos";
import "../../node_modules/aos/dist/aos.css";
import styles from '../../styles/resetpassword.module.scss'
function Banner() {
    useEffect(() => {
        AOS.init();
    },[])
    return (
        <div className={styles.banner_section}>
            <div className="container">
                <div >
                    <h1>Reset Password</h1>
                </div>
            </div>
        </div>
        
         
    )
}
export default Banner