import React, { useEffect } from 'react'
import AOS from "aos";
import "../../node_modules/aos/dist/aos.css";
import styles from '../../styles/resetpassword.module.scss'
import SubmitButton from '../../components/submit-button';
import ValidationError from '../../components/common/validation-error';
import Link from 'next/link'
function FormSection(props) {

    const Btnenable=()=>{
        return null
    }
    
    useEffect(() => {
        AOS.init();
    },[])
    return (
        <div className={styles.form_section}>
            <div className="container">
                <div className="form_container">
                    <h3>RESET YOUR PASSWORD</h3>
                    <div className='response_container'>
                          <div className={props.reset_message_type?props.reset_message_type==1?"sucsess":"error":"error"}>
                             <span>{props.reset_message? props.reset_message:""} {props.reset_message_type?props.reset_message_type==1?<Link href='/login'>login</Link> : '':"" }</span>
                          </div>
                    </div>
                       <div className={styles.form}> 
                           <div className={styles.form_group}>
                                <label className={styles.form_label} >New Password</label>
                               <input type="password" name="new_password" className={styles.form_control} onChange={props.handleChange} value={props.resetpassword?props.resetpassword.new_password:""}/>
                               {props.errors?props.errors.new_password?<ValidationError message={props.errors.new_password} />:null:""}
                           </div>
                           <div className={styles.form_group}>
                               <label className={styles.form_label}>Re-type Password</label>
                               <input type="password" name="re_password" className={styles.form_control} onChange={props.handleChange} value={props.resetpassword?props.resetpassword.re_password:""}/>
                               {props.errors?props.errors.re_password?<ValidationError message={props.errors.re_password} />:null:""}
                           </div>
                           <div className={styles.form_group}>
                                <SubmitButton btn_label="Submit" className="fp_green_btn" submit={props.doSubmit} submit_status={props.reset_submit_status?props.reset_submit_status:false} animation="border" variant="dark" size="sm" Btnenable={Btnenable} />
                            </div>
                    </div>
                 </div>
            </div>
        </div>  
    )
}
export default FormSection