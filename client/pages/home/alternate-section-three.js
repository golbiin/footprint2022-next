import React, { useEffect } from 'react'
import Image from 'next/image'
import styles from '../../styles/Home.module.scss'
import AOS from "aos";
import Link from 'next/link'
function AlternateSection3() {
    useEffect(() => {
        AOS.init();
    },[])
    return (
        <div className="container">
        <div className={styles.alternate_section+ " fp_content_block"}>
         <div className="row last" data-aos="zoom-in">
            <div className={styles.col_6+" col-md-6 "+styles.animate_section}>
                <img src="/images/home-3.png" className={styles.home_3_img} width={509} height={509} alt="business-developer" loading="eager"/>
                <div className={styles.animate_block+" "+styles.home_gif_animation}>
                 <div className={styles.animate_me+ " " +styles.gif_animate_bottom_sec4 }><img src="/images/aniamte/sec3-animate1.png" width={318} height={217} alt="business-developer" className='common_next_img_updated' loading="eager" /></div>
                </div>
            </div>
            <div className="col-md-6">
                <div className={styles.fp_call_in}>
                <h2>Stand out amongst Local Competition</h2>
                    <ul>
                        <li>Build and Refine your Digital Footprint in order to increase sales.</li>
                        <li>Increase customer call rate, booking and buying!</li>
                        <li>Outrank your competitors in search results</li>
                        <li>Reign supreme!</li>
                    </ul>
                    <Link href="/request-sample" ><a href="" className="fp_green_btn">Get a Sample</a></Link>
                </div> 
            </div>
         
         </div>
         </div>
        </div>
    )
}
export default AlternateSection3