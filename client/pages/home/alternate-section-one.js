import React, { useEffect } from 'react'
import Image from 'next/image'
import styles from '../../styles/Home.module.scss'
import AOS from "aos";
import Link from 'next/link'

function AlternateSection1() {
    useEffect(() => {
        AOS.init();
    },[])
    return (
        <div className="container">
        <div className={styles.alternate_section+ " fp_content_block"} data-aos="zoom-in">
         <div className="row first">
            <div className={styles.col_6+" col-md-6 "+styles.animate_section }>
            <img src="/images/home-1.png" width={459} height={459} alt="business-developer" loading="eager" className='common_next_img_updated' />
            <div className={styles.animate_block+" "+styles.home_gif_animation}>
                <div className={styles.animate_me+ " " +styles.gif_animate_top } data-aos="slide-up"><img src="/images/aniamte/sec1-animate1.png" width={294} height={81} className='common_next_img_updated' alt="business-developer" loading="eager" /></div>
                <div className={styles.animate_me+ " " +styles.gif_animate_bottom } data-aos="slide-up"><img src="/images/aniamte/sec1-animate2.png" width={241} height={81} className='common_next_img_updated' alt="business-developer" loading="eager" /></div>
            </div>
            </div>
            <div className={styles.col_6+" col-md-6"}>
                <h2>When your business needs a developer, you can rely on Footprint.io.</h2>
                <p>Footprint started as a Web Development Shop. So when you hire Footprint, you&apos;re getting over 80 years + of Website Development experience, that&apos;s guaranteed to pack a punch.</p>
                <p> Learn how your business could benefit by utilizing our Web Development Experience and enhance your digital footprint.</p>        
                <Link href="/about-us" ><a className="fp_green_btn">Learn More</a></Link>

            </div>
          
         </div>
         </div>
        </div>
    )
}
export default AlternateSection1