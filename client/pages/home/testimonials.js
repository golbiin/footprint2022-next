import { useEffect, useState } from 'react';
import Image from 'next/image'
import styles from '../../styles/Home.module.scss'
import Marquee from "react-fast-marquee";
import * as fetchSiteData from "../../services/fetchSiteData";
import Spinner from "react-bootstrap/Spinner";

function Testimonials() {
    const [loadImg, setLoadImg] = useState(false);
    const [loader, setLoader] = useState(true);
    const [reviewsFirst, setReviewsFirst] = useState([]);
    const [reviewsSecond, setReviewsSecond] = useState([]);
    useEffect(() => {
        getReviews();    
           
      },[1]);

      const getReviews = async() => {
        const response = await fetchSiteData.getReviews();
        console.log("response", response)
        if (response && response.data.status === 1) {
          setLoader(false);
          let reviewsLoad = [];
          if(response.data.data.length > 0){
            reviewsLoad = response.data.data;
            const chunkSize = (response.data.data.length / 2).toFixed();
            const groups = reviewsLoad.map((e, i) => { 
                return i % chunkSize === 0 ? reviewsLoad.slice(i, i + chunkSize) : null; 
           }).filter(e => { return e; });
           setReviewsFirst(groups[0])
           setReviewsSecond(groups[1])
           setLoadImg(true);
          /*  setTimeout(()=>{
                setLoadImg(true);
            }, 3000);  */
        }
            
        } else {
          setLoader(false)
        }
      }

                 

      console.log({reviewsFirst, reviewsSecond})
    return ( 
        <div className={styles.testimonial_section}>
                <div className='container'>
                <h2>Hear what 400+ clients have to say...</h2>
                </div> 
                {
                    loader ? 
                    <div className="spinner">
                        <Spinner animation="grow" variant="success" />  
                            <span>Connecting...</span>
                    </div>
                    :<>
                <Marquee pauseOnHover={false} speed={30} direction="left">
                <div className={styles.testimonial_box_row}>
                     {reviewsFirst.length > 0 ? 
                     reviewsFirst.map((review, index) => {
                         var rating = parseInt(review.rating);
                         return(
                            <div className={styles.col_3} key={review._id}>
                            <div className={styles.top_row}>
                            {loadImg?
                                <img src={review.user_image === "" ? "/images/user.png" :  review.user_image}  className={styles.user_img} width={66} height={66} alt="profile" loading="eager" />
                               : null }
                                <div className={styles.testimonial_rating}>
                                    <h5>{review.user_name}</h5>
                                    <div className={styles.rating_star}>
                                       {
                                           (rating === 3)?
                                            <>
                                           <span className={styles.full}></span>
                                           <span className={styles.full}></span>
                                           <span className={styles.full}></span>
                                           </>
                                           :(rating === 4)? <>
                                           <span className={styles.full}></span>
                                           <span className={styles.full}></span>
                                           <span className={styles.full}></span>
                                           <span className={styles.full}></span>
                                           </>
                                           : <>
                                           <span className={styles.full}></span>
                                           <span className={styles.full}></span>
                                           <span className={styles.full}></span>
                                           <span className={styles.full}></span>
                                           <span className={styles.full}></span>
                                           </>
                                       }
                                        <span>{rating}.0</span> 
                                    </div>
                                </div> 
                            </div>
                            <div className={styles.bottom_row}>
                                <p>{review.testimony.substr(0, 150)}{review.testimony.length > 150? '...' : null}</p>
                            </div>
                          </div>
                         )
                       
                     })
                     :
                     null
                    }
                 
                </div>
                </Marquee>
                <Marquee pauseOnHover={false} speed={30} direction="right">
                <div className={styles.testimonial_box_row}>
                {reviewsSecond.length > 0 ? 
                     reviewsSecond.map((review, index) => {
                         var rating = parseInt(review.rating);
                         return(
                            <div className={styles.col_3} key={review._id}>
                            <div className={styles.top_row}>
                            {loadImg?
                                <img src={review.user_image === "" ? "/images/user.png" :  review.user_image}  className={styles.user_img} width={66} height={66} alt="profile" loading="eager" />
                               : null }
                               <div className={styles.testimonial_rating}>
                                    <h5>{review.user_name}</h5>
                                    <div className={styles.rating_star}>
                                       {
                                           (rating === 3)?
                                            <>
                                           <span className={styles.full}></span>
                                           <span className={styles.full}></span>
                                           <span className={styles.full}></span>
                                           </>
                                           :(rating === 4)? <>
                                           <span className={styles.full}></span>
                                           <span className={styles.full}></span>
                                           <span className={styles.full}></span>
                                           <span className={styles.full}></span>
                                           </>
                                           : <>
                                           <span className={styles.full}></span>
                                           <span className={styles.full}></span>
                                           <span className={styles.full}></span>
                                           <span className={styles.full}></span>
                                           <span className={styles.full}></span>
                                           </>
                                       }
                                        <span>{rating}.0</span> 
                                    </div>
                                </div> 
                            </div>
                            <div className={styles.bottom_row}>
                                <p>{review.testimony.substr(0, 150)}{review.testimony.length > 150? '...' : null}</p>
                            </div>
                          </div>
                         )
                       
                     })
                     :
                     null
                    }
                </div>
                </Marquee>
                </>
                }
                {/* <a className='fp_green_btn' href='#'>Get Your Free Sample</a> */}
        </div>
    )
}
export default Testimonials 