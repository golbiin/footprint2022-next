import React, { useEffect,useState } from 'react'
import styles from '../../styles/Home.module.scss'
import AOS from "aos";
import Joi from "joi-browser";
import "../../node_modules/aos/dist/aos.css";
import ValidationError from '../../components/common/validation-error';
import { useRouter } from 'next/router';
function Banner() {
    
    const router = useRouter()
    const [UserData,setUserData]=useState({email:"",project_name:""});
    const [errors,setErrors]=useState([]);
    useEffect(() => {
        AOS.init();
    },[])

    const workChange = async ({ currentTarget: input }) => {
      const errors = { ...errors };
      const errorMessage = validateProperty(input);
      console.log(input.name,input.value);
      UserData[input.name] = input.value;
      if (errorMessage) errors[input.name] = errorMessage;
      else delete errors[input.name];
      UserData[input.name] = input.value;
      setUserData(UserData);
      setErrors(errors);
    };

    const schema = {
        email: Joi.string()
          .required()
          .email()
          .error(() => {
            return {
              message:
                "Please enter a properly formatted email."
            };
          }),
          project_name: Joi.optional().allow("")
    };
    
    const validateProperty = ({ name, value }) => {
        const obj = { [name]: value };
        const lschema = { [name]: schema[name] };
        const { error } = Joi.validate(obj, lschema);
        return error ? error.details[0].message : null;
    };

    const submitForm = ()=>{
        const errors = { ...errors };
        let result = Joi.validate(UserData,schema);
    
        if (result.error) {
          let path = result.error.details[0].path[0];
          let errormessage = result.error.details[0].message;
          errors[path] = errormessage;
          setErrors(errors);
        } else { 
            router.push(`/request-sample?email=${UserData.email}&&project_name=${UserData.project_name}`,'request-sample')
        }
    }
    
    return (
        <div className="container">
         <div className={styles.banner_section}>
             <h1 data-aos="fade-up">Create your Digital Footprint.</h1>
             <h5 data-aos="fade-up">We&apos;ll run through a full Website Analysis and provide you with a complimentary Homepage Design.</h5>
             <div className={styles.banner_form} data-aos="fade-up">
                 <div className={styles.form_group}>
                     <input type="text" name="email" className={styles.form_control} placeholder="Your Email" onChange={workChange}/>
                     {errors.email?<div className={styles.form_validate}><ValidationError message={errors.email} /></div>:null}
                     </div>
                 <div className={styles.form_group}>
                     <input type="text" name="project_name" className={styles.form_control} placeholder="Project Name" onChange={workChange}/>
                     
                 </div>
                <button className="fp_green_btn banner_btn" onClick={submitForm} >Get Your Sample </button>
             </div>
         </div>
        </div>
    )
}
export default Banner