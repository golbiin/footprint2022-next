import React, { useEffect ,useState} from 'react'
import Footer from '../../components/common/footer'
import Header from '../../components/common/header'
import dynamic from 'next/dynamic';
const AlternateSection1 = dynamic(() => import('./alternate-section-one'))

//import AlternateSection1 from './alternate-section-one'
import AlternateSection3 from './alternate-section-three'
import AlternateSection2 from './alternate_section-two'
import Banner from './banner'
import ClientLogo from './client-logo'
//import IconBoxSection from './icon-box-section'
import OurPortfolio from './our-portfolio'
import Testimonials from './testimonials'

function Home() {
    const seoData = {
        title: "Website Development, Apps, Design and Marketing | Footprint.io",
        description: "Hire Website & App Developers, Graphic Designers, and Digital Marketers at affordable rates. You have the vision. We can make it a reality."
    }
    const [loadCheck, SetLoadCheck] = useState(false);
    useEffect(() => {
        console.log("did mounttttttt")
        SetLoadCheck(true);
    },[])
    return (<main>
        {!loadCheck? <div
            className='preload-layout'>
        </div>: null }
        <Header seoData={seoData}/>
        <Banner />
        <ClientLogo />
        <AlternateSection1 />
        <AlternateSection2 />
        <AlternateSection3 />
        <Testimonials />
        <OurPortfolio />
        <Footer />
      </main>)
  }
  export default Home