import React, { useEffect } from 'react'
import Image from 'next/image'
import styles from '../../styles/Home.module.scss'
import AOS from "aos";
import Link from 'next/link'
import "react-responsive-carousel/lib/styles/carousel.min.css"; // requires a loader
import { Carousel } from 'react-responsive-carousel';
function OurPortfolio() {
  useEffect(() => {
    AOS.init();
},[])
    return ( 
        <div className={styles.our_portfolio_section}>
                <div className='container' data-aos="zoom-in" >
                <h2>Our Portfolio</h2>
                </div> 
                <div className={styles.portfolio_mobile} >
                <Carousel showThumbs={false} showStatus={false}>
                <div className={styles.eachItem}>
                <img
                    alt="rv"
                    src="/images/portfolio/furrion.jpg"
                    layout="responsive"
                    loading="eager"
                    
                  />  
                </div>
                <div className={styles.eachItem}>
                <img src="/images/portfolio/gunnar.jpg" layout="responsive"  alt="gunnar" loading="eager" />
                </div>
                <div className={styles.eachItem}>
                <img src="/images/portfolio/sezzle.jpg" layout="responsive"   alt="sezzle" loading="eager"/>
                </div>
                <div className={styles.eachItem}>
                <img src="/images/portfolio/cny.jpg" layout="responsive"  alt="sezzle" loading="eager"/>
                </div>
                <div className={styles.eachItem}>
                <img src="/images/portfolio/flower.jpg" layout="responsive"   alt="sezzle" loading="eager"/>
                </div>
                 
            </Carousel>
                  </div>
                <div className={styles.portfolio_box_row} >
                  
                  <div className={styles.col_3}>
          
                 
                   <a> 
                   <img
                    className='common_next_img_updated'
                    alt="rv"
                    src="/images/portfolio/furrion.jpg"
                    layout="responsive"
                    loading="eager"
                    width={348}
                    height={495}
                  />   
                  </a>  
                  </div>
                  <div className={styles.col_3}>
                  <a> <img src="/images/portfolio/gunnar.jpg" className='common_next_img_updated' layout="responsive" width={348} height={495} alt="gunnar" loading="eager" /></a>
                  </div>
                  <div className={styles.col_3}>
                  <a> <img src="/images/portfolio/sezzle.jpg" className='common_next_img_updated' layout="responsive" width={348} height={495} alt="sezzle" loading="eager"/></a>
                  </div>
                  <div className={styles.col_3}>
                  <a> <img src="/images/portfolio/cny.jpg" className='common_next_img_updated' layout="responsive" width={348} height={495} alt="smartengage" loading="eager" /></a>
                  </div>
                  <div className={styles.col_3}>
                  <a> <img src="/images/portfolio/flower.jpg" className='common_next_img_updated' layout="responsive" width={348} height={495} alt="evolv" loading="eager" /></a>
                  </div>
                </div>
                <div className={styles.portfolio_btn}>
                  <Link href="/portfolio"><a className='fp_green_btn portfolio_contact'>View Portfolio and Link</a></Link>
                  <Link href="/request-sample"><a className='fp_green_btn'>Get Your Free Sample</a></Link>
                </div>
              

        </div>
    )
}
export default OurPortfolio 