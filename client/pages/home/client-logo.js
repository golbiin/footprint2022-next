import React, { useEffect } from 'react'
import Image from 'next/image'
import styles from '../../styles/Home.module.scss'
import AOS from "aos";
function ClientLogo() {
    useEffect(() => {
        AOS.init();
    },[])
    return (
        <div className={styles.client_logo_section}>
         <div className="container">
         <div className={styles.client_logo}>
             <h3>We&apos;ve developed Websites and Apps using the Worlds Most Popular Platforms.</h3>
             <div className={styles.logos} data-aos="zoom-in">
                <img src="/images/logos/1.png"   width={212} height={44} alt="stripe" loading="eager" layout="fixed" />
                <img src="/images/logos/2.png"  width={53} height={63} alt="gusto" loading="eager" layout="fixed"/>
                <img src="/images/logos/3.png"  width={83} height={71} alt="square" loading="eager" layout="fixed" />
                <img src="/images/logos/4.png"  width={136} height={32} alt="bigcommerce" loading="eager" layout="fixed"/>
                <img src="/images/logos/5.png"  width={58} height={61} alt="shopify" loading="eager" layout="fixed" />
                <img src="/images/logos/6.png"  width={144} height={71} alt="fresh books" loading="eager" layout="fixed"/>
                <img src="/images/logos/8.png"  width={157} height={69} alt="React Js" loading="eager" layout="fixed"/>
             </div>
         </div>
         </div>
         </div>
    )
}
export default ClientLogo