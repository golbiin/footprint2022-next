import React, { useEffect } from 'react'
import Image from 'next/image'
import styles from '../../styles/Home.module.scss'
import AOS from "aos";
import Link from 'next/link'
function AlternateSection2() {
    useEffect(() => {
        AOS.init();
    },[])
    return (
        <div className="container">
        <div className={styles.alternate_section+ " fp_content_block"}>
         <div className="row two" data-aos="zoom-in">
            <div className="col-md-4">
                    <h2>Customers are trying to find you.</h2>
                    <p>Your digital footprint is what matters. 80% of people searching for a business will rely on Google and other Search Engines to find your business. Of that 80%, more than half count on your business&apos; reviews before they contact you.</p>        
                    <Link href="/contact-us" ><a href="" className="fp_green_btn">Get In Touch</a></Link>
            </div>
            <div className={styles.col_8+ " col-md-8 "+styles.animate_section }  data-aos="zoom-in">
                <img src="/images/home-2.png" className='common_next_img_updated' width={511} height={510} alt="customers-developer" loading="eager" />
                <div className={styles.animate_block+" "+styles.home_gif_animation}>
                <div className={styles.animate_me+ " " +styles.gif_animate_bottom_sec2 }><img src="/images/aniamte/sec2-animate1.png" width={224} height={81} alt="business-developer" className='common_next_img_updated' loading="eager" /></div>
            </div>
            </div>
           
         </div>
         </div>
        </div>
    )
}
export default AlternateSection2