import React, { useEffect } from 'react'
import Image from 'next/image'
import styles from '../../styles/Home.module.scss'
import AOS from "aos";
function IconBoxSection() {
    useEffect(() => {
        AOS.init();
    },[])
    return ( 
        <div className="container" data-aos="zoom-in">
        <div className={styles.icon_box_section}>
        <h2>Sed in libero ut nibh <br/>placerat accum.</h2>
         <div className={styles.icon_box_row}>
           <div className={styles.col_4}>
               <Image src="/images/icon1.png" width={51} height={51} alt="icon1" loading="eager" layout="fixed"/>
               <h4>Donec elit libero, soda</h4>
               <p>Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos hymenaeos.</p>
           </div>
           <div className={styles.col_4}>
               <Image src="/images/icon2.png" width={51} height={51} alt="icon1" loading="eager" layout="fixed"/>
               <h4>Donec elit libero, soda</h4>
               <p>Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos hymenaeos.</p>
           </div>
           <div className={styles.col_4}>
               <Image src="/images/icon3.png" width={51} height={51} alt="icon1" loading="eager" layout="fixed"/>
               <h4>Donec elit libero, soda</h4>
               <p>Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos hymenaeos.</p>
           </div>
           <div className={styles.col_4}>
               <Image src="/images/icon4.png" width={51} height={51} alt="icon1"/>
               <h4>Donec elit libero, soda</h4>
               <p>Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos hymenaeos.</p>
           </div>
           <div className={styles.col_4}>
               <Image src="/images/icon5.png" width={51} height={51} alt="icon1"/>
               <h4>Donec elit libero, soda</h4>
               <p>Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos hymenaeos.</p>
           </div>
           <div className={styles.col_4}>
              <Image src="/images/icon6.png" width={51} height={51} alt="icon1"/>
              <h4>Donec elit libero, soda</h4>
              <p>Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos hymenaeos.</p>
           </div>
         
         </div>
         </div>
        </div>
    )
}
export default IconBoxSection 