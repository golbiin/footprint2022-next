import '../styles/globals.scss'
import 'bootstrap/dist/css/bootstrap.css';
import "../styles/aos/dist/aos.css";
function MyApp({ Component, pageProps }) {
  return <Component {...pageProps} />
}

export default MyApp
