import React, { useEffect } from 'react'
import Header from '../../components/common/header';
import Footer from '../../components/common/footer';
import ContactForm from './contact-form';


function ContactUs() {
    const seoData = {
        title: "Contact | Footprint.io",
        description: "Get immediate support. We’d love to hear from you. An agent is always available for issues, concerns, or questions."
    }
    return (
        <div>
            <Header seoData={seoData}/>
            <ContactForm />
            <Footer />
        </div>
    )
}
export default ContactUs