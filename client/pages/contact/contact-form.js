import React, { useEffect,useState } from 'react'
import styles from '../../styles/Contact.module.scss'
import { useRouter } from "next/router"
import AOS from "aos";
import Joi, { errors } from "joi-browser";
import ValidationError from '../../components/common/validation-error';
import SubmitButton from '../../components/submit-button';
import publicIp from 'public-ip';
import * as userService from "../../services/userUservice";

function ContactForm(props) {
    const router = useRouter();
    const [contatctData,setContactData]=useState({name:"",email:"",phone:"",message:"",topic:""});
    const [errors,setErrors]=useState([]);
    const [submit_status,setSumbit]=useState(false);
    const [response_type,setResponseType]=useState("");
    const [response_message,setResponseMessage]=useState("");
    useEffect(() => {
      const loadContact = {...contatctData};
        AOS.init();
        if(window.sessionStorage.getItem("contact-topic") !== null){
          loadContact.topic = window.sessionStorage.getItem("contact-topic");
          setContactData(loadContact);       
        }  
    },[props])
     const schema = {
        name: Joi.string()
          .required()
          .error(() => {
            return {
              message:
                "Please be sure you’ve filled in the Name."
            };
          }),
        email: Joi.string()
          .required()
          .email()
          .error(() => {
            return {
              message:
                "Please enter a properly formatted email."
            };
          }),
        phone: Joi.number()
          .required()
          .error(() => {
            return {
              message:
                "Please be sure you’ve filled in the Phone Number."
            };
          }),
        message: Joi.string()
          .required()
          .error(() => {
            return {
              message:
                "Please be sure you’ve filled in the Message."
            };
          }),
        topic: Joi.optional().label("Topic")
      };
    
    const handleChange = async ({ currentTarget: input }) => {
        setResponseMessage("");
        const errors = { ...errors };
        const errorMessage = validateProperty(input);
        if (errorMessage) errors[input.name] = errorMessage;
        else delete errors[input.name];
        contatctData[input.name] = input.value;
        setContactData(contatctData);
        setErrors(errors);
      };
    
    const validateProperty = ({ name, value }) => {
        const obj = { [name]: value };
        const lschema = { [name]: schema[name] };
        const { error } = Joi.validate(obj, lschema);
        return error ? error.details[0].message : null;
      };
    
    const doSubmit = async () => {
        const errors = { ...errors }; 
        const { error } = Joi.validate(contatctData, schema);
        if (error) {
          let path = error.details[0].path[0];
          let errormessage = error.details[0].message;
          errors[path] = errormessage;
          setErrors(errors);
        } else {
          setSumbit(true);
           
            try {
              const type = "contact";
              const page = "contact";
              let ip = await publicIp.v4();
              if (ip) {
                const response = await userService.contact(
                  contatctData,
                  type,
                  page,
                  ip
                );
                 if (Object.keys(response).length > 0) {
                    sessionStorage.removeItem("contact-topic");
                    contatctData.topic = "";
                    contatctData.name = "";
                    contatctData.email = "";
                    contatctData.phone = "";
                    contatctData.message = "";
                  setContactData(contatctData);
                  console.log(contatctData);
                  setResponseType( response.data.status);
                  setResponseMessage(response.data.message);
                  setSumbit(false);
                  window.scrollTo({ top: 0, left: 0, behavior: "smooth" });
                }
              }
            } catch (err) {
              console.log(err);
            }
        
        }
      };

    const checkBtnenable = () => {
      return null;
        const { error } = Joi.validate(contatctData, schema);
        console.log(error,contatctData)
        if (!error) {
          return null;
        }
        const errors = {};
        for (let item of error.details) errors[item.path[0]] = item.message;
        return errors;
    };
      
    return(
        <div className="container">
            <div className={styles.form_section} data-aos="zoom-in">
            <div className={styles.form}>
                    <h1>Get In Touch</h1>
                    <div className={styles.res_contain+' response_container'}>
                          <div className={response_type==1?"sucsess":"error"}>
                             <span className={styles.form_response}>{response_message} </span>
                          </div>
                    </div>
                    <label>
                      <b>Topic</b> : {contatctData.topic}
                    </label>
                    <br />
                    
                    <div className={styles.form_group}>
                    <label>Name<span>*</span></label>
                    <input
                      type="text"
                      className={styles.form_control}
                      placeholder="Name"
                      name="name"
                      value={contatctData.name}
                      onChange={handleChange}
                    />
                    {errors.name?<ValidationError message={errors.name} />:null}
                    </div>

                    <div className={styles.form_group}>
                    <label>Email<span>*</span></label>
                    <input
                      type="email"
                      className={styles.form_control}
                      placeholder="Email"
                      name="email"
                      value={contatctData.email}
                      onChange={handleChange}
                    />
                    {errors.email?<ValidationError message={errors.email} />:null}
                    </div>

                    <div className={styles.form_group}>
                    <label>Phone<span>*</span></label>
                    <input
                      type="text"
                      className={styles.form_control}
                      placeholder="Phone"
                      name="phone"
                      value={contatctData.phone}
                      onChange={handleChange}
                    />
                    {errors.phone?<ValidationError message={errors.phone} />:null}
                    </div>

                    <div className={styles.form_group}>
                    <label>Message<span>*</span></label>
                    <textarea
                      type="text"
                      className={styles.form_control}
                      placeholder="What do you need help with?"
                      name="message"
                      style={{ minHeight: "100px" }}
                      value={contatctData.message}
                      onChange={handleChange}
                    />
                     {errors.message?<ValidationError message={errors.message} />:null}
                    </div>
                    <SubmitButton btn_label="Submit" className="fp_green_btn" submit_status={submit_status} animation="border" variant="dark" size="sm" submit={doSubmit} Btnenable={checkBtnenable}/>
                  </div>
            </div>
        </div>
    )

}
export default ContactForm