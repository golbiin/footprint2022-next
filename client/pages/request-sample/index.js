import React, { useEffect } from 'react'
import Header from '../../components/common/header';
import Footer from '../../components/common/footer';
import FormOne from './formOne';

function Contact() {
    const seoData = {
        title: "Request a Sample Design | Footprint.io",
        description: "Visualize what your website or app would look like. Completely free with no obligation. Fill in the form below to start the process."
    }
    return (
        <div>
            <Header seoData={seoData}/>
            <FormOne />
        </div>
    )
}
export default Contact