import React, { Component } from "react";
import Spinner from "react-bootstrap/Spinner";
import styles from '../../styles/RequestForm.module.scss'
class uploadMultiple extends React.Component {
  constructor(props) {
    super(props);
    this.hiddenFileInput = React.createRef();
  }
  dragOver = e => {
    e.preventDefault();
  };

  dragEnter = e => {
    e.preventDefault();
  };

  dragLeave = e => {
    e.preventDefault();
  };

  fileDrop = e => {
    e.preventDefault();
    const files = e.dataTransfer.files;
    let upload_data = [];
    for (var i = 0; i < files.length; i++) {
      // this.checkMimeType(files[i]);
      const fsize = files[i].size;
      const size = Math.round(fsize / 1024);
      const types = [
        "image/png",
        "image/jpeg",
        "image/gif",
        "application/pdf",
        "text/plain"
      ];
      if (!types.includes(files[i].type)) {
        let error = files[i].type + " is not a supported format\n";
        this.props.setUploadError(error);
        return false;
      } else if (size > 9216) {
        let error = "File size exeeded\n";
        this.props.setUploadError(error);
        return false;
      } else {
        upload_data.push(files[i]);
      }
    }
    this.props.onMultipleUpload(upload_data);
  };

  onChangeHandler = event => {
    let upload_data = [];
    let files = event.target.files;
    for (var i = 0; i < files.length; i++) {
      // this.checkMimeType(files[i]);
      files[i].fileType = files[i].type.includes("image") ? "image" : "attach";
      upload_data.push(files[i]);
    }
    if(files.length)
      this.props.onMultipleUpload(upload_data);
  };

  handleClick = event => {
    this.hiddenFileInput.current.click();
  };
  deleteFile = index => {
    this.props.onDelete(index);
  };
  render() {
    let { data } = this.props;
    console.log("PROPS", this.props.upload_status);
    return (
      <React.Fragment>
        <div
          className={styles.drag_upload}
          onDragOver={this.dragOver}
          onDragEnter={this.dragEnter}
          onDragLeave={this.dragLeave}
          onDrop={this.fileDrop}
        >
          <div className={styles.upload_btn}>
            <div
              className={styles.upload_button}
              //onClick={this.handleClick}
            >
              <label htmlFor="file-upload" className={styles.upload_button_label}>
                + Upload files
              </label>
              <input
                ref={this.hiddenFileInput}
                id="file-upload"
                type="file"
                className={styles.form_control + " form-control " + styles.file_upload}
                onChange={this.onChangeHandler}
                multiple
                style={{ display: "none" }}
              />
            </div>
            <p>
              Drag & drop any images or documents that might be helpful in
              explaining your brief here (Max file size: 25 MB)
            </p>
          </div>
          {this.props.uploadError != "" ? (
            <span className="red-validate-error">{this.props.uploadError}</span>
          ) : (
            ""
          )}
          <div className={styles.file_list}>
            { data !== undefined && data.project_file
              ? data.project_file.map((eachFile, index) => (
                  <div
                    className={styles.each_file_small}
                    style={
                      eachFile.fileType === "image"
                        ? {}
                        : { background: "#FFF" }
                    }
                    key={index}
                  >
                    {eachFile.fileType === "image" ? (
                      eachFile.name
                    ) : (
                      <React.Fragment>
                        <div className={styles.attach_file}>{eachFile.name}</div>
                        <div
                          className={styles.close}
                          onClick={() => this.deleteFile(index)}
                        >
                           
                        </div>
                      </React.Fragment>
                    )}
                  </div>
                ))
              : null}
            {this.props.upload_status ? (
              this.props.upload_status == true ? (
                <Spinner animation="grow" variant="success"></Spinner>
              ) : (
                ""
              )
            ) : (
              ""
            )}
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default uploadMultiple;
