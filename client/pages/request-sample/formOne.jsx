import React, { Component } from "react";
import Joi from "joi-browser";
import UploadMultiple from "./uploadMultiple";
import * as uploadService from "../../services/uploadService";
import * as authServices from "../../services/authService";
import * as userServices from "../../services/userUservice";
import Multiselect from "multiselect-react-dropdown";
import styles from '../../styles/RequestForm.module.scss'
import Link from "next/link";
import { withRouter } from 'next/router'


class FormOne extends Component {
  constructor(props) {
    super(props);
    this.hiddenFileInput = React.createRef();
  }
  state = {
    step: 1,
    upload_status: false,
    uploadError: "",
    data: {
      project_name: "",
      project_details: "",
      project_file: [],
      project_skill: [],
      project_type: [],
      project_pay: ""
    },
    remainingCount: 4000,
    image: "",
    errors: "",
    submit_status: false,
    response_message: "",
    response_type: "",
    enter: false,
    levelcat: false,
    leveltype: false,
    levelpay: false,
    levelestimate: false,
    clickClass: "",
    selectedValue: [],
    developerType: [
      { name: "HTML", id: 6 },
      { name: "PHP", id: 4 },
      { name: "mySQL", id: 50 },
      { name: "jQuery", id: 51 },
      { name: "ReactJS", id: 7 },
      { name: "Angular", id: 8 },
      { name: "Node.js", id: 52 },
      { name: "MongoDB", id: 53 },
      { name: "JSON", id: 54 },
      { name: "Wordpress", id: 9 },
      { name: "Shopify", id: 55 },
      { name: "Joomla", id: 56 },
      { name: "Android", id: 11 },
      { name: "iOS", id: 10 },
      { name: "Hybrid", id: 12 },
      { name: "Logo", id: 13 },
      { name: "Broucher", id: 14 },
      { name: "Flyer", id: 15 },
      { name: "Business cards", id: 16 }
    ],
    skillOption: [
      { name: "I’m not sure", id: 0 },
      { name: "CMS", id: 1 },
      { name: "Custom HTML", id: 2 },
      { name: "Ecommerce", id: 3 },
      { name: "PHP", id: 4 },
      { name: "Javascript", id: 5 },
      { name: "HTML", id: 6 },
      { name: "ReactJS", id: 7 },
      { name: "Angular", id: 8 },
      { name: "Wordpress", id: 9 },
      { name: "iOS", id: 10 },
      { name: "Android", id: 11 },
      { name: "Hybrid", id: 12 },
      { name: "Logo", id: 13 },
      { name: "Broucher", id: 14 },
      { name: "Flyer", id: 15 },
      { name: "Business cards", id: 16 },
      { name: "Presentation Slides", id: 17 },
      { name: "mySQL", id: 50 },
      { name: "jQuery", id: 51 },
      { name: "Node.js", id: 52 },
      { name: "MongoDB", id: 53 },
      { name: "JSON", id: 54 },
      { name: "Shopify", id: 55 },
      { name: "Joomla", id: 56 }
    ],
    respMsg: ""
  };

  nextStep = () => {
    this.setState({ step: this.state.step + 1 });
  };
  prevStep = () => {
    this.setState({ step: this.state.step - 1 });
  };

  keydownHandler = e => {
    console.log("this.inputElement", this.inputElement);
    if (e.keyCode === 13 && e.ctrlKey && this.inputElement != null) {
      this.setState({ clickClass: styles.click });
      this.inputElement.click();
      setTimeout(() => this.setState({ clickClass: "" }), 100);
    }
  };

  componentWillUnmount() {
    document.removeEventListener("keydown", this.keydownHandler);
  }

  componentDidMount() {
    console.log('this.props--- ', this.props.router.query.project_name)
    const data = { ...this.state.data };    
    data["project_name"] = this.props.router.query.project_name !== undefined ? this.props.router.query.project_name : '';
    this.setState({ data });
    document.addEventListener("keydown", this.keydownHandler);
  }

  schema = {
    project_name: Joi.string()
      .required()
      .error(() => {
        return {
          message: "Please be sure you’ve filled in the name."
        };
      }),
    project_details: Joi.optional(),
    project_file: Joi.optional(),
    project_skill: Joi.optional(),
    project_type: Joi.optional(),
    project_pay: Joi.optional()
  };

  handleChange = async ({ currentTarget: input }) => {
    this.setState({ message: "" });
    const errors = { ...this.state.errors };
    const data = { ...this.state.data };
    let remainingCount = this.state.remainingCount;

    if (input.name === "project_details" && input.value.length > 4000)
      return null;
    else if (input.name === "project_details")
      remainingCount = 4000 - input.value.length;

    const errorMessage = this.validateProperty(input);
    if (errorMessage) errors[input.name] = errorMessage;
    else delete errors[input.name];
    data[input.name] = input.value;

    this.setState({ data, errors, remainingCount });
  };

  validateProperty = ({ name, value }) => {
    const obj = { [name]: value };
    const schema = { [name]: this.schema[name] };
    const { error } = Joi.validate(obj, schema);
    return error ? error.details[0].message : null;
  };

  doSubmit = async () => {
    const errors = { ...this.state.errors };
    const data = { ...this.state.data };
    const { error } = Joi.validate(data, this.schema);
    if (error) {
      let path = error.details[0].path[0];
      let errormessage = error.details[0].message;
      errors[path] = errormessage;
      this.setState({ errors });
      console.log(error);
    } else {
      //this.setState({ submit_status: true });
      console.log(767, window.location.pathname);
      /*       if (window.location.pathname === "/landing-v3") {
        this.props.propsHistory.push({
          pathname: "/landing-form",
          state: {
            v3FormData: this.state.data
          }
        });
      } */
      this.setState({ levelcat: true, step: 2 });

      var selectedPageSkills = sessionStorage.getItem("selectedPageSkills");
      var selectedValue = [];
      if (selectedPageSkills !== null && selectedPageSkills.length !== 0) {
        selectedValue = JSON.parse(selectedPageSkills);
        data["project_skill"] = selectedValue;
      }
      this.setState({ data, selectedValue });
    }
  };

  goLevel3 = async () => {
    this.setState({ leveltype: true, step: 3 });
  };

  onSelect = (selectedList, selectedItem) => {
    const data = { ...this.state.data };
    data["project_skill"] = selectedList;
    this.setState({ data });
    sessionStorage.setItem("selectedPageSkills", JSON.stringify(selectedList));
  };

  onRemove = (selectedList, removedItem) => {
    const data = { ...this.state.data };
    data["project_skill"] = selectedList;
    this.setState({ data });
    console.log("selectedList", selectedList);
    sessionStorage.setItem("selectedPageSkills", JSON.stringify(selectedList));
  };

  goTypeChoose = type => {
    const data = { ...this.state.data };

    const index = data["project_type"].indexOf(type);
    if (index > -1) {
      data["project_type"].splice(index, 1);
    } else {
      data["project_type"].push(type);
    }
    console.log("datadata", data);
    this.setState({
      levelpay: true,
      levelestimate: true,
      data
    });
  };

  goPayChoose = pay => {
    const data = { ...this.state.data };

    data["project_pay"] = pay;
    this.setState({
      data,
      levelestimate: true
    });
  };

  HireSubmit = async () => {
    let data = { ...this.state.data };
    console.log("data", data)
    const loggedUserData=authServices.getCurrentUser();

    if (loggedUserData) {

      const response = await userServices.loginSaveProjectData({
        project_data: this.state.data
      });
      console.log(7777, response);
      if (response) {
        this.setState({ respMsg: response.data.message });
        setTimeout(
          () =>
            this.props.router.push({
              pathname: "/dashboard"
            }),
          3000
        );
      } 
    } else {
      
//      window.sessionStorage.setItem("request-sample", {...data});
      window.sessionStorage.setItem("request-sample", JSON.stringify(data));
      this.props.router.push({
        pathname: '/register',
        query: {...data},
    }, 'register');  
      

    }
  };



  deleteFile = index => {
    let data = { ...this.state.data };
    let upload_data = data.project_file;
    upload_data.splice(index, 1);
    this.setState({ upload_data });
  };

  multipleUpload = async files => {
    let data = { ...this.state.data };
    let project_file = data.project_file;
    this.setState({ upload_status: true, uploadError: "" });
    if (Array.isArray(files) && files.length) {
      const response = await uploadService.uploadMultiple(files);
      if (response.data.status === 1) {
        let file_locations = response.data.data.file_location;
        file_locations.map(item => {
          let image_Obj = { location: item.Location, name: item.certname };
          project_file.push(image_Obj);
        });
        this.setState({ data, upload_status: false });
      }
    }
  };

  setUploadError = error => {
    this.setState({ uploadError: error });
  };

  render() {
     const { remainingCount, data, clickClass, step } = this.state;
    const { layout } = this.props;
    const cmsData = this.props.cmsData !== undefined ? this.props.cmsData : {};

    return (
      <div className={styles.landing_form_page}>
      <div className={styles.form_container}>

      <div className="head">
        <h1>You have the vision. We have your dream team to execute it.</h1>
        <p className={styles.sub_title}>
        We’re eager to show you what your Website / App could look like.
        </p>
      </div>
        
      <div className={styles.landing_form} id="form">
        <div className={styles.form_section+ " row "+styles.title}>
          <div className="col-md-12">
            <h2>
            Tell us a bit more about your project.
            </h2>
            <p>
            We’ll ask you a few questions so that we can begin drafting a design concept.
            </p>
          </div>
        </div>
        {this.state.response_message ? (
          <div className={styles.form_section+ " row"}>
            <div className="col-md-12">
              <span
                className={
                  this.state.response_type === 1
                    ? "green-sucess"
                    : "red-error-login"
                }
              >
                {this.state.response_message}
              </span>
            </div>
          </div>
        ) : (
          ""
        )}
        <div
          className={styles.form_section+ " row"}
          style={layout === "fold" && step !== 1 ? { display: "none" } : {}}
        >
          <div className="col-md-12">
            <div className={styles.form_group}>
              <label>Choose a name for your project</label>
              <input
                type="text"
                name="project_name"
                placeholder="Enter your project name"
                className={ styles.form_control+ " form-control" }
                onChange={this.handleChange}
                value={this.state.data.project_name}
              />
              <span
                className={
                  this.state.errors.project_name ? "red-validate-error" : ""
                }
              >
                {this.state.errors.project_name
                  ? this.state.errors.project_name
                  : ""}
              </span>
            </div>

            <div className={styles.form_group}>
              <label>Tell us more about your project</label>

              <textarea
                className={ styles.form_control +" form-control " +styles.textarea}
                placeholder="Describe your project here..."
                name="project_details"
                onChange={this.handleChange}
                value={this.state.data.project_details}
                maxLength="4000"
              />
              <span className={styles.remaining_characters}>
                {remainingCount} characters remaining
              </span>

              <span
                className={
                  this.state.errors.project_details ? "red-validate-error" : ""
                }
              >
                {this.state.errors.project_details
                  ? this.state.errors.project_details
                  : ""}
              </span>
            </div>
            <div className={styles.form_group}>
              <label>What skills are required?</label>
              <Multiselect
                options={this.state.skillOption} // Options to display in the dropdown
                selectedValues={this.state.selectedValue} // Preselected value to persist in dropdown
                onSelect={this.onSelect} // Function will trigger on select event
                onRemove={this.onRemove} // Function will trigger on remove event
                displayValue="name" // Property name to display in the dropdown options
                className={styles.searchWrapper}
                placeholder={
                  data["project_skill"].length
                    ? "Choose More"
                    : "Please select a skill set"
                }
              />
              <span
                className={
                  this.state.errors.project_skill ? "red-validate-error" : ""
                }
              >
                {this.state.errors.project_skill
                  ? this.state.errors.project_skill
                  : ""}
              </span>
              <p> Enter categories that describe your project.</p>
            </div>

            <div className={styles.form_group + " "+ styles.button}>
              {this.state.levelcat === false ? (
                <div className={styles.button_box}>
                  <button
                    className={clickClass + " "+styles.btn}
                    ref={input => (this.inputElement = input)}
                    onClick={this.doSubmit}
                    disabled={this.state.upload_status}
                  >
                    Next →
                  </button>
                  <span className={styles.press_enter}>Press CTRL + ENTER</span>
                </div>
              ) : (
                ""
              )}
            </div>
          </div>
        </div>
        {this.state.levelcat === true ? (
          <div
            className={styles.form_section+ " row"}
            style={layout === "fold" && step !== 2 ? { display: "none" } : {}}
          >
            <div className="col-md-12">
              <div className={styles.form_group}>
                <UploadMultiple
                  onMultipleUpload={this.multipleUpload}
                  data={this.state.data}
                  upload_status={this.state.upload_status}
                  onDelete={this.deleteFile}
                  setUploadError={this.setUploadError}
                  uploadError={this.state.uploadError}
                />
              </div>

              <div className={styles.form_group +" "+styles.button}>
                {this.state.leveltype === false ? (
                  <div className={styles.button_box}>
                    <button
                      className={clickClass + " "+styles.btn}
                      ref={input => (this.inputElement = input)}
                      onClick={this.goLevel3}
                    >
                      Next →
                    </button>
                    <span className={styles.press_enter}>Press CTRL + ENTER</span>
                  </div>
                ) : (
                  ""
                )}
              </div>
            </div>
          </div>
        ) : (
          ""
        )}
        {this.state.leveltype === true ? (
          <div
            className={styles.form_section+ " row"}
            style={layout === "fold" && step !== 3 ? { display: "none" } : {}}
          >
            <div className="col-md-12">
              <div className={styles.form_group_title}>
                {" "}
                <label>How would you like to get it done?</label>
              </div>
            </div>
            <div className="col-md-3">
              <div
                className={
                  data["project_type"].indexOf("website") > -1
                    ? styles.level_inner + " "+styles.level_active
                    : styles.level_inner
                }
                onClick={() => this.goTypeChoose("website")}
              >
                <img
                  src="images/icons/website.png"
                  className="image-fluid"
                />
                <h3>Website</h3>
              </div>
            </div>
            <div className="col-md-3">
              <div
                className={
                  data["project_type"].indexOf("mobile-app") > -1
                    ? styles.level_inner + " "+styles.level_active
                    : styles.level_inner
                }
                onClick={() => this.goTypeChoose("mobile-app")}
              >
                <img
                  src="images/icons/mobile-app.png"
                  className="image-fluid"
                />
                <h3>Mobile Apps</h3>
              </div>
            </div>
            <div className="col-md-3">
              <div
                className={
                  data["project_type"].indexOf("graphic-design") > -1
                    ? styles.level_inner + " "+styles.level_active
                    : styles.level_inner 
                }
                onClick={() => this.goTypeChoose("graphic-design")}
              >
                <img
                  src="images/icons/graphic-design.png"
                  className="image-fluid"
                />
                <h3>Graphic Design</h3>
              </div>
            </div>
            <div className="col-md-3">
              <div
                className={
                  data["project_type"].indexOf("logo-broshures") > -1
                    ? styles.level_inner + " "+styles.level_active
                    : styles.level_inner
                }
                onClick={() => this.goTypeChoose("logo-broshures")}
              >
                <img
                  src="images/icons/logo-brochure.png"
                  className="image-fluid"
                />
                <h3>Logo & Brochure</h3>
              </div>
            </div>

            {this.state.levelpay === true && layout === "fold" ? (
              <div className="col-md-12">
                <div className={styles.form_group +" "+styles.button}>
                  <div className={styles.button_box}>
                    <button
                      className={clickClass + " "+styles.btn}
                      ref={input => (this.inputElement = input)}
                      onClick={this.nextStep}
                    >
                      Next →
                    </button>
                    <span className={styles.press_enter}>Press CTRL + ENTER</span>
                  </div>
                </div>
              </div>
            ) : (
              ""
            )}
          </div>
        ) : (
          ""
        )}

       {/*  {this.state.levelpay === true ? (
          <div
            className="row form-section"
            style={layout === "fold" && step !== 4 ? { display: "none" } : {}}
          >
            <div className="col-md-12">
              <div className="form-group-title">
                {" "}
                <label>How do you want to pay?</label>
              </div>
            </div>
            <div className="col-md-6">
              <div
                className={
                  data["project_pay"] === "pay-by-hour"
                    ? "level-inner level-active"
                    : "level-inner"
                }
                onClick={() => this.goPayChoose("pay-by-hour")}
              >
                 
                <h3>Pay by the hour</h3>
              </div>
            </div>
            <div className="col-md-6">
              <div
                className={
                  data["project_pay"] === "pay-weekly"
                    ? "level-inner level-active"
                    : "level-inner"
                }
                onClick={() => this.goPayChoose("pay-weekly")}
              >
                 
                <h3>Pay Weekly</h3>
              </div>
            </div>
            <div className="col-md-6">
              <div
                className={
                  data["project_pay"] === "pay-monthly"
                    ? "level-inner level-active"
                    : "level-inner"
                }
                onClick={() => this.goPayChoose("pay-monthly")}
              >
                
                <h3>Pay Monthly</h3>
              </div>
            </div>
          </div>
        ) : (
          ""
        )} */}

        {this.state.levelestimate === true ? (
          <div className={styles.form_section+ " row"}>
            <div className="col-md-12">
              <div className={styles.form_group +" "+styles.button}>
                <div className={styles.button_box}>
                  <button
                    className={clickClass + " " + styles.btn}
                    ref={input => (this.inputElement = input)}
                    onClick={this.HireSubmit}
                  >
                    Next →
                  </button>
                  <span className={styles.press_enter}>Press CTRL + ENTER</span>
                </div>
              </div>
              <div className={styles.respMsg}>{this.state.respMsg}</div>
            </div>
          </div>
        ) : (
          ""
        )}
      </div>
      </div>
      </div>
    );
  }
}
 
export default withRouter(FormOne);
