
import bstyles from '../../styles/Dashboardbilling.module.scss'
import Joi from "joi-browser";
import React, { useEffect ,useState} from 'react'
import * as userService from "../../services/userUservice";
import SubmitButton from '../../components/submit-button';
import ValidationError from '../../components/common/validation-error';
const pattern = /[.]/g;
function MiddleBox() {
    const [billing,setBilling]=useState({first_name: "", last_name: "", work_email: "", company: "", website: "",  office_phone: "", contact_address: "", contact_address_two: "",country: "",city: "", zip: "", state: "",calling: ""});
    const [profile,setProfile]=useState([]);
    const [password,setPassword]=useState({  current_password: "",new_password: "",retype_password: ""})
    const [submit_status,setSubmitStatus]=useState(false);
    const [response_message,setResMsg]=useState(false); 
    const [response_type,setResType]=useState("");
    const [errors,setErrors]=useState([]);
    const [loading,setLoading]=useState(true);
    const [password_check,setPasswordShow]=useState(false);
    const[lidata,setLidata]=useState("");
    const  schema = {
      first_name: Joi.string()
      .required()
      .error(() => {
        return {
          message: "Please enter your first name"
        };
      }),
    last_name: Joi.string()
      .required()
      .error(() => {
        return {
          message: "Please enter your last name"
        };
      }),
    login_email: Joi.string()
      .required()
      .email()
      .error(() => {
        return {
          message:
            "The email that you’ve entered is invalid. Please enter a properly formatted email."
        };
      }),
    contact_address: Joi.string()
      .required()
      .error(() => {
        return {
          message: "Please enter your contact address"
        };
      }),

    country: Joi.string()
      .required()
      .error(() => {
        return {
          message: "Please select your country"
        };
      }),
    city: Joi.string()
      .required()
      .error(() => {
        return {
          message: "Please select your city"
        };
      }),
    zip: Joi.string()
      .required()
      .error(() => {
        return {
          message: "Please enter your zip"
        };
      }),
    state: Joi.string()
      .required()
      .error(() => {
        return {
          message: "Please enter your state"
        };
      }),
    website: Joi.optional(),
    calling: Joi.optional(),
    contact_address_two: Joi.optional(),
    company: Joi.optional(),
    office_phone: Joi.optional(),
    work_email: Joi.optional()
      };

    const checkBtnenable = () => {
        return null;
        const { error } = Joi.validate(billing, schema);
        if (!error) {
          return null;
        }
        const errors = {};
        for (let item of error.details) errors[item.path[0]] = item.message;
        console.log(errors);
        return errors;
      };

     const handleChange = async ({ currentTarget: input }) => {
        const n_errors = { ...errors };
        const errorMessage = validateProperty(input);
        if (errorMessage) n_errors[input.name] = errorMessage;
        else delete n_errors[input.name];
        billing[input.name] = input.value;
        setErrors(n_errors);
        setBilling(billing);
        console.log("BILLING",billing);
      };
    
    const  validateProperty = ({ name, value }) => {
        const obj = { [name]: value };
        const bschema = { [name]: schema[name] };
        const { error } = Joi.validate(obj, bschema);
        return error ? error.details[0].message : null;
      };

    const handleChangePassword = async ({ currentTarget: input }) => {
        const errors = { ...errors };
        const lidata = { ...lidata };
        setResMsg("");
        errors.validatepwd = "";
        password[input.name] = input.value;
        if (input.name === "new_password") {
          errors.new_password = "do not match the conditions";
          if (input.value.length > 7) {
            lidata.lengths = "tick";
          } else {
            lidata.lengths = "";
          }
          if (input.value.match(/[a-z]/)) {
            lidata.lower = "tick";
          } else {
            lidata.lower = "";
          }
          if (input.value.match(/[A-Z]/)) {
            lidata.upper = "tick";
          } else {
            lidata.upper = "";
          }
          if (input.value.match(/[0-9]/)) {
            lidata.num = "tick";
          } else {
            lidata.num = "";
          }
          if (input.value.match(/[!@#$%^&*()?/]/)) {
            lidata.special = "tick";
          } else {
            lidata.special = "";
          }
        } else if (input.name === "retype_password") {
          if (password.new_password !== input.value) {
            errors.retype_password = "The passwords you’ve entered does not match.";
          } else {
            errors.retype_password = "";
          }
        } else if (input.name === "current_password") {
          if (input.value === "") {
            errors.current_password = "Please enter your current password";
          } else {
            errors.current_password = "";
          }
        }
        setPassword(password);
        setErrors(errors);
        setLidata(lidata);
      };

      const passwordCheck = () => {
        password["current_password"] = "";
        password["new_password"] = "";
        password["retype_password"] = "";
        setPassword(password);
        setPasswordShow(!password_check);
      };
    
     const doSubmit = async () => {
        setSubmitStatus(true);
        const n_errors = { ...errors };

        console.log(billing);
        const { error } = Joi.validate(billing,schema);
        if (error) {
          let path = error.details[0].path[0];
          let errormessage = error.details[0].message;
          n_errors[path] = errormessage;
          setSubmitStatus(false);
          setErrors(n_errors)
        } else {
          if (password_check === true) {
            if (
              password.current_password !== "" &&
              password.new_password !== "" &&
              password.retype_password !== "" &&
              password.new_password.length > 7 &&
              password.new_password.match(/[a-z]/) &&
              password.new_password.match(/[A-Z]/) &&
              password.new_password.match(/[0-9]/) &&
              password.new_password.match(/[!@#$%^&*()?/]/) &&
              password.new_password === password.retype_password
            ) {
              const response = await userService.updateSettings(
                billing,
                profile,
                password.new_password,
                password.current_password
              );
              if (response && response.data) {
                setResType(response.data.status);
                setResMsg(response.data.message);
                setSubmitStatus(false);
              }
            } else {
              n_errors.validatepwd =
                "Please enter and match the conditions for password.";
              errors.new_password = "do not match the conditions";
              setErrors(n_errors)
              setSubmitStatus(false);
            }
          } else {
            const response = await userService.updateSettings(
              billing,
              profile,
              password.new_password,
              password.current_password
            );
            if (response && response.data) {
              setResType(response.data.status);
              setResMsg(response.data.message);
              setSubmitStatus(false);
            }
          }
        }
      };

    const  getUserdetails=async()=>{
        const response = await userService.geUserProfile();
        if (response && response.data) {
          if (response.data.status === 1) {
            let n_billing = { ...billing };
            let n_profile = { ...profile };
              n_billing = response.data.data.billing
              ? response.data.data.billing
              : billing;
              n_billing.login_email=response.data.data.email;
              n_profile = response.data.data.profile
              ? response.data.data.profile
              : profile;
              setBilling(n_billing);
              setProfile(n_profile)
              setLoading(false);
          } else {
            setLoading(false);
          }
        }
    }

    useEffect(() => {
       getUserdetails();
    },[])

    return(
        <div className={bstyles.middle_box_col_12 + " col"} >
            <div className={bstyles.inner_container}>
                <div className={bstyles.title}>Settings</div>
                <div className={bstyles.content}>
                {loading ? (
                    <div className="row">
                      <div className="col-md-12">
                        <div className={bstyles.form+ " "+ "row"}>
                          <div className="spinner">
                            {/* <Spinner animation="grow" variant="warning" /> */}
                            <span>Connecting...</span>
                          </div>
                        </div>
                      </div>
                    </div>
              
                ) : (
                  <div className={bstyles.form+ " "+ "row"}>
                    <div className='response_container'>
                        <div className={response_type?response_type==1?"sucsess":"error":""}>
                         <span>{response_message}</span>
                        </div>
                    </div>
                 
                    <div className={bstyles.form_group+ " "+ "col-md-4"}>
                      <label className={bstyles.label_top}>First Name</label>
                      <input
                        type="text"
                        className="form-control"
                        placeholder="First Name"
                        name="first_name"
                        onChange={handleChange}
                        value={billing.first_name}
                      />
                       {errors.first_name?<ValidationError message={errors.first_name} />:null} 
                    </div>
                    <div className={bstyles.form_group+ " "+ "col-md-4"}>
                      <label  className={bstyles.label_top}>Last Name</label>
                      <input
                        type="text"
                        className="form-control"
                        placeholder="Last Name"
                        name="last_name"
                        onChange={handleChange}
                        value={billing.last_name}
                      />
                       {errors.last_name?<ValidationError message={errors.last_name} />:null} 
                    </div>
                    <div className={bstyles.form_group+ " "+ "col-md-4"}>
                      <label  className={bstyles.label_top}>Email address (at work)</label>
                      <input
                        type="email"
                        className="form-control"
                        placeholder="Email address"
                        name="login_email"
                        onChange={handleChange}
                        value={billing.login_email}
                        disabled
                      />
                     {errors.email?<ValidationError message={errors.email} />:null} 
                    </div>
                    <div className={bstyles.form_group+ " "+ "col-md-12"}>
                      <label>Contact Address</label>
                      <input
                        type="text"
                        className="form-control"
                        placeholder="Contact Address"
                        name="contact_address"
                        onChange={handleChange}
                        value={billing.contact_address}
                      />
                     {errors.contact_address?<ValidationError message={errors.contact_address} />:null} 
                    </div>
                    <div className={bstyles.form_group+ " "+ "col-md-3"}>
                      <label>Country</label>
                   
                      <input
                        type="text"
                        className="form-control"
                        placeholder="Country"
                        name="country"
                        onChange={handleChange}
                        value={billing.country}
                      />
                      {errors.country?<ValidationError message={errors.country} />:null} 
                    </div>
                    <div className={bstyles.form_group+ " "+ "col-md-3"}>
                      <label>City</label>
                      <input
                        type="text"
                        className="form-control"
                        placeholder="City"
                        name="city"
                        onChange={handleChange}
                        value={billing.city}
                      />
                      {errors.city?<ValidationError message={errors.city} />:null} 
                    </div>
                    <div className={bstyles.form_group+ " "+ "col-md-3"}>
                      <label>ZIP Code</label>
                      <input
                        type="text"
                        className="form-control"
                        placeholder="ZIP Code"
                        name="zip"
                        onChange={handleChange}
                        value={billing.zip}
                      />
                     {errors.zip?<ValidationError message={errors.zip} />:null} 
                    </div>
                    <div className={bstyles.form_group+ " "+ "col-md-3"}>
                      <label>State</label>
                      <input
                        type="text"
                        className="form-control"
                        placeholder="State"
                        name="state"
                        onChange={handleChange}
                        value={billing.state}
                      />
                      {errors.state?<ValidationError message={errors.state} />:null} 
                    </div>
                    <div className={bstyles.form_group+" "+bstyles.change_password+ " col-md-12"}>
                      <input
                        type="checkbox"
                        name="password_check"
                        onChange={passwordCheck}
                        value={password_check}
                        id="exampleCheck1"
                      />
                      <label
                        className="form-check-label"
                        htmlFor="exampleCheck1"
                      >
                        Change Password
                      </label>
                    </div>
                    {errors.validatepwd? <div className={bstyles.form_group+ " "+ "col-md-12"}>
                     <ValidationError message={errors.validatepwd} />
                    </div>:null} 
                    {password_check ? (
                      <>
                        <div className={bstyles.form_group+ " "+ "col-md-4"}>
                          <label>Current Password</label>
                          <input
                            type="password"
                            className="form-control"
                            placeholder="Current Password"
                            name="current_password"
                            onChange={handleChangePassword}
                            value={password.current_password}
                          />
                          {errors.current_password?<ValidationError message={errors.current_password} />:null} 
                        </div>
                        <div className={bstyles.form_group+ " "+ "col-md-4"}>
                          <label>New Password</label>
                          <input
                            type="password"
                            className="form-control"
                            placeholder="New Password"
                            name="new_password"
                            onChange={handleChangePassword}
                            value={password.new_password}
                          />
                        </div>
                        <div className={bstyles.form_group+ " "+ "col-md-4"}>
                          <label>Re-Type Password</label>
                          <input
                            type="password"
                            className="form-control"
                            placeholder="Re-Type Password"
                            name="retype_password"
                            onChange={handleChangePassword}
                            value={password.retype_password}
                          />
                         {errors.retype_password?<ValidationError message={errors.retype_password} />:null} 
                        </div>

                        {errors.new_password ? (
                          <div className={bstyles.pass_validate}>
                            <ul>
                              <li className={lidata.lower}>
                                One lowercase character
                              </li>
                              <li className={lidata.upper}>
                                One uppercase character
                              </li>
                              <li className={lidata.num}>
                                One number
                              </li>
                              <li className={lidata.special}>
                                One special character
                              </li>
                              <li className={lidata.lengths}>
                                8 characters minimum
                              </li>
                            </ul>
                          </div>
                        ) : (
                          ""
                        )}
                      </>
                    ) : null}
                    <div className="col-md-12 btns">
                    <SubmitButton btn_label="Submit" className="fp_green_btn" submit_status={submit_status} animation="border" variant="dark" size="sm" submit={doSubmit} Btnenable={checkBtnenable}/>
                    </div>
                  </div>
                )}
                </div>
            </div>
        </div>

    )
}

export default MiddleBox