import React, { useEffect } from 'react'
import Image from 'next/image'
import styles from '../../styles/services.module.scss'
import AOS from "aos";
function ServiceBottom() {
    useEffect(() => {
        AOS.init();
    },[])
    return(
        <div className="container">
             <div className={styles.servicebottom_section+ " services_bottom"} data-aos="zoom-in">
                <h2>Off-page SEO</h2>
                <p style={{maxWidth: "900px"}}>Off-page SEO builds your website&apos;s domain authority. You can&apos;t outrank your competitors without this key SEO magic. The goal is to make your site relevant and credible in the eyes of both search engines and actual users. Footprint.io has helped numerous clients maximize the power of Off-page SEO.</p> 
            
                <div className={styles.service_bottom+ " bottom_service"} data-aos="zoom-in">
                    <div className="row" data-aos="zoom-in">
                    <div className={styles.col_6+" col-md-6"+" service_two center"}>
                        <img src="/images/service_sectiontwo.png" className='common_next_img_updated' width={500} height={523} alt="duis" loading="eager"/>
                    </div>
                    <div className={styles.col_6+" col-md-6"}>
                            <div className={styles.service_sectiontwo}>
                                <ul>
                                    <li>
                                        <h5>Boost Ranking</h5>
                                        You need backlinks to build your domain authority. The higher authority your site is, the better your ranking will be on the search engine results pages. Backlinks are a signal of trust, relevance, and competence. Focus on making your company awesome, and leave the rest to us.
                                    </li>
                                    <li>
                                        <h5>Improve Exposure</h5>
                                        The higher your ranking will be, the better your exposure is. It&apos;s not just about getting exposed to SERPs. It&apos;s also about capturing the audience of the sites you&apos;re linking from. The more organic, the better. And we have our own way of achieving amazing results for you.
                                    </li>
                                    <li>
                                        <h5>Increase Authority</h5>
                                        Again, we aren&apos;t just talking about your Domain Authority score in the eyes of Google. Your actual authority in the eyes of your target audience is also at stake here. Off-page SEO helps you gain people&apos;s trust, and that easily means thriving business for you.
                                    </li>
                                </ul>
                            </div>
                        </div>
                    
                    </div>
                </div>
            </div>
         </div>
    )

}
export default ServiceBottom