import React, { useEffect } from 'react'
import Image from 'next/image'
import styles from '../../styles/services.module.scss'
import AOS from "aos";
function ServiceTop() {
    useEffect(() => {
        AOS.init();
    },[])
    return(
        <div className="container">
            <div className={styles.service_topsection + " service_top"} data-aos="zoom-in">
                <div className="row first" data-aos="zoom-in">
                   <div className={styles.col_6+" col-md-6"+" brochure_sectionone"}>
                        <div className={styles.service_sectionone}>
                            
                            <ul>
                                <li>
                                    <h5>Backlink Experts</h5>
                                    
                                    From choosing the right keywords to creating the right content to finding all backlink opportunities for you, our backlink experts are capable of doing it all. Our link building strategies are proven to deliver.  
                                </li>
                                <li>
                                    <h5>White Hat SEO</h5>
                                    
                                    We deliver results while making sure that they&apos;re lasting and sustainable. Some might deliver fast results but don&apos;t really last very long because of undesirable methods that Google disqualifies. We use approved methods that lead to organic and lasting outcomes. 
                                </li>  
                                <li>
                                    <h5>High-quality Links</h5>
                                    
                                    It&apos;s not just about the quantity of backlinks. It&apos;s most importantly about the quality. Quality matters more than quantity, and we know how to make high-quality backlinks happen. We&apos;ll get those relevant and high-authority links that you need to win and thrive.
                                </li>
                               
                            </ul>
                        </div>
                       
                  </div>
                <div className={styles.col_6+" col-md-6"+" service_one center"}>
                    <img src="/images/service_sectionone.png" className='common_next_img_updated' width={500} height={523} alt="duis" loading="eager"/>
                </div>
                </div>
            </div>
        </div>
    )

}
export default ServiceTop