import React, { useEffect } from 'react'
import Image from 'next/image'
import styles from '../../styles/services.module.scss'
import AOS from "aos";
function ServiceBanner() {
    useEffect(() => {
        AOS.init();
    },[])
    return(
        <div className="container">
            <div className={styles.service_section} data-aos="zoom-in">
             <h1>Why Choose Our Off-Page SEO Services</h1>
                
                <p style={{maxWidth: "900px"}}>
                Off-page SEO is an essential part of every SEO campaign. You can&apos;t possibly rank well on Google without off-page SEO strategies in place, and it&apos;s hard to do it all alone. Allow us to be an extension of your company. Our off-page SEO specialists are ready to rank you on page one. </p>
            </div>
        </div>
    )

}
export default ServiceBanner