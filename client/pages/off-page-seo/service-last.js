import React, { useEffect } from 'react'
import Image from 'next/image'
import styles from '../../styles/services.module.scss'
import AOS from "aos";
function ServiceLast() {
    useEffect(() => {
        AOS.init();
    },[])
    return(
        <div className="container">
            <div className={styles.service_last+ " services_last"} data-aos="zoom-in">
                    <div className="row last" data-aos="zoom-in">
                        <div className={styles.col_6+" col-md-6"+" brochure_sectionlast"}>
                                <div className={styles.service_sectionthree}>
                                    <ul>
                                        <li>
                                            <h5>Build Reputation</h5>
                                            The more you focus on delivering high-quality content and helping users find the answers they need through off-page SEO, the stronger your reputation will be. Off-page SEO is all about making sure that your target audience notices you and sees your worth, so they can soon enjoy what you have to offer. 
                                        </li>
                                          <li>
                                            <h5>Enhance Social Signals</h5>
                                            Social media is another space to maximize off-page SEO tactics. Search engines like Google also monitor social signals as indicators of value and relevance. It&apos;s one way to boost your overall social media visibility.
                                        </li>
                                        <li>
                                            <h5>Dominate Online</h5>
                                            Consumers are now most efficiently captured online. If you are not maximizing the enormous digital space yet, you are missing out on a lot of opportunity for progress and wealth. Work on making your products or services awesome and make sure that your promising brand gets noticed. Footprint.io is here to help you win and dominate.  
                                        </li> 
                                    </ul>
                                </div>
                        </div>
                        <div className={styles.col_6+" col-md-6"+" service_three center"}>
                            <img src="/images/service_sectionthree.png" className='common_next_img_updated' width={500} height={523} alt="duis" loading="eager"/>
                        </div>
                    </div>
                   
            </div>
        </div>
        )

    }
    export default ServiceLast