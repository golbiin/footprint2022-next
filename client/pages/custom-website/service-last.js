import React, { useEffect } from 'react'
import Image from 'next/image'
import styles from '../../styles/services.module.scss'
import AOS from "aos";
function ServiceLast() {
    useEffect(() => {
        AOS.init();
    },[])
    return(
        <div className="container">
            <div className={styles.service_last+ " services_last"} data-aos="zoom-in">
                    <div className="row last" data-aos="zoom-in">
                        <div className={styles.col_6+" col-md-6"+" brochure_sectionlast"}>
                                <div className={styles.service_sectionthree}>
                                    <ul>
                                        <li>
                                            <h5> Better User Experience</h5>
                                            With a custom-built website, you won&apos;t have to deal with the problems website templates tag along, such as security vulnerabilities, slow page load time, web browser incompatibility, unresponsive designs, etc. Allow us to create the best custom website for you, and together, let&apos;s achieve the best user experience your target audience deserves.
                                        </li>
                                          <li>
                                            <h5>Better Scalability </h5>
                                            Most premade templates don&apos;t have the foundation you need to grow your business. Custom websites, on the other hand, are programmed to manage high traffic as your business grows. Keep growth in mind and choose the scalability a custom website offers. Footprint.io can help you achieve a scalable, easy-to-navigate website that captures visitors attention and functions seamlessly. 
                                        </li>
                                        <li>
                                            <h5>SEO Advantage</h5>
                                            Going custom also has an SEO advantage. Remember that User Experience is also a key ranking factor. From site speed to responsiveness to code structure, the search engines pay attention to every detail that promotes user satisfaction. Stand out and rank to the top with a custom website expertly built just for you.  Rely on our team of experienced web developers and designers and expect excellence.
                                        </li> 
                                    </ul>
                                </div>
                        </div>
                        <div className={styles.col_6+" col-md-6"+" service_three center"}>
                            <img src="/images/service_sectionthree.png" className='common_next_img_updated' width={500} height={523} alt="duis" loading="eager"/>
                        </div>
                    </div>
                   
            </div>
        </div>
        )

    }
    export default ServiceLast