import React, { useEffect } from 'react'
import Image from 'next/image'
import styles from '../../styles/services.module.scss'
import AOS from "aos";
function ServiceBanner() {
    useEffect(() => {
        AOS.init();
    },[])
    return(
        <div className="container">
            <div className={styles.service_section} data-aos="zoom-in">
             <h1>Custom Website </h1>
                
                <p style={{maxWidth: "1200px"}}>
                From new startups to large enterprises, Footprint.io helps clients across the globe create custom websites to advance their business in the digital world. From the first meeting to the final deployment and maintenance stage, we’ll make sure that your custom website represents your brand flawlessly.
                </p>
            </div>
        </div>
    )

}
export default ServiceBanner