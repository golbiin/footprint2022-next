import React, { useEffect } from 'react'
import Image from 'next/image'
import styles from '../../styles/services.module.scss'
import AOS from "aos";
function ServiceBottom() {
    useEffect(() => {
        AOS.init();
    },[])
    return(
        <div className="container">
             <div className={styles.servicebottom_section+ " services_bottom"} data-aos="zoom-in">
                <h2>Custom Website Design and Development</h2>
                <p style={{maxWidth: "900px"}}>Custom websites may have a higher upfront cost, but the benefits are undeniable. If you are serious about establishing a strong web presence and an online brand that stands out, you can&apos;t go wrong with a custom-built website. Footprint.io is ready to be at your service. </p> 
            
                <div className={styles.service_bottom+ " bottom_service"} data-aos="zoom-in">
                    <div className="row" data-aos="zoom-in">
                    <div className={styles.col_6+" col-md-6"+" service_two center"}>
                        <img src="/images/service_sectiontwo.png" className='common_next_img_updated' width={500} height={523} alt="duis" loading="eager"/>
                    </div>
                    <div className={styles.col_6+" col-md-6"}>
                            <div className={styles.service_sectiontwo}>
                                <ul>
                                    <li>
                                        <h5>Fully Customizable</h5>
                                        Templates can&apos;t give you the level of customization you need to create a website that perfectly works for you. If you simply need a home page, contact form, FAQ section, and so on, it probably will be enough. But if you require more features that can bring about the best user experience and business workflow, you need a dedicated team of web developers to turn your ideas into reality.
                                    </li>
                                    <li>
                                        <h5>Higher Security Level</h5>
                                        Especially if you have sensitive data stored in your website such as users personal information or have an integrated payment system in place, you need to make sure that your website&apos;s security level is absolute. Template themes come withnumerous plugins that may pose security threats. Don&apos;t leave anything to chance. Footprint.io has got your back.
                                    </li>
                                    <li>
                                        <h5>Better Performance </h5>
                                        Custom websites, especially if done by pros, definitely load faster than most pre-built themes. Premade themes usually mean a lot of redundant codes and plugins. This slows down the performance which can harm your site&apos;s overall user experience and conversion rate. 
                                    </li>
                                </ul>
                            </div>
                        </div>
                    
                    </div>
                </div>
            </div>
         </div>
    )

}
export default ServiceBottom