import React, { useEffect } from 'react'
import Image from 'next/image'
import styles from '../../styles/services.module.scss'
import AOS from "aos";
function ServiceTop() {
    useEffect(() => {
        AOS.init();
    },[])
    return(
        <div className="container">
            <div className={styles.service_topsection + " service_top"} data-aos="zoom-in">
                <div className="row first" data-aos="zoom-in">
                   <div className={styles.col_6+" col-md-6"+" brochure_sectionone"}>
                        <div className={styles.service_sectionone}>
                            
                            <ul>
                                <li>
                                    <h5>Highly Skilled Team</h5>
                                    
                                    We are backed by a talented and brilliant team of web developers that are true experts in their field. We gather a dedicated team for each client and ensure that only remarkable specialists are on board. We have an excellent portfolio and a proven record of numerous successful projects. 
                                </li>
                                <li>
                                    <h5>Ongoing Support</h5>
                                    
                                    If you use templates, you have no one to turn to when the website has design errors or is unable to deploy properly. If your company heavily relies on a solid web presence, you can&apos;t take the risk of not employing custom designers that provide ongoing support, updates, and improvement.
                                </li>  
                                <li>
                                    <h5>Quality Assurance</h5>
                                    
                                    Premade themes aren&apos;t tested on your company&apos;s web host, and you may end up running into problems that are impossible to troubleshoot. Our custom web programmers develop and deploy websites in your working environment to detect possible complications, giving you utmost peace of mind.  
                                </li>
                               
                            </ul>
                        </div>
                       
                  </div>
                <div className={styles.col_6+" col-md-6"+" service_one center"}>
                    <img src="/images/service_sectionone.png" className='common_next_img_updated' width={500} height={523} alt="duis" loading="eager"/>
                </div>
                </div>
            </div>
        </div>
    )

}
export default ServiceTop