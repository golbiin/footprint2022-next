import Footer from "../../components/common/footer"
import Header from "../../components/common/header"
import styles from '../../styles/Cancellation.module.scss'
function PrivacyPolicy() {
    return(
        <div>
            <Header />
            <div className="container">
                <div className={styles.cancellation_cotainer}>
                <h1>Terms and Conditions</h1>
                <h2>1. Definitions</h2>
                <p>The following terms and conditions document is a legal agreement between Footprint Incorporated (“Footprint”) and you, the client (“Client”), for the purposes of web site design or development. These Terms and Conditions set forth the provisions under which the Client may use the services supplied.
                </p>

                <h2>2. Acceptance of Work</h2>
                <p>Quotations are valid for 20 days from date of issue.<br></br>
When the Client places an order to purchase a web site, web site updates or other services from Footprint, the order represents an offer to
Footprint which is accepted by Footprint only when an invoice is sent to the Client. No contract for the supply of services exists between
Client and Footprint until Footprint sends an invoice to the Client for payment. The invoice equals acceptance by Footprint of Client’s offer
to purchase services from Footprint and this acceptance of work is a valid contract between Client and Footprint. <br></br>
Any other services on the order which have not been included in the invoice do not form part of the contract. The Client agrees to check the
details of the invoice are correct and should print and keep a copy for their records.
Footprint reserves the right to withdraw from the contract at any time.
                </p>
                <h2>3. Permission and Copyright</h2>
                <p>All pages, images, text and code on Footprint’s web site at http://www.Footprint.com/ is copyrighted material.<br></br>
Copyright of the completed web designs, images, pages, code and source files created by Footprint for the project shall be with the Client
upon final payment only by prior written agreement. Without agreement and full payment from Client, ownership of designs and all code is with
Footprint.<br></br>
These terms of use grant a non-exclusive limited license so that the Client can use the design on one web site on one domain name only. The
Client is not permitted to use a design for more than one website without prior written agreement between the Client and Footprint.
Client agrees that resale or distribution of the completed files in full or in part is forbidden unless prior written agreement is made between the
Client and Footprint.<br></br>
Client hereby agrees that all media and content made available to Footprint for use in the project are either owned by the Client or used with
legal permission of the original authors or copyright owners. The Client agrees to hold harmless, protect and defend Footprint from any claim
or suit that may arise as a result of using Client supplied media and content.<br></br>
Client agrees that Footprint may include development credits and links within any code Footprint designs, builds or amends.<br></br>
Client agrees that Footprint reserves the right to include any work done for the Client in a portfolio of Footprint work. </p>
                <h2>4. Domain Names and Hosting</h2>
                <p>Client agrees to take all legal responsibility for use of third party domain name, hosting and email services and hereby agrees to indemnify and
hold harmless Footprint from any claim resulting from the Client’s publication of material and use of the domain name, hosting and email
services. </p>
                <h2>5. Project Details</h2>
                <p>Client agrees to provide any or all required information and content to Footprint according to the agreed schedule to enable Footprint to
build and complete the website work as part of an agreed project. Client also agrees that if they do not provide all content required on a timely
basis, Footprint reserves the right upon notice to cancel, reject, refuse sale to or work with a Client. There will be no refunds issued for
payments made to Footprint once the project has commenced.<br></br>
Client agrees that an HTML page built from a graphic design may not exactly match the original design because of the difference between the
display in design software and the rendering of HTML code by internet browser software. Footprint agrees to try and match the design as
closely as is possible when building the code.<br></br>
Footprint endeavours to create pages that are search engine friendly, however, Footprint gives no guarantee that the site will become listed
with search engines or of certain search results. In no event shall Footprint be held liable for any changes in search engine rankings as a
result of using Footprint code.<br></br>
If an error or issue with the design or code arises during the project which does not allow the design or code to match the original specification,
then Client agrees that Footprint can apply a nearest available alternative solution.<br></br>
After site completion, a Client or a third party of their choosing may wish to edit their web site code themselves to make updates. However, the
Client agrees that in so doing they assume full responsibility for any issues which occur as a result of changing the code themselves. If Client
or a third party of their choosing edits the web site code and this results in functionality errors or the page displaying incorrectly, then
Footprint reserves the right to quote for work to repair the web site.<br></br>
Footprint reserves the right to assign subcontractors in whole or as part of a project if needed.<br></br>
All communications between Footprint and Client shall be by email, telephone, Skype or postal mail, except where agreed at Footprint’s
discretion. </p>
                <h2>6. Web Browsers</h2>
                <p>Footprint shall make every reasonable and standard effort to ensure sites are designed to be viewed by the majority of visitors. Sites are
designed to work with the main browsers Internet Explorer, Google Chrome and Mozilla Firefox latest releases. Client agrees that Footprint
cannot guarantee correct functionality with all browser software across different operating systems.<br></br>
Clients agree that after handover of files, any updated software versions of the main browsers Internet Explorer, Google Chrome and Mozilla
Firefox, domain name setup changes or hosting setup changes thereafter may affect the functionality and display of their web site. As such,
Footprint reserves the right to quote for any work involved in changing the web site design or web site code for it to work with updated
browser software, domain name or hosting changes. </p>
                <h2>7. Payment Terms</h2>
                <p>All prices quoted are subject to applicable HST charges.<br></br>
Footprint shall provide a specific quotation for the Client’s proposed work including a total amount for the whole project, broken down into an
initial deposit amount to begin the project work, and then various project phases with corresponding amounts.  </p>
<p className={styles.bold}>
A non-refundable payment of the deposit is considered to be acceptance of a mutual contract, and acceptance of the Terms and
Conditions as stated in this document. 
</p>
<p>The remaining amounts specified on the quotation shall become due as the work for each phase is completed to the reasonable satisfaction of
the Client. Footprint reserves the right not to begin the work until the said deposit has been paid in full, and thereafter reserves the right not to
begin the work on the next phase until the previous phase has been paid in full.<br></br>
All invoices must be paid in full within 5 days of the invoice date and Footprint will continue work only where an invoice has been paid by the
Client for the work. Once an invoice is sent to the Client it must be paid either by Paypal or Stripe, or such other alternate payment method as
disclosed by Footprint.<br></br>
Additional work requested by the Client which is not specified in the agreed quotation is subject to a separate quotation and Footprint
reserves the right to quote or accept additional work or not. If additional work is accepted by Footprint, it may affect timing and overall delivery
time of the project.</p>
                <h2>7A. Approval of Phase Work</h2>
                <p>On completion of a phase of the work, the Client will be notified and have the opportunity to review it. The Client should notify Footprint in
writing by email of any unsatisfactory points within 5 days of receipt of such notification. Any aspect of the work which has not been reported in
writing to Footprint as unsatisfactory within the 5-day review period, will be deemed to have been approved. Once approved, or deemed
approved, work cannot subsequently be rejected, and the phase will be deemed to have been completed, and the amount of invoice for such
phase must be paid in full. Payment must be made in full before Footprint shall begin work on a further phase. </p>
                <h2>7B. Rejected Work</h2>
                <p>If the Client rejects the work within the 5-day review period, or will not approve subsequent work performed by Footprint to remedy any points
reported by the Client as unsatisfactory, and Footprint considers that the Client is unreasonable in its repeated rejection of the work, the
contract will be deemed to have expired and Footprint may take any legal measures to recover payment for the completed work and
reasonable expenses incurred in recovering payment. Footprint shall be entitled to keep the non-refundable deposit amount in this event.
Client may request that Footprint cancel a project in writing by email to Footprint and the project is cancelled only if Footprint confirms work
has not been started on the project. If Footprint has begun or completed the work and the Client no longer requires the files, but have agreed
to the work, they are still obliged to pay Footprint for the work that has been carried out.<br></br>
All invoices are submitted by email except where required otherwise by regulations or agreed at Footprint’s discretion.<br></br>
Billing inquiries and disputes should be brought to Footprint’s attention within 5 days of the invoice date. Failure to do so will be deemed to be
an admission that the invoice and charges are accurate </p>
                <h2>8. 30 Day Limited Warranty</h2>
                <p>Footprint provides a 30 day period from the completion of the project and handover of files to Client, for Client to ensure that all aspects of the
website are functioning properly, and to guarantee quality and Client satisfaction. During this 30 day period, Footprint will make any
necessary changes to ensure that all aspects of the website are functioning properly, and will make minor updates to the completed website
free of charge.<br></br>
While every effort is made to make sure files are error free, Footprint cannot guarantee that the display or functionality of the web design or
the web site will be uninterrupted or error free. If during the 30 day period, errors are found in code Footprint has created and the main
browsers Internet Explorer, Google Chrome and Mozilla Firefox, domain name setup and hosting setup are the same as when work began,
then Footprint shall correct these errors for the Client free of charge.<br></br>
This warranty does not apply to any work that is outside the scope of the quotation provided by Footprint to the Client, nor does it apply to
updating or replacing website text, major page reconstruction, data-entry which can be performed through the Clientʼs administrative
environment, navigation structure changes or repairing any alterations made to the original source code provided </p>
                <h2>9. Liability Disclaimer</h2>
                <p>Footprint provides its web site and the contents thereof on an “as is” basis and makes no warranties with regard to the site and its contents,
or fitness of services offered for a particular purpose. Footprint cannot guarantee the functionality or operations of its web site or that it will be
uninterrupted or error free, nor does it warrant that the contents are current, accurate or complete.<br></br>
Footprint endeavors to provide a web site within given delivery time scales to the best of its ability. However, the Client agrees that Footprint
is not liable for any claims, losses, costs incurred or compensation due to any failure to carry out services within a given delivery schedule.
The Client agrees Footprint is not liable for absence of service as a result of illness or holiday. The Client agrees Footprint is not liable for
any failure to carry out services for reasons beyond its control including but not limited to acts of God, telecommunication problems, software
failure, hardware failure, third party interference, government, emergency on major scale or any social disturbance of extreme nature such as
industrial strike, riot, terrorism and war or any act or omission of any third party services.<br></br>
Footprint is not liable for any consequences or financial losses such as, but not limited to, loss of business, profit, revenue, contract, data or
potential savings, relating to services provided. </p>
<p className={styles.bold}>
Footprint shall have no liability to the Client or any third parties for any damages, including but not limited to, claims,
losses, lost profits, lost savings, or other incidental, consequential, or special damages arising out of the operation
of or inability to operate these web pages, email, or web site, even if Footprint has been advised of the possibility of
such damages. Client specifically agrees that Footprint’s total liability of for all claims of any kind arising as a result 
    </p>
    <p>
    Footprint reserves the right to quote for any updates as separate work. Client agrees Footprint is not liable for any failure to inform or
implement these updates to their site. Client agrees that it shall defend, indemnify, save and hold Footprint harmless from any and all
demands, liabilities, costs, losses and claims arising from omission to inform or implement these updates.
    </p>
                <h2>10. Indemnification</h2>
                <p>Client agrees to use all Footprint services and facilities at their own risk and agrees to defend, indemnify, save and hold Footprint harmless
from any and all demands, liabilities, costs, losses and claims against Footprint that may arise directly or indirectly from any service provided
or agreed to be provided or any product or service sold by the Client or its related third parties. Client agrees this indemnification extends to all
aspects of the project, including but not limited to web site content and choice of domain name.<br></br>
Client also agrees to defend, indemnify and hold Footprint harmless against any liabilities arising out of injury to person or property caused by
any service provided or agreed to be provided or any product or service sold by the Client or third parties, including but not limited to,
infringement of copyright, infringement of proprietary rights, misinformation, delivery of defective products or services which is harmful to any
person, business, company or organization. </p>
                <h2>11. Confidential Information</h2>
                <p>Footprint agrees that, except as directed by the Client or as required by law, it will not at any time during or after the term of this agreement
disclose any Client confidential information. Likewise, the Client agrees that it will not disclose any confidential information from or about
Footprint to any other party except as directed by Footprint or required by law. </p>
                <h2>12. Privacy Policy</h2>
                <p>In projects Footprint and any third party associates shall use information provided by a Client in relation to this agreement in accordance with
the Canada Personal Information Protection and Electronic Documents Act and Ontario Freedom of Information and Protection of Privacy Act,
and also for the following purposes: to identify the Client in communications from Footprint to them; and for Footprint to contact the Client
from time to time to offer them services or products which may be of interest to or benefit the Client. </p>
                <h2>13. Interpretation</h2>
                <p>Footprint reserves the right to terminate a project with a Client at any time without prior notification, if Client is in breach of any of these terms
and conditions. Footprint shall be the sole arbiter in deciding what constitutes a breach. In the case of such termination, all payments made
by Client are non-refundable.<br></br>
This agreement shall be governed by the laws of Ontario and of Canada applicable in Ontario for contracts made and to be entirely performed
within Ontario. Any and all disputes arising under this agreement shall be resolved by binding arbitration in accordance with the provisions of
the Ontario Arbitrations Act or any successor legislation, ad may be amended from time to time, by a single arbitrator chosen by the parties or
appointed by an Ontario court with jurisdiction. There shall be no appeal from the award of the arbitrator.
Where one or more terms of this contract are held to be void or unenforceable for whatever reason, any other terms of the contract not so held
will remain valid and enforceable at law.<br></br>
Footprint reserves the right to alter these Terms and Conditions at any time without prior notice. Any new version of these Terms and
Conditions will have a new effective date as indicated below </p>
<p className={styles.bold}>By accepting a quotation or making a payment of invoice to use the services supplied, the Client acknowledges that
it has read, understood, and accepts the Terms and Conditions of this Agreement, and agrees to be legally bound by
these Terms and Conditions.</p>
 
             
                </div>
            </div>
            <Footer />
        </div>
    )
}

export default PrivacyPolicy
