import apiUrl from "../config/server";
const axios = require("axios").default;

export function getAllcats() {
  return axios.post(apiUrl + "/portfolio/getCategories");
}

export function getPortfolios() {
  return axios.post(apiUrl + "/portfolio/getPortfolios");
}
