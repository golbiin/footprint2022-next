import apiUrl from "../config/server";
const axios = require("axios").default;
const tokenKey = "userToken";

export function signupUser(users) {
    return axios.post(apiUrl + "/user/signup", {
      email: users.email,
      password: users.password,
      notify_me:users.notify_me,
      leadsData:users.leadsData
    });
  }

export function createProject(data) {
    return axios.post(apiUrl + "/user/create_project", {
      project_name: data.project_name,
      website: data.website,
      token: localStorage.getItem(tokenKey)
    });
  }
export function readWorkingDetails(id) {
    return axios.post(apiUrl + "/user/readWorkingDetails", {
      id: id,
      token: localStorage.getItem(tokenKey)
    });
  }
  
  export function getProjects() {
    return axios.post(apiUrl + "/user/get_projects", {
      token: localStorage.getItem(tokenKey)
    });
  } 

  export function verify(id) {
    return axios.post(apiUrl + "/user/verifyAccount", {
      user_id: id
    });
  }

  export function contact(data, type, page, ip) {
    return axios.post(apiUrl + "/contacts/contact", {
      data: data,
      type: type,
      page: page,
      ip: ip,
    });
  }
  export function add_news_letter(data) {
    console.log("add_news_letter")
    return axios.post(apiUrl + "/contacts/add_news_letter", {
      data: data
    });
  }

  export function getWorkingDetailsList() {
    return axios.post(apiUrl + "/user/getWorkingDetailsList", {
      token: localStorage.getItem(tokenKey)
    });
  }

  export function geUserProfile() {
    return axios.post(apiUrl + "/user/get_profile", {
      token: localStorage.getItem("userToken")
    });
  }

  export function updateBillingAddress(data) {
    return axios.post(apiUrl + "/user/updateBillingAddress", {
      token: localStorage.getItem("userToken"),
      billing: data
    });
  }

  export function updateSettings(
    data,
    profileData,
    password = "",
    current_password = ""
  ) {
    return axios.post(apiUrl + "/user/updateSettings", {
      token: localStorage.getItem("userToken"),
      billing: data,
      profile: profileData,
      password: password,
      current_password: current_password
    });
  }

  export function loginSaveProjectData(data) {
    return axios.post(apiUrl + "/user/loginSaveProjectData", {
      token: localStorage.getItem("userToken"),
      data: data
    });
  }
  