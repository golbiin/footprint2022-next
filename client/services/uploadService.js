

import apiUrl from "../config/server";
const axios = require("axios").default;
export function uploadMultiple(upload_data) {
  const upload = new FormData();
  upload_data.map(eachFile => {
    upload.append("file", eachFile);
  });
  return axios({
    method: "post",
    url: apiUrl + "/uploadMultiple/uploadData/",
    data: upload,
    headers: { "Content-Type": "multipart/form-data" }
  });
}
