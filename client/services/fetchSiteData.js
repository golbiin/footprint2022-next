import apiUrl from "../config/server";
const axios = require("axios").default;

export function getReviews() {
  return axios.post(apiUrl + "/fetchsitedata/get_reviews");
}
export function get_reviews_all() {
  return axios.post(apiUrl + "/fetchsitedata/get_reviews_all");
}
