import apiUrl from "../config/server";
const axios = require("axios").default;
import jwtDecode from "jwt-decode";
const sign = require('jwt-encode');
const secret = 'footprint2022';
const tokenKey = "userToken";

export function authenticate(users, leadsData) {
    return axios.post(apiUrl + "/authenticate", {
      email: users.email,
      password: users.password,
      leadsData: leadsData
    });
  }
  
export function loginWithJwt(jwt) {
    localStorage.setItem(tokenKey, jwt);
    return true;
  }

export function logout() {
    localStorage.removeItem(tokenKey);
  }

export function getCurrentUser() {
    try {
      const jwt = localStorage.getItem(tokenKey);
      return jwtDecode(jwt);
    } catch (ex) {
      return null;
    }
  }

  export function resendVerification(email) {
    return axios.post(apiUrl + "/user/sendVerificationmail", {
      email: email
    });
  }
  
  export function forgotPassword(data) {
    return axios.post(apiUrl + "/user/forgotPassword", {
      data: data
    });
  }
  
  export function resetPassword(data, user_id) {
    return axios.post(apiUrl + "/user/resetPassword", {
      data: data,
      user_id: user_id
    });
  }

  export function createCart(data) {
    try {
      const jwt = sign(data, secret);
      sessionStorage.setItem("!@#$", jwt);
      return true;
    } catch (ex) {
      return null;
    }
  }

  export function getCart() {
    try {
      let cart=sessionStorage.getItem("!@#$");
      return jwtDecode(cart);
    } catch (ex) {
      return null;
    }
  }

  export function removeCart() {
    try {
      sessionStorage.removeItem("!@#$");
      return true;
    } catch (ex) {
      return null;
    }
  }