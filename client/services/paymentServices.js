import apiUrl from "../config/server";
const axios = require("axios").default;
const tokenKey = "userToken";
 
export function savecard(id, name, email="") {
    return axios.post(apiUrl + "/payment/save_card", {
        payment_method_id: id,
        name: name,
        email: email,
        token: localStorage.getItem(tokenKey)
    });
} 
export function removeCard(id) {
  return axios.post(apiUrl + "/payment/remove_card", {
    data: id,
    token: localStorage.getItem(tokenKey)
  });
}

export function getsavedCard() {
  return axios.post(apiUrl + "/payment/get_saved_cards", {
    token: localStorage.getItem(tokenKey)
  });
} 
export function payment(data) {
    return axios.post(apiUrl + "/payment", {
      data: data,
      token: localStorage.getItem(tokenKey)
    });
  }

  export function paymentBycheque(data,timeData) {
    return axios.post(apiUrl + "/payment/payment_by_cheque", {
      data: data,
      timedata:timeData,
      token: localStorage.getItem(tokenKey)
    });
  }
 
  export function getPayments() {
    return axios.post(apiUrl + "/payment/get_payments", {
      token: localStorage.getItem(tokenKey)
    });
  } 
  export function getSubscriptions() {
    return axios.post(apiUrl + "/payment/get_subscriptions", {
      token: localStorage.getItem(tokenKey)
    });
  } 