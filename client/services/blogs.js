import SERVER_URL from "../config/server";
const axios = require("axios").default;

export function getAllBlogs(data) {
  return axios.post(SERVER_URL + "/blogs/getAllBlogs", {
    data: data,
  });
}
export function getAllBlogsCategory(data) {
  return axios.post(SERVER_URL + "/blogs/getAllBlogsCategory", {
    data: data,
  });
}
export function getSingleBlog(data) {
  return axios.post(SERVER_URL + "/blogs/getSingleBlog", {
    data: data,
  });
}

export function blog_contact(data, type, page, ip, humankey) {
  return axios.post(SERVER_URL + "/contacts/blog_contact", {
    data: data,
    type: type,
    page: page,
    ip: ip
  });
}