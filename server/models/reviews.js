const mongoose = require("mongoose");
const review = new mongoose.Schema({
  user_name: { type: String },
  testimony: { type: String },
  rating: { type: String },
  user_image: { type: String },
  created_on: { type: Date },
  updated_on: { type: Date },
  status: { type: Boolean, default: true }, 
  deleteStatus: { type: Boolean, default: false },
});
module.exports = mongoose.model("review", review);
