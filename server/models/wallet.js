const mongoose = require("mongoose");
const wallet = new mongoose.Schema({
  user_id: { type: mongoose.Schema.ObjectId },
  coupon_id: { type: mongoose.Schema.ObjectId },
  subscription_id: { type: mongoose.Schema.ObjectId },
  coupon_code: { type: String },
  amount: { type: Number },
  balance: { type: Number },
  transacion_type: { type: String },
  details: { type: String },
  added_by: { type: String, default:"user" },
  added_on: { type: Date },
  status: { type: Boolean, default: true },
});
module.exports = mongoose.model("wallet", wallet);
