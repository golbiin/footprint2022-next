const mongoose = require("mongoose");

const coupons = new mongoose.Schema({
  user: { type: mongoose.Schema.ObjectId },
  coupon_code: { type: String },
  coupon_amount: { type: Number },
  redeem_status: { type: Boolean, default: false },
  redeem_date: { type: Date },
  expire_on: { type: Date },
  created_on: { type: Date },
  status: { type: Boolean, default: false }
});

module.exports = mongoose.model("coupons", coupons);
