const mongoose = require("mongoose");
const portfolioCats = new mongoose.Schema({
  parent: { type: String },
  sub_categories: { type: Array }
});
module.exports = mongoose.model("portfolio_cat", portfolioCats);
