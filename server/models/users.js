const mongoose = require("mongoose");
const users = new mongoose.Schema({
  email: { type: String },
  password: { type: String },
  email_verification: { type: Boolean, default: 0 },
  notify_me: { type: Boolean, default: false },
  status: { type: Boolean, default: true },
  deleteStatus: { type: Boolean, default: false },
  created_on: { type: Date },
  last_login: { type: Date },
  email_verification:{type:Boolean, default:false},
  company_name:{ type: String },
  profile: { type: Object },
  billing: { type: Object },
});

module.exports = mongoose.model("users", users);
