const mongoose = require("mongoose");

const newsLetter = new mongoose.Schema({
  news_email: { type: String },
  submitted_on: { type: Date },
  ip: { type: String },
  status: { type: Boolean, default: true }
});

module.exports = mongoose.model("newsLetter", newsLetter);
