const mongoose = require("mongoose");
const payment = new mongoose.Schema({
  user_id: { type: mongoose.Schema.ObjectId, required: true },
  billing_address: { type: Object },
  payment_method_id: { type: String },
  first_name: { type: String },
  last_name: { type: String },
  package: { type: Object },
  discount: { type: Object },
  payment_method: { type: String },
  payment_date: { type: String },
  timezone: { type: String },
  payment_status: { type: String },
  payment_response: { type: Object },
  payment_id: { type: String },
  subscription_id: { type: mongoose.Schema.ObjectId },
  charged_by: { type: String },
  status: { type: Number }
});
module.exports = mongoose.model("payment", payment);
