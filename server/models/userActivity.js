const mongoose = require("mongoose");
const user_activity = new mongoose.Schema({
  user_id: { type: mongoose.Schema.ObjectId },
  date: { type: Date },
  activity: { type: String },
  details: { type: Object }
});

module.exports = mongoose.model("user_activities", user_activity);
