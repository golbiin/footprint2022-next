const mongoose = require("mongoose");
const leads = new mongoose.Schema({
  user_id: { type: mongoose.Schema.ObjectId },
  project_name: { type: String },
  project_details: { type: String },
  project_type: { type: Object },
  project_file: { type: Object },
  project_skill: { type: Object },
  added_page: { type: String, default:"register" },
  added_on: { type: Date },
  status: { type: Boolean, default: true },
});
module.exports = mongoose.model("leads", leads);