const mongoose = require("mongoose");
const portfolio = new mongoose.Schema({
  title: { type: String },
  url: { type: String },
  portfolio_image: { type: String },
  main_category: { type: mongoose.Schema.ObjectId },
  sub_category: { type: Array },
  languages: { type: Array },
  order: { type: Number }
});
module.exports = mongoose.model("portfolios", portfolio);
