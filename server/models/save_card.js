const mongoose = require("mongoose");

const saveCard = new mongoose.Schema({
  user_id: { type: mongoose.Schema.ObjectId, required: true },
  customer_id: { type: String, required: true },
  payment_method_id: { type: String, required: true },
  last_updated: { type: Date },
  status: { type: Number }
});

module.exports = mongoose.model("save_card", saveCard);
