const mongoose = require("mongoose");
const workingDetails = new mongoose.Schema({
  user_id: { type: mongoose.Schema.ObjectId, required: true },
  project_id: { type: mongoose.Schema.ObjectId, required: true },
  report_date: { type: Date },
  working_hours: { type: String },
  post_title: { type: String },
  post_details: { type: String },
  post_status: { type: Boolean },
  read_post: {type: Boolean, default: false},
  status: { type: Boolean },
});
module.exports = mongoose.model("working_details", workingDetails);