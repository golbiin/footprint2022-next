const mongoose = require("mongoose");
const blog_authors = new mongoose.Schema({
  cat_name: { type: String },
  order: { type: Number }
});
module.exports = mongoose.model("blog_authors", blog_authors);
