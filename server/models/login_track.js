const mongoose = require("mongoose");
const track_user_login = new mongoose.Schema({
  user_id: { type: mongoose.Schema.ObjectId },
  ip: { type: String },
  login_date: { type: Date }
});

module.exports = mongoose.model("track_user_logins", track_user_login);
