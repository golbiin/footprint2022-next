const mongoose = require("mongoose");
const blog = new mongoose.Schema({
  blog_title: { type: String },
  page_slug: { type: String },
  meta_key: { type: String },
  meta_desc: { type: String },
  short_desc: { type: String },
  blog_content: { type: String },
  category: {  type: Object }, 
  blog_image: { type: String },
  author: { type: String },
  created_on: { type: Date },
  updated_on: { type: Date },
  published_on: { type: Date },
  status: { type: Boolean, default: true },
  deleteStatus: { type: Boolean, default: false },
 
});
module.exports = mongoose.model("blog", blog);
