const mongoose = require("mongoose");

const contacts = new mongoose.Schema({
  contact_data: { type: Object },
  submitted_on: { type: Date },
  viewd_status: { type: Boolean },
  contact_type: { type: String },
  page: { type: String },
  ip: { type: String },
  status: { type: Boolean, default: false }
});

module.exports = mongoose.model("contacts", contacts);
