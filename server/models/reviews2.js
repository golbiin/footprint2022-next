const mongoose = require("mongoose");
const reviews = new mongoose.Schema({
  title: { type: String },
  url: { type: String },
  portfolio_image: { type: String },
  main_category: { type: mongoose.Schema.ObjectId },
  sub_category: { type: Array },
  languages: { type: Array },
  order: { type: String }
});
module.exports = mongoose.model("reviews", reviews);
