const mongoose = require("mongoose");

const subscriptions = new mongoose.Schema({
  user_id: { type: mongoose.Schema.ObjectId, required: true },
  plan: { type: Object },
  customer_id: { type: String },
  payment_method_id: { type: String },
  subscribed_on: { type: String, required: true },
  renewd_on: { type: String },
  expire_on: { type: String },
  canceled_on: { type: String },
  status: { type: Number }
});

module.exports = mongoose.model("subscriptions", subscriptions);
