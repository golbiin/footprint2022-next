const mongoose = require("mongoose");
const tracking = new mongoose.Schema({
  login_user: { type: Boolean },
  user_id: { type: mongoose.Schema.ObjectId },
  ip: { type: String },
  date: { type: Date },
  visit_start: { type: Date },
  visit_end: { type: Date },
  page_url: { type: String },
  page_slug: { type: String },
  duration: { type: Number },
  status: { type: Boolean, default: true }
});

module.exports = mongoose.model("trackings", tracking);
