const mongoose = require("mongoose");
const admin = new mongoose.Schema({
  first_name: { type: String },
  last_name: { type: String },
  email: { type: String, required: true, unique: true },
  username: { type: String, required: true, unique: true },
  password: { type: String },
  profile_img: { type: String },
  role: { type: String },
  created_on: { type: Date },
  last_login: { type: Date },
  status: { type: Boolean },
});
module.exports = mongoose.model("admin", admin);
