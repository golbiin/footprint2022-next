const mongoose = require("mongoose");
const projects = new mongoose.Schema({
  user_id: { type: mongoose.Schema.ObjectId },
  project_name: { type: String },
  website: { type: String },
  added_on: { type: Date },
  status: { type: Boolean, default: true },
});
module.exports = mongoose.model("projects", projects);