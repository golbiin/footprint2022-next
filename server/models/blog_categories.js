const mongoose = require("mongoose");
const blog_categories = new mongoose.Schema({
  cat_name: { type: String },
  order: { type: Number }
});
module.exports = mongoose.model("blog_categories", blog_categories);
