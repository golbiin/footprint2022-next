const mongoose = require("mongoose");
const blog_layout = new mongoose.Schema({
  cat_name: { type: String },
  order: { type: Number }
});
module.exports = mongoose.model("blog_layout", blog_layout);
