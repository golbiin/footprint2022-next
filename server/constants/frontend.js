const FRONTEND_DEV_URLS = ["http://localhost:3000"];

const FRONTEND_PROD_URLS = [
  "https://www.toothzen.webeteerprojects.com/",
  "https://toothzen.com/"
];
// const FRONTEND_PROD_URLS = [
//   "https://toothzen.com/",
// ];

module.exports =
  process.env.NODE_ENV === "production"
    ? FRONTEND_PROD_URLS
    : FRONTEND_DEV_URLS;
