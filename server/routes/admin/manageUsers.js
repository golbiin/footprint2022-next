//load dependencies
const express = require("express");
const path = require("path");
const joi = require("joi");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcryptjs");
const moment = require("moment");
const sgMail = require("@sendgrid/mail");
var fs = require("fs");
const mustache = require("mustache");
const mongoose = require("mongoose");

//Include schema
const Users = require("../../models/users");
const workingDetails = require("../../models/working_details");
const Contacts = require("../../models/contacts");
//include joi validation methods
const Validate = require("../validations/validate");

const config = require("../../config/config.json");
const url = config.uploadURL;
const jwtKey = config.jwsPrivateKey;
const sendGridKey = config.sendGridKey;
const fromMail = config.fromEmail;

//http request port set
const router = express.Router();
router.post("/getAllUser", async (req, res) => {
  const token = req.body.token;
  const number_of_pages = req.body.data.number_of_pages;
  const active_page = req.body.data.activePage;
  //search//

  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);

      try {
        let key = req.body.data.key;
        let condition = [];
        let type = false;
        let conditions = {
          $and: [{ deleteStatus: type }] 
        };
        if (key != "") {
          condition = [
            {
              "profile.name": {
                $regex: new RegExp(key, "", "i")
              }
            },
            {
              email: {
                $regex: new RegExp(key, "i")
              }
            },
            {
              email: key
            }
          ];
        }
        if (condition.length > 0) {
          conditions = {
            $and: [
              {
                $or: condition
              },
              { deleteStatus: type }
            ]
          };
        } else {
          conditions = {
            $and: [{ deleteStatus: type }]
          };
        }

        Users.find(conditions, async function(err, log) {
          if (err) {
            res.send({
              status: 0,
              message: "Oops! " + err.name + ": " + err.message
            });
          } else {
            res.send({
              status: 1,
              message: "Sucess!",
              data: log
            });
          }
        })
          .sort({ created_on: -1 })
          .skip(number_of_pages * (active_page - 1))
          .limit(number_of_pages);
      } catch (err) {
        res.send({
          status: 0,
          message: "Oops! " + err.name + ": " + err.message
        });
      }
    } catch (err) {
      res.send({
        status: 0,
        message: "Oops! " + err.name + ": " + err.message
      });
    }
  }
});
router.post("/exportData", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      try {
        Users.find(async function(err, log) {
          if (err) {
            res.send({
              status: 0,
              message: "Oops! " + err.name + ": " + err.message
            });
          } else {
            res.send({
              status: 1,
              message: "Sucess!",
              data: log
            });
          }
        });
      } catch (err) {
        res.send({
          status: 0,
          message: "Oops! " + err.name + ": " + err.message
        });
      }
    } catch (err) {
      res.send({
        status: 0,
        message: "Oops! " + err.name + ": " + err.message
      });
    }
  }
});
router.post("/changeStatus", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      try {
        var condition = { _id: req.body.user_id };
        var update_value = {
          status: req.body.status
        };
        Users.findByIdAndUpdate(condition, update_value, async function(
          err,
          log
        ) {
          if (err) {
            res.send({
              status: 0,
              message: "Oops! " + err.name + ": " + err.message
            });
          } else {
            res.send({
              status: 1,
              message: "Sucess!"
            });
          }
        });
      } catch (err) {
        res.send({
          status: 0,
          message: "Oops! " + err.name + ": " + err.message
        });
      }
    } catch (err) {
      res.send({
        status: 0,
        message: "Oops! " + err.name + ": " + err.message
      });
    }
  }
});

router.post("/deleteUser", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      try {
        var condition = { _id: req.body.user_id };
        var update_value = {
          deleteStatus: true
        };
        Users.findByIdAndUpdate(condition, update_value, async function(
          err,
          log
        ) {
          if (err) {
            res.send({
              status: 0,
              message: "Oops! " + err.name + ": " + err.message
            });
          } else {
            res.send({
              status: 1,
              message: "Sucess!"
            });
          }
        });
      } catch (err) {
        res.send({
          status: 0,
          message: "Oops! " + err.name + ": " + err.message
        });
      }
    } catch (err) {
      res.send({
        status: 0,
        message: "Oops! " + err.name + ": " + err.message
      });
    }
  }
});
router.post("/getCount", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      try {
        let key = req.body.data.key;
        let condition = [];
        let type = false;
        let conditions = {
          $and: [{ deleteStatus: type }]
        };
        if (key != "") {
          condition = [
            {
              "profile.name": {
                $regex: new RegExp(key, "", "i")
              }
            },
            {
              email: {
                $regex: new RegExp(key, "i")
              }
            },
            {
              email: key
            }
          ];
        }
        if (condition.length > 0) {
          conditions = {
            $and: [
              {
                $or: condition
              },
              { deleteStatus: type }
            ]
          };
        } else {
          conditions = {
            $and: [{ deleteStatus: type }]
          };
        }
        Users.countDocuments(conditions, async function(err, log) {
          if (err) {
            res.send({
              status: 0,
              message: "Oops! " + err.name + ": " + err.message
            });
          } else {
            res.send({
              status: 1,
              message: "success",
              data: { total: log }
            });
          }
        });
      } catch (err) {
        res.send({
          status: 0,
          message: "Oops! " + err.name + ": " + err.message
        });
      }
    } catch (err) {
      res.send({
        status: 0,
        message: "Oops! " + err.name + ": " + err.message
      });
    }
  }
});
router.post("/getWorkingHoursCount", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      try {
        workingDetails.countDocuments(
          { user_id: mongoose.Types.ObjectId(req.body.user_id) },
          async function(err, log) {
            if (err) {
              res.send({
                status: 0,
                message: "Oops! " + err.name + ": " + err.message
              });
            } else {
              res.send({
                status: 1,
                message: "success",
                data: { total: log }
              });
            }
          }
        );
      } catch (err) {
        res.send({
          status: 0,
          message: "Oops! " + err.name + ": " + err.message
        });
      }
    } catch (err) {
      res.send({
        status: 0,
        message: "Oops! " + err.name + ": " + err.message
      });
    }
  }
});

router.post("/getSingleUser", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      try {
        Users.findOne(
          { _id: req.body.user_id },
          { _id: 1, email: 1, billing: 1 },
          async function(err, log) {
            if (err) {
              res.send({
                status: 0,
                message: "Oops! " + err.name + ": " + err.message
              });
            } else {
              res.send({
                status: 1,
                message: "success",
                data: { user_data: log }
              });
            }
          }
        );
      } catch (err) {
        res.send({
          status: 0,
          message: "Oops! " + err.name + ": " + err.message
        });
      }
    } catch (err) {
      res.send({
        status: 0,
        message: "Oops! " + err.name + ": " + err.message
      });
    }
  }
});

router.post("/updateWorkingHours", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      console.log(
        "new Date(req.body.data.date)",
        moment(req.body.data.date).format("YYYY-MM-DD")
      );
      //user existance check
      workingDetails.findOne(
        {
          project_id: mongoose.Types.ObjectId(req.body.project_id),
          report_date: moment(req.body.data.date).format("YYYY-MM-DD")
        },
        async function(error, log) {
          if (log != null) {
            console.log("log", log);
            try {
              var post_status = req.body.data.postDetails != "" ? 1 : 0;
              var update_value = {
                working_hours: req.body.data.workHours,
                post_title: req.body.data.post_title,
                post_details: req.body.data.postDetails,
                post_status: post_status
              };

              var condition = { _id: log._id };

              workingDetails.findByIdAndUpdate(
                condition,
                update_value,
                async function(err, log) {
                  if (err) {
                    res.send({
                      status: 0,
                      message: "Oops! " + err.name + ": " + err.message
                    });
                  } else {
                    res.send({
                      status: 1,
                      message: "Working Details updated successfully."
                    });
                  }
                }
              );
            } catch (err) {
              res.send({
                status: 0,
                message: "Oops! " + err.name + ": " + err.message
              });
            }
          } else {
            if (req.body.data) {
              var post_status = req.body.data.postDetails != "" ? 1 : 0;
              const newDetails = new workingDetails({
                user_id: req.body.user_id,
                project_id: req.body.project_id,
                report_date: moment(req.body.data.date).format("YYYY-MM-DD"),
                working_hours: req.body.data.workHours,
                post_title: req.body.data.post_title,
                post_details: req.body.data.postDetails,
                post_status: post_status,
                status: 1
              });
              try {
                await newDetails
                  .save()
                  .then(() => {
                    res.send({
                      status: 1,
                      message: "Working Details added successfully."
                    });
                  })
                  .catch(err => {
                    res.send({
                      status: 0,
                      message: "Oops! " + err.name + ": " + err.message
                    });
                  });
              } catch (err) {
                res.send({
                  status: 0,
                  message: "Oops! " + err.name + ": " + err.message
                });
              }
            }
          }
        }
      );
    } catch (err) {
      res.send({
        status: 0,
        message: "Oops! " + err.name + ": " + err.message
      });
    }
  }
});

router.post("/getWorkingDetailsList", async (req, res) => {
  const token = req.body.token;

  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);

      try {
        workingDetails
          .aggregate([
            { $match: { project_id: mongoose.Types.ObjectId(req.body.project_id) } },
            { $sort: { report_date: -1 } },
            {
              $lookup: {
                from: "projects",
                localField: "project_id",
                foreignField: "_id",
                as: "project"
              }
            }
          ])
          .exec((err, workingDetails) => {
            if (err) {
              res.send({
                status: 0,
                message: "Oops! " + err.name + ": " + err.message
              });
            } else if (workingDetails != "") {
              res.send({
                status: 1,
                message: "workingDetails fetched sucessfuly!",
                data: workingDetails
              });
            } else {
              res.send({
                status: 0,
                message: "No data found"
              });
            }
          });
      } catch (err) {
        res.send({
          status: 0,
          message: "Oops! " + err.name + ": " + err.message
        });
      }
    } catch (err) {
      res.send({
        status: 0,
        message: "Oops! " + err.name + ": " + err.message
      });
    }
  }
});

router.post("/getWorkingDetails", async (req, res) => {
  const token = req.body.token;

  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      try {
        workingDetails.findOne(
          {
            project_id: mongoose.Types.ObjectId(req.body.project_id),
            report_date: moment(req.body.report_date).format("YYYY-MM-DD")
          },
          async function(err, log) {
            console.log("log", log);
            if (err) {
              res.send({
                status: 0,
                message: "Oops! " + err.name + ": " + err.message
              });
            } else {
              res.send({
                status: 1,
                message: "Sucess!",
                data: log
              });
            }
          }
        );
      } catch (err) {
        res.send({
          status: 0,
          message: "Oops! " + err.name + ": " + err.message
        });
      }
    } catch (err) {
      res.send({
        status: 0,
        message: "Oops! " + err.name + ": " + err.message
      });
    }
  }
});

router.post("/updateUser", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      try {
        var update_value = {
          billing: req.body.data.billing
        };

        var condition = { _id: req.body.data._id };

        Users.findByIdAndUpdate(condition, update_value, async function(
          err,
          log
        ) {
          if (err) {
            res.send({
              status: 0,
              message: "Oops! " + err.name + ": " + err.message
            });
          } else {
            res.send({
              status: 1,
              message: "User details changed successfully.",
              data: { user_data: log }
            });
          }
        });
      } catch (err) {
        res.send({
          status: 0,
          message: "Oops! " + err.name + ": " + err.message
        });
      }
    } catch (err) {
      res.send({
        status: 0,
        message: "Oops! " + err.name + ": " + err.message
      });
    }
  }
});
router.post("/searchUser", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      let key = req.body.data.key;
      let type = req.body.data.type;
      let condition = [];
      if (type === "dentist") {
        condition = [
          {
            "profile_data.dname": {
              $regex: new RegExp(key, "", "i")
            }
          },
          {
            "profile_data.dtitle": {
              $regex: new RegExp(key, "i")
            }
          },
          {
            email: key
          }
        ];
      } else if (type === "lab") {
        condition = [
          {
            "profile_data.labName": {
              $regex: new RegExp(key, "i")
            }
          },
          {
            email: key
          }
        ];
      } else if (type === "patient") {
        condition = [
          {
            first_name: {
              $regex: new RegExp(key, "i")
            }
          },
          {
            last_name: {
              $regex: new RegExp(key, "i")
            }
          },
          {
            email: key
          }
        ];
      }
      try {
        Users.find(
          {
            $and: [
              {
                $or: condition
              },
              { account_type: type }
            ]
          },
          async function(err, log) {
            if (err) {
              res.send({
                status: 0,
                message: "Oops! " + err.name + ": " + err.message
              });
            } else {
              if (log.length > 0) {
                res.send({
                  status: 1,
                  message: "Fetched Dentist successfully.",
                  data: log
                });
              } else {
                res.send({
                  status: 0,
                  message: "No dentist available."
                });
              }
            }
          }
        ).sort({ _id: 1 });
      } catch (err) {
        res.send({
          status: 0,
          message: "Oops! " + err.name + ": " + err.message
        });
      }
    } catch (err) {
      res.send({
        status: 0,
        message: "Oops! " + err.name + ": " + err.message
      });
    }
  }
});

router.post("/getContactRequest", async (req, res) => {
  const token = req.body.token;
  const number_of_pages = req.body.data.number_of_pages;
  const active_page = req.body.data.activePage;
  //search//

  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      try {
        Contacts.find(async function(err, log) {
          if (err) {
            res.send({
              status: 0,
              message: "Oops! " + err.name + ": " + err.message
            });
          } else {
            res.send({
              status: 1,
              message: "Sucess!",
              data: log
            });
          }
        })
          .skip(number_of_pages * (active_page - 1))
          .limit(number_of_pages)
          .sort({ _id: -1 });
      } catch (err) {
        res.send({
          status: 0,
          message: "Oops! " + err.name + ": " + err.message
        });
      }
    } catch (err) {
      res.send({
        status: 0,
        message: "Oops! " + err.name + ": " + err.message
      });
    }
  }
});

router.post("/getSingleRequest", async (req, res) => {
  const token = req.body.token;

  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);

      try {
        Contacts.find({ _id: req.body.data }, async function(err, log) {
          if (err) {
            res.send({
              status: 0,
              message: "Oops! " + err.name + ": " + err.message
            });
          } else {
            if (!log.viewd_status) {
              Contacts.findByIdAndUpdate(
                { _id: req.body.data },
                { viewd_status: true },
                async function(err, logs) {
                  res.send({
                    status: 1,
                    message: "Sucess!",
                    data: log
                  });
                }
              );
            } else {
              res.send({
                status: 1,
                message: "Sucess!",
                data: log
              });
            }
          }
        });
      } catch (err) {
        res.send({
          status: 0,
          message: "Oops! " + err.name + ": " + err.message
        });
      }
    } catch (err) {
      res.send({
        status: 0,
        message: "Oops! " + err.name + ": " + err.message
      });
    }
  }
});

module.exports = router;
