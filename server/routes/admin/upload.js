//load dependencies
const express = require("express");
const path = require("path");

const jwt = require("jsonwebtoken");
const multer = require("multer");
const AWS = require("aws-sdk");
var fs = require("fs");
const config = require("../../config/config.json");
const url = config.uploadURL;
const jwtKey = config.jwsPrivateKey;

/*******************upload path setting******/
var storage = multer.diskStorage({
  destination: function(req, file, cb) {
    cb(null, url);
  },
  filename: function(req, file, cb) {
    cb(null, Date.now() + "-" + file.originalname);
  }
});
var upload = multer({
  storage: storage
}).single("file");
/*******************end upload path setting*/
//http request port set
const router = express.Router();
router.post("/uploadData/:token", async (req, res) => {
  // console.log("dd");
  const token = req.params.token;
  if (!token) {
    res.send({
      status: 0,
      message: "invalid request"
    });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      const user_id = payload._id;
      try {
        await upload(req, res, async function(err) {
          if (err instanceof multer.MulterError) {
            res.send({
              status: 0,
              message:
                "Oops! something went wrong with uploading profile image."
            });
          } else if (err) {
            res.send({
              status: 0,
              message: "Oops! " + err.name + ": " + err.message
            });
          } else {
            try {
              await awsUpload(req, result => {
                if (result.message) {
                  //error
                  return res
                    .status(500)
                    .json({ status: 0, message: result.message });
                } else {
                  //sucess
                  const path = url + req.file.filename + "";
                  fs.unlinkSync(path);
                  res.send({
                    status: 1,
                    message: "File uploaded",
                    data: { file_location: result }
                  });
                }
              });
            } catch (err) {
              res.send({
                status: 0,
                message: "Oops! " + err.name + ": " + err.message
              });
            }
          }
        });
      } catch (err) {
        res.send({
          status: 0,
          message: "Oops! " + err.name + ": " + err.message
        });
      }
    } catch (err) {
      res.send({
        status: 0,
        message: "Oops! " + err.name + ": " + err.message
      });
    }
  }
});
function awsUpload(req, callback) {
  AWS.config.update({
    accessKeyId: config.accessKeyId,
    secretAccessKey: config.secretAccessKey
  });
  var s3 = new AWS.S3();
  var filePath = url + req.file.filename + "";
  //configuring parameters
  var params = {
    Bucket: config.BucketName,
    Body: fs.createReadStream(filePath),
    Key: "portfolio/" + Date.now() + "_" + req.file.filename,
    ACL: "public-read"
  };

  s3.upload(params, function(err, data) {
    //handle error
    if (err) {
      callback(err);
    }
    //success
    if (data) {
      console.log("entry1");
      callback(data.Location);
    }
  });
}

module.exports = router;
