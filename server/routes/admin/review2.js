//load dependencies
const express = require("express");
const path = require("path");
const joi = require("joi");
const jwt = require("jsonwebtoken");
var fs = require("fs");
const mongoose = require("mongoose");
const config = require("../../config/config.json");
const jwtKey = config.jwsPrivateKey;
//Include schema
const Reviews = require("../../models/reviews");
const Portfolio_categories = require("../../models/portfolioCategories");
var ObjectId = require("mongodb").ObjectID;
//http request port set
const router = express.Router();

router.post("/getCategories", async (req, res) => {
  const token = req.body.token;

  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      try {
        Portfolio_categories.find(async function(err, log) {
          if (err) {
            res.send({
              status: 0,
              message: "Oops! " + err.name + ": " + err.message
            });
          } else {
            res.send({
              status: 1,
              message: "Sucess!",
              data: log
            });
          }
        });
      } catch (err) {
        res.send({
          status: 0,
          message: "Oops! " + err.name + ": " + err.message
        });
      }
    } catch (err) {
      res.send({
        status: 0,
        message: "Oops! " + err.name + ": " + err.message
      });
    }
  }
});

router.post("/getreviews", async (req, res) => {
  const token = req.body.token;

  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      let limit = req.body.pages;
      let active_page = req.body.activePage;
      try {
        Reviews.find(async function(err, log) {
          if (err) {
            res.send({
              status: 0,
              message: "Oops! " + err.name + ": " + err.message
            });
          } else {
            res.send({
              status: 1,
              message: "Sucess!",
              data: log
            });
          }
        })
          .sort({ title: 1 })
          .skip(limit * (active_page - 1))
          .limit(limit);
      } catch (err) {
        res.send({
          status: 0,
          message: "Oops! " + err.name + ": " + err.message
        });
      }
    } catch (err) {
      res.send({
        status: 0,
        message: "Oops! " + err.name + ": " + err.message
      });
    }
  }
});

router.post("/addreview", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      try {
        let portfolio = req.body.data;
        let subCats = req.body.sub_cat;
        let language = req.body.language;
        let category_array = [];
        let language_array = [];
        subCats.map((cat, cindex) => {
          let new_id = ObjectId(cat.id);
          let category_array_item = {
            name: cat.name,
            id: new_id,
            status: cat.status
          };
          category_array.push(category_array_item);
        });
        language.map((lang, language_index) => {
          let new_id = ObjectId(lang.id);
          let lng_array_item = {
            name: lang.name,
            id: new_id,
            status: lang.status
          };
          language_array.push(lng_array_item);
        });
        Reviews.map((item, index) => {
          const newPortfolio = new Portfolio({
            title: item.title,
            url: item.url,
            portfolio_image: item.image,
            main_category: req.body.main_cat,
            sub_category: category_array,
            languages: language_array,
            order: 999 //default order
          });

          try {
            newPortfolio
              .save()
              .then(() => {
                res.send({
                  status: 1,
                  message: "Portfolio added successfully."
                });
              })
              .catch(err => {
                res.send({
                  status: 0,
                  message: "Oops! " + err.name + ": " + err.message
                });
              });
          } catch (err) {
            res.send({
              status: 0,
              message: "Oops! " + err.name + ": " + err.message
            });
          }
        });
      } catch (err) {
        res.send({
          status: 0,
          message: "Oops! " + err.name + ": " + err.message
        });
      }
    } catch (err) {
      res.send({
        status: 0,
        message: "Oops! " + err.name + ": " + err.message
      });
    }
  }
});

router.post("/updatereview", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      try {
        var condition = { _id: req.body.id };
        var update_value = {
          sub_category: req.body.data.sub_category,
          languages: req.body.data.languages,
          title: req.body.data.title,
          url: req.body.data.url,
          portfolio_image: req.body.data.portfolio_image,
          main_category: req.body.data.main_category,
          order: req.body.data.order
        };

        Reviews.findByIdAndUpdate(condition, update_value, async function(
          err,
          log
        ) {
          if (err) {
            res.send({
              status: 0,
              message: "Oops! " + err.name + ": " + err.message
            });
          } else {
            res.send({
              status: 1,
              message: "Portfolio updated"
            });
          }
        });
      } catch (err) {
        res.send({
          status: 0,
          message: "Oops! " + err.name + ": " + err.message
        });
      }
    } catch (err) {
      res.send({
        status: 0,
        message: "Oops! " + err.name + ": " + err.message
      });
    }
  }
});

router.post("/deletereview", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      try {
        Reviews.deleteOne({ _id: req.body.data }, async function(err, log) {
          if (err) {
            res.send({
              status: 0,
              message: "Oops! " + err.name + ": " + err.message
            });
          } else {
            res.send({
              status: 1,
              message: "Sucess!"
            });
          }
        });
      } catch (err) {
        res.send({
          status: 0,
          message: "Oops! " + err.name + ": " + err.message
        });
      }
    } catch (err) {
      res.send({
        status: 0,
        message: "Oops! " + err.name + ": " + err.message
      });
    }
  }
});
router.post("/getCount", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      try {
        Reviews.countDocuments(async function(err, log) {
          if (err) {
            res.send({
              status: 0,
              message: "Oops! " + err.name + ": " + err.message
            });
          } else {
            res.send({
              status: 1,
              message: "success",
              data: { total: log }
            });
          }
        });
      } catch (err) {
        res.send({
          status: 0,
          message: "Oops! " + err.name + ": " + err.message
        });
      }
    } catch (err) {
      res.send({
        status: 0,
        message: "Oops! " + err.name + ": " + err.message
      });
    }
  }
});
module.exports = router;
