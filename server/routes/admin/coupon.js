//load dependencies
const express = require("express");
const path = require("path");
const joi = require("joi");
const jwt = require("jsonwebtoken");
const mongoose = require("mongoose");
const moment = require("moment");
const config = require("../../config/config.json");
const jwtKey = config.jwsPrivateKey;
const sendGridKey = config.sendGridKey;
const fromMail = config.fromEmail;

//Include schema
const Coupons = require("../../models/coupons");
const Users = require("../../models/users");
const Wallet = require("./../../models/wallet");
//http request port set
const router = express.Router();
var fs = require("fs");
const mustache = require("mustache");
const sgMail = require("@sendgrid/mail");
var momentTimezone = require('moment-timezone');

router.post("/", async (req, res) => {
  const token = req.body.token;

  if (!token) {
    res.send({ status: 0, message: "Sorry, session expired" });
  } else {
    try {
      let payload = jwt.verify(token, jwtKey);
      let limit = req.body.pages;
      let active_page = req.body.activePage;
      let skip = limit * (active_page - 1);
      console.log(limit);
      try {
        Coupons.aggregate([
          { $sort: { _id: -1 } },
          { $limit: skip + limit },
          { $skip: skip },
          {
            $lookup: {
              from: "users",
              localField: "user",
              foreignField: "_id",
              as: "users"
            }
          },
          {
            $unwind: "$users"
          },
          {
            $project: {
              _id: 1,
              dentist_id: 1,
              coupon_code: 1,
              coupon_amount: 1,
              redeem_status: 1,
              expire_on: 1,
              "users.profile": 1
            }
          }
        ]).exec((err, coupons) => {
          if (err) {
            res.send({
              status: 0,
              message: "Oops! " + err.name + ": " + err.message
            });
          } else if (coupons != "") {
            res.send({
              status: 1,
              message: "Coupons Fectched sucessfully!",
              data: coupons
            });
          }
        });
      } catch (err) {
        res.send({
          status: 2,
          message: "Oops! " + err.name + ": " + err.message
        });
      }
    } catch (err) {
      res.send({
        status: 2,
        message: "Oops! " + err.name + ": " + err.message
      });
    }
  }
});

router.post("/getAllUser", async (req, res) => {
  const token = req.body.token;

  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);

      try {
        Users.find(
          { deleteStatus: false, status: true },
          { _id: 1, profile: 1 },
          async function(err, log) {
            if (err) {
              res.send({
                status: 0,
                message: "Oops! " + err.name + ": " + err.message
              });
            } else {
              res.send({
                status: 1,
                message: "Sucess!",
                data: log
              });
            }
          }
        );
      } catch (err) {
        res.send({
          status: 0,
          message: "Oops! " + err.name + ": " + err.message
        });
      }
    } catch (err) {
      res.send({
        status: 0,
        message: "Oops! " + err.name + ": " + err.message
      });
    }
  }
});

router.post("/addCpoupon", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      try {
        const newCoupon = new Coupons({
          user: req.body.data.user,
          coupon_code: req.body.data.coupon_code,
          coupon_amount: req.body.data.coupon_amount,
          redeem_date: req.body.data.redeem_date,
          created_on: moment().format("MM-DD-YYYY hh:mm:ss"),
          expire_on: req.body.data.expire_on
        });
        try {
          newCoupon
            .save()
            .then(() => {
              Users.find(
                { _id: req.body.data.user },
                { email: 1, profile: 1 },
                async function(err, log) {
                  if (log != null) {
                    res.send({
                      status: 1,
                      message: "Coupon added successfully."
                    });
                    // let sendData = {
                    //   email: log[0].email,
                    //   coupon: req.body.data.coupon_code,
                    //   name:
                    //     log[0].profile.first_name +
                    //     " " +
                    //     log[0].profile.last_name,
                    //   amount: req.body.data.coupon_amount,
                    //   expire: req.body.data.expire_on
                    // };
                    // sendCouponmail(sendData, result => {
                    //   if (result == "success") {
                    //     res.send({
                    //       status: 1,
                    //       message: "Coupon send to user."
                    //     });
                    //   } else if (result == "failed") {
                    //     res.send({
                    //       status: 1,
                    //       message: "Failed to send coupon mail"
                    //     });
                    //   }
                    // });
                  } else if (err) {
                    res.send({
                      status: 0,
                      message: "Oops! " + err.name + ": " + err.message
                    });
                  }
                }
              );
            })
            .catch(err => {
              res.send({
                status: 0,
                message: "Oops! " + err.name + ": " + err.message
              });
            });
        } catch (err) {
          res.send({
            status: 0,
            message: "Oops! " + err.name + ": " + err.message
          });
        }
      } catch (err) {
        res.send({
          status: 0,
          message: "Oops! " + err.name + ": " + err.message
        });
      }
    } catch (err) {
      res.send({
        status: 0,
        message: "Oops! " + err.name + ": " + err.message
      });
    }
  }
});

router.post("/deleteCoupon", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      try {
        Coupons.deleteOne({ _id: req.body.data }, async function(err, log) {
          if (err) {
            res.send({
              status: 0,
              message: "Oops! " + err.name + ": " + err.message
            });
          } else {
            res.send({
              status: 1,
              message: "Sucess!"
            });
          }
        });
      } catch (err) {
        res.send({
          status: 0,
          message: "Oops! " + err.name + ": " + err.message
        });
      }
    } catch (err) {
      res.send({
        status: 0,
        message: "Oops! " + err.name + ": " + err.message
      });
    }
  }
});

router.post("/getCount", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      try {
        Coupons.countDocuments(async function(err, log) {
          if (err) {
            res.send({
              status: 0,
              message: "Oops! " + err.name + ": " + err.message
            });
          } else {
            res.send({
              status: 1,
              message: "success",
              data: { total: log }
            });
          }
        });
      } catch (err) {
        res.send({
          status: 0,
          message: "Oops! " + err.name + ": " + err.message
        });
      }
    } catch (err) {
      res.send({
        status: 0,
        message: "Oops! " + err.name + ": " + err.message
      });
    }
  }
});

function sendCouponmail(newCoupon, callback) {
  let template = fs
    .readFileSync(path.join(__dirname + "/mail-templates/coupon.html"), {
      encootding: "utf-8"
    })
    .toString();
  //let urlpass = config.siteurl + "verify/?id=" + newUser._id + "";
  var view = {
    name: newCoupon.name,
    coupon_code: newCoupon.coupon,
    amount: newCoupon.amount,
    imageUrl: config.imageUrl,
    expire: moment(newCoupon.expire).format("MM-DD-YYYY hh:mm:ss")
  };
  var createTemplate = mustache.render(template, view);
  sgMail.setApiKey(sendGridKey);
  console.log(newCoupon);
  const message = {
    to: newCoupon.email,
    from: { email: fromMail, name: "OutSourceMe" },
    subject: "OutsourceMe Discount Coupon",
    html: createTemplate
  };

  try {
    sgMail
      .send(message)
      .then(() => {
        callback("success");
        console.log("success");
      })
      .catch(error => {
        // //Log friendly error
        console.error("send err", error.toString());
        // res.status(400).send(err);
        // //Extract error msg
        const { message, code, response } = error;

        // //Extract response msg
        const { headers, body } = response;
        console.log(message, body.errors);
        callback("failed");
      });
  } catch (err) {
    callback("failed");
    console.log("Error show", err);
  }
}

/******** User Wallet ********/

router.post("/transcations", async (req, res) => {
  const token = req.body.token;
  const user_id = req.body.user_id;
  if (!token) {
    res.send({ status: 0, message: "Sorry session expired" });
  } else {
    try {

      try {
        Wallet.find({ user_id: user_id, status:true }, async function(err, log) {
          if (err) {
            res.send({
              status: 0,
              message: "Oops! " + err.name + ": " + err.message
            });
          } else {
            res.send({
              status: 1,
              message: "success",
              data: log
            });
          }
        }).sort({ _id: -1 });
      } catch (err) {}
    } catch (err) {}
  }
});

router.post("/wallet-action", async (req, res) => {
  const token = req.body.token;
  const data = req.body.data;
  const timezone = req.body.timezone;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
        
        userId = data.user;
        try {

          Wallet.findOne(
            { user_id: mongoose.Types.ObjectId(userId) },
            async function(err, logExist) {
              if (err) {
                res.send({
                  status: 0,
                  message: "Oops! " + err.name + ": " + err.message
                });
              } else {
      
                let currentAmount = parseFloat(data.coupon_amount);
                let totalBalance = currentAmount;
                let transacion_type = data.transacion_type;
                if(logExist !== null){
                  if(transacion_type === "credit")
                    totalBalance = currentAmount + parseFloat(logExist.balance);
                  else if(transacion_type === "debit")
                    totalBalance = parseFloat(logExist.balance) - currentAmount;
                }
                else{
                  transacion_type = "credit";
                }
                const newWallet = new Wallet({
                  user_id: userId,
                  amount: currentAmount,
                  balance: totalBalance,
                  transacion_type: transacion_type,
                  added_by : "admin",
                  added_on: momentTimezone().tz(timezone).format("MM-DD-YYYY hh:mm:ss")
                });
                try {
                  newWallet
                    .save()
                    .then(() => {

                      res.send({
                        status: 1,
                        message: "Wallet Added successfully.",
                        data: newWallet
                      });
                    })
                    .catch(err => {
                      res.send({
                        status: 0,
                        message: "Oops! " + err.name + ": " + err.message
                      });
                    });
                } catch (err) {
                  res.send({
                    status: 0,
                    message: "Oops! " + err.name + ": " + err.message
                  });
                }
                    
                    
                  }
                }
                ).sort({ _id: -1 });
                
                
              } catch (err) {
                res.send({
            status: 0,
            message: "Oops! " + err.name + ": " + err.message
          });
        }
                
    } catch (err) {
      res.send({
        status: 0,
        message: "Oops! " + err.name + ": " + err.message
      });
    }
  }
});


module.exports = router;
