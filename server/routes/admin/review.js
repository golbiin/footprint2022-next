//load dependencies
const express = require('express')
const path = require('path')
const joi = require('joi')
const jwt = require('jsonwebtoken')
var fs = require('fs')
const mongoose = require('mongoose')
const config = require('../../config/config.json')
const jwtKey = config.jwsPrivateKey
//Include schema
const Blog = require('../../models/blogs')
const Review = require('../../models/reviews')
const Blog_categories = require('../../models/blog_categories')
const BlogLayout = require('../../models/blog_layout')
const BlogAuthors = require('../../models/blog_authors')
var ObjectId = require('mongodb').ObjectID;
const moment = require('moment')
//http request port set
const router = express.Router()

router.post('/createReview', async (req, res) => {
  const token = req.body.token
  if (!token) {
    res.send({ status: 0, message: 'Invalid Request' })
  } else {
    try {
      payload = jwt.verify(token, jwtKey)
      try {
        let review = req.body.data
         let time = moment().format('MM-DD-YYYY hh:mm:ss');
        const newReview = new Review({
          user_name: review.user_name.trim(),
          testimony: review.testimony.trim(),
          rating: review.rating.trim(),
          user_image: review.image,
          created_on: time,
          updated_on: time,
          status: true
        })
        try {
          newReview
            .save()
            .then(() => {
              res.send({
                status: 1,
                message: 'Review added successfully.',
                data: {
                  _id: newReview._id,
                }
              })
            })
            .catch(err => {
              res.send({
                status: 0,
                message: 'Oops! ' + err.name + ': ' + err.message
              })
            })
        } catch (err) {
          res.send({
            status: 0,
            message: 'Oops! ' + err.name + ': ' + err.message
          })
        }
      } catch (err) {
        res.send({
          status: 0,
          message: 'Oops! ' + err.name + ': ' + err.message
        })
      }
    } catch (err) {
      res.send({
        status: 0,
        message: 'Oops! ' + err.name + ': ' + err.message
      })
    }
  }
})

router.post('/getAllBlogs', async (req, res) => {
  const token = req.body.token
  const number_of_pages = req.body.data.number_of_pages
  const active_page = req.body.data.activePage
  //search//

  if (!token) {
    res.send({ status: 0, message: 'Invalid Request' })
  } else {
    try {
      payload = jwt.verify(token, jwtKey)

      try {
        let key = req.body.data.key
        let condition = []
        let type = false
        let conditions = {
          $and: [{ deleteStatus: type }]
        }
        if (key != '') {
          condition = [
            {
              user_name: {
                $regex: new RegExp(key, '', 'i')
              }
            },
            {
              user_name: {
                $regex: new RegExp(key, 'i')
              }
            },
            {
              user_name: key
            }
          ]
        }
        if (condition.length > 0) {
          conditions = {
            $and: [
              {
                $or: condition
              },
              { deleteStatus: type }
            ]
          }
        } else {
          conditions = {
            $and: [{ deleteStatus: type }]
          }
        }
       
        Review.find(conditions, async function (err, log) {
          if (err) {
            res.send({
              status: 0,
              message: 'Oops! ' + err.name + ': ' + err.message
            })
          } else {
            res.send({
              status: 1,
              message: 'Success!',
              data: log
            })
          }
        })
          .sort({ created_on: -1 })
          .skip(number_of_pages * (active_page - 1))
          .limit(number_of_pages)
      } catch (err) {
        res.send({
          status: 0,
          message: 'Oops! ' + err.name + ': ' + err.message
        })
      }
    } catch (err) {
      res.send({
        status: 0,
        message: 'Oops! ' + err.name + ': ' + err.message
      })
    }
  }
});

router.post('/getCount', async (req, res) => {
  const token = req.body.token
  if (!token) {
    res.send({ status: 0, message: 'Invalid Request' })
  } else {
    try {
      payload = jwt.verify(token, jwtKey)
      try {
        let key = req.body.data.key
        let condition = []
        let type = false
        let conditions = {
          $and: [{ deleteStatus: type }]
        }
        if (key != '') {
          condition = [
            {
              user_name: {
                $regex: new RegExp(key, '', 'i')
              }
            },
            {
              user_name: {
                $regex: new RegExp(key, 'i')
              }
            },
            {
              user_name: key
            }
          ]
        }
        if (condition.length > 0) {
          conditions = {
            $and: [
              {
                $or: condition
              },
              { deleteStatus: type }
            ]
          }
        } else {
          conditions = {
            $and: [{ deleteStatus: type }]
          }
        }

        Review.countDocuments(conditions, async function (err, log) {
          if (err) {
            res.send({
              status: 0,
              message: 'Oops! ' + err.name + ': ' + err.message
            })
          } else {
            res.send({
              status: 1,
              message: 'success',
              data: { total: log }
            })
          }
        })
      } catch (err) {
        res.send({
          status: 0,
          message: 'Oops! ' + err.name + ': ' + err.message
        })
      }
    } catch (err) {
      res.send({
        status: 0,
        message: 'Oops! ' + err.name + ': ' + err.message
      })
    }
  }
})

router.post('/blogsChangeStatus', async (req, res) => {
  const token = req.body.token
  if (!token) {
    res.send({ status: 0, message: 'Invalid Request' })
  } else {
    try {
      payload = jwt.verify(token, jwtKey)
      try {
        var condition = { _id: req.body.blog_id }
        var update_value = {
          status: req.body.status
        }
        Review.findByIdAndUpdate(condition, update_value, async function (
          err,
          log
        ) {
          if (err) {
            res.send({
              status: 0,
              message: 'Oops! ' + err.name + ': ' + err.message
            })
          } else {
            res.send({
              status: 1,
              message: 'Success!'
            })
          }
        })
      } catch (err) {
        res.send({
          status: 0,
          message: 'Oops! ' + err.name + ': ' + err.message
        })
      }
    } catch (err) {
      res.send({
        status: 0,
        message: 'Oops! ' + err.name + ': ' + err.message
      })
    }
  }
})

router.post('/deleteBlog', async (req, res) => {
  const token = req.body.token
  if (!token) {
    res.send({ status: 0, message: 'Invalid Request' })
  } else {
    try {
      payload = jwt.verify(token, jwtKey)
      try {
        var condition = { _id: req.body.blog_id }
        var update_value = {
          deleteStatus: true
        }
        Review.findByIdAndUpdate(condition, update_value, async function (
          err,
          log
        ) {
          if (err) {
            res.send({
              status: 0,
              message: 'Oops! ' + err.name + ': ' + err.message
            })
          } else {
            res.send({
              status: 1,
              message: 'Success!'
            })
          }
        })
      } catch (err) {
        res.send({
          status: 0,
          message: 'Oops! ' + err.name + ': ' + err.message
        })
      }
    } catch (err) {
      res.send({
        status: 0,
        message: 'Oops! ' + err.name + ': ' + err.message
      })
    }
  }
})

router.post('/getSingleBlog', async (req, res) => {
  const token = req.body.token
  if (!token) {
    res.send({ status: 0, message: 'Invalid Request' })
  } else {
    try {
      payload = jwt.verify(token, jwtKey)
      try {
        Review.findOne({ _id: req.body.blog_id }, async function (err, log) {
          if (err) {
            res.send({
              status: 0,
              message: 'Oops! ' + err.name + ': ' + err.message
            })
          } else {
            res.send({
              status: 1,
              message: 'success',
              data: log
            })
          }
        })
      } catch (err) {
        res.send({
          status: 0,
          message: 'Oops! ' + err.name + ': ' + err.message
        })
      }
    } catch (err) {
      res.send({
        status: 0,
        message: 'Oops! ' + err.name + ': ' + err.message
      })
    }
  }
})

router.post('/updateBlog', async (req, res) => {
  const token = req.body.token
  if (!token) {
    res.send({ status: 0, message: 'Invalid Request' })
  } else {
    try {
      payload = jwt.verify(token, jwtKey)
      try {
        let blog = req.body.data
        var update_value = {
          user_name: blog.user_name.trim(),
          testimony: blog.testimony.trim(),
          rating: blog.rating.trim(),
          user_image: blog.image,
          updated_on: moment().format('MM-DD-YYYY hh:mm:ss')
        }
        var condition = { _id: req.body.blog_id }
        Review.findByIdAndUpdate(condition, update_value, async function (
          err,
          log
        ) {
          if (err) {
            res.send({
              status: 0,
              message: 'Oops! ' + err.name + ': ' + err.message
            })
          } else {
            res.send({
              status: 1,
              message: 'Review updated successfully.',
              data: log
            })
          }
        })
      } catch (err) {
        res.send({
          status: 0,
          message: 'Oops! ' + err.name + ': ' + err.message
        })
      }
    } catch (err) {
      res.send({
        status: 0,
        message: 'Oops! ' + err.name + ': ' + err.message
      })
    }
  }
})

router.post('/getAllBlogCategory', async (req, res) => {
  const token = req.body.token
  if (!token) {
    res.send({ status: 0, message: 'Invalid Request' })
  } else {
    try {
      Blog_categories.find({}, async function (err, log) {
        if (err) {
          res.send({
            status: 0,
            message: 'Oops! ' + err.name + ': ' + err.message
          })
        } else {
          if (log.length > 0) {
            res.send({
              status: 1,
              message: 'Fetched Category successfully.',
              data: log
            })
          } else {
            res.send({
              status: 0,
              message: 'No Category available.'
            })
          }
        }
      }).sort({ date: -1 })
    } catch (e) {
      res.send({
        status: 0,
        message: 'Oops! ' + e.name + ': ' + e.message
      })
    }
  }
});
router.post('/getAllBlogLayout', async (req, res) => {
  const token = req.body.token
  if (!token) {
    res.send({ status: 0, message: 'Invalid Request' })
  } else {
    try {
      BlogLayout.find(async function (err, log) {
        if (err) {
          res.send({
            status: 0,
            message: 'Oops! ' + err.name + ': ' + err.message
          })
        } else {
          console.log("log", log)
          if (log.length > 0) {
            res.send({
              status: 1,
              message: 'Fetched Blog Layout successfully.',
              data: log
            })
          } else {
            res.send({
              status: 0,
              message: 'No Blog Layout available.'
            })
          }
        }
      }).sort({ date: -1 })
    } catch (e) {
      res.send({
        status: 0,
        message: 'Oops! ' + e.name + ': ' + e.message
      })
    }
  }
})
router.post('/getAllBlogAuthors', async (req, res) => {
  const token = req.body.token
  if (!token) {
    res.send({ status: 0, message: 'Invalid Request' })
  } else {
    try {
      BlogAuthors.find(async function (err, log) {
        if (err) {
          res.send({
            status: 0,
            message: 'Oops! ' + err.name + ': ' + err.message
          })
        } else {
          console.log("log", log)
          if (log.length > 0) {
            res.send({
              status: 1,
              message: 'Fetched Authors successfully.',
              data: log
            })
          } else {
            res.send({
              status: 0,
              message: 'No Authors available.'
            })
          }
        }
      }).sort({ date: -1 })
    } catch (e) {
      res.send({
        status: 0,
        message: 'Oops! ' + e.name + ': ' + e.message
      })
    }
  }
})

module.exports = router
