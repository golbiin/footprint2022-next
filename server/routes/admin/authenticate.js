//load dependencies
const express = require("express");
const path = require("path");
const joi = require("joi");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcryptjs");
const moment = require("moment");
const sgMail = require("@sendgrid/mail");
var fs = require("fs");
const mustache = require("mustache");
//Include schema
const Admin = require("../../models/admin");
//include joi validation methods
const Validate = require("../validations/validate");
const config = require("../../config/config.json");
const jwtKey = config.jwsPrivateKey;
const sendGridKey = config.sendGridKey;
const fromMail = config.fromEmail;

//http request port set
const router = express.Router();

router.post("/authenticate", async (req, res) => {
  try {
    Validate.validateadminLogin(req, result => {
      if (result.error) {
        //validation error
        res.status(400).send(result.error.details[0].message);
      } else {
        //user login check
        try {
          Admin.findOne(
            {
              $or: [
                { username: req.body.username },
                { email: req.body.username }
              ]
            },
            function(err, log) {
              if (err) {
                res.send({
                  status: 0,
                  message: "Oops! " + err.name + ": " + err.message
                });
              } else if (!log) {
                res.send({
                  status: 0,
                  message:
                    "There is no existing admin with the details that you’ve provided."
                });
              } else {
                const validatepassword = bcrypt.compareSync(
                  req.body.password,
                  log.password
                );

                if (!validatepassword) {
                  res.send({
                    status: 0,
                    message:
                      "You have entered an invalid username and/or password."
                  });
                } else {
                  
                  log.last_login = moment().format("MM-DD-YYYY hh:mm:ss");
                  log.save();
                  
                  const token = jwt.sign(
                    {
                      _id: log._id,
                      emailid: log.email,
                      first_name: log.first_name,
                      last_name: log.last_name,
                      role: log.role,
                      profile_img: log.profile_img
                    },
                    jwtKey
                  );
                  res.send({
                    status: 1,
                    message: "Login success! You will be redirected now",
                    data: { token: token }
                  });
                }
              }
            }
          );
        } catch (ex) {
          res
            .status(400)
            .send("Oops! Something went wrong please try again later");
        }
      }
    });
  } catch (err) {
    console.log(err);
    res.status(400).send(ex);
  }
});
router.post("/create_admin", async (req, res) => {
  try {
    const salt = bcrypt.genSaltSync(10);
    let createdon = moment().format("MM-DD-YYYY hh:mm:ss");
    bcryptPassword = bcrypt.hashSync(req.body.password, salt);
    const newUser = new Admin({
      first_name: req.body.first_name,
      last_name: req.body.last_name,
      email: req.body.email,
      username: req.body.username,
      password: bcryptPassword,
      created_on: createdon,
      status: 0
    });
    newUser
      .save()
      .then(() => {
        res.send("success");
      })
      .catch(err => {
        res.status(400).send(err);
      });
  } catch (err) {
    res.send(err);
  }
});

module.exports = router;
