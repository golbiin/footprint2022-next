const stripe = require("../../constants/stripe");
const express = require("express");
const jwt = require("jsonwebtoken");
const mongoose = require("mongoose");
const moment = require("moment");
var momentTimezone = require('moment-timezone');
const { v4: uuidv4 } = require("uuid");
const Payment = require("../../models/payment");
const saveCard = require("../../models/save_card");
const Users = require("../../models/users");
const Subscriptions = require("../../models/subscriptions");
const config = require("../../config/config.json");
const url = config.uploadURL;
const jwtKey = config.jwsPrivateKey;
const sendGridKey = config.sendGridKey;
const fromMail = config.fromEmail;
const async = require("async");

//http request port set
const router = express.Router();

router.post("/getAllSubscriptions", async (req, res) => {
  const token = req.body.token;
  const category = req.body.category;
  const limit = req.body.data.number_of_pages;
  const active_page = req.body.data.activePage;
  let skip = limit * (active_page - 1);
  //search//

  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      let payload = jwt.verify(token, jwtKey);
      let user_id = payload._id;
      try {
        async.parallel(
          {
            getTotalScubscriptions: function(callback) {
              let result = "";
              Subscriptions.countDocuments({ status: category }, async function(
                err,
                log
              ) {
                if (err) {
                  result = {
                    status: 0,
                    message: "Oops! " + err.name + ": " + err.message
                  };
                  callback(null, result);
                } else {
                  result = {
                    status: 1,
                    message: "success",
                    data: { total: log }
                  };
                  callback(null, result);
                }
              });
            },
            getbyLimit: function(callback) {
              Subscriptions.aggregate([
                { $match: { status: category } },
                { $sort: { expire_on: 1 } },
                { $limit: skip + limit },
                { $skip: skip },
                {
                  $lookup: {
                    from: "users",
                    localField: "user_id",
                    foreignField: "_id",
                    as: "user"
                  }
                },
                {
                  $unwind: "$user"
                },
                { $match: { "user.deleteStatus": false } },
                {
                  $project: {
                    _id: 1,
                    customer_id: 1,
                    payment_method_id: 1,
                    subscribed_on: 1,
                    expire_on: 1,
                    canceled_on:1,
                    status: 1,
                    plan: 1,
                    user_id: 1,
                    "user.billing": 1,
                    "user.profile": 1,
                    "user.deleteStatus": 1,
                    "user.email": 1
                  }
                }
              ]).exec((err, subscriptions) => {
                let result = "";
                if (err) {
                  result = {
                    status: 0,
                    message: "Oops! " + err.name + ": " + err.message
                  };
                  callback(null, result);
                } else if (subscriptions != "") {
                  result = {
                    status: 1,
                    message: "Subscriptions fetched sucessfuly!",
                    data: subscriptions
                  };
                  callback(null, result);
                } else {
                  result = {
                    status: 0,
                    message: "No data found",
                    data: subscriptions
                  };
                  callback(null, result);
                }
              });
            }
          },
          function(err, results) {
            if (err) {
              res.send({
                status: 2,
                message: "Oops! " + err.name + ": " + err.message
              });
            } else {
              res.send({ status: 1, data: results });
            }
          }
        );
      } catch (err) {
        res.send({
          status: 2,
          message: "Oops! " + err.name + ": " + err.message
        });
      }
    } catch (err) {
      res.send({
        status: 0,
        message: "Oops! " + err.name + ": " + err.message
      });
    }
  }
});

router.post("/getSingleSubscription", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      let payload = jwt.verify(token, jwtKey);
      let user_id = payload._id;
      let sub_id = req.body.id;

      try {
        Subscriptions.aggregate([
          { $match: { _id: mongoose.Types.ObjectId(sub_id) } },
          { $sort: { expire_on: 1 } },
          {
            $lookup: {
              from: "users",
              localField: "user_id",
              foreignField: "_id",
              as: "user"
            }
          },
          {
            $unwind: "$user"
          },
          {
            $project: {
              _id: 1,
              customer_id: 1,
              payment_method_id: 1,
              subscribed_on: 1,
              expire_on: 1,
              status: 1,
              plan: 1,
              user_id: 1,
              "user.billing": 1,
              "user.profile": 1
            }
          }
        ]).exec((err, subscriptions) => {
          if (err) {
            res.send({
              status: 0,
              message: "Oops! " + err.name + ": " + err.message
            });
          } else if (subscriptions != "") {
            res.send({
              status: 1,
              message: "Subscriptions fetched sucessfuly!",
              data: subscriptions
            });
          } else {
            res.send({
              status: 0,
              message: "No data found"
            });
          }
        });
      } catch (err) {
        res.send({
          status: 2,
          message: "Oops! " + err.name + ": " + err.message
        });
      }
    } catch (err) {
      res.send({
        status: 0,
        message: "Oops! " + err.name + ": " + err.message
      });
    }
  }
});

router.post("/cancelSingleSubscriptions", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      let payload = jwt.verify(token, jwtKey);
      let sub_id = req.body.id;
      //mongoose.Types.ObjectId(sub_id)
      // console.log(555, user_id);
      try {
        Payment.findOne(
          {
            subscription_id: sub_id
          },
          async function(err, log) {
            if (log) {
               console.log(333, log._id);
              const subscription_id = log.payment_response.id;
                console.log(444, subscription_id);
               const deleted = await stripe.subscriptions.del(subscription_id);
              console.log("Cancel response", deleted);
              if (deleted.status === "canceled") {
               
                var condition = { _id: sub_id };
                var update_value = {
                  status: 0,
                  canceled_on: moment().format("MM-DD-YYYY hh:mm:ss")
                };
               /*  res.send({
                  status: 1,
                  message: "Subscriptions Cancelled sucessfuly!"
                }); */

               try {
                  Subscriptions.findByIdAndUpdate(condition, update_value, async function (
                    err,
                    log
                  ) {
                    if (err) {
                      console.log(err)
                      res.send({
                        status: 0,
                        message: "Something went wrong, please try again."
                      });
                       
                    } else {
                      res.send({
                        status: 1,
                        message: "Subscriptions Cancelled sucessfuly!"
                      });
                       
                    }
                  });
                } catch (err) {
                  console.log(err)
                  res.send({
                    status: 0,
                    message: "Something went wrong, please try again."
                  });
                   
                }  

                
              } else {
                console.log("test")
                res.send({
                  status: 0,
                  message: "Something went wrong, please try again."
                });
              } 
            } else {
              console.log("test3")
              res.send({
                status: 0,
                message: "Something went wrong, please try again."
              });
            }
          }
        );
      } catch (err) {
        res.send({
          status: 2,
          message: "Oops! " + err.name + ": " + err.message
        });
      }
    } catch (err) {
      res.send({
        status: 0,
        message: "Oops! " + err.name + ": " + err.message
      });
    }
  }
});

module.exports = router;
