//load dependencies
const express = require("express");
const jwt = require("jsonwebtoken");
const moment = require("moment");
const bcrypt = require("bcryptjs");
const mongoose = require("mongoose");
//Include schema

const Admin = require("../../models/admin");
const Tracking = require("../../models/tracking");
const LoginTrack = require("../../models/login_track");
const UserActivity = require("../../models/userActivity");
const Users = require("../../models/users");
const Projects = require("../../models/projects");
const Leads = require("../../models/leads");
const Newsletter=require("../../models/newsLetter")
const config = require("../../config/config.json");
const jwtKey = config.jwsPrivateKey;

//http request port set
const router = express.Router();

router.post("/updatePassword", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      let payload = jwt.verify(token, jwtKey);
      let password = req.body.data.currentPassword;
      try {
        Admin.findOne({ _id: payload._id }, async function(err, log) {
          if (err) {
            res.send({
              status: 0,
              message: "Oops! " + err.name + ": " + err.message
            });
          } else {
            const validatepassword = bcrypt.compareSync(password, log.password);
            if (!validatepassword) {
              res.send({
                status: 0,
                message: "You have entered invalid current password"
              });
            } else {
              const salt = bcrypt.genSaltSync(10);
              let bcryptPassword = bcrypt.hashSync(
                req.body.data.newPassword,
                salt
              );
              log.password = bcryptPassword;
              log.first_name = "Admin";
              log.save();
              res.send({
                status: 1,
                message: "Password changed successfully."
              });
            }
          }
        });
      } catch (err) {
        res.send({
          status: 0,
          message: "Oops! " + err.name + ": " + err.message
        });
      }
    } catch (err) {
      res.send({
        status: 0,
        message: "Oops! " + err.name + ": " + err.message
      });
    }
  }
});
router.post("/getAllUserTrackings", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    let limit = req.body.data.number_of_pages;
    let active_page = req.body.data.activePage;
    let skip = limit * (active_page - 1);
    let condition = { status: true };
    let id = req.body.id;

    try {
      Tracking.find({ user_id: id }, async function(err, log) {
        if (err) {
          res.send({
            status: 0,
            message: "Oops! " + err.name + ": " + err.message
          });
        } else {
          res.send({
            status: 1,
            message: "Sucess!",
            data: log
          });
        }
      })
        .sort({ date: -1 })
        .skip(limit * (active_page - 1))
        .limit(limit);
    } catch (err) {
      res.send({
        status: 0,
        message: "Oops! " + err.name + ": " + err.message
      });
    }
  }
});
router.post("/getTotalUserTracking", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      try {
        let id = req.body.id;
        Tracking.countDocuments({ user_id: id }, async function(err, log) {
          if (err) {
            res.send({
              status: 0,
              message: "Oops! " + err.name + ": " + err.message
            });
          } else {
            res.send({
              status: 1,
              message: "Sucess!",
              data: { total: log }
            });
          }
        }).sort({ date: -1 });
      } catch (err) {
        res.send({
          status: 0,
          message: "Oops! " + err.name + ": " + err.message
        });
      }
    } catch (err) {
      res.send({
        status: 0,
        message: "Oops! " + err.name + ": " + err.message
      });
    }
  }
});
router.post("/getAllLogins", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    let limit = req.body.data.number_of_pages;
    let active_page = req.body.data.activePage;
    let skip = limit * (active_page - 1);
    try {
      Users.aggregate([
        { $match: { deleteStatus: false } },
        { $sort: { last_login: -1 } },
        { $limit: skip + limit },
        { $skip: skip },
        {
          $project: {
            _id: 1,
            last_login: 1,
            created_on: 1,
            profile: 1,
            email: 1
          }
        }
      ]).exec((err, tracking) => {
        if (err) {
          res.send({
            status: 0,
            message: "Oops! " + err.name + ": " + err.message
          });
        } else if (tracking != "") {
          res.send({
            status: 1,
            message: "Tracking Fectched sucessfully!",
            data: tracking
          });
        }
      });
    } catch (err) {
      res.send({
        status: 0,
        message: "Oops! " + err.name + ": " + err.message
      });
    }
  }
});
router.post("/getTotalLogins", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      try {
        Users.aggregate([
          { $match: { deleteStatus: false } },
          { $sort: { last_login: -1 } },
          {
            $project: {
              _id: 1,
              last_login: 1,
              created_on: 1,
              profile: 1,
              email: 1
            }
          }
        ]).exec((err, tracking) => {
          if (err) {
            res.send({
              status: 0,
              message: "Oops! " + err.name + ": " + err.message
            });
          } else if (tracking != "") {
            res.send({
              status: 1,
              message: "Tracking Fectched sucessfully!",
              data: { total: tracking.length }
            });
          }
        });
      } catch (err) {
        res.send({
          status: 0,
          message: "Oops! " + err.name + ": " + err.message
        });
      }
    } catch (err) {
      res.send({
        status: 0,
        message: "Oops! " + err.name + ": " + err.message
      });
    }
  }
});
router.post("/getAllLeads", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    let limit = req.body.data.number_of_pages;
    let active_page = req.body.data.activePage;
    let skip = limit * (active_page - 1);

    try {
      Leads
        .aggregate([
          { $sort: { _id: -1 }},
          { $limit: skip + limit },
          { $skip: skip },
          {
            $lookup: {
              from: "users",
              localField: "user_id",
              foreignField: "_id",
              as: "user"
            }
          }
        ])
        .exec((err, projects) => {
          if (err) {
            res.send({
              status: 0,
              message: "Oops! " + err.name + ": " + err.message
            });
          } else if (projects != "") {
            res.send({
              status: 1,
              message: "Leads fetched sucessfuly!",
              data: projects
            });
          } else {
            res.send({
              status: 0,
              message: "No data found"
            });
          }
        });
    } catch (err) {
      res.send({
        status: 0,
        message: "Oops! " + err.name + ": " + err.message
      });
    }
  }
});



router.post("/getTotalLeads", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      try {
        Leads
        .aggregate([
          { $sort: { _id: -1 } },
          {
            $lookup: {
              from: "users",
              localField: "user_id",
              foreignField: "_id",
              as: "user"
            }
          }
        ]).exec((err, projects) => {
          if (err) {
            res.send({
              status: 0,
              message: "Oops! " + err.name + ": " + err.message
            });
          } else if (projects != "") {
            res.send({
              status: 1,
              message: "projects Fectched sucessfully!",
              data: { total: projects.length }
            });
          }
        });
      } catch (err) {
        res.send({
          status: 0,
          message: "Oops! " + err.name + ": " + err.message
        });
      }
    } catch (err) {
      res.send({
        status: 0,
        message: "Oops! " + err.name + ": " + err.message
      });
    }
  }
});

router.post("/getTotalLeads", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      try {
      
        Projects
        .aggregate([
          { $sort: { _id: -1 } },
          {
            $lookup: {
              from: "users",
              localField: "user_id",
              foreignField: "_id",
              as: "user"
            }
          }
        ]).exec((err, projects) => {
          if (err) {
            res.send({
              status: 0,
              message: "Oops! " + err.name + ": " + err.message
            });
          } else if (projects != "") {
            res.send({
              status: 1,
              message: "projects Fectched sucessfully!",
              data: { total: projects.length }
            });
          }
        });
      } catch (err) {
        res.send({
          status: 0,
          message: "Oops! " + err.name + ": " + err.message
        });
      }
    } catch (err) {
      res.send({
        status: 0,
        message: "Oops! " + err.name + ": " + err.message
      });
    }
  }
});
router.post("/getAllProjects", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    let limit = req.body.data.number_of_pages;
    let active_page = req.body.data.activePage;
    let skip = limit * (active_page - 1);

    try {
      Projects
        .aggregate([
          { $sort: { _id: -1 } },
          {
            $lookup: {
              from: "users",
              localField: "user_id",
              foreignField: "_id",
              as: "user"
            }
          }
        ])
        .exec((err, projects) => {
          if (err) {
            res.send({
              status: 0,
              message: "Oops! " + err.name + ": " + err.message
            });
          } else if (projects != "") {
            res.send({
              status: 1,
              message: "projects fetched sucessfuly!",
              data: projects
            });
          } else {
            res.send({
              status: 0,
              message: "No data found"
            });
          }
        });
    } catch (err) {
      res.send({
        status: 0,
        message: "Oops! " + err.name + ": " + err.message
      });
    }
  }
});
router.post("/getTotalProjects", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      try {
      
        Projects
        .aggregate([
          { $sort: { _id: -1 } },
          {
            $lookup: {
              from: "users",
              localField: "user_id",
              foreignField: "_id",
              as: "user"
            }
          }
        ]).exec((err, projects) => {
          if (err) {
            res.send({
              status: 0,
              message: "Oops! " + err.name + ": " + err.message
            });
          } else if (projects != "") {
            res.send({
              status: 1,
              message: "projects Fectched sucessfully!",
              data: { total: projects.length }
            });
          } else {
            res.send({
              status: 1,
              message: "No Projects Found!",
              data: { total: projects.length }
            });
          }
        });
      } catch (err) {
        res.send({
          status: 0,
          message: "Oops! " + err.name + ": " + err.message
        });
      }
    } catch (err) {
      res.send({
        status: 0,
        message: "Oops! " + err.name + ": " + err.message
      });
    }
  }
});
router.post("/getAllUserActivities", async (req, res) => {
  const token = req.body.token;
  const id = req.body.id;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    let limit = req.body.data.number_of_pages;
    let active_page = req.body.data.activePage;
    let skip = limit * (active_page - 1);
    // let condition = { status: true };
    // if (req.body.key) {
    //   condition = {
    //     page_slug: req.body.key
    //   };
    // }

    try {
      UserActivity.find({ user_id: id }, async function(err, log) {
        if (err) {
          res.send({
            status: 0,
            message: "Oops! " + err.name + ": " + err.message
          });
        } else {
          res.send({
            status: 1,
            message: "Sucess!",
            data: log
          });
        }
      })
        .sort({ date: -1 })
        .skip(limit * (active_page - 1))
        .limit(limit);
    } catch (err) {
      res.send({
        status: 0,
        message: "Oops! " + err.name + ": " + err.message
      });
    }
  }
});
router.post("/getTotalUserActivity", async (req, res) => {
  const token = req.body.token;
  const id = req.body.id;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      try {
        UserActivity.countDocuments({ user_id: id }, async function(err, log) {
          if (err) {
            res.send({
              status: 0,
              message: "Oops! " + err.name + ": " + err.message
            });
          } else {
            res.send({
              status: 1,
              message: "Sucess!",
              data: { total: log }
            });
          }
        }).sort({ date: -1 });
      } catch (err) {
        res.send({
          status: 0,
          message: "Oops! " + err.name + ": " + err.message
        });
      }
    } catch (err) {
      res.send({
        status: 0,
        message: "Oops! " + err.name + ": " + err.message
      });
    }
  }
});
router.post("/chartData", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      Tracking.aggregate([
        {
          $group: {
            _id: {
              page_slug: "$page_slug"
            },
            page_slug: { $first: "$page_slug" },
            duration: { $first: "$duration" },
            totalDuration: { $sum: "$duration" }
          }
        },
        {
          $project: {
            _id: 1,
            page_slug: 1,
            totalDuration: 1
          }
        }
      ]).exec((err, tracking) => {
        if (err) {
          res.send({
            status: 0,
            message: "Oops! " + err.name + ": " + err.message
          });
        } else if (tracking != "") {
          res.send({
            status: 1,
            message: "Tracking Fectched sucessfully!",
            data: tracking
          });
        }
      });
    } catch (err) {
      res.send({
        status: 0,
        message: "Oops! " + err.name + ": " + err.message
      });
    }
  }
});
router.post("/getNewsLetter", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    let limit = req.body.data.number_of_pages;
    let active_page = req.body.data.activePage;
    let skip = limit * (active_page - 1);

    try {
      Newsletter
        .aggregate([
          { $sort: { _id: -1 } },
          {
            $lookup: {
              from: "users",
              localField: "user_id",
              foreignField: "_id",
              as: "user"
            }
          }
        ])
        .exec((err, projects) => {
          if (err) {
            res.send({
              status: 0,
              message: "Oops! " + err.name + ": " + err.message
            });
          } else if (projects != "") {
            res.send({
              status: 1,
              message: "projects fetched sucessfuly!",
              data: projects
            });
          } else {
            res.send({
              status: 0,
              message: "No data found"
            });
          }
        });
    } catch (err) {
      res.send({
        status: 0,
        message: "Oops! " + err.name + ": " + err.message
      });
    }
  }
});
router.post("/getAllNewsLetter", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    let limit = req.body.data.number_of_pages;
    let active_page = req.body.data.activePage;
    let skip = limit * (active_page - 1);

    try {
      Newsletter
        .aggregate([
          { $sort: { _id: -1 } },
          {
            $lookup: {
              from: "users",
              localField: "user_id",
              foreignField: "_id",
              as: "user"
            }
          }
        ])
        .exec((err, projects) => {
          if (err) {
            res.send({
              status: 0,
              message: "Oops! " + err.name + ": " + err.message
            });
          } else if (projects != "") {
            res.send({
              status: 1,
              message: "projects fetched sucessfuly!",
              data: projects
            });
          } else {
            res.send({
              status: 0,
              message: "No data found"
            });
          }
        });
    } catch (err) {
      res.send({
        status: 0,
        message: "Oops! " + err.name + ": " + err.message
      });
    }
  }
});
router.post("/getTotalNewsLetter", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      try {
      
        Newsletter
        .aggregate([
          { $sort: { _id: -1 } },
          {
            $lookup: {
              from: "users",
              localField: "user_id",
              foreignField: "_id",
              as: "user"
            }
          }
        ]).exec((err, projects) => {
          if (err) {
            res.send({
              status: 0,
              message: "Oops! " + err.name + ": " + err.message
            });
          } else if (projects != "") {
            res.send({
              status: 1,
              message: "projects Fetched sucessfully!",
              data: { total: projects.length }
            });
          }
        });
      } catch (err) {
        res.send({
          status: 0,
          message: "Oops! " + err.name + ": " + err.message
        });
      }
    } catch (err) {
      res.send({
        status: 0,
        message: "Oops! " + err.name + ": " + err.message
      });
    }
  }
});
router.post("/getLeadDetails", async (req, res) => {
  const token = req.body.token;

  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);

      try {
        Leads.find({ _id: req.body.id }, async function(err, log) {
          if (err) {
            res.send({
              status: 0,
              message: "Oops! " + err.name + ": " + err.message
            });
          } else {
            res.send({
              status: 1,
              message: "Sucess!",
              data: log
            });
          }
        });
      } catch (err) {
        res.send({
          status: 0,
          message: "Oops! " + err.name + ": " + err.message
        });
      }
    } catch (err) {
      res.send({
        status: 0,
        message: "Oops! " + err.name + ": " + err.message
      });
    }
  }
});
module.exports = router;
