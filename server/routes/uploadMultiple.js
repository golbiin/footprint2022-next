//load dependencies
const express = require("express");
const path = require("path");
const joi = require("joi");
const jwt = require("jsonwebtoken");
const mustache = require("mustache");
const multer = require("multer");
const AWS = require("aws-sdk");
var fs = require("fs");
const config = require("../config/config.json");
const url = config.uploadURL;
const jwtKey = config.jwsPrivateKey;
const sendGridKey = config.sendGridKey;
const fromMail = config.fromEmail;
/*******************upload path setting******/
var storage = multer.diskStorage({
  destination: function(req, file, cb) {
    cb(null, url);
  },
  filename: function(req, file, cb) {
    cb(null, Date.now() + "-" + file.originalname);
  }
});
var upload = multer({
  storage: storage
}).array("file");
/*******************end upload path setting*/
//http request port set
const router = express.Router();

router.post("/uploadData", async (req, res) => {
  try {
    await upload(req, res, async function(err) {
      if (err instanceof multer.MulterError) {
        res.send({
          status: 0,
          message: "Oops! something went wrong with uploading profile image."
        });
      } else if (err) {
        res.send({
          status: 0,
          message: "Oops! " + err.name + ": " + err.message
        });
      } else {
        try {
          await awsUpload(req, result => {
            if (result.message) {
              //error
              return res
                .status(500)
                .json({ status: 0, message: result.message });
            } else {
              //sucess
              res.send({
                status: 1,
                message: "File uploaded",
                data: { file_location: result }
              });
            }
          });
        } catch (err) {
          res.send({
            status: 0,
            message: "Oops! " + err.name + ": " + err.message
          });
        }
      }
    });
  } catch (err) {
    res.send({
      status: 0,
      message: "Oops! " + err.name + ": " + err.message
    });
  }
});
function awsUpload(req, callback) {
  const file = req.files;
  // console.log("FILE", file);
  AWS.config.update({
    accessKeyId: config.accessKeyId,
    secretAccessKey: config.secretAccessKey
  });
  var s3 = new AWS.S3();

  //Where you want to store your file
  var ResponseData = [];

  file.map(item => {
    console.log("item", url, item.filename);
    var filePath = url + item.filename + "";
    var params = {
      Bucket: config.BucketName,
      Body: fs.createReadStream(filePath),
      Key: "portfolio/" + Date.now() + "_" + item.filename,
      ACL: "public-read"
    };
    s3.upload(params, function(err, data) {


      fs.unlink(url + item.filename, (err) => {
        if (err) {
          console.error(err)
          return
        }
      
        //file removed
      })

      if (err) {
        console.log("err", err);
        callback(err);
        //res.json({ error: true, Message: err });
      } else {
        data.certname = item.originalname;
        data.certtype = item.mimetype;
        ResponseData.push(data);
        if (ResponseData.length == file.length) {
          console.log("ResponseData", ResponseData);
          callback(ResponseData);
          /* res.json({
            error: false,
            Message: "File Uploaded    SuceesFully",
            Data: ResponseData,
          }); */
        }
      }
    });
  });

  /*   var filePath = url + req.file.filename + "";
  //configuring parameters
  var params = {
    Bucket: config.BucketName,
    Body: fs.createReadStream(filePath),
    Key: "profile_data/" + Date.now() + "_" + req.file.filename,
  };
  s3.upload(params, function (err, data) {
    //handle error
    if (err) {
      callback(err);
    }
    //success
    if (data) {
      console.log("entry1");
      callback(data.Location);
    }
  }); */
}

/* router.post("/upload", multipleUpload, function (req, res) {
  const file = req.files;
  let s3bucket = new AWS.S3({
    accessKeyId: IAM_USER_KEY,
    secretAccessKey: IAM_USER_SECRET,
    Bucket: "BUCKET_NAME",
  });
  s3bucket.createBucket(function () {
    let Bucket_Path = "BUCKET_PATH";
    //Where you want to store your file
    var ResponseData = [];

    file.map((item) => {
      var params = {
        Bucket: BucketPath,
        Key: item.originalname,
        Body: item.buffer,
        ACL: "public-read",
      };
      s3bucket.upload(params, function (err, data) {
        if (err) {
          res.json({ error: true, Message: err });
        } else {
          ResponseData.push(data);
          if (ResponseData.length == file.length) {
            res.json({
              error: false,
              Message: "File Uploaded    SuceesFully",
              Data: ResponseData,
            });
          }
        }
      });
    });
  });
}); */

module.exports = router;
