const Joi = require("joi");
//signup validation
module.exports.Usersignup = function(req, callback) {
  const Signupvalidate = {
    email: Joi.string()
      .required()
      .email()
      .label("Email"),
    password: Joi.string()
      .required()
      .min(8)
      .regex(/^[a-zA-Z0-9!@#$%^&*()]{8,20}$/)
      .label("Password"),
      leadsData:Joi.optional().label("leadsData"),
    notify_me: Joi.any()
      .optional()
      .label("tips")
  };
  result = Joi.validate(req.body, Signupvalidate);
  callback(result);
};
//login validation
module.exports.validateLogin = function(req, callback) {
  const loginSchema = {
    email: Joi.string()
      .required()
      .email()
      .label("Email"),
    password: Joi.string()
      .required()
      .label("Password"),
    leadsData:Joi.optional().label("leadsData"),
  };
  result = Joi.validate(req.body, loginSchema);
  callback(result);
};
//adminlogin validation
module.exports.validateadminLogin = function(req, callback) {
  const loginSchema = {
    username: Joi.string()
      .required()
      .label("username"),
    password: Joi.string()
      .required()
      .label("Password")
  };
  result = Joi.validate(req.body, loginSchema);
  callback(result);
};
