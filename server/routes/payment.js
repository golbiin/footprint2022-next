const stripe = require("../constants/stripe");
const express = require("express");
const jwt = require("jsonwebtoken");
const mongoose = require("mongoose");
const moment = require("moment");
const { v4: uuidv4 } = require("uuid");
const router = express.Router();
const Payment = require("../models/payment");
const saveCard = require("../models/save_card");
const Users = require("../models/users");
const User_activities = require("../models/userActivity");
const Subscriptions = require("../models/subscriptions");
const Wallet = require("./../models/wallet");
const config = require("../config/config.json");
const jwtKey = config.jwsPrivateKey;
var momentTimezone = require('moment-timezone');


var fs = require("fs");
const mustache = require("mustache");
const sendGridKey = config.sendGridKey;
const fromMail = config.fromEmail;
const path = require("path");
const sgMail = require("@sendgrid/mail");

 
router.post("/", async (req, res) => {
  const token = req.body.token;
  const data = req.body.data;
  const pricingpackage = data.pricingpackage;
  const timezone = data.timezone;

  if (!token) {
    return res.send({ status: 0, message: "Sorry session expired" });
  }
  else {
    try {
      payload = jwt.verify(token, jwtKey);
      userId = payload.id;
      
      const newPayment = new Payment({
        user_id: data.user_id,
        billing_address: data.billingDetails,
        payment_method_id: data.id,
        first_name: data.first_name,
        last_name: data.last_name,
        payment_method: "stripe",
        package: pricingpackage,
        payment_date: data.currentTime,
        timezone: data.timezone,
        charged_by: data.charged_by,
        payment_status: 0,
        status: 0
      });
      try {
        await newPayment
          .save()
          .then(async () => {
            let payment_id = newPayment._id;
            let payment_amount = pricingpackage.totalAmount;
            let amount = parseInt(payment_amount * 100); // replace with payment_amount_convert
            let payment_amount_convert = parseInt(payment_amount * 100);

           // let CURRENCY = data.currency;
            let CURRENCY = data.currency;
            
            let paymentId = data.id;
            let customer_id = data.customer_id;
            let idempotencyKey = uuidv4();
            var condition = { _id: payment_id };
            try {
              //save payment
          
              
              let packageDetails = pricingpackage.category +" - "+" plan (Monthly Renewal)"; 
              
              
           

              let termDays = 7;
              let interval = 'month';
              let interval_count = 1;
              
 
              const product = await stripe.products.create({
                name: packageDetails,
              });
              console.log("product", product)
              if(product){

              let coupon = '';
                   
                 

                  const price = await stripe.prices.create({
                    unit_amount: amount,
                    currency: CURRENCY,
                    recurring: {
                      interval,
                      interval_count,
                  },
                    product: product.id,
                  });

                  if(price){ 
                    
                    try{
                      let subscription = '';
                  
                        
                        subscription = await stripe.subscriptions.create({
                          customer: customer_id,
                          default_payment_method: paymentId,
                          items: [
                            {price: price.id},
                          ],
                         
                        },
                        { idempotencyKey });
                      
/* 
                      console.log("subscription", subscription ); 
                      res.send({
                        status: 1,
                        message: "Payment successful.",
                        paymentId: subscription.id,
                        
                      }); */
                  
                    
                        const NewSubscription = new Subscriptions({
                          user_id: payload._id,
                          plan: pricingpackage,
                          customer_id: customer_id,
                          payment_method_id: paymentId,
                          subscribed_on: momentTimezone().tz(timezone).format("MM-DD-YYYY HH:mm:ss"),
                          expire_on: momentTimezone().tz(timezone).add(termDays, "days").format("MM-DD-YYYY HH:mm:ss"),
                          status: 1
                        });
                        try {
                          await NewSubscription.save()
                            .then(() => {
                            
                              var update_value = {
                                payment_status: 1,
                                payment_response: subscription,
                                subscription_id: NewSubscription._id,
                              };
                              let updateData = {
                                condition: condition,
                                update_value: update_value
                              };
                           /*    let maildata = {
                                email: data.billingDetails.email,
                                name: data.billingDetails.name,
                                company: data.billingDetails.company,
                                subject: "Footprint payment received",
                                pricingpackage: pricingpackage,
                                expire_on: momentTimezone().tz(timezone).add(termDays, "days").format("MM-DD-YYYY"),
                                renewd_on: momentTimezone().tz(timezone).format("MM-DD-YYYY")
                              };
                              sendPymentMail(maildata);
                              notifyMailtoAdmin(maildata); */
                              updatePayment(updateData, result => {
                                if (result.status == 0) {
                                  res.send({
                                    status: 1,
                                    message:
                                      "Payment successful, Failed to save payment data.",
                                    paymentId: subscription.id,
                                    error: result.error
                                  });
                                } else {
                                  
                                   
                                    res.send({
                                      status: 1,
                                      message: "Payment successful.",
                                      paymentId: subscription.id
                                    });
                                    
                                  
                                }
                              });
                            })
                            .catch(err => {
                              updatePayment(updateData, result => {
                                if (result.status == 0) {
                                  res.send({
                                    status: 1,
                                    message:
                                      "Payment successful, Failed to save payment data.",
                                    paymentId: subscription.id,
                                    error: result.error
                                  });
                                } else {
                                  res.send({
                                    status: 1,
                                    message: "Payment successful.",
                                    paymentId: subscription.id
                                  });
                                }
                              });
                            });
                        } catch (err) {
                          updatePayment(updateData, result => {
                            if (result.status == 0) {
                              res.send({
                                status: 1,
                                message:
                                  "Payment successful, Failed to save payment data," +
                                  err.name +
                                  ": " +
                                  err.message,
                                paymentId: subscription.id,
                                error: result.error
                              });
                            } else {
                              res.send({
                                status: 1,
                                message:
                                  "Payment successful, " +
                                  err.name +
                                  ": " +
                                  err.message,
                                paymentId: subscription.id
                              });
                            }
                          });
                        }
                      

           

                 
            }catch (err) {
              console.log("err", err);
              
              res.send({
                status: 0,
                message: "Oops! " + err.name + ": " + err.message
              });
            } 
        } 
      }

            } 
            catch (err) { }
          })
          .catch(err => {
            res.send({
              status: 2,
              message: "Oops! " + err.name + ": " + err.message
            });
          });
      } catch (err) {
        console.log("err", err);
        res.send({
          status: 2,
          message: "Oops! " + err.name + ": " + err.message
        });
      }
    } catch (err) {
      res.send({
        status: 0,
        message: "Oops! " + err.name + ": " + err.message
      });
    }
  }  
});


function updatePayment(updateData, callback) {
  let condition = updateData.condition;
  let update_value = updateData.update_value;
  try {
    Payment.findByIdAndUpdate(condition, update_value, async function (
      err,
      log
    ) {
      if (err) {
        let result = {
          status: 0,
          error: err
        };
        callback(result);
        // res.send({
        //   status: 1,
        //   message: "Payment successful, Failed to save payment data.",
        //   paymentId: paymentIntent.id,
        //   error: err
        // });
      } else {
        let result = {
          status: 1
        };
        callback(result);
        // res.send({
        //   status: 1,
        //   message: "Payment successful.",
        //   paymentId: paymentIntent.id
        // });
      }
    });
  } catch (err) {
    let result = {
      status: 0,
      error: err
    };
    callback(result);
  }
}

router.post("/save_card", async (req, res) => {
  const payment_method_id = req.body.payment_method_id;
  const token = req.body.token;
  console.log("payment_method_id", payment_method_id);
  if (!token) {
    return res.send({ status: 0, message: "Sorry session expired" });
  } else {
    try {
      const payload = jwt.verify(token, jwtKey);
 
      try {
      
        const customer = await stripe.customers.create({
          payment_method: payment_method_id,
          name : req.body.name,
          email : req.body.email,
        });
 
        if (customer) {
          console.log("customer", customer);     
            
          const newCard = new saveCard({
            user_id: payload._id,
            customer_id: customer.id,
            payment_method_id: payment_method_id,
            last_updated: moment().format("MM-DD-YYYY hh:mm:ss"),
            status: 0
          });
          try {
            await newCard
              .save()
              .then(() => {
/*                 Users.findOne({ _id: payload._id }, { profile: 1 }, function (err, log) {
                  if (log != "") {
                    console.log(log);
                    let user_name =
                      log.profile.first_name + " " + log.profile.last_name;
                    let activity_data = {
                      user: payload._id,
                      activity: "New card saved",
                      details: {
                        message: user_name + " saved new card details"
                      }
                    };
                    saveActivity(activity_data);
                  }
                }); */
                res.send({
                  status: 1,
                  message: "Your card details added successfully.",
                  data: { customer_id: customer.id }
                });
              })
              .catch(err => {
                res.send({
                  status: 0,
                  message: "Oops! " + err.name + ": " + err.message
                });
              });
          } catch (err) {
            res.send({
              status: 0,
              message: "Oops! " + err.name + ": " + err.message
            });
          }
        }
      } catch (err) {
        res.send({
          status: 0,
          message: "Oops! " + err.name + ": " + err.message
        });
      }
    } catch (err) {
      res.send({
        status: 0,
        message: "Oops! " + err.name + ": " + err.message
      });
    }
  }
}); 

router.post("/remove_card", async (req, res) => {
  const id = req.body.data;
  const token = req.body.token;
  if (!token) {
    return res.send({ status: 0, message: "Sorry session expired" });
  } else {
    try {
      const payload = jwt.verify(token, jwtKey);
      saveCard.findByIdAndDelete({ _id: id }, async function (err, log) {
        if (err) {
          res.send({
            status: 0,
            message: "Oops! " + err.name + ": " + err.message
          });
        } else {
        /*   Users.findOne({ _id: payload._id }, { profile: 1 }, function (err, log) {
            if (log != "") {
              console.log(log);
              let user_name =
                log.profile.first_name + " " + log.profile.last_name;
              let activity_data = {
                user: payload._id,
                activity: "Card Removed",
                details: {
                  message: user_name + " removed the card."
                }
              };
              saveActivity(activity_data);
            }
          }); */
          res.send({ status: 1, message: "Card deleted" });
        }
      });
    } catch (err) {
      res.send({
        status: 0,
        message: "Oops! " + err.name + ": " + err.message
      });
    }
  }
});
router.post("/get_saved_cards", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    return res.send({ status: 0, message: "Sorry session expired" });
  } else {
    try {
     
      const payload = jwt.verify(token, jwtKey);
      let data = {
        id: payload._id
      };
      getCardDetails(data, result => {
        res.send(result);
      });
    } catch (err) {
      res.send({
        status: 0,
        message: "Oops! " + err.name + ": " + err.message
      });
    }
  }
});
function getCardDetails(data, callback) {
  let userId = data.id;

  try {
    saveCard.findOne({ user_id: mongoose.Types.ObjectId(userId) }, function (
      err,
      log
    ) {
      if (err) {
        let result = {
          status: 0,
          message: "Oops! " + err.name + ": " + err.message
        };
        callback(result);
      } else if (!log) {
        let result = {
          status: 0,
          message: "No saved cards"
        };
        callback(result);
      } else {
        let cards = [];
        let carDetails = [];
        cards.push(log);
        let status = false;
        cards.forEach(async (element, index) => {
          const paymentMethods = await stripe.paymentMethods.list({
            customer: element.customer_id,
            type: "card"
          });
          if (paymentMethods) {
            let res = {
              saved: element,
              customer_data: paymentMethods.data[0].card
            };
            carDetails.push(res);
          }
          let last_indx = cards.length - 1;
          if (index == last_indx) {
            let result = {
              status: 1,
              message: "Card details fetched.",
              data: carDetails
            };
            callback(result);
          }
        });
      }
    }).sort({ _id: -1 });
  } catch (err) {
    let result = {
      status: 0,
      message: "Oops! " + err.name + ": " + err.message
    };
    callback(result);
  }
}
 
router.post("/payment_by_cheque", async (req, res) => {
  const token = req.body.token;
  const data = req.body.data;
  const timeData=req.body.timedata;

  if (!token) {
    return res.send({ status: 0, message: "Sorry session expired" });
  }
  else {
    try {
      let payload = jwt.verify(token, jwtKey);
      let userId = payload._id;
      let billingDetails={
        company_name:data.company_name,
        country:data.country,
        city:data.state,
        phone:data.phone
      }
      let pricingpackage={
        plan:data.plan,
        planDescription:data.planDescription,
        planValue:data.planValue,
        category:data.category,
        categoryvalue:data.categoryValue,
        sliderNumber:data.sliderNumber,
        sliderValue:data.sliderValue,
        totalAmount:data.totalAmount
      }
      const newPayment = new Payment({
        user_id: userId,
        billing_address:billingDetails,
        payment_method: "cheque",
        package: pricingpackage,
        payment_date: timeData.currentTime,
        timezone: timeData.timezone,
        charged_by: "user",
        payment_status: 0,
        status: 0
      });
      try {
        await newPayment
          .save()
          .then(async () => {
            const newSubscription=new Subscriptions({
              user_id:userId,
              plan: pricingpackage,
              subscribed_on: timeData.currentTime,
              status: 1
            })
            await newSubscription.save().then(()=>{
              res.send({
                status: 1,
                message: "Subscription successfull!"
              });
            })
            .catch(err => {
              res.send({
                status: 0,
                message: "Oops! " + err.name + ": " + err.message
              });
            });
          })  
          .catch(err => {
            res.send({
              status: 0,
              message: "Oops! " + err.name + ": " + err.message
            });
          });
    }
    catch (err) {
      res.send({
        status: 0,
        message: "Oops! " + err.name + ": " + err.message
      });
    }
  }
  catch (err) {
    res.send({
      status: 0,
      message: "Oops! " + err.name + ": " + err.message
    });
  }
 }
});

function saveActivity(data) {
  let new_activity = new User_activities({
    user_id: data.user,
    date: moment().format("MM-DD-YYYY"),
    activity: data.activity,
    details: data.details
  });
  new_activity.save();
}


router.post("/get_payments", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Sorry session expired" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      const user_id = payload._id;
      try {
        Payment.find({ user_id: user_id }, async function(err, log) {
          if (err) {
            res.send({
              status: 0,
              message: "Oops! " + err.name + ": " + err.message
            });
          } else {
            res.send({
              status: 1,
              message: "success",
              data: log
            });
          }
        }).sort({ _id: -1 });
      } catch (err) {}
    } catch (err) {}
  }
});
router.post("/get_subscriptions", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Sorry session expired" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      const user_id = payload._id;
      console.log("user_iduser_iduser_id", user_id)
      try {
        Subscriptions.find({ user_id: user_id , status: 1 }, async function(err, log) {
          if (err) {
            res.send({
              status: 0,
              message: "Oops! " + err.name + ": " + err.message
            });
          } else {
            res.send({
              status: 1,
              message: "success",
              data: log
            });
          }
        }).sort({ _id: -1 });
      } catch (err) {}
    } catch (err) {}
  }
});

module.exports = router;
