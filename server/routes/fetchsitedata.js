//load dependencies
const express = require("express");
const path = require("path");
const joi = require("joi");
const jwt = require("jsonwebtoken");
var fs = require("fs");
const mongoose = require("mongoose");
const config = require("../config/config.json");
const jwtKey = config.jwsPrivateKey;
//Include schema
const Portfolio = require("../models/portfolio");
const Portfolio_categories = require("../models/portfolioCategories");
const Review = require("../models/reviews");

//http request port set
const router = express.Router();

router.post("/get_reviews", async (req, res) => {

    try {
      try {
        Review.find({ deleteStatus: false, status : true }, async function(err, log) {
          if (err) {
            res.send({
              status: 0,
              message: "Oops! " + err.name + ": " + err.message
            });
          } else {
            res.send({
              status: 1,
              message: "success",
              data: log
            });
          }
        }).sort({ _id: -1 }).limit(15);
      } catch (err) {
        res.send({
          status: 0,
          message: "Oops! " + err.name + ": " + err.message
        });
      }
    } catch (err) {
      res.send({
        status: 0,
        message: "Oops! " + err.name + ": " + err.message
      });
    }
});
router.post("/get_reviews_all", async (req, res) => {

    try {
      try {
        Review.find({ deleteStatus: false, status : true }, async function(err, log) {
          if (err) {
            res.send({
              status: 0,
              message: "Oops! " + err.name + ": " + err.message
            });
          } else {
            res.send({
              status: 1,
              message: "success",
              data: log
            });
          }
        }).sort({ _id: -1 });
      } catch (err) {
        res.send({
          status: 0,
          message: "Oops! " + err.name + ": " + err.message
        });
      }
    } catch (err) {
      res.send({
        status: 0,
        message: "Oops! " + err.name + ": " + err.message
      });
    }
});

 
 
module.exports = router;
