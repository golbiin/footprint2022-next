const express = require("express");
const path = require("path");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcryptjs");
const moment = require("moment");
const sgMail = require("@sendgrid/mail");

var fs = require("fs");
const mustache = require("mustache");
//Include schema
const Contacts = require("../models/contacts");
const NewsLetter = require("../models/newsLetter");
const config = require("../config/config.json");
const jwtKey = config.jwsPrivateKey;
const sendGridKey = config.sendGridKey;
const fromMail = config.fromEmail;


const RECAPTCHA_SERVER_KEY =
  process.env.NODE_ENV === "development"
    ? "6LeQNSAaAAAAAF919JSqyIlYd5ptLtuPwSx6qjlS"
    : "6LeIxAcTAAAAAGG-vFI1TnRWxMZNFuojJ4WifJWe";

//http request port set
const router = express.Router();

router.post("/contact", async (req, res) => {
  let contact_data = req.body.data;
  let type = req.body.type;
  let createdon = moment().format("MM-DD-YYYY hh:mm:ss");
  const newContact = new Contacts({
    contact_data: contact_data,
    contact_type: type,
    page: req.body.page,
    ip: req.body.ip,
    viewd_status: false,
    submitted_on: createdon
  });
  try {
    await newContact
      .save()
      .then(() => {
        // res.send({
        //   status: 1,
        //   message: "Thank you! We will get back to you soon."
        // });
        var contact = {};
        if(type === 'contact')
          contact = {
            email: contact_data.email,
            first_name: contact_data.name,
            phone: contact_data.phone
          };
        else if(type === 'book-call')
        contact = {
          email: contact_data.email,
          first_name: contact_data.first_name,
          last_name: contact_data.last_name,
          phone: contact_data.phone
        };


        try {
          sendMail(newContact, result => {
            if (result == "success") {
              res.send({
                status: 1,
                message: "Thank you! We will get back to you soon."
              });
            } else if (result == "failed") {
              res.send({
                status: 1,
                message:
                  "Thank you! We will get back to you soon. Failed to send verification mail, You’ll be redirected."
              });
            }
          });
        } catch (err) {
          res.send({
            status: 0,
            message: "Oops! " + err.name + ": " + err.message
          });
        }
      })
      .catch(err => {
        res.send({
          status: 0,
          message: "Oops! " + err.name + ": " + err.message
        });
      });
  } catch (err) {
    res.send({
      status: 0,
      message: "Oops! " + err.name + ": " + err.message
    });
  }
});
router.post("/add_news_letter", async (req, res) => {
  let contact_data = req.body.data;

  let createdon = moment().format("MM-DD-YYYY hh:mm:ss");
  const newNewsLetter = new NewsLetter({
    news_email: contact_data.email,
    submitted_on: createdon
  });
  try {
    await newNewsLetter
      .save()
      .then(() => {

          res.send({
            status: 1,
            message: "Thank you! Subscription Added."
          });
        
      })
      .catch(err => {
        res.send({
          status: 0,
          message: "Oops! " + err.name + ": " + err.message
        });
      });
  } catch (err) {
    res.send({
      status: 0,
      message: "Oops! " + err.name + ": " + err.message
    });
  }
});


function sendMail(newUser, callback) {
   console.log("kkkkkk", newUser);
  if (newUser.contact_type === "book-call") {
    console.log("kkkkkk");
    let template = fs
      .readFileSync(path.join(__dirname + "/mail-templates/bookCall.html"), {
        encootding: "utf-8"
      })
      .toString();
    let view = {
      name:
        newUser.contact_data.first_name + " " + newUser.contact_data.last_name,
      email: newUser.contact_data.email ? newUser.contact_data.email : "",
      phone: newUser.contact_data.phone ? newUser.contact_data.phone : "",
      company_name: newUser.contact_data.company_name
        ? newUser.contact_data.company_name
        : "",
      company_size: newUser.contact_data.company_size
        ? newUser.contact_data.company_size
        : "",
      imageUrl: config.imageUrl
    };
    let subject = "OutsourceMe Book A Call";
    var createTemplate = mustache.render(template, view);
    sgMail.setApiKey(sendGridKey);
    const message = {
      to: config.admin,
      from: { email: config.fromEmail, name: "OutSourceMe" },
      subject: subject,
      html: createTemplate
    };

    try {
      sgMail
        .send(message)
        .then(() => {
          callback("success");
          console.log("success");
        })
        .catch(error => {
          // //Log friendly error
          console.error("send err", error.toString());
          // res.status(400).send(err);
          // //Extract error msg
          const { message, code, response } = error;

          // //Extract response msg
          const { headers, body } = response;
          console.log(message, body.errors);
          callback("failed");
        });
    } catch (err) {
      callback("failed");
      console.log("Error show", err);
    }
  } else if (newUser.contact_type === "contact") {
    let template = fs
      .readFileSync(path.join(__dirname + "/mail-templates/contact.html"), {
        encootding: "utf-8"
      })
      .toString();
    let view = {
      name: newUser.contact_data.name,
      email: newUser.contact_data.email,
      phone: newUser.contact_data.phone,
      topic: newUser.contact_data.topic,
      message: newUser.contact_data.message,
      imageUrl: config.imageUrl
    };
    let subject = "Footprint Contact Request";
    var createTemplate = mustache.render(template, view);
    sgMail.setApiKey(sendGridKey);
    const message = {
      to: config.admin,
      from: { email: config.fromEmail, name: "Footprint" },
      subject: subject,
      html: createTemplate
    };

    try {
      sgMail
        .send(message)
        .then(() => {
          callback("success");
          console.log("success");
        })
        .catch(error => {
          // //Log friendly error
          console.error("send err", error.toString());
          // res.status(400).send(err);
          // //Extract error msg
          const { message, code, response } = error;

          // //Extract response msg
          const { headers, body } = response;
          console.log(message, body.errors);
          callback("failed");
        });
    } catch (err) {
      callback("failed");
      console.log("Error show", err);
    }
  } else if (newUser.contact_type === "banner-form") {
    let template = fs
      .readFileSync(
        path.join(__dirname + "/mail-templates/banner-contact.html"),
        {
          encootding: "utf-8"
        }
      )
      .toString();
    let view = {
      name: newUser.contact_data.first_name,
      email: newUser.contact_data.email,
      phone: newUser.contact_data.phone,
      imageUrl: config.imageUrl
    };
    if(newUser.contact_data.company_name)
      view["company_name"] = newUser.contact_data.company_name;
    if(newUser.contact_data.message)
    view["message"] = newUser.contact_data.message;
    let subject = "Footprint Contact Request";
    var createTemplate = mustache.render(template, view);
    sgMail.setApiKey(sendGridKey);
    const message = {
      to: config.admin,
      from: { email: config.fromEmail, name: "Footprint" },
      subject: subject,
      html: createTemplate
    };

    try {
      sgMail
        .send(message)
        .then(() => {
          callback("success");
          console.log("success");
        })
        .catch(error => {
          // //Log friendly error
          console.error("send err", error.toString());
          // res.status(400).send(err);
          // //Extract error msg
          const { message, code, response } = error;

          // //Extract response msg
          const { headers, body } = response;
          console.log(message, body.errors);
          callback("failed");
        });
    } catch (err) {
      callback("failed");
      console.log("Error show", err);
    }
  } else if (newUser.contact_type === "blog-form") {
    let template = fs
      .readFileSync(
        path.join(__dirname + "/mail-templates/blog-contact.html"),
        {
          encootding: "utf-8"
        }
      )
      .toString();
    let view = {
      name: newUser.contact_data.first_name,
      email: newUser.contact_data.email,
      phone: newUser.contact_data.phone,
      imageUrl: config.imageUrl
    };
    if(newUser.contact_data.company_name)
      view["company_name"] = newUser.contact_data.company_name;
    if(newUser.contact_data.message)
    view["message"] = newUser.contact_data.message;
    let subject = "Footprint Contact Request";
    var createTemplate = mustache.render(template, view);
    sgMail.setApiKey(sendGridKey);
    const message = {
      to: config.admin,
      from: { email: config.fromEmail, name: "Footprint" },
      subject: subject,
      html: createTemplate
    };

    try {
      sgMail
        .send(message)
        .then(() => {
          callback("success");
          console.log("success");
        })
        .catch(error => {
          // //Log friendly error
          console.error("send err", error.toString());
          // res.status(400).send(err);
          // //Extract error msg
          const { message, code, response } = error;

          // //Extract response msg
          const { headers, body } = response;
          console.log(message, body.errors);
          callback("failed");
        });
    } catch (err) {
      callback("failed");
      console.log("Error show", err);
    }
  }
}


module.exports = router;
