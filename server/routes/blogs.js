//load dependencies
const express = require("express");
const path = require("path");
const joi = require("joi");
const jwt = require("jsonwebtoken");
var fs = require("fs");
const mongoose = require("mongoose");
const config = require("../config/config.json");
const jwtKey = config.jwsPrivateKey;
//Include schema
const Portfolio = require("../models/portfolio");
const Portfolio_categories = require("../models/portfolioCategories");
const Blog = require('./../models/blogs')
const Blog_categories = require('./../models/blog_categories')
var async = require("async");
//http request port set
const router = express.Router();

router.post('/getAllBlogsCategory', async (req, res) => {
  const number_of_pages = req.body.data.number_of_blogs
  const active_page = req.body.data.activePage
  const category = req.body.data.category

  //search//

      try {
        let conditions = {};
    //    console.log("category.length", category.length);
        if(category.length)
          conditions = { status: true , deleteStatus: false, category: { $in: category } };
        else
          conditions = { status: true , deleteStatus: false };
        //  conditions = { status: true , deleteStatus: false, category: { $in: [ '6167c3bf80b43a1318d44d0f', '6167c3bf80b43a1318d44d0d' ] } };
        Blog.find(conditions, async function (err, log) {
          if (err) {
            res.send({
              status: 0,
              message: 'Oops! ' + err.name + ': ' + err.message
            })
          } else {
            res.send({
              status: 1,
              message: 'Success!',
              data: log
            })
          }
        }).sort({ created_on: -1 })
          .skip(number_of_pages * (active_page - 1))
          .limit(number_of_pages)
      } catch (err) {
        res.send({
          status: 0,
          message: 'Oops! ' + err.name + ': ' + err.message
        })
      }
});

router.post("/getAllBlogs", async (req, res) => {
  async.parallel(
    {
      smallBlackList: function(callback) {
        const number_of_pages = 6;
        const active_page = 1;
        const category = ["6168282c26265e48ccc25049"];
        try {
          let conditions = {};
          //    console.log("category.length", category.length);
          if (category.length)
            conditions = {
              status: true,
              deleteStatus: false,
              category: { $in: category }
            };
          else conditions = { status: true, deleteStatus: false };
          //  conditions = { status: true , deleteStatus: false, category: { $in: [ '6167c3bf80b43a1318d44d0f', '6167c3bf80b43a1318d44d0d' ] } };
          Blog.find(conditions, async function(err, log) {
            if (err) {
              callback(null, {
                status: 0,
                message: "Oops! " + err.name + ": " + err.message
              });
            } else {
              callback(null, {
                status: 1,
                data: log
              });
            }
          })
            .sort({ created_on: -1 })
            .skip(number_of_pages * (active_page - 1))
            .limit(number_of_pages);
        } catch (err) {
          res.send({
            status: 0,
            message: "Oops! " + err.name + ": " + err.message
          });
        }
      },
      largeWhiteList: function(callback) {
        const number_of_pages = 4;
        const active_page = 1;
        const category = ["6168282c26265e48ccc25047"];
        try {
          let conditions = {};
          //    console.log("category.length", category.length);
          if (category.length)
            conditions = {
              status: true,
              deleteStatus: false,
              category: { $in: category }
            };
          else conditions = { status: true, deleteStatus: false };
          //  conditions = { status: true , deleteStatus: false, category: { $in: [ '6167c3bf80b43a1318d44d0f', '6167c3bf80b43a1318d44d0d' ] } };
          Blog.find(conditions, async function(err, log) {
            if (err) {
              callback(null, {
                status: 0,
                message: "Oops! " + err.name + ": " + err.message
              });
            } else {
              callback(null, {
                status: 1,
                data: log
              });
            }
          })
            .sort({ created_on: -1 })
            .skip(number_of_pages * (active_page - 1))
            .limit(number_of_pages);
        } catch (err) {
          res.send({
            status: 0,
            message: "Oops! " + err.name + ": " + err.message
          });
        }
      },
      smallWhiteList: function(callback) {
        const number_of_pages = 6;
        const active_page = 1;
        const category = ["6168282c26265e48ccc25048"];
        try {
          let conditions = {};
          //    console.log("category.length", category.length);
          if (category.length)
            conditions = {
              status: true,
              deleteStatus: false,
              category: { $in: category }
            };
          else conditions = { status: true, deleteStatus: false };
          //  conditions = { status: true , deleteStatus: false, category: { $in: [ '6167c3bf80b43a1318d44d0f', '6167c3bf80b43a1318d44d0d' ] } };
          Blog.find(conditions, async function(err, log) {
            if (err) {
              callback(null, {
                status: 0,
                message: "Oops! " + err.name + ": " + err.message
              });
            } else {
              callback(null, {
                status: 1,
                data: log
              });
            }
          })
            .sort({ created_on: -1 })
            .skip(number_of_pages * (active_page - 1))
            .limit(number_of_pages);
        } catch (err) {
          res.send({
            status: 0,
            message: "Oops! " + err.name + ": " + err.message
          });
        }
      }
    },
    function(err, results) {
      res.send({ status: 1, data: results });
      // console.log(results);
      // // results now equals to: results.one: 'abc\n', results.two: 'xyz\n'
    }
  );

 
});

router.post('/getSingleBlog', async (req, res) => {

  const page_slug = req.body.data.page_slug;

  //search//

      try {
        let conditions = {page_slug : page_slug, status: true , deleteStatus: false};

        Blog.findOne(conditions, async function (err, log) {
          if (err) {
            res.send({
              status: 0,
              message: 'Oops! ' + err.name + ': ' + err.message
            })
          } else {
            res.send({
              status: 1,
              message: 'Success!',
              data: log
            })
          }
        });
      } catch (err) {
        res.send({
          status: 0,
          message: 'Oops! ' + err.name + ': ' + err.message
        })
      }
});

router.post('/getTest', async (req, res) => {

 

  //search//

      try {

        Blog.find( async function (err, log) {
          if (err) {
            res.send({
              status: 0,
              message: 'Oops! ' + err.name + ': ' + err.message
            })
          } else {
            res.send({
              status: 1,
              message: 'Success!',
              data: log
            })
          }
        });
     
      } catch (err) {
        res.send({
          status: 0,
          message: 'Oops! ' + err.name + ': ' + err.message
        })
      }
});
 
module.exports = router;
