const express = require("express");
const path = require("path");
const bcrypt = require("bcryptjs");
const moment = require("moment");
const sgMail = require("@sendgrid/mail");
var fs = require("fs");
const mustache = require("mustache");
const jwt = require("jsonwebtoken");
const mongoose = require("mongoose");

const Users = require("../models/users");
const Validate = require("./validations/validate");
const config = require("../config/config.json");
const jwtKey = config.jwsPrivateKey;
const sendGridKey = config.sendGridKey;
const fromMail = config.fromEmail;
const router = express.Router();
const Projects = require("../models/projects");
const Leads = require("../models/leads");
const workingDetails = require("../models/working_details");

router.post("/get_profile", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Sorry session expired" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      const user_id = payload._id;
      try {
        Users.findOne({ _id: user_id }, function(err, log) {
          if (err) {
            res.send({
              status: 0,
              message: "Oops! " + err.name + ": " + err.message
            });
          } else if (!log) {
            res.send({
              status: 0,
              message:
                "There is no existing user with the details that you’ve provided."
            });
          } else {
            res.send({
              status: 1,
              message: "User details fetched.",
              data: log
            });
          }
        });
      } catch (err) {
        res.send({
          status: 0,
          message: "Oops! " + err.name + ": " + err.message
        });
      }
    } catch (err) {
      res.send({
        status: 0,
        message: "Oops! " + err.name + ": " + err.message
      });
    }
  }
});
router.post("/signup", async (req, res) => {
  console.log("req", req.body)
    try {
      Validate.Usersignup(req, result => {
        if (result.error) {
          //validation error
          console.log("req", result.error.details[0].message)
          res.send({
            status: 0,
            message: result.error.details[0].message
          });
        } else {
          //user existance check
          Users.findOne(
            { email: req.body.email, deleteStatus: false },
            async function(error, log) {
              if (log != null) {
                res.send({
                  status: 0,
                  message: "User with provided email already exist."
                });
              } else {
                //decrypt password
                const salt = bcrypt.genSaltSync(10);
                let createdon = moment().format("MM-DD-YYYY hh:mm:ss");
                bcryptPassword = bcrypt.hashSync(req.body.password, salt);
                //create new Users instance with request data
                const newUser = new Users({
                  email: req.body.email,
                  password: bcryptPassword,
                  notify_me: req.body.notify_me,
                  created_on: createdon
                });
                //save user to mongoo db
                try {
                  await newUser
                    .save()
                    .then(() => {
                      let user_token = jwt.sign(
                        {
                          _id: newUser._id,
                          email: newUser.email,
                          created_on: newUser.created_on
                        },
                        jwtKey
                      );
                      try {
                   
                        if(req.body.leadsData !== undefined && Object.keys(req.body.leadsData).length !== 0)
                          createProjectLanding(newUser._id, req.body.leadsData);
                        sendverifymail(newUser, result => {
                          if (result == "success") {
                            res.send({
                              status: 1,
                              message:
                                "You have registered successfully, You’ll be redirected.",
                              data: {
                                token: user_token
                              }
                            });
                          } else if (result == "failed") {
                            res.send({
                              status: 1,
                              message:
                                "You have registered successfully. Failed to send verification mail, You’ll be redirected.",
                              data: {
                                token: user_token
                              }
                            });
                          }
                        });
                      } catch (err) {
                        console.log("req- 2", "Oops! " + err.name + ": " + err.message)
                        res.send({
                          status: 0,
                          message: "Oops! " + err.name + ": " + err.message
                        });
                      }
                    })
                    .catch(err => {
                      console.log("req- 3", "Oops! " + err.name + ": " + err.message)
                      res.send({
                        status: 0,
                        message: "Oops! " + err.name + ": " + err.message
                      });
                    });
                } catch (err) {
                  console.log("req- 4", "Oops! " + err.name + ": " + err.message)
                  res.send({
                    status: 0,
                    message: "Oops! " + err.name + ": " + err.message
                  });
                }
              }
            }
          );
        }
      });
    } catch (err) {
      console.log("req- 5", "Oops! " + err.name + ": " + err.message)
      res.send({
        status: 0,
        message: "Oops! " + err.name + ": " + err.message
      });
    }
});

//varify mail sending//
router.post("/sendVerificationmail", async (req, res) => {
  try {
    Users.findOne({ email: req.body.email }, async function(error, log) {
      if (error) {
        res
          .status(400)
          .send({ status: 0, message: "Oops! Something went wrong." });
      } else if (log != null) {
        try {
          sendverifymail(log, result => {
            if (result == "success") {
              res.send({
                status: 1,
                message: "Please check your mail and verify your account "
              });
            } else if (result == "failed") {
              res.send({
                status: 0,
                message:
                  "Oops! Something went wrong with sending verification mail."
              });
            }
          });
        } catch (err) {
          res.send({
            status: 0,
            message: "Oops! " + err.name + ": " + err.message
          });
        }
      }
    });
  } catch (err) {
    res.send({
      status: 0,
      message: "Oops! " + err.name + ": " + err.message
    });
  }
});

router.post("/verifyAccount", async (req, res) => {
  //const user_id = req.query.id;
  const user_id = req.body.user_id;

  try {
    Users.findOne({ _id: user_id }, async function(err, log) {
      if (err) {
        res.send({
          status: 0,
          message: "Oops! " + err.name + ": " + err.message
        });
      } else if (log == null) {
        res.send({
          status: 0,
          message: "We currently have no account with that email."
        });
      } else {
        if (log.email_verification == true) {
          res.send({
            status: 1,
            message: "You have already verified your account. "
          });
        } else {
          log.email_verification = true;
          log.save();
          res.send({
            status: 1,
            message:
              "Your account has been successfully verified. Please continue to "
          });
        }
      }
    });
  } catch (err) {
    res.send({
      status: 0,
      message: "Oops! " + err.name + ": " + err.message
    });
  }
});

router.post("/forgotPassword", async (req, res) => {
  if (req.body.data.forgot_email) {
    try {
      Users.findOne(
        { email: req.body.data.forgot_email, deleteStatus: false },
        await function(err, log) {
          if (err) {
            res.send({
              status: 0,
              message: "Oops! " + err.name + ": " + err.message
            });
          } else if (!log) {
            res.send({
              status: 0,
              message:
                "There is no existing user with the details that you’ve provided."
            });
          } else {
            try {
              sendForgotpwdMail(log, result => {
                if (result == "success") {
                  res.send({
                    status: 1,
                    message:
                      "Please check your email and click on provided link to reset your password."
                  });
                } else {
                  res.send({
                    status: 0,
                    message:
                      "Oops! Something went wrong with sending verification mail."
                  });
                }
              });
            } catch (err) {
              res.send({
                status: 0,
                message: "Oops! " + err.name + ": " + err.message
              });
            }
          }
        }
      );
    } catch (err) {
      res.send({
        status: 0,
        message: "Oops! " + err.name + ": " + err.message
      });
    }
  } else {
    res.send({ status: 0, message: "Please enter a properly formatted email" });
  }
});
function sendForgotpwdMail(userLog, callback) {
  let template = fs
    .readFileSync(path.join(__dirname + "/mail-templates/forgotPwd.html"), {
      encootding: "utf-8"
    })
    .toString();
  let urlpass = config.siteurl + "resetpassword/"+ userLog._id + "";

  var view = {
    id: userLog._id,
    name: userLog.email,
    siteuUrl: urlpass,
    imageUrl: config.imageUrl
  };
  var createTemplate = mustache.render(template, view);

  sgMail.setApiKey(sendGridKey);
  const message = {
    to: userLog.email,
    from: { email: fromMail, name: "Footprint" },
    subject: "Footprint Reset Password Link",
    html: createTemplate
  };

  try {
    sgMail
      .send(message)
      .then(() => {
        callback("success");
        console.log("success");
      })
      .catch(error => {
        // //Log friendly error
        //console.error("send err", error.toString());
        // res.status(400).send(err);
        // //Extract error msg
        //const { message, code, response } = error;

        // //Extract response msg
        //const { headers, body } = response;
        //console.log(message, body.errors);
        callback("failed");
      });
  } catch (err) {
    callback("failed");
    console.log("Error show", err);
  }
}
/*************Reset password ***********/
router.post("/resetPassword", async (req, res) => {
  if (req.body.user_id) {
    try {
      Users.findOne(
        { _id: req.body.user_id },
        await function(err, log) {
          if (err) {
            res.send({
              status: 0,
              message: "Oops! " + err.name + ": " + err.message
            });
          } else if (!log) {
            res.send({
              status: 0,
              message:
                "There is no existing user with the details that you’ve provided."
            });
          } else {
            try {
              //decrypt password
              const salt = bcrypt.genSaltSync(10);
              let createdon = moment().format("MM-DD-YYYY hh:mm:ss");
              bcryptPassword = bcrypt.hashSync(req.body.data.new_password, salt);
              log.password = bcryptPassword;
              log.save();
              res.send({
                status: 1,
                message:
                  "You have successfully updated your password. Please continue to "
              });
            } catch (err) {
              res.send({
                status: 0,
                message: "Oops! " + err.name + ": " + err.message
              });
            }
          }
        }
      );
    } catch (err) {
      res.send({
        status: 0,
        message: "Oops! " + err.name + ": " + err.message
      });
    }
  } else {
    res.send({ status: 0, message: "Invalid request." });
  }
});
function sendverifymail(newUser, callback) {
  let template = fs
    .readFileSync(path.join(__dirname + "/mail-templates/emailVerify.html"), {
      encootding: "utf-8"
    })
    .toString();
  let urlpass = config.siteurl + "verify/" + newUser._id + "";
  var view = {
    id: newUser._id,
    name: newUser.email,
    siteuUrl: urlpass,
    imageUrl: config.imageUrl
  };
  var createTemplate = mustache.render(template, view);
  sgMail.setApiKey(sendGridKey);
  const message = {
    to: newUser.email,
    from: { email: fromMail, name: "Footprint" },
    subject: "Footprint Account Verification",
    html: createTemplate
  };

  try {
    sgMail
      .send(message)
      .then(() => {
        callback("success");
        console.log("success");
      })
      .catch(error => {
        // //Log friendly error
        console.error("send err", error.toString());
        // res.status(400).send(err);
        // //Extract error msg
        const { message, code, response } = error;

        // //Extract response msg
        const { headers, body } = response;
        console.log(message, body.errors);
        callback("failed");
      });
  } catch (err) {
    callback("failed");
    console.log("Error show", err);
  }
}


router.post("/readWorkingDetails", async (req, res) => {
  const token = req.body.token;
  const data = req.body;
   if (!token) {
    return res.send({ status: 0, message: "Sorry session expired" });
  } else {
    try {
      const payload = jwt.verify(token, jwtKey);
           
 

        try {
          var condition = { _id: req.body.id , user_id : payload._id}
          var update_value = {
            read_post: true
          }
          workingDetails.findByIdAndUpdate(condition, update_value, async function (
            err,
            log
          ) {
            if (err) {
              res.send({
                status: 0,
                message: 'Oops! ' + err.name + ': ' + err.message
              })
            } else {
              res.send({
                status: 1,
                message: 'Success!'
              })
            }
          })
        } catch (err) {
          res.send({
            status: 0,
            message: 'Oops! ' + err.name + ': ' + err.message
          })
        }
        
      
    } catch (err) {
      res.send({
        status: 0,
        message: "Oops! " + err.name + ": " + err.message
      });
    }
  }
}); 
router.post("/create_project", async (req, res) => {
  const token = req.body.token;
  const data = req.body;
   if (!token) {
    return res.send({ status: 0, message: "Sorry session expired" });
  } else {
    try {
      const payload = jwt.verify(token, jwtKey);
           
        let new_project = new Projects({
          user_id: payload._id,
          project_name: data.project_name,
          website: data.website,
          added_on: moment().format("MM-DD-YYYY hh:mm:ss"),
        });
          try {
            await new_project
              .save()
              .then(() => {
 
                res.send({
                  status: 1,
                  message: "Project created successfully.",
                  data: { project: new_project }
                });
              })
              .catch(err => {
                res.send({
                  status: 0,
                  message: "Oops! " + err.name + ": " + err.message
                });
              });
          } catch (err) {
            res.send({
              status: 0,
              message: "Oops! " + err.name + ": " + err.message
            });
          }
        
      
    } catch (err) {
      res.send({
        status: 0,
        message: "Oops! " + err.name + ": " + err.message
      });
    }
  }
}); 
 
router.post("/get_projects", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Sorry session expired" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      const user_id = payload._id;
      try {
        Projects.find({ user_id: user_id }, async function(err, log) {
          if (err) {
            res.send({
              status: 0,
              message: "Oops! " + err.name + ": " + err.message
            });
          } else {
            res.send({
              status: 1,
              message: "success",
              data: log
            });
          }
        }).sort({ _id: -1 });
      } catch (err) {}
    } catch (err) {}
  }
});

router.post("/getWorkingDetailsList", async (req, res) => {
  const token = req.body.token;
  console.log("user_idsssssss")
 
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      const user_id = payload._id;
      console.log("user_id", user_id)
      try {
        workingDetails
          .aggregate([
            { $match: { user_id: mongoose.Types.ObjectId(user_id), post_status: true } },
            { $sort: { report_date: -1 } },
            {
              $lookup: {
                from: "projects",
                localField: "project_id",
                foreignField: "_id",
                as: "project"
              }
            }
          ])
          .exec((err, workingDetails) => {
            if (err) {
              res.send({
                status: 0,
                message: "Oops! " + err.name + ": " + err.message
              });
            } else if (workingDetails != "") {
              res.send({
                status: 1,
                message: "workingDetails fetched sucessfuly!",
                data: workingDetails
              });
            } else {
              res.send({
                status: 0,
                message: "No data found"
              });
            }
          });
      } catch (err) {
        res.send({
          status: 0,
          message: "Oops! " + err.name + ": " + err.message
        });
      }
    } catch (err) {
      res.send({
        status: 0,
        message: "Oops! " + err.name + ": " + err.message
      });
    }
  }
});

router.post("/updateBillingAddress", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Sorry session expired" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      const user_id = payload._id;
      try {
        var update_value = { billing: req.body.billing };
        var condition = { _id: user_id };
        Users.findByIdAndUpdate(condition, update_value, async function(
          err,
          log
        ) {
          if (err) {
            res.send({
              status: 0,
              message: "Oops! " + err.name + ": " + err.message
            });
          } else {
            // let user_name =
            //   log.profile.first_name + " " + log.profile.last_name;
            // let activity_data = {
            //   user: user_id,
            //   activity: "Billing details Changed",
            //   details: {
            //     message: user_name + " changed billing details."
            //   }
            // };
            // saveActivity(activity_data);
            res.send({
              status: 1,
              message: "Profile Successfully Updated."
            });
          }
        });
      } catch (err) {
        res.send({
          status: 0,
          message: "Oops! " + err.name + ": " + err.message
        });
      }
    } catch (err) {
      res.send({
        status: 0,
        message: "Oops! " + err.name + ": " + err.message
      });
    }
  }
});

router.post("/updateSettings", async (req, res) => {
  const token = req.body.token;
  if (!token) {
    res.send({ status: 0, message: "Sorry session expired" });
  } else {
    try {
      payload = jwt.verify(token, jwtKey);
      const user_id = payload._id;
      try {
        if (req.body.password != "" && req.body.current_password != "") {
          Users.findOne({ _id: user_id }, async function(error, log) {
            const validatepassword = bcrypt.compareSync(
              req.body.current_password,
              log.password
            );

            if (validatepassword === false) {
              res.send({
                status: 0,
                message: "You have entered an invalid current password."
              });
            } else {
              //decrypt password
              const salt = bcrypt.genSaltSync(10);
              bcryptPassword = bcrypt.hashSync(req.body.password, salt);

              var update_value = {
                password: bcryptPassword,
                billing: req.body.billing,
                profile: req.body.profile
                //email: req.body.billing.login_email
              };
              console.log(update_value);
              var condition = { _id: user_id };
              Users.findByIdAndUpdate(condition, update_value, async function(
                err,
                log
              ) {
                if (err) {
                  res.send({
                    status: 0,
                    message: "Oops! " + err.name + ": " + err.message
                  });
                } else {
                  // let user_name =
                  //   log.profile.first_name + " " + log.profile.last_name;
                  // let activity_data = {
                  //   user: user_id,
                  //   activity: "Settings Changed",
                  //   details: {
                  //     message: user_name + " changed profile settings."
                  //   }
                  // };
                  // saveActivity(activity_data);
                  res.send({
                    status: 1,
                    message: "Profile Successfully Updated!!!"
                  });
                }
              });
            }
          });
        } else {
          var update_value = {
            billing: req.body.billing,
            profile: req.body.profile
          };
        }
        var condition = { _id: user_id };
        Users.findByIdAndUpdate(condition, update_value, async function(
          err,
          log
        ) {
          if (err) {
            res.send({
              status: 0,
              message: "Oops! " + err.name + ": " + err.message
            });
          } else {
            // let user_name =
            //   log.profile.first_name + " " + log.profile.last_name;
            // let activity_data = {
            //   user: user_id,
            //   activity: "Settings Changed",
            //   details: {
            //     message: user_name + " changed profile settings."
            //   }
            // };
            // saveActivity(activity_data);
            res.send({
              status: 1,
              message: "Profile Successfully Updated."
            });
          }
        });
      } catch (err) {
        res.send({
          status: 0,
          message: "Oops! " + err.name + ": " + err.message
        });
      }
    } catch (err) {
      res.send({
        status: 0,
        message: "Oops! " + err.name + ": " + err.message
      });
    }
  }
});

router.post("/loginSaveProjectData", async (req, res) => {
  const token = req.body.token;
  let data = req.body.data.project_data;
  if (!token) {
    res.send({ status: 0, message: "Invalid Request" });
  } else {
    try {
      let payload = jwt.verify(token, jwtKey);
      let user_id = payload._id;
      try {
        createProjectLanding(user_id, data);
        res.send({
          status: 1,
          message: "Thank you for contacting with us. Our team will contact you soon."
        });
      } catch (err) {
        res.send({
          status: 2,
          message: "Oops! " + err.name + ": " + err.message
        });
      }
    } catch (err) {
      res.send({
        status: 0,
        message: "Oops! " + err.name + ": " + err.message
      });
    }
  }
});

function createProjectLanding(user_id, data, page="landing") {
  let new_project = new Leads({
    user_id: user_id,
    project_name: data.project_name,
    project_details: data.project_details,
    project_pay: data.project_pay,
    project_type: data.project_type,
    project_file: data.project_file,
    project_skill: data.project_skill,
    added_page: page,
    added_on: moment().format("MM-DD-YYYY hh:mm:ss"),
  });
  new_project.save();

  let template = fs
  .readFileSync(path.join(__dirname + "/mail-templates/lead.html"), {
    encootding: "utf-8"
  })
  .toString();
  let view = {
    imageUrl: config.imageUrl
  };
  let subject = "Footprint Lead Request";
  var createTemplate = mustache.render(template, view);
  sgMail.setApiKey(sendGridKey);
  const message = {
    to: config.admin,
    from: { email: config.fromEmail, name: "Footprint" },
    subject: subject,
    html: createTemplate
  };

  try {
    sgMail
      .send(message)
      .then(() => {
        console.log("success");
      })
      .catch(error => {
        // //Log friendly error
        console.error("send err", error.toString());
        // res.status(400).send(err);
        // //Extract error msg
        const { message, code, response } = error;

        // //Extract response msg
        const { headers, body } = response;
        console.log(message, body.errors);
      });
  } catch (err) {
    console.log("Error show", err);
  }

}
module.exports = router;