//load dependencies
const express = require("express");
const joi = require("joi");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcryptjs");
const moment = require("moment");
//Include schema
const Users = require("../models/users");
const Leads = require("../models/leads");
//include joi validation methods
const path = require("path");
const Validate = require("./validations/validate");
const config = require("../config/config.json");
const sgMail = require("@sendgrid/mail");
var fs = require("fs");
const mustache = require("mustache");
const jwtKey = config.jwsPrivateKey;
const sendGridKey = config.sendGridKey;
const fromMail = config.fromEmail;
const router = express.Router();

router.post("/", async (req, res) => {
    //validate user request using joi
    try {
      Validate.validateLogin(req, result => {
        if (result.error) {
          res.send({
            status: 0,
            message: result.error.details[0].message
          });
        } else {
          //user login check
          try {
            Users.findOne(
              { email: req.body.email, deleteStatus: false },
              function(err, log) {
                if (err) {
                  res.send({
                    status: 0,
                    message: "Oops! " + err.name + ": " + err.message
                  });
                } else if (!log) {
                  res.send({
                    status: 0,
                    message:
                      "There is no existing user with the details that you’ve provided."
                  });
                } else {
                  const validatepassword = bcrypt.compareSync(
                    req.body.password,
                    log.password
                  );
                  const token = jwt.sign(
                    {
                      _id: log._id,
                      email: log.email,
                      account_type: log.account_type,
                      account_status: log.status,
                      created_on: log.created_on
                    },
                    jwtKey
                  );
                  if (!validatepassword) {
                    res.send({
                      status: 0,
                      message:
                        "You have entered an invalid email id and/or password."
                    });
                  } else if (log.email_verification === false) {
                    res.send({
                      status: 2,
                      message:
                        "Your account hasn't been verified. Please click below to re-verify your account.",
                      data: { email: log.email }
                    });
                  } else if (log.status === false) {
                    res.send({
                      status: 0,
                      message:
                        "Your account has been suspended! Please contact admin.",
                      data: { email: log.email }
                    });
                  } else {
                    log.last_login = moment().format("MM-DD-YYYY hh:mm:ss");
                    log.save();
                    const token = jwt.sign(
                      {
                        _id: log._id,
                        email: log.email,
                        account_status: log.status,
                        created_on: log.created_on
                      },
                      jwtKey
                    );
                    if(req.body.leadsData !== undefined && Object.keys(req.body.leadsData).length !== 0)
                          createProjectLanding(log._id, req.body.leadsData);
                    res.send({
                      status: 1,
                      message:
                        "You’ve logged in successfully. You’ll be redirected.",
                      data: { token: token }
                    });
                  }
                }
              }
            );
          } catch (ex) {
            res.send({
              status: 0,
              message: "Oops! " + ex.name + ": " + ex.message
            });
          }
        }
      });
    } catch (err) {
      res.send({
        status: 0,
        message: "Oops! " + ex.name + ": " + ex.message
      });
    }
  });

  function createProjectLanding(user_id, data, page="landing") {
    let new_project = new Leads({
      user_id: user_id,
      project_name: data.project_name,
      project_details: data.project_details,
      project_pay: data.project_pay,
      project_type: data.project_type,
      project_file: data.project_file,
      project_skill: data.project_skill,
      added_page: page,
      added_on: moment().format("MM-DD-YYYY hh:mm:ss"),
    });
    new_project.save();
  
    let template = fs
    .readFileSync(path.join(__dirname + "/mail-templates/lead.html"), {
      encootding: "utf-8"
    })
    .toString();
    let view = {
      imageUrl: config.imageUrl
    };
    let subject = "Footprint Lead Request";
    var createTemplate = mustache.render(template, view);
    sgMail.setApiKey(sendGridKey);
    const message = {
      to: config.admin,
      from: { email: config.fromEmail, name: "Footprint" },
      subject: subject,
      html: createTemplate
    };
  
    try {
      sgMail
        .send(message)
        .then(() => {
          console.log("success");
        })
        .catch(error => {
          // //Log friendly error
          console.error("send err", error.toString());
          // res.status(400).send(err);
          // //Extract error msg
          const { message, code, response } = error;
  
          // //Extract response msg
          const { headers, body } = response;
          console.log(message, body.errors);
        });
    } catch (err) {
      console.log("Error show", err);
    }
  
  }
module.exports = router;