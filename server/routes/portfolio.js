//load dependencies
const express = require("express");
const path = require("path");
const joi = require("joi");
const jwt = require("jsonwebtoken");
var fs = require("fs");
const mongoose = require("mongoose");
const config = require("../config/config.json");
const jwtKey = config.jwsPrivateKey;
//Include schema
const Portfolio = require("../models/portfolio");
const Portfolio_categories = require("../models/portfolioCategories");

//http request port set
const router = express.Router();

router.post("/getCategories", async (req, res) => {
    try {
      Portfolio_categories.find(async function(err, log) {
        if (err) {
          res.send({
            status: 0,
            message: "Oops! " + err.name + ": " + err.message
          });
        } else {
          res.send({
            status: 1,
            message: "Sucess!",
            data: log
          });
        }
      }).sort({ order: 1 });
    } catch (err) {
      res.send({
        status: 0,
        message: "Oops! " + err.name + ": " + err.message
      });
    }
});

router.post("/getPortfolios", async (req, res) => {
 
    try {
      Portfolio.find(async function(err, log) {
        if (err) {
          res.send({
            status: 0,
            message: "Oops! " + err.name + ": " + err.message
          });
        } else {
          res.send({
            status: 1,
            message: "Sucess!",
            data: log
          });
        }
      }).sort({ order: 1 });
    } catch (err) {
      res.send({
        status: 0,
        message: "Oops! " + err.name + ": " + err.message
      });
    }
});

 
 
module.exports = router;
