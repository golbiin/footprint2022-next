//Load dependencies
const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const dbconnect = require("./config/connection");
const path = require("path");
const http = require("http");
// const config = require("./config/config.json");

const user = require("./routes/user");
const authenticate = require("./routes/authenticate");

const adminauth = require("./routes/admin/authenticate");
const manageusers = require("./routes/admin/manageUsers");
const settings = require("./routes/admin/settings");
const subscriptions = require("./routes/admin/subscriptions");
const portfolio = require("./routes/admin/portfolio");
const review = require("./routes/admin/review");
const uploads = require("./routes/admin/upload");
const coupons = require("./routes/admin/coupon");
const blogs = require("./routes/admin/blogs");
const payment = require("./routes/payment");
const portfolioFront = require("./routes/portfolio");
const fetchsitedata = require("./routes/fetchsitedata");
const blogsClient = require("./routes/blogs");
const contacts = require("./routes/contacts");
const uploadMultiple = require("./routes/uploadMultiple");
const app = express();
app.use(cors());
app.use(express.json());

app.use(express.static("public"));
app.use("/images", express.static("images"));

//default request method
app.get("/", (req, res) => res.send("API Haandle!" + process.env.NODE_ENV));
app.use("/api/user", user);
app.use("/api/authenticate", authenticate);
app.use("/api/payment", payment);
app.use("/api/portfolio", portfolioFront);
app.use("/api/fetchsitedata", fetchsitedata);
app.use("/api/blogs", blogsClient);


//admin routes
app.use("/api/admin", adminauth);
app.use("/api/admin/manage/users", manageusers);
app.use("/api/admin/settings", settings);
// app.use("/api/admin/subscriptions", subscriptions);
// app.use("/api/contacts", contacts);
app.use("/api/admin/portfolio", portfolio);
app.use("/api/admin/review", review);
app.use("/api/admin/upload", uploads);
app.use("/api/admin/coupons", coupons);
app.use("/api/admin/subscriptions", subscriptions);
app.use("/api/admin/blogs", blogs);
app.use("/api/contacts", contacts);
app.use("/api/uploadMultiple", uploadMultiple);
// //connect mongoDb
dbconnect();
var httpServer = http.createServer(app);
httpServer.listen(4000, function() {
  console.log("server is running on port :4000");
});
