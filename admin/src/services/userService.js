import apiUrl from "../constants/server";
const axios = require("axios").default;

export function signupUser(users, ip) {
  return axios.post(apiUrl + "/user/signup", {
    email: users.email,
    password: users.password,
    tips_feedback: users.tips,
    products_promotions: users.promotions,
    page: users.page,
    ip: ip
  });
}

export function verify(id) {
  return axios.post(apiUrl + "/user/verifyAccount", {
    user_id: id
  });
}

export function geUserProfile() {
  return axios.post(apiUrl + "/user/get_profile", {
    token: localStorage.getItem("userToken")
  });
}

export function updateBillingAddress(data) {
  return axios.post(apiUrl + "/user/updateBillingAddress", {
    token: localStorage.getItem("userToken"),
    billing: data
  });
}

export function updateUserProfile(data) {
  return axios.post(apiUrl + "/user/updateUserProfile", {
    token: localStorage.getItem("userToken"),
    profile: data
  });
}

export function contact(data, type, page, ip, humankey) {
  return axios.post(apiUrl + "/contacts/contact", {
    data: data,
    type: type,
    page: page,
    ip: ip,
    humankey: humankey
  });
}
export function banner_contact(data, type, page, ip, humankey) {
  return axios.post(apiUrl + "/contacts/banner_contact", {
    data: data,
    type: type,
    page: page,
    ip: ip
  });
}
export function updateSettings(
  data,
  profileData,
  password = "",
  current_password = ""
) {
  return axios.post(apiUrl + "/user/updateSettings", {
    token: localStorage.getItem("userToken"),
    billing: data,
    profile: profileData,
    password: password,
    current_password: current_password
  });
}

export function getTransactions() {
  return axios.post(apiUrl + "/user/transcations", {
    token: localStorage.getItem("userToken")
  });
}
export function getSubscriptionList() {
  return axios.post(apiUrl + "/user/subscription-list", {
    token: localStorage.getItem("userToken")
  });
}

export function getSubscriptons() {
  return axios.post(apiUrl + "/user/subscription", {
    token: localStorage.getItem("userToken")
  });
}
export function getWorkingDetails() {
  return axios.post(apiUrl + "/user/getWorkingDetails", {
    token: localStorage.getItem("userToken")
  });
}
