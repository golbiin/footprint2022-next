import SERVER_URL from "../../constants/server";
const tokenKey = "admin_token";
const axios = require("axios").default;
const UPLOADAPI = "https://api.outsourceme.com/api";

export function getAllcats() {
  return axios.post(SERVER_URL + "/admin/review/getCategories", {
    token: localStorage.getItem(tokenKey)
  });
}

export function getAllreview(activePageNum, number_of_pages) {
  return axios.post(SERVER_URL + "/admin/review/getreviews", {
    token: localStorage.getItem(tokenKey),
    activePage: activePageNum,
    pages: number_of_pages
  });
}

export function getTotalreview() {
  return axios.post(SERVER_URL + "/admin/review/getCount", {
    token: localStorage.getItem(tokenKey)
  });
}

export function addreview(data, parent, child, language) {
  return axios.post(SERVER_URL + "/admin/review/addreview", {
    token: localStorage.getItem(tokenKey),
    data: data,
    main_cat: parent,
    sub_cat: child,
    language: language
  });
}

export function updatereview(data, id) {
  return axios.post(SERVER_URL + "/admin/review/updatereview", {
    token: localStorage.getItem(tokenKey),
    data: data,
    id: id
  });
}

export function deletereview(id) {
  return axios.post(SERVER_URL + "/admin/review/deletereview", {
    token: localStorage.getItem(tokenKey),
    data: id
  });
}

// export function uploadFile(formData, config) {
//   return axios.post(UPLOADAPI + "/admin/uploads", formData, config, {
//     token: localStorage.getItem(tokenKey)
//   });
// }

export function uploadFile(upload_data) {
  console.log(upload_data);
  let token = localStorage.getItem(tokenKey);
  const upload = new FormData();
  upload.append("file", upload_data);
  return axios.post(
    SERVER_URL + "/admin/upload/uploadData/" + token,
    upload,
    {}
  );
}
