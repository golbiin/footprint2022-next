import SERVER_URL from "../../constants/server";
const tokenKey = "admin_token";
const axios = require("axios").default;

export function createBlog(blog) {
  return axios.post(SERVER_URL + "/admin/blogs/createBlog", {
    data: blog,
    token: localStorage.getItem(tokenKey)
  });
}

export function getAllBlogs(data) {
  return axios.post(SERVER_URL + "/admin/blogs/getAllBlogs", {
    data: data,
    token: localStorage.getItem(tokenKey)
  });
}
export function getCount(data) {
  return axios.post(SERVER_URL + "/admin/blogs/getCount", {
    data: data,
    token: localStorage.getItem(tokenKey)
  });
}

export function changeStatus(id, status) {
  return axios.post(SERVER_URL + "/admin/blogs/blogsChangeStatus", {
    blog_id: id,
    status: status,
    token: localStorage.getItem(tokenKey)
  });
}

export function deleteBlog(id) {
  return axios.post(SERVER_URL + "/admin/blogs/deleteBlog", {
    blog_id: id,
    token: localStorage.getItem(tokenKey)
  });
}
export function getSingleBlog(Id) {
  return axios.post(SERVER_URL + "/admin/blogs/getSingleBlog", {
    blog_id: Id,
    token: localStorage.getItem(tokenKey)
  });
}
export function UpdateBlog(Id,data) {
  return axios.post(SERVER_URL + "/admin/blogs/updateBlog", {
    blog_id: Id,
    data: data,
    token: localStorage.getItem(tokenKey)
  });
}

export function getAllBlogCategory() {
  return axios.post(SERVER_URL + "/admin/blogs/getAllBlogCategory", {
    token: localStorage.getItem(tokenKey)
  });
}
export function getAllBlogAuthors() {
  return axios.post(SERVER_URL + "/admin/blogs/getAllBlogAuthors", {
    token: localStorage.getItem(tokenKey)
  });
}
export function getAllBlogLayout() {
  return axios.post(SERVER_URL + "/admin/blogs/getAllBlogLayout", {
    token: localStorage.getItem(tokenKey)
  });
}

export function uploadFile(upload_data) {
  console.log(upload_data);
  let token = localStorage.getItem(tokenKey);
  const upload = new FormData();
  upload.append("file", upload_data);
  return axios.post(
    SERVER_URL + "/admin/upload/uploadData/" + token,
    upload,
    {}
  );
}


