import apiUrl from "../../constants/server";
import jwtDecode from "jwt-decode";
const axios = require("axios").default;
const tokenKey = "admin_token";
const rememberKey = "admin_remember_token";

export function authenticate(users) {
  return axios.post(apiUrl + "/admin/authenticate", {
    username: users.username,
    password: users.password,
  });
}
export function loginWithJwt(jwt, remember) {
  localStorage.setItem(tokenKey, jwt);
  if (remember) {
    localStorage.setItem(rememberKey, jwt);
  }
  return true;
}

export function logout() {
  localStorage.removeItem(rememberKey);
  localStorage.removeItem(tokenKey);
}
export function getCurrentUser() {
  try {
    const jwt = localStorage.getItem(tokenKey);
    return jwtDecode(jwt);
  } catch (ex) {
    return null;
  }
}
