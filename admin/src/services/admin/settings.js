import apiUrl from "../../constants/server";
const tokenKey = "admin_token";
const axios = require("axios").default;

export function changePassword(data) {
  return axios.post(apiUrl + "/admin/settings/updatePassword", {
    data: data,
    token: localStorage.getItem(tokenKey)
  });
}
export function getTotalTracking(key) {
  return axios.post(apiUrl + "/admin/settings/getTotalTracking", {
    token: localStorage.getItem(tokenKey),
    key: key
  });
}
export function getAllTrackings(data, key) {
  return axios.post(apiUrl + "/admin/settings/getAllTrackings", {
    token: localStorage.getItem(tokenKey),
    data: data,
    key: key
  });
}
export function getTotalUserTracking(id) {
  return axios.post(apiUrl + "/admin/settings/getTotalUserTracking", {
    token: localStorage.getItem(tokenKey),
    id: id
  });
}
export function getAllUserTrackings(id, data) {
  return axios.post(apiUrl + "/admin/settings/getAllUserTrackings", {
    token: localStorage.getItem(tokenKey),
    data: data,
    id: id
  });
}
export function getTotalActivities(key) {
  return axios.post(apiUrl + "/admin/settings/getTotalActivity", {
    token: localStorage.getItem(tokenKey),
    key: key
  });
}
export function getAllActivities(data, key) {
  return axios.post(apiUrl + "/admin/settings/getAllActivities", {
    token: localStorage.getItem(tokenKey),
    data: data,
    key: key
  });
}
export function getTotalUserActivities(id) {
  return axios.post(apiUrl + "/admin/settings/getTotalUserActivity", {
    token: localStorage.getItem(tokenKey),
    id: id
  });
}
export function getAllUserActivities(id, data) {
  return axios.post(apiUrl + "/admin/settings/getAllUserActivities", {
    token: localStorage.getItem(tokenKey),
    data: data,
    id: id
  });
}
export function getTotalLogins(data) {
  return axios.post(apiUrl + "/admin/settings/getTotalLogins", {
    token: localStorage.getItem(tokenKey),
    data: data
  });
}
export function getAllLogins(data) {
  return axios.post(apiUrl + "/admin/settings/getAllLogins", {
    token: localStorage.getItem(tokenKey),
    data: data
  });
}
export function getTotalLeads(data) {
  return axios.post(apiUrl + "/admin/settings/getTotalLeads", {
    token: localStorage.getItem(tokenKey),
    data: data
  });
}
export function getAllLeads(data) {
  return axios.post(apiUrl + "/admin/settings/getAllLeads", {
    token: localStorage.getItem(tokenKey),
    data: data
  });
}
export function getNewsletter(data) {
  return axios.post(apiUrl + "/admin/settings/getNewsLetter", {
    token: localStorage.getItem(tokenKey),
    data: data
  });
}

export function getTotalProjects(data) {
  return axios.post(apiUrl + "/admin/settings/getTotalProjects", {
    token: localStorage.getItem(tokenKey),
    data: data
  });
}
export function getTotalNewsLetter(data) {
  return axios.post(apiUrl + "/admin/settings/getTotalNewsLetter", {
    token: localStorage.getItem(tokenKey),
    data: data
  });
}
export function getAllProjects(data) {
  return axios.post(apiUrl + "/admin/settings/getAllProjects", {
    token: localStorage.getItem(tokenKey),
    data: data
  });
}
export function getAllNewsLetter(data) {
  return axios.post(apiUrl + "/admin/settings/getAllNewsLetter", {
    token: localStorage.getItem(tokenKey),
    data: data
  });
}
export function getChartData() {
  return axios.post(apiUrl + "/admin/settings/chartData", {
    token: localStorage.getItem(tokenKey)
  });
}

export function getLeadDetails(id) {
  return axios.post(apiUrl + "/admin/settings/getLeadDetails", {
    token: localStorage.getItem(tokenKey),
    id: id
  });
}
