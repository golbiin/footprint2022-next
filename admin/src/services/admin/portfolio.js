import SERVER_URL from "../../constants/server";
const tokenKey = "admin_token";
const axios = require("axios").default;
const UPLOADAPI = "https://api.outsourceme.com/api";

export function getAllcats() {
  return axios.post(SERVER_URL + "/admin/portfolio/getCategories", {
    token: localStorage.getItem(tokenKey)
  });
}

export function getAllportfolio(activePageNum, number_of_pages) {
  return axios.post(SERVER_URL + "/admin/portfolio/getPortfolios", {
    token: localStorage.getItem(tokenKey),
    activePage: activePageNum,
    pages: number_of_pages
  });
}

export function getTotalPortfolio() {
  return axios.post(SERVER_URL + "/admin/portfolio/getCount", {
    token: localStorage.getItem(tokenKey)
  });
}

export function addPortfolio(data, parent, child, language) {
  return axios.post(SERVER_URL + "/admin/portfolio/addPortfolio", {
    token: localStorage.getItem(tokenKey),
    data: data,
    main_cat: parent,
    sub_cat: child,
    language: language
  });
}

export function updatePortfolio(data, id) {
  return axios.post(SERVER_URL + "/admin/portfolio/updatePortfolio", {
    token: localStorage.getItem(tokenKey),
    data: data,
    id: id
  });
}

export function deletePortfolio(id) {
  return axios.post(SERVER_URL + "/admin/portfolio/deletePortfolio", {
    token: localStorage.getItem(tokenKey),
    data: id
  });
}

// export function uploadFile(formData, config) {
//   return axios.post(UPLOADAPI + "/admin/uploads", formData, config, {
//     token: localStorage.getItem(tokenKey)
//   });
// }

export function uploadFile(upload_data) {
  console.log(upload_data);
  let token = localStorage.getItem(tokenKey);
  const upload = new FormData();
  upload.append("file", upload_data);
  return axios.post(
    SERVER_URL + "/admin/upload/uploadData/" + token,
    upload,
    {}
  );
}
