import SERVER_URL from "../../constants/server";
const tokenKey = "admin_token";
const axios = require("axios").default;
export function getAllusers(data) {
  return axios.post(SERVER_URL + "/admin/manage/users/getAllUser", {
    data: data,
    token: localStorage.getItem(tokenKey)
  });
}
export function exportData(type) {
  return axios.post(SERVER_URL + "/admin/manage/users/exportData", {
    token: localStorage.getItem(tokenKey),
    account_type: type
  });
}

export function uploadProfile(upload_data) {
  let token = localStorage.getItem(tokenKey);
  const upload = new FormData();
  upload.append("file", upload_data);
  return axios.post(SERVER_URL + "/upload/uploadData/" + token, upload, {});
}
export function changeStatus(id, status) {
  return axios.post(SERVER_URL + "/admin/manage/users/changeStatus", {
    user_id: id,
    status: status,
    token: localStorage.getItem(tokenKey)
  });
}
export function featuredStatus(id, status) {
  return axios.post(SERVER_URL + "/admin/manage/users/featuredStatus", {
    user_id: id,
    status: status,
    token: localStorage.getItem(tokenKey)
  });
}
export function deleteUser(user_id) {
  return axios.post(SERVER_URL + "/admin/manage/users/deleteUser", {
    user_id: user_id,
    token: localStorage.getItem(tokenKey)
  });
}
export function getCount(data) {
  return axios.post(SERVER_URL + "/admin/manage/users/getCount", {
    data: data,
    token: localStorage.getItem(tokenKey)
  });
}
export function getWorkingHoursCount(data) {
  return axios.post(SERVER_URL + "/admin/manage/users/getWorkingHoursCount", {
    user_id: data,
    token: localStorage.getItem(tokenKey)
  });
}
export function getSingleUser(user_id) {
  return axios.post(SERVER_URL + "/admin/manage/users/getSingleUser", {
    user_id: user_id,
    token: localStorage.getItem(tokenKey)
  });
}

export function updateUserdata(labUser) {
  return axios.post(SERVER_URL + "/admin/manage/users/updateUser", {
    data: labUser,
    token: localStorage.getItem(tokenKey)
  });
}
export function updateWorkingHours(workDetails, project_id, user_id) {
  return axios.post(SERVER_URL + "/admin/manage/users/updateWorkingHours", {
    data: workDetails,
    user_id: user_id,
    project_id: project_id,
    token: localStorage.getItem(tokenKey)
  });
}
export function getWorkingDetails(project_id, report_date) {
  return axios.post(SERVER_URL + "/admin/manage/users/getWorkingDetails", {
    project_id: project_id,
    report_date: report_date,
    token: localStorage.getItem(tokenKey)
  });
}
export function getWorkingDetailsList(project_id) {
  return axios.post(SERVER_URL + "/admin/manage/users/getWorkingDetailsList", {
    project_id: project_id,
    token: localStorage.getItem(tokenKey)
  });
}

export function searchUser(data) {
  return axios.post(SERVER_URL + "/admin/manage/users/searchUser", {
    data: data,
    token: localStorage.getItem(tokenKey)
  });
}

export function getContactRequest(data) {
  let token = localStorage.getItem(tokenKey);
  return axios.post(SERVER_URL + "/admin/manage/users/getContactRequest", {
    token: localStorage.getItem(tokenKey),
    data: data
  });
}

export function getSingleRequest(id) {
  return axios.post(SERVER_URL + "/admin/manage/users/getSingleRequest", {
    token: localStorage.getItem(tokenKey),
    data: id
  });
}
