import SERVER_URL from "../../constants/server";
const tokenKey = "admin_token";
const axios = require("axios").default;

export function getAllusers(data) {
  return axios.post(SERVER_URL + "/admin/coupons/getAllUser", {
    token: localStorage.getItem(tokenKey)
  });
}

export function getAllCoupons(activePage, num_of_pages) {
  return axios.post(SERVER_URL + "/admin/coupons", {
    token: localStorage.getItem(tokenKey),
    activePage: activePage,
    pages: num_of_pages
  });
}

export function getTotalCoupons() {
  return axios.post(SERVER_URL + "/admin/coupons/getCount", {
    token: localStorage.getItem(tokenKey)
  });
}

export function addCoupon(data) {
  return axios.post(SERVER_URL + "/admin/coupons/addCpoupon", {
    token: localStorage.getItem(tokenKey),
    data: data
  });
}

export function deleteCoupon(id) {
  return axios.post(SERVER_URL + "/admin/coupons/deleteCoupon", {
    token: localStorage.getItem(tokenKey),
    data: id
  });
}


/* User Wallet */

export function getTransactions(user_id) {
  return axios.post(SERVER_URL + "/admin/coupons/transcations", {
    token: localStorage.getItem(tokenKey),
    user_id: user_id
  });
}

export function walletAction(data, timezone) {
  return axios.post(SERVER_URL + "/admin/coupons/wallet-action", {
    token: localStorage.getItem(tokenKey),
    data: data,
    timezone: timezone
  });
}