import SERVER_URL from "../../constants/server";
const tokenKey = "admin_token";
const axios = require("axios").default;

export function getAllSubscriptions() {
  return axios.post(SERVER_URL + "/admin/subscriptions/getAllSubscriptions", {
    //data: data,
    token: localStorage.getItem(tokenKey)
  });
}

export function getSubscriptionsBylimit(data, category) {
  return axios.post(SERVER_URL + "/admin/subscriptions/getAllSubscriptions", {
    data: data,
    category:category,
    token: localStorage.getItem(tokenKey)
  });
}

export function getSingleSubscriptions(id) {
  return axios.post(SERVER_URL + "/admin/subscriptions/getSingleSubscription", {
    id: id,
    token: localStorage.getItem(tokenKey)
  });
}
export function stripeCharge(data) {
  return axios.post(SERVER_URL + "/admin/subscriptions/stripeCharge", {
    data: data,
    token: localStorage.getItem(tokenKey)
  });
}
export function cancelSingleSubscriptions(id) {
  return axios.post(
    SERVER_URL + "/admin/subscriptions/cancelSingleSubscriptions",
    {
      id: id,
      token: localStorage.getItem(tokenKey)
    }
  );
}