import apiUrl from "../constants/server";
const axios = require("axios").default;

export function payment(data) {
  return axios.post(apiUrl + "/payment", {
    data: data,
    token: localStorage.getItem("userToken")
  });
}

export function stripeCharge(data) {
  return axios.post(apiUrl + "/payment", {
    data: data,
    token: localStorage.getItem("admin_token")
  });
}

export function confirmPayment(data) {
  return axios.post(apiUrl + "/payment/confirm_payment", {
    data: data,
    token: localStorage.getItem("userToken")
  });
}

export function savecard(id, name, email="") {
  return axios.post(apiUrl + "/payment/save_card", {
    data: id,
    name: name,
    email: email,
    token: localStorage.getItem("userToken")
  });
}
export function attachCustomer(payment_id, customer_id) {
  return axios.post(apiUrl + "/payment/attach-customer", {
    payment_id: payment_id,
    customer_id: customer_id,
    token: localStorage.getItem("userToken")
  });
}


export function removeCard(id) {
  return axios.post(apiUrl + "/payment/remove_card", {
    data: id,
    token: localStorage.getItem("userToken")
  });
}

export function getsavedCard() {
  return axios.post(apiUrl + "/payment/get_saved_cards", {
    token: localStorage.getItem("userToken")
  });
}

export function getCustomer(data) {
  return axios.post(apiUrl + "/payment/get_customer", {
    token: localStorage.getItem("userToken"),
    data: data
  });
}

export function getSingleSubscriptions(id) {
  return axios.post(apiUrl + "/payment/getSingleSubscription", {
    id: id,
    token: localStorage.getItem("userToken")
  });
}
export function cancelSingleSubscriptions(id) {
  return axios.post(
    apiUrl + "/payment/cancelSingleSubscriptions",
    {
      id: id,
      token: localStorage.getItem("userToken")
    }
  );
}