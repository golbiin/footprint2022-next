import React, { Component } from 'react'
import Adminheader from '../common/adminHeader'
import Adminsidebar from '../common/adminSidebar'
import Spinner from 'react-bootstrap/Spinner'
import * as manageBlog from '../../../services/admin/manageBlog'
import Joi from 'joi-browser'
import { Link } from 'react-router-dom'
import { CKEditor } from '@ckeditor/ckeditor5-react'
import ClassicEditor from '@ckeditor/ckeditor5-build-classic'
import { MultiSelect } from 'react-multi-select-component'
import UploadImage from "../common/uploadImage";
import "antd/dist/antd.css";
import { DatePicker } from "antd";
import moment from "moment-timezone";


class AddBlog extends Component {
  state = {
    blog_details_class:"",
    blog_content_class:"",
    show_details:false,
    blog: {
      blog_title: '',
      page_slug: '',
      meta_key: '',
      meta_desc: '',
      short_desc: '',
      author: '',
      publish_date: new Date(),
      blog_content: '',
    },
    submitStatus: false,
    errors: {},
    category: [],
    category_options: [],
    category_selected: [],
    image : ''
  }

  UserSchema = {
    blog_title: Joi.string()
      .required()
      .error(() => {
        return {
          message:
            'Please be sure you’ve filled in the Blog Title. It is not allowed to be empty.'
        }
      }),
    page_slug: Joi.string()
      .required()
      .error(() => {
        return {
          message:
            'Please be sure you’ve filled in the Page Slug. It is not allowed to be empty.'
        }
      }),
    meta_key: Joi.string()
      .required()
      .error(() => {
        return {
          message:
            'Please be sure you’ve filled in the Meta Keywords. It is not allowed to be empty.'
        }
      }),
    meta_desc: Joi.string()
      .required()
      .error(() => {
        return {
          message:
            'Please be sure you’ve filled in the Meta Description. It is not allowed to be empty.'
        }
      }),
    short_desc: Joi.string()
      .required()
      .error(() => {
        return {
          message:
            'Please be sure you’ve filled in the Short Description. It is not allowed to be empty.'
        }
      }),
    author: Joi.string()
      .required()
      .error(() => {
        return {
          message:
            'Please be sure you’ve filled in the Author Name. It is not allowed to be empty.'
        }
      }),
    publish_date: Joi.optional().label('Publish Date'), 
    blog_content: Joi.optional().label('Blog Content')
  }
  componentDidMount = async () => {
    this.getAllBlogCategory();
/*     this.getAllBlogLayout();
    this.getAllBlogAuthors(); */
  }
  getAllBlogLayout = async () => {
    const response = await manageBlog.getAllBlogLayout();
    console.log("response++++++++++++++", response);
/*     if (response.data.status == 1) {
      const Setdata = { ...this.state.category }
      const data_related_row = []
      const data_selected_row = []

      response.data.data.map((Cats, index) => {
        const Setdata = {}
        Setdata.value = Cats._id
        Setdata.label = Cats.cat_name

        if (this.state.category) {
          if (this.state.category.indexOf(Cats._id) > -1) {
            data_selected_row.push(Setdata)
          }
        }

        data_related_row.push(Setdata)
      })

      this.setState({ category_options: data_related_row })
      this.setState({ category_selected: data_selected_row })
    } */
  }
  getAllBlogAuthors = async () => {
    const response = await manageBlog.getAllBlogAuthors()
    if (response.data.status == 1) {
      console.log("response.data", response.data);
    }
  }
  getAllBlogCategory = async () => {
    const response = await manageBlog.getAllBlogCategory()
    if (response.data.status == 1) {
      const Setdata = { ...this.state.category }
      const data_related_row = []
      const data_selected_row = []

      response.data.data.map((Cats, index) => {
        const Setdata = {}
        Setdata.value = Cats._id
        Setdata.label = Cats.cat_name

        if (this.state.category) {
          if (this.state.category.indexOf(Cats._id) > -1) {
            data_selected_row.push(Setdata)
          }
        }

        data_related_row.push(Setdata)
      })

      this.setState({ category_options: data_related_row })
      this.setState({ category_selected: data_selected_row })
    }
  }
  setSelected = event => {
    const data_selected_row = []
    event.map((selectValues, index) => {
      data_selected_row[index] = selectValues.value
    })

    this.setState({ category: data_selected_row })
    this.setState({ category_selected: event })
  }

  handleChange = (event, type = null) => {
    let blog = { ...this.state.blog }
    const errors = { ...this.state.errors }
    console.log('blog2', blog)
    this.setState({ message: '' })
    delete errors.validate
    let name = event.target.name //input field  name
    let value = event.target.value //input field value
    const errorMessage = this.validateUserdata(name, value)
    if (errorMessage) errors[name] = errorMessage
    else delete errors[name]

    if (blog) {
      blog[name] = value
    }
    if(event.target.name === 'blog_title'){
      blog['page_slug'] = this.convertToSlug(event.target.value.trim());
    }
    this.setState({ blog, errors });
    
  }


  convertToSlug = (Text) => {
    return Text
        .toLowerCase()
        .replace(/ /g,'-')
        .replace(/[^\w-]+/g,'')
        ;
  }

  handleChangeEditor = (data, editorName) => {
    let blog = { ...this.state.blog }

    if (blog) {
      blog[editorName] = data
    }
    this.setState({ blog })
  }

  handleChangeDate = date => {
    let blog = { ...this.state.blog };
    blog.publish_date = date;

    this.setState({ blog });
  };
  validateUserdata = (name, value) => {
    const obj = { [name]: value }
    const UserSchema = { [name]: this.UserSchema[name] }
    const { error } = Joi.validate(obj, UserSchema)
    return error ? error.details[0].message : null
  }

  submitBlog = async () => {
    this.setState({ submitStatus: true })
    let blog = { ...this.state.blog }
    const errors = { ...this.state.errors }
    let user_validate = Joi.validate(blog, this.UserSchema)
    console.log('user_validate', blog)
    if (user_validate.error) {
      if (user_validate.error) {
        let path = user_validate.error.details[0].path[0]
        let errormessage = user_validate.error.details[0].message
        errors[path] = errormessage
      }
      errors['validate'] = 'Please be sure you’ve filled all fields....'
      window.scrollTo({ top: 0, left: 0, behavior: 'smooth' })
      this.setState({ errors })
      this.setState({
        submitStatus: false,
        message: '',
        responsetype: ''
      })
    } else {
      if (this.isEmpty(errors)) {
        this.createBlog()
      }
    }
  }
  toggleShow =async (class_name, class_value) =>{
    if(class_name === 'blog_details_class')
      this.setState({blog_details_class: class_value === ''? "show" : "" })
    else if(class_name === 'blog_content_class')
      this.setState({blog_content_class: class_value === ''? "show" : "" })
    
 
  }
  createBlog = async () => {
    let blog = { ...this.state.blog }
    blog['category'] = this.state.category ? this.state.category : [];
    blog['image'] = this.state.image;
    const response = await manageBlog.createBlog(blog)
    if (response.data.status === 1) {
      window.scrollTo({ top: 0, left: 0, behavior: 'smooth' })
      this.setState({
        submitStatus: false,
        message: response.data.message,
        responsetype: 'sucess'
      });
      if(response.data.data)
      setTimeout(() => {
        this.props.history.push({
          pathname: "/admin/edit-blog/"+response.data.data._id,
        });
      }, 500);
    } else {
      window.scrollTo({ top: 0, left: 0, behavior: 'smooth' })
      this.setState({
        submitStatus: false,
        message: response.data.message,
        responsetype: 'error'
      })
    }
  }

  isEmpty = obj => {
    for (var key in obj) {
      if (obj.hasOwnProperty(key)) return false
    }
    return true
  }

  onuplaodProfile = async (value, index, item) => {
    let file = value.target.files[0];
    const formData = new FormData();
    formData.append("image", file);
    const config = {
      headers: {
        "content-type": "multipart/form-data"
      }
    };
    // const response = await managePortfolio.uploadFile(formData, config);
    const response = await manageBlog.uploadFile(file);
    if (response && response.data) {
          this.setState({
            image: response.data.data.file_location,
          })
    }
  };
 
  render () {
    const { blog,image,blog_details_class, blog_content_class } = this.state

     return (
      <React.Fragment>
        <div id="add-blog" className='container-fluid admin-body'>
          <div className='admin-row'>
            <div className='col-md-2 col-sm-12 sidebar'>
              <Adminsidebar props={this.props} />
            </div>
            <div className='col-md-10 col-sm-12 content'>
              <div className='row content-row'>
                <div className='col-md-12 header'>
                  <Adminheader />
                </div>
                <div className='col-md-12 top-black-bar'>
                  <div className='bread-crumbs'>
                    <span className='navs blogs'>Blogs</span>
                    <span className='navs nav-sub'>-</span>
                    <span className='navs nav-sub'>Add New</span>
                  </div>
               
                </div>
                <div className='col-md-12 content-block'>
                  <div className='container tz-admin-manage-block edit-data'>
                    <h1>Add New Blog</h1>
                    <p className={this.state.responsetype}>
                      {this.state.message}
                    </p>
                    <React.Fragment>
                      <p className='error'>{this.state.errors.validate}</p>
                      <div className='card'>
                        <div className='card-header' onClick={()=>this.toggleShow('blog_details_class', blog_details_class)} >Blog details</div>
                        <div className= {blog_details_class + ' card-body' } >
                          <div className='form-group'></div>

                          <div className='form-group'>
                            <label>Add Title</label>
                            <input
                              type='text'
                              name='blog_title'
                              className='form-control'
                              value={blog.blog_title ? blog.blog_title : ''}
                              onChange={e => this.handleChange(e)}
                            />
                            {this.state.errors.blog_title !== '' ? (
                              <div className='validate_error'>
                                <p>{this.state.errors.blog_title}</p>
                              </div>
                            ) : null}
                          </div>
                          <div className='form-group'>
                            <label>Page Slug</label> <label style={{color:'green'}}> ( Allowed Characters: A-Z , a-z , 0-9 , - , _ )</label>
                            <input
                              type='text'
                              name='page_slug'
                              className='form-control'
                              value={blog.page_slug ? blog.page_slug : ''}
                              onChange={e => this.handleChange(e)}
                            />
                            {this.state.errors.page_slug !== '' ? (
                              <div className='validate_error'>
                                <p>{this.state.errors.page_slug}</p>
                              </div>
                            ) : null}
                          </div>
                          <div className='form-group'>
                            <label>Meta Keywords</label>
                            <input
                              type='text'
                              name='meta_key'
                              className='form-control'
                              value={blog.meta_key ? blog.meta_key : ''}
                              onChange={e => this.handleChange(e)}
                            />
                            {this.state.errors.blog_title !== '' ? (
                              <div className='validate_error'>
                                <p>{this.state.errors.meta_key}</p>
                              </div>
                            ) : null}
                          </div>
                          <div className='form-group'>
                            <label>Meta Description</label>
                            <input
                              type='text'
                              name='meta_desc'
                              className='form-control'
                              value={blog.meta_desc ? blog.meta_desc : ''}
                              onChange={e => this.handleChange(e)}
                            />
                            {this.state.errors.meta_desc !== '' ? (
                              <div className='validate_error'>
                                <p>{this.state.errors.meta_desc}</p>
                              </div>
                            ) : null}
                          </div>

                          <div className='form-group'>
                            <label>Short Description</label>
                            <input
                              type='text'
                              name='short_desc'
                              className='form-control'
                              value={blog.short_desc ? blog.short_desc : ''}
                              onChange={e => this.handleChange(e)}
                            />
                            {this.state.errors.short_desc !== '' ? (
                              <div className='validate_error'>
                                <p>{this.state.errors.short_desc}</p>
                              </div>
                            ) : null}
                          </div>

                          <div className='form-group'>
                            <label>Category</label>
                            <MultiSelect
                              options={this.state.category_options}
                              value={this.state.category_selected}
                              onChange={this.setSelected} //  labelledBy={"Select"}
                            />
                          </div>


                          {/* <div className="form-group blog-image">
                          <label>Image</label>
                          <div className="add_row"  >
                                <UploadImage
                                  onuplaodProfile={this.onuplaodProfile}
                                  image = {image}
                                />
                              
                              </div>
          
                          </div>
                          
                          <div className='form-group'>
                            <label>Content Section</label>
                            <CKEditor
                              editor={ClassicEditor}
                              data={blog.blog_content ? blog.blog_content : ''}
                              onReady={editor => {
                                // You can store the "editor" and use when it is needed.
                                console.log('Editor is ready to use!', editor)
                              }}
                              onChange={(event, editor) => {
                                const data = editor.getData()
                                console.log({ event, editor, data })

                                this.handleChangeEditor(data, 'blog_content')
                              }}
                              onBlur={(event, editor) => {
                                console.log('Blur.', editor)
                              }}
                              onFocus={(event, editor) => {
                                console.log('Focus.', editor)
                              }}
                            />
                          </div> */}
                          <div className='form-group'>
                            <label>Author</label>
                            <input
                              type='text'
                              name='author'
                              className='form-control'
                              value={blog.author ? blog.author : ''}
                              onChange={e => this.handleChange(e)}
                            />
                            {this.state.errors.author !== '' ? (
                              <div className='validate_error'>
                                <p>{this.state.errors.author}</p>
                              </div>
                            ) : null}
                          </div>
                          <div className='form-group'>
                            <label>Publish Date</label>
                            <DatePicker
                              format="YYYY-MM-D"
                              className='form-control ant-datepicker'
                              onChange={e => this.handleChangeDate(e)}
                              defaultValue={moment(blog.publish_date ? blog.publish_date : new Date())}
                            />
                            
                            {this.state.errors.publish_date !== '' ? (
                              <div className='validate_error'>
                                <p>{this.state.errors.publish_date}</p>
                              </div>
                            ) : null}
                          </div>
                          
                        
                        </div>
                      </div>
                      <div className='card'>
                          
                          <div className='card-header' onClick={()=>this.toggleShow('blog_content_class', blog_content_class)}>Blog Content</div>
                          
                              <div className={blog_content_class + 'card-body'}>
                                <div className='form-group'></div>
                                  <div className='col-md-12 headcolumn'>
                                    <h1>Blog title</h1>
                                  </div>
                                    <div className='col-md-12 firstcolumn'>
                                      
                                      <div className='col-md-6 contentpara' >
                                        <h3 contentEditable={true}>add blog</h3>
                                        <p contentEditable={true}>aa</p>
                                      </div>
                                      <div className='col-md-6 contentimg'>
                                        <input type="file" id="myFile" name="filename"></input>
                                      </div>
                                    </div>
                                   
                                    
                              </div>
                      </div>
                      <div className='submit'>
                        <button
                          disabled={this.state.submitStatus}
                          className='submit-btn'
                          onClick={this.submitBlog}
                        >
                          {this.state.submitStatus === true ? (
                            <Spinner
                              animation='border'
                              variant='light'
                              size='sm'
                            />
                          ) : (
                            ''
                          )}
                          Submit
                        </button>
                        <Link className='cancel-btn' to='/admin/blogs'>
                          Cancel
                        </Link>
                      </div>
                    </React.Fragment>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    )
  }
}

export default AddBlog