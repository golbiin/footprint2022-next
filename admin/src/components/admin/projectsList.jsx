import React, { Component } from "react";
import Adminheader from "./common/adminHeader";
import Adminsidebar from "./common/adminSidebar";
import * as settings from "../../services/admin/settings";
import Spinner from "react-bootstrap/Spinner";
import Pagination from "react-js-pagination";
import Moment from "react-moment";
import moment from "moment-timezone";
import { Link } from "react-router-dom";
import { CSVLink } from "react-csv";

class ProjectsList extends Component {
  state = {
    data: { activePage: 1, number_of_pages: 10, key: "" },

    loginData: [],
    checkStatus: false,
    loader: true,
    searchSpec: "",
    modal: false,
    csvData: [],
    total: 0
  };

  componentDidMount = async () => {
    this.getAllLeads();
  };

  getAllLeads = async () => {
    const totalresponse = await settings.getTotalProjects();
    if (totalresponse) {
      if (totalresponse.data.status === 1) {
        let total = totalresponse.data.data.total;
        this.setState({ total: total });
      }
    }

    const response = await settings.getAllProjects(this.state.data);
    if (response.data.status === 1) {
      this.setState({ loginData: response.data.data, loader: false });
    } else {
      this.setState({ loader: false });
    }
  };
  handlePageChange = pageNum => {
    let data = { ...this.state.data };
    data.activePage = pageNum;
    this.setState({ data, loader: true });
    setTimeout(() => this.getAllLeads(), 1);
  };

  render() {
    return (
      <React.Fragment>
        <div className="container-fluid admin-body">
          <div className="admin-row">
            <div className="col-md-2 col-sm-12 sidebar">
              <Adminsidebar props={this.props} />
            </div>
            <div className="col-md-10 col-sm-12 content">
              <div className="row content-row">
                <div className="col-md-12 header">
                  <Adminheader />
                </div>
                <div className="col-md-12 top-black-bar">
                  <div className="bread-crumbs col-md-6 col-sm-12">
                    <span className="navs users">Projects List</span>
                  </div>
                  {/* <div className="searchbox col-md-6 col-sm-12">
                    <span></span>
                    <input
                      type="text"
                      placeholder="Search"
                      name="search"
                      className="search form-control"
                      onChange={e => this.searchUser(e)}
                      value={this.state.data.key}
                    />
                  </div> */}
                </div>
                <div className="col-md-12 content-block">
                  <div className="tz-admin-manage-block">
                    <div className="title-bar">
                      <div className="title">Projects List</div>
                    </div>
                    <div className="listusers table-responsive">
                      <table className="table">
                        <thead>
                          <tr>
                          <th scope="col" className="date">
                              PROJECT NAME
                            </th>
                            <th scope="col" className="date">
                              EMAIL
                            </th>
                            <th scope="col" className="date">
                            PROJECT TYPE
                            </th>
                            <th scope="col" className="reg-status">
                              CREATED ON
                            </th>
                            <th scope="col" className="reg-status">
                               Work Details
                            </th>
                          </tr>
                        </thead>
                        <tbody>
                          {this.state.loader ? (
                            <tr>
                              <td
                                colSpan="6"
                                style={{
                                  textAlign: "center",
                                  background: "#fff"
                                }}
                              >
                                <div className="admin-spinner">
                                  <Spinner animation="grow" variant="dark" />
                                  <span>Connecting...</span>
                                </div>
                              </td>
                            </tr>
                          ) : this.state.loginData.length !== 0 ? (
                            this.state.loginData.map((data, index) => {
                              return (
                                <tr className="odd" key={index}>
                                
                                  <td className="email" style={{textTransform:'capitalize'}}>
                                    {data.project_name ? data.project_name : "NA"}
                                  </td>
                                  <td className="email">
                                    {data.user[0] && data.user[0].email? data.user[0].email: "NA"}
                                  </td>
                                  <td className="email">
                                    {data.project_type ? data.project_type : "NA"}
                                  </td>
                                  <td className="email">
                                    {data.added_on ? (
                                      <Moment format="YYYY/MM/DD HH:mm:ss">
                                        {data.added_on}
                                      </Moment>
                                    ) : (
                                      "NA"
                                    )}
                                  </td>
                                  <td className="actions">
                                    <Link
                                      className="red-btn"
                                      to={
                                        "/admin/users/working-hours-list/" +
                                        data._id + "/" + data.user[0]._id
                                      }
                                    >
                                      View
                                    </Link>
                                  </td>
                                </tr>
                              );
                            })
                          ) : (
                            <tr>
                              <td
                                colSpan="8"
                                className="odd"
                                style={{ textAlign: "center" }}
                              >
                                No records found.
                              </td>
                            </tr>
                          )}
                        </tbody>
                      </table>
                      {this.state.loginData.length !== 0 ? (
                        <div className="paginate">
                          <Pagination
                            hideFirstLastPages
                            prevPageText={<i className="left" />}
                            nextPageText={<i className="right" />}
                            activePage={this.state.data.activePage}
                            itemsCountPerPage={10}
                            totalItemsCount={this.state.total}
                            pageRangeDisplayed={4}
                            onChange={this.handlePageChange}
                          />
                        </div>
                      ) : (
                        ""
                      )}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default ProjectsList;
