import React, { Component } from 'react'
import Adminheader from '../common/adminHeader'
import Adminsidebar from '../common/adminSidebar'
import Spinner from 'react-bootstrap/Spinner'
import * as manageReview from '../../../services/admin/manageReview'
import Joi from 'joi-browser'
import { Link } from 'react-router-dom'
 
import UploadImage from "../common/uploadImage";
import "antd/dist/antd.css";
 

class EditBlog extends Component {
  state = {
    blog: {
      user_name: '',
      testimony: '',
      rating: '',
    },
    loadStatus: false,
    submitStatus: false,
    errors: {},
    upload_data: '',
    category: [],
    category_options: [],
    category_selected: [],
    image: ''
  }

  UserSchema = {
    user_name: Joi.string()
      .required()
      .error(() => {
        return {
          message:
            'Please be sure you’ve filled in the User Name. It is not allowed to be empty.'
        }
      }),
    testimony: Joi.string()
      .required()
      .error(() => {
        return {
          message:
            'Please be sure you’ve filled in the Testimony. It is not allowed to be empty.'
        }
      }),
    rating: Joi.string()
      .required()
      .error(() => {
        return {
          message:
            'Please be sure you’ve filled in the Rating. It is not allowed to be empty.'
        }
      }),
  }
  componentDidMount = async () => {
    const id = this.props.match.params.id
    const response = await manageReview.getSingleBlog(id)
    let blog = { ...this.state.blog }
    if (response.data.status === 1) {
      blog['user_name'] = response.data.data.user_name
      blog['testimony'] = response.data.data.testimony
      blog['rating'] = response.data.data.rating

      this.setState({
        category: response.data.data.category
          ? response.data.data.category
          : [],
        image: response.data.data.user_image ? response.data.data.user_image : '',
        blog: blog,
        loadStatus:true
      })

      this.getAllBlogCategory()
    }
  }

  handleChange = (event, type = null) => {
    let blog = { ...this.state.blog }
    const errors = { ...this.state.errors }

    this.setState({ message: '' })
    delete errors.validate
    let name = event.target.name //input field  name
    let value = event.target.value //input field value
    const errorMessage = this.validateUserdata(name, value)
    if (errorMessage) errors[name] = errorMessage
    else delete errors[name]

    if (blog) {
      blog[name] = value
    }

    this.setState({ blog, errors });
    
  }


 

 
  validateUserdata = (name, value) => {
    const obj = { [name]: value }
    const UserSchema = { [name]: this.UserSchema[name] }
    const { error } = Joi.validate(obj, UserSchema)
    return error ? error.details[0].message : null
  }

  submitBlog = async () => {
    this.setState({ submitStatus: true })
    let blog = { ...this.state.blog }
    const errors = { ...this.state.errors }
    let user_validate = Joi.validate(blog, this.UserSchema)

    if (user_validate.error) {
      if (user_validate.error) {
        let path = user_validate.error.details[0].path[0]
        let errormessage = user_validate.error.details[0].message
        errors[path] = errormessage
      }
      errors['validate'] = 'Please be sure you’ve filled all fields....'
      window.scrollTo({ top: 0, left: 0, behavior: 'smooth' })
      this.setState({ errors })
      this.setState({
        submitStatus: false,
        message: '',
        responsetype: ''
      })
    } else {
      if (this.isEmpty(errors)) {
        this.UpdateBlog()
      }
    }
  }

  getAllBlogCategory = async () => {
    const response = await manageReview.getAllBlogCategory()
    if (response.data.status == 1) {
      const Setdata = { ...this.state.category }
      const data_related_row = []
      const data_selected_row = []

      response.data.data.map((Cats, index) => {
        const Setdata = {}
        Setdata.value = Cats._id
        Setdata.label = Cats.cat_name

        if (this.state.category) {
          if (this.state.category.indexOf(Cats._id) > -1) {
            data_selected_row.push(Setdata)
          }
        }

        data_related_row.push(Setdata)
      })

      this.setState({ category_options: data_related_row })
      this.setState({ category_selected: data_selected_row })
    }
  }
  UpdateBlog = async () => {
    const id = this.props.match.params.id
    let blog = { ...this.state.blog }
    blog['image'] = this.state.image;
    const response = await manageReview.UpdateBlog(id, blog)
    if (response.data.status === 1) {
      window.scrollTo({ top: 0, left: 0, behavior: 'smooth' })
      this.setState({
        submitStatus: false,
        message: response.data.message,
        responsetype: 'sucess'
      })
    } else {
      window.scrollTo({ top: 0, left: 0, behavior: 'smooth' })
      this.setState({
        submitStatus: false,
        message: response.data.message,
        responsetype: 'error'
      })
    }
  }

  isEmpty = obj => {
    for (var key in obj) {
      if (obj.hasOwnProperty(key)) return false
    }
    return true
  }

  setSelected = event => {
    const data_selected_row = []
    event.map((selectValues, index) => {
      data_selected_row[index] = selectValues.value
    })

    this.setState({ category: data_selected_row })
    this.setState({ category_selected: event })
  }

  onuplaodProfile = async (value, index, item) => {

    
    this.setState({ submitStatus: true })
    let file = value.target.files[0];
    const formData = new FormData();
    formData.append("image", file);
    const config = {
      headers: {
        "content-type": "multipart/form-data"
      }
    };
    // const response = await managePortfolio.uploadFile(formData, config);
    const response = await manageReview.uploadFile(file);
    if (response && response.data) {
      this.setState({
        image: response.data.data.file_location,
        submitStatus: false
      })
    }
  };

  render() {
    const { blog ,image } = this.state
    console.log('blog---',blog);
    console.log('image---',image);
    return (
      <React.Fragment>
        <div className='container-fluid admin-body'>
          <div className='admin-row'>
            <div className='col-md-2 col-sm-12 sidebar'>
              <Adminsidebar props={this.props} />
            </div>
            <div className='col-md-10 col-sm-12 content'>
              <div className='row content-row'>
                <div className='col-md-12 header'>
                  <Adminheader />
                </div>
                <div className='col-md-12 top-black-bar'>
                  <div className='bread-crumbs'>
                    <span className='navs blogs'>Reviews</span>
                    <span className='navs nav-sub'>-</span>
                    <span className='navs nav-sub'>Edit</span>
                  </div>
                  <div className="export">
                            <Link
                                          class="add-new"
                                          to={
                                            "/admin/add-blog/"
                                          }
                                        >
                              Add New
                              </Link>
                             
                      </div>
                </div>
                {this.state.loadStatus?
                <div className='col-md-12 content-block'>
                  <div className='container tz-admin-manage-block edit-data'>
                    <h1>Edit Review</h1>
                    <p className={this.state.responsetype}>
                      {this.state.message}
                    </p>
                    <React.Fragment>
                      <p className='error'>{this.state.errors.validate}</p>
                      <div className='card'>
                        <div className='card-header'>Review details</div>
                        <div className='card-body'>
                          <div className='form-group'></div>

                          <div className='form-group'>
                            <label>Title</label>
                            <input
                              type='text'
                              name='user_name'
                              className='form-control'
                              value={blog.user_name ? blog.user_name : ''}
                              onChange={e => this.handleChange(e)}
                            />
                            {this.state.errors.user_name !== '' ? (
                              <div className='validate_error'>
                                <p>{this.state.errors.user_name}</p>
                              </div>
                            ) : null}
                          </div>
                        
                          <div className='form-group'>
                            <label>Testimony</label>
                            <textarea  name='testimony'
                              className='form-control'
                              value={blog.testimony ? blog.testimony : ''}
                              onChange={e => this.handleChange(e)} />
                            
                            {this.state.errors.user_name !== '' ? (
                              <div className='validate_error'>
                                <p>{this.state.errors.testimony}</p>
                              </div>
                            ) : null}
                          </div>
                          <div className='form-group'>
                            <label>Rating</label>
                            <select  name='rating'
                              className='form-control'
                              value={blog.rating ? blog.rating : ''}
                              onChange={e => this.handleChange(e)}>
                              <option value="5">5</option>
                              <option value="4">4</option>
                              <option value="3">3</option>
                            </select>
                           
                            {this.state.errors.rating !== '' ? (
                              <div className='validate_error'>
                                <p>{this.state.errors.rating}</p>
                              </div>
                            ) : null}
                          </div>
                      


                          <div className="form-group blog-image">
                            <label>Image</label>
                            <div className="add_row"  >
                              <UploadImage
                                onuplaodProfile={this.onuplaodProfile}
                                image={image}
                              />

                            </div>

                          </div>
                         


                        </div>
                      </div>

                      <div className='submit'>
                        <button
                          className='submit-btn'
                          onClick={this.submitBlog}
                          disabled={this.state.submitStatus}
                        >
                          {this.state.submitStatus === true ? (
                            <Spinner
                              animation='border'
                              variant='light'
                              size='sm'
                            />   
                          ) : (
                              ''
                            )}
                          Submit
                        </button>
                        <Link className='cancel-btn' to='/admin/blogs'>
                          Cancel
                        </Link>
                      </div>
                    </React.Fragment>
                  </div>
                </div>
                :null }
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    )
  }
}

export default EditBlog
