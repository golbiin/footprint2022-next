import React, { Component } from 'react'
import Adminheader from '../common/adminHeader'
import Adminsidebar from '../common/adminSidebar'
import Spinner from 'react-bootstrap/Spinner'
import * as manageReview from '../../../services/admin/manageReview'
import Joi from 'joi-browser'
import { Link } from 'react-router-dom'
 
import UploadImage from "../common/uploadImage";
import "antd/dist/antd.css";
 

class AddReview extends Component {
  state = {
    blog: {
      user_name: '',
      testimony: '',
      rating: '',
    },
    submitStatus: false,
    errors: {},
    category: [],
    category_options: [],
    category_selected: [],
    image : ''
  }

  UserSchema = {
    user_name: Joi.string()
      .required()
      .error(() => {
        return {
          message:
            'Please be sure you’ve filled in the User Name. It is not allowed to be empty.'
        }
      }),
    testimony: Joi.string()
      .required()
      .error(() => {
        return {
          message:
            'Please be sure you’ve filled in the Testimony. It is not allowed to be empty.'
        }
      }),
    rating: Joi.string()
      .required()
      .error(() => {
        return {
          message:
            'Please be sure you’ve filled in the Rating. It is not allowed to be empty.'
        }
      })
  }
  componentDidMount = async () => {
 
  }
 

  handleChange = (event, type = null) => {
    let blog = { ...this.state.blog }
    const errors = { ...this.state.errors }
    console.log('blog2', blog)
    this.setState({ message: '' })
    delete errors.validate
    let name = event.target.name //input field  name
    let value = event.target.value //input field value
    const errorMessage = this.validateUserdata(name, value)
    if (errorMessage) errors[name] = errorMessage
    else delete errors[name]

    if (blog) {
      blog[name] = value
    }
 
    this.setState({ blog, errors });
    
  }


 

 

 
  validateUserdata = (name, value) => {
    const obj = { [name]: value }
    const UserSchema = { [name]: this.UserSchema[name] }
    const { error } = Joi.validate(obj, UserSchema)
    return error ? error.details[0].message : null
  }

  submitBlog = async () => {
    this.setState({ submitStatus: true })
    let blog = { ...this.state.blog }
    const errors = { ...this.state.errors }
    let user_validate = Joi.validate(blog, this.UserSchema)
    console.log('user_validate', blog)
    if (user_validate.error) {
      if (user_validate.error) {
        let path = user_validate.error.details[0].path[0]
        let errormessage = user_validate.error.details[0].message
        errors[path] = errormessage
      }
      errors['validate'] = 'Please be sure you’ve filled all fields....'
      window.scrollTo({ top: 0, left: 0, behavior: 'smooth' })
      this.setState({ errors })
      this.setState({
        submitStatus: false,
        message: '',
        responsetype: ''
      })
    } else {
      if (this.isEmpty(errors)) {
        this.createBlog()
      }
    }
  }

  createBlog = async () => {
    let blog = { ...this.state.blog }
    blog['image'] = this.state.image;
    const response = await manageReview.createReview(blog)
    if (response.data.status === 1) {
      window.scrollTo({ top: 0, left: 0, behavior: 'smooth' })
      this.setState({
        submitStatus: false,
        message: response.data.message,
        responsetype: 'sucess'
      });
      if(response.data.data)
      setTimeout(() => {
        this.props.history.push({
          pathname: "/admin/edit-review/"+response.data.data._id,
        });
      }, 500);
    } else {
      window.scrollTo({ top: 0, left: 0, behavior: 'smooth' })
      this.setState({
        submitStatus: false,
        message: response.data.message,
        responsetype: 'error'
      })
    }
  }

  isEmpty = obj => {
    for (var key in obj) {
      if (obj.hasOwnProperty(key)) return false
    }
    return true
  }

  onuplaodProfile = async (value, index, item) => {
    let file = value.target.files[0];
    const formData = new FormData();
    formData.append("image", file);
    const config = {
      headers: {
        "content-type": "multipart/form-data"
      }
    };
    // const response = await managePortfolio.uploadFile(formData, config);
    const response = await manageReview.uploadFile(file);
    if (response && response.data) {
          this.setState({
            image: response.data.data.file_location,
          })
    }
  };

 

  render () {
    const { blog, image } = this.state
    return (
      <React.Fragment>
        <div className='container-fluid admin-body'>
          <div className='admin-row'>
            <div className='col-md-2 col-sm-12 sidebar'>
              <Adminsidebar props={this.props} />
            </div>
            <div className='col-md-10 col-sm-12 content'>
              <div className='row content-row'>
                <div className='col-md-12 header'>
                  <Adminheader />
                </div>
                <div className='col-md-12 top-black-bar'>
                  <div className='bread-crumbs'>
                    <span className='navs blogs'>Reviews</span>
                    <span className='navs nav-sub'>-</span>
                    <span className='navs nav-sub'>Add New</span>
                  </div>
               
                </div>
                <div className='col-md-12 content-block'>
                  <div className='container tz-admin-manage-block edit-data'>
                    <h1>Add New Review</h1>
                    <p className={this.state.responsetype}>
                      {this.state.message}
                    </p>
                    <React.Fragment>
                      <p className='error'>{this.state.errors.validate}</p>
                      <div className='card'>
                        <div className='card-header'>Review details</div>
                        <div className='card-body'>
                          <div className='form-group'></div>

                          <div className='form-group'>
                            <label>Name</label>
                            <input
                              type='text'
                              name='user_name'
                              className='form-control'
                              value={blog.user_name ? blog.user_name : ''}
                              onChange={e => this.handleChange(e)}
                            />
                            {this.state.errors.user_name !== '' ? (
                              <div className='validate_error'>
                                <p>{this.state.errors.user_name}</p>
                              </div>
                            ) : null}
                          </div>
                          
                          <div className='form-group'>
                            <label>Testimony</label>
                            <textarea  name='testimony'
                              className='form-control'
                              value={blog.testimony ? blog.testimony : ''}
                              onChange={e => this.handleChange(e)} />
                            
                            {this.state.errors.user_name !== '' ? (
                              <div className='validate_error'>
                                <p>{this.state.errors.testimony}</p>
                              </div>
                            ) : null}
                          </div>
                          <div className='form-group'>
                            <label>Rating</label>
                            <select  name='rating'
                              className='form-control'
                              value={blog.rating ? blog.rating : ''}
                              onChange={e => this.handleChange(e)}>
                              <option value="">SELECT</option>
                              <option value="5">5</option>
                              <option value="4">4</option>
                              <option value="3">3</option>
                            </select>
                            
                            {this.state.errors.rating !== '' ? (
                              <div className='validate_error'>
                                <p>{this.state.errors.rating}</p>
                              </div>
                            ) : null}
                          </div>

                           


                          <div className="form-group blog-image">
                          <label>Image</label>
                          <div className="add_row"  >
                                <UploadImage
                                  onuplaodProfile={this.onuplaodProfile}
                                  image = {image}
                                />
                              
                              </div>
          
                          </div>
                          
                          
                          
                        </div>
                      </div>

                      <div className='submit'>
                        <button
                          disabled={this.state.submitStatus}
                          className='submit-btn'
                          onClick={this.submitBlog}
                        >
                          {this.state.submitStatus === true ? (
                            <Spinner
                              animation='border'
                              variant='light'
                              size='sm'
                            />
                          ) : (
                            ''
                          )}
                          Submit
                        </button>
                        <Link className='cancel-btn' to='/admin/blogs'>
                          Cancel
                        </Link>
                      </div>
                    </React.Fragment>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    )
  }
}

export default AddReview;