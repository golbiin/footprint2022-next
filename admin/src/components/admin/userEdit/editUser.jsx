import React, { Component } from "react";
import Adminheader from "../common/adminHeader";
import Adminsidebar from "../common/adminSidebar";
import Spinner from "react-bootstrap/Spinner";
import * as manageUser from "../../../services/admin/manageUser";

import Joi from "joi-browser";
import { Link } from "react-router-dom";

class EditUser extends Component {
  state = {
    user: [],
    billing: {
      first_name: "", last_name: "", work_email: "", company: "", website: "",  office_phone: "", contact_address: "", contact_address_two: "",country: "",city: "", zip: "", state: "",calling: ""
    },
    validated: false,
    userStatus: true,
    submitStatus: false,
    errors: {},
    upload_data: ""
  };
  UserSchema = {
    first_name: Joi.string()
      .required()
      .error(() => {
        return {
          message:
            "Please be sure you’ve filled in the Name. It is not allowed to be empty."
        };
      }),
    last_name: Joi.string()
      .required()
      .error(() => {
        return {
          message:
            "Please be sure you’ve filled in the Name. It is not allowed to be empty."
        };
      }),
      work_email:Joi.string().optional(),
      company:Joi.string().optional(),
      Website:Joi.string().optional(),
      office_phone:Joi.string().optional(),
      contact_address:Joi.string().optional(),
      contact_address_two:Joi.string().optional(),
      country:Joi.string().optional(),
      city:Joi.string().optional(),
      state:Joi.string().optional(),
      zip:Joi.string().optional(),
      calling:Joi.string().optional(),
  };
  componentDidMount = async () => {
    const id = this.props.match.params.id;
    const response = await manageUser.getSingleUser(id);
    if (response.data.status === 1) {
      if (response.data.data.user_data.billing ) {
        this.setState({
          user: response.data.data.user_data
        });
      } else {
        //case2:user with profile data not entered
        let newarray = {
          _id: response.data.data.user_data._id,
          billing: this.state.billing
        };
        this.setState({
          user: newarray
        });
        console.log("initial", this.state.user);
      }
    } else {
      this.setState({
        userStatus: false,
        message: response.data.message,
        responsetype: "error"
      });
    }
  };
  /****************handle change************* */
  handleChange = (event, type = null) => {
    let user = { ...this.state.user };
    const errors = { ...this.state.errors };
    console.log("user2", user);
    this.setState({ message: "" });
    delete errors.validate;
    let name = event.target.name; //input field  name
    let value = event.target.value; //input field value
    const errorMessage = this.validateUserdata(name, value);
    if (errorMessage) errors[name] = errorMessage;
    else delete errors[name];
    let billing = user["billing"];
    if (billing) {
      billing[name] = value;
    }
    console.log(billing);
    this.setState({ user, errors });
  };
  //--------------------------------------------------------------//

  validateUserdata = (name, value) => {
    const obj = { [name]: value };
    const UserSchema = { [name]: this.UserSchema[name] };
    const { error } = Joi.validate(obj, UserSchema);
    return error ? error.details[0].message : null;
  };
  /****************submit chnages************** */
  updatuserSubmit = async () => {
    this.setState({ submitStatus: true });
    let user = { ...this.state.user };
    const errors = { ...this.state.errors };
    let user_validate = Joi.validate(user.profile, this.UserSchema);
    console.log("user_validate", user_validate);
    this.updatuser();
  };
  updatuser = async () => {
    let user = { ...this.state.user };
    const response = await manageUser.updateUserdata(user);
    if (response.data.status === 1) {
      window.scrollTo({ top: 0, left: 0, behavior: "smooth" });
      this.setState({
        submitStatus: false,
        message: response.data.message,
        responsetype: "sucess"
      });
    } else {
      window.scrollTo({ top: 0, left: 0, behavior: "smooth" });
      this.setState({
        submitStatus: false,
        message: response.data.message,
        responsetype: "error"
      });
    }
  };
  isEmpty = obj => {
    for (var key in obj) {
      if (obj.hasOwnProperty(key)) return false;
    }
    return true;
  };

  render() {
    return (
      <React.Fragment>
        <div className="container-fluid admin-body">
          <div className="admin-row">
            <div className="col-md-2 col-sm-12 sidebar">
              <Adminsidebar props={this.props} />
            </div>
            <div className="col-md-10 col-sm-12 content">
              <div className="row content-row">
                <div className="col-md-12 header">
                  <Adminheader />
                </div>
                <div className="col-md-12 top-black-bar">
                  <div className="bread-crumbs">
                    <span className="navs users">Users</span>
                    <span className="navs nav-sub">-</span>
                    <span className="navs nav-sub">Edit</span>
                  </div>
                </div>
                <div className="col-md-12 content-block">
                  <div className="container tz-admin-manage-block edit-data">
                    <h1>Edit profile page</h1>
                    <p className={this.state.responsetype}>
                      {this.state.message}
                    </p>

                    {this.state.user ? (
                      this.state.user.length !== 0 ? (
                        <React.Fragment>
                          <p className="error">{this.state.errors.validate}</p>
                          <div className="card">
                            <div className="card-header">User details</div>
                            <div className="card-body">
                              <div className="form-group"></div>

                              <div className="form-group">
                                <label>First Name</label>
                                <input
                                  type="text"
                                  name="first_name"
                                  className="form-control half"
                                  value={this.state.user.billing?this.state.user.billing.first_name:""}
                                  onChange={e => this.handleChange(e)}
                                />
                                {this.state.errors.first_name !== "" ? (
                                  <div className="validate_error">
                                    <p>{this.state.errors.first_name}</p>
                                  </div>
                                ) : null}
                              </div>
                              <div className="form-group">
                                <label>Last Name</label>
                                <input
                                  type="text"
                                  name="last_name"
                                  className="form-control half"
                                  value={this.state.user.billing?this.state.user.billing.last_name:""}
                                  onChange={e => this.handleChange(e)}
                                />
                                {this.state.errors.last_name !== "" ? (
                                  <div className="validate_error">
                                    <p>{this.state.errors.last_name}</p>
                                  </div>
                                ) : null}
                              </div>
                            </div>
                          </div>

                          <div className="submit">
                            <button
                              className="submit-btn"
                              onClick={this.updatuserSubmit}
                            >
                              {this.state.submitStatus === true ? (
                                <Spinner
                                  animation="border"
                                  variant="light"
                                  size="sm"
                                />
                              ) : (
                                ""
                              )}
                              Submit
                            </button>
                            <Link className="cancel-btn" to="/admin/users">
                              Cancel
                            </Link>
                          </div>
                        </React.Fragment>
                      ) : (
                        <div className="admin-spinner">
                          <Spinner animation="grow" variant="dark" />
                          <span>Connecting...</span>
                        </div>
                      )
                    ) : (
                      <div className="admin-spinner">
                        <Spinner animation="grow" variant="dark" />
                        <span>Connecting...</span>
                      </div>
                    )}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default EditUser;
