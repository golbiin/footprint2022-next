import React, { Component } from "react";
import Adminheader from "../common/adminHeader";
import Adminsidebar from "../common/adminSidebar";
import * as manageUser from "../../../services/admin/manageUser";
import Spinner from "react-bootstrap/Spinner";
import Pagination from "react-js-pagination";
import Moment from "react-moment";
import DeleteConfirm from "../common/deleteConfirm";
import { Link } from "react-router-dom";
import { CSVLink } from "react-csv";

class WorkingHoursList extends Component {
  state = {
    data: { type: "user", activePage: 1, number_of_pages: 10, key: "" },
    userData: [],
    userDataType: "",
    checkStatus: false,
    loader: true,
    searchSpec: "",
    modal: false
  };
  handlePageChange = pageNumber => {
    let data = { ...this.state.data };
    let userData = [...this.state.userData];
    data.activePage = pageNumber;
    userData = [];
    this.setState({ data, userData, loader: true });
    setTimeout(() => this.getallusers(), 1);
  };

  componentDidMount = async () => {
    this.getallusers();
  };
  getallusers = async () => {
    const project_id = this.props.match.params.project_id;
    const totalresponse = await manageUser.getWorkingHoursCount(project_id);
    if (totalresponse) {
      if (totalresponse.data.status === 1) {
        let totalUsers = totalresponse.data.data.total;
        this.setState({ totalUsers: totalUsers });
      }
    } 

    const responseDetails = await manageUser.getWorkingDetailsList(project_id);
    console.log("responseDetails", responseDetails);

    const response = await manageUser.getAllusers(this.state.data);
    if (responseDetails.data.status === 1) {
      this.setState({ userData: responseDetails.data.data, loader: false });
    } else {
      this.setState({ loader: false });
    }
  };

  deleteUser = async () => {
    console.log("ddd");
    let id = this.state.user;
    let lab_index = this.state.index;
    const response = await manageUser.deleteUser(id);
    if (response) {
      if (response.data.status === 1) {
        let newuserData = [...this.state.userData];
        newuserData.splice(lab_index, 1);
        this.setState({ userData: newuserData });
        const response = await manageUser.getCount(this.state.data.type);
        if (response) {
          let totalUsers = response.data.data.total;
          this.setState({ totalUsers: totalUsers });
          this.getallusers();
          this.toogle();
        }
      }
    }
  };
  saveAndtoogle = (id, lab_index) => {
    this.setState({ user: id, index: lab_index });
    this.toogle();
  };
  toogle = () => {
    let status = !this.state.modal;
    this.setState({ modal: status });
  };

  redirect = (user_id, report_date) => {
    this.props.history.push({
      pathname: "/admin/users/working-hours/" + this.props.match.params.project_id + "/" + this.props.match.params.user_id,
      state: {
        report_date: report_date
      }
    });
  };

  render() {
    const images = require.context("../../../assets/images/admin", true);
    console.log("userDatas", this.state.userData);
    return (
      <React.Fragment>
        <div className="container-fluid admin-body">
          <div className="admin-row">
            <div className="col-md-2 col-sm-12 sidebar">
              <Adminsidebar props={this.props} />
            </div>
            <div className="col-md-10 col-sm-12 content">
              <div className="row content-row">
                <div className="col-md-12 header">
                  <Adminheader />
                </div>
                <div className="col-md-12 top-black-bar">
                  <div className="bread-crumbs col-md-6 col-sm-12">
                    <span className="navs users">Working Details</span>
                  </div>
                </div>
                <div className="col-md-12 content-block">
                  <div className="tz-admin-manage-block">
                    <div className="title-bar">
                      <div className="title">Working hours List</div>
                      <div className="export">
                        <Link
                          to={
                            "/admin/users/working-hours/" +
                            this.props.match.params.project_id + "/" + this.props.match.params.user_id
                          }
                        >
                          <button className="export-btn">Add New</button>
                        </Link>
                      </div>
                    </div>
                    <div className="listusers table-responsive">
                      <table className="table">
                        <thead>
                          <tr>
                            <th scope="col" className="name">
                              PROJECT NAME
                            </th>
                            <th scope="col" className="date">
                              REPORT DATE
                            </th>
                            <th scope="col" className="email">
                              POST TITLE
                            </th>
                            <th scope="col" className="email">
                              WORKING HOURS
                            </th>
                            <th scope="col" className="reg-status">
                              WORKING DETAILS
                            </th>

                            <th scope="col" className="actions"></th>
                          </tr>
                        </thead>
                        <tbody>
                          {this.state.loader ? (
                            <tr>
                              <td
                                colSpan="6"
                                style={{
                                  textAlign: "center",
                                  background: "#fff"
                                }}
                              >
                                <div className="admin-spinner">
                                  <Spinner animation="grow" variant="dark" />
                                  <span>Connecting...</span>
                                </div>
                              </td>
                            </tr>
                          ) : this.state.userData.length !== 0 ? (
                            this.state.userData.map((userData, index) => (
                              <tr className="odd" key={index}>
                                <td className="name" style={{textTransform:'capitalize'}}>
                                  {userData.project[0] &&
                                  userData.project[0].project_name
                                    ? userData.project[0].project_name 
                                    : "NA"}
                                </td>
                                <td className="date">
                                  <Moment format="DD/MM/YYYY">
                                    {userData.report_date}
                                  </Moment>
                                </td>
                                <td className="email">
                                  {userData.post_title}
                                </td>
                                <td className="email">
                                  {userData.working_hours}
                                </td>
                                <td className="reg-status">
                                  {userData.post_details ? (
                                    <span>
                                      <a
                                        class="dropdown-item"
                                        onClick={() =>
                                          this.redirect(
                                            userData.project[0]._id,
                                            userData.report_date
                                          )
                                        }
                                        style={{
                                          textDecoration: "underline",
                                          color: "#11cdef",
                                          cursor: "pointer"
                                        }}
                                      >
                                        View
                                      </a>
                                    </span>
                                  ) : (
                                    <span>No Post Added</span>
                                  )}
                                </td>

                                <td className="actions">
                                  {/* <span
                                      data-toggle="dropdown"
                                      aria-expanded="false"
                                    ></span> */}
                                  <div className="lg-screen-actions">
                                    <div class="dropdown">
                                      <button
                                        class="btn  dropdown-toggle"
                                        type="button"
                                        data-toggle="dropdown"
                                        aria-haspopup="true"
                                        aria-expanded="false"
                                      ></button>
                                      <div class="dropdown-menu">
                                        <Link
                                          class="dropdown-item"
                                          onClick={() =>
                                            this.saveAndtoogle(
                                              userData._id,
                                              index
                                            )
                                          }
                                        >
                                          Delete
                                        </Link>
                                        <Link
                                          class="dropdown-item"
                                          to={
                                            "/admin/users/working-hours/" +
                                            this.props.match.params.project_id + "/" + this.props.match.params.user_id
                                          }
                                        >
                                          Edit
                                        </Link>
                                      </div>
                                    </div>
                                  </div>
                                  <div className="sm-screen-actions">
                                    <Link
                                      class="btn-edit btn"
                                      to={
                                        "/admin/users/chat-url/" + userData._id
                                      }
                                    >
                                      Chat URL
                                    </Link>
                                    <Link
                                      class="btn-edit btn"
                                      to={"/admin/users/edit/" + userData._id}
                                    >
                                      Edit
                                    </Link>
                                    <Link
                                      className="btn btn-dlt"
                                      onClick={() =>
                                        this.saveAndtoogle(userData._id, index)
                                      }
                                    >
                                      Delete
                                    </Link>
                                  </div>
                                </td>
                              </tr>
                            ))
                          ) : (
                            <tr>
                              <td
                                colSpan="8"
                                className="odd"
                                style={{ textAlign: "center" }}
                              >
                                No records found.
                              </td>
                            </tr>
                          )}
                        </tbody>
                      </table>
                      {this.state.userData.length !== 0 ? (
                        <div className="paginate">
                          <Pagination
                            hideFirstLastPages
                            prevPageText={<i className="left" />}
                            nextPageText={<i className="right" />}
                            activePage={this.state.data.activePage}
                            itemsCountPerPage={10}
                            totalItemsCount={this.state.totalUsers}
                            pageRangeDisplayed={4}
                            onChange={this.handlePageChange}
                          />
                        </div>
                      ) : (
                        ""
                      )}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <DeleteConfirm
          modal={this.state.modal}
          toogle={this.toogle}
          deleteUser={this.deleteUser}
        />
      </React.Fragment>
    );
  }
}

export default WorkingHoursList;
