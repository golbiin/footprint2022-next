import React, { Component } from "react";
import Adminheader from "../common/adminHeader";
import Adminsidebar from "../common/adminSidebar";
import Spinner from "react-bootstrap/Spinner";
import * as manageUser from "../../../services/admin/manageUser";

import Joi from "joi-browser";
import { Link } from "react-router-dom";

class ChatUrl extends Component {
  state = {
    patient: [],
    profile: {
      chatUrl: ""
    },
    validated: false,
    userStatus: true,
    submitStatus: false,
    errors: {},
    upload_data: ""
  };
  UserSchema = {
    chatUrl: Joi.string()
      .required()
      .error(() => {
        return {
          message:
            "Please be sure you’ve filled in the Chat Url. It is not allowed to be empty."
        };
      })
  };
  componentDidMount = async () => {
    const id = this.props.match.params.id;
    const response = await manageUser.getSingleUser(id);
    if (response.data.status === 1) {
      if (response.data.data.user_data.profile) {
        this.setState({
          patient: response.data.data.user_data
        });
      } else {
        //case2:user with profile data not entered
        let newarray = {
          _id: response.data.data.user_data._id,
          profile: this.state.profile
        };
        this.setState({
          patient: newarray
        });
        console.log("initial", this.state.patient);
      }
    } else {
      this.setState({
        userStatus: false,
        message: response.data.message,
        responsetype: "error"
      });
    }
  };
  /****************handle change************* */
  handleChange = (event, type = null) => {
    let patient = { ...this.state.patient };
    const errors = { ...this.state.errors };
    console.log("patient2", patient);
    this.setState({ message: "" });
    delete errors.validate;
    let name = event.target.name; //input field  name
    let value = event.target.value; //input field value
    const errorMessage = this.validateUserdata(name, value);
    if (errorMessage) errors[name] = errorMessage;
    else delete errors[name];
    let profile = patient["profile"];
    if (profile) {
      profile[name] = value;
    }
    console.log("profile", profile);
    this.setState({ patient, errors });
  };
  //--------------------------------------------------------------//

  validateUserdata = (name, value) => {
    const obj = { [name]: value };
    const UserSchema = { [name]: this.UserSchema[name] };
    const { error } = Joi.validate(obj, UserSchema);
    return error ? error.details[0].message : null;
  };
  validateProfiledata = (name, value) => {
    const obj = { [name]: value };
    const Profileschema = { [name]: this.profileSchema[name] };
    const { error } = Joi.validate(obj, Profileschema);
    return error ? error.details[0].message : null;
  };
  /****************submit chnages************** */
  updateChatSubmit = async () => {
    this.setState({ submitStatus: true });
    let patient = { ...this.state.patient };
    const errors = { ...this.state.errors };
    let profileData = patient.profile_data;
    let user_validate = Joi.validate(patient, this.UserSchema);
    let profile_validate = Joi.validate(profileData, this.profileSchema);
    if (user_validate.error || profile_validate.error) {
      if (user_validate.error) {
        let path = user_validate.error.details[0].path[0];
        let errormessage = user_validate.error.details[0].message;
        errors[path] = errormessage;
      }
      if (profile_validate.error) {
        let path = profile_validate.error.details[0].path[0];
        let errormessage = profile_validate.error.details[0].message;
        errors[path] = errormessage;
      }
      errors["validate"] = "Please be sure you’ve filled all fields.";
      window.scrollTo({ top: 0, left: 0, behavior: "smooth" });
      this.setState({ errors });
      this.setState({
        submitStatus: false,
        message: "",
        responsetype: ""
      });
      this.setState({ errors });
    } else {
      if (this.isEmpty(errors)) {
        if (this.state.upload_data) {
          const response1 = await manageUser.uploadProfile(
            this.state.upload_data
          );
          if (response1.data.status === 1) {
            let filepath = response1.data.data.file_location;
            patient["profile_data"]["profile_image"] = filepath;
            this.setState({ patient });
            this.updateChat();
          } else {
            window.scrollTo({ top: 0, left: 0, behavior: "smooth" });
            this.setState({
              submitStatus: false,
              message: response1.data.message,
              responsetype: "error"
            });
          }
        } else {
          this.updateChat();
        }
      }
    }
  };
  updateChat = async () => {
    let patient = { ...this.state.patient };
    const response = await manageUser.updateUserdata(patient);
    if (response.data.status === 1) {
      window.scrollTo({ top: 0, left: 0, behavior: "smooth" });
      this.setState({
        submitStatus: false,
        message: response.data.message,
        responsetype: "sucess"
      });
    } else {
      window.scrollTo({ top: 0, left: 0, behavior: "smooth" });
      this.setState({
        submitStatus: false,
        message: response.data.message,
        responsetype: "error"
      });
    }
  };
  isEmpty = obj => {
    for (var key in obj) {
      if (obj.hasOwnProperty(key)) return false;
    }
    return true;
  };
  onuplaodProfile = async (value, item) => {
    let errors = { ...this.state.errors };
    let upload_data = { ...this.state.upload_data };
    if (item === "errors") {
      errors["profile_image"] = value;
    } else {
      let file = value.target.files[0];
      upload_data = file;
      this.setState({ upload_data });
    }
  };
  render() {
    return (
      <React.Fragment>
        <div className="container-fluid admin-body">
          <div className="admin-row">
            <div className="col-md-2 col-sm-12 sidebar">
              <Adminsidebar props={this.props} />
            </div>
            <div className="col-md-10 col-sm-12 content">
              <div className="row content-row">
                <div className="col-md-12 header">
                  <Adminheader />
                </div>
                <div className="col-md-12 top-black-bar">
                  <div className="bread-crumbs">
                    <span className="navs users">Users</span>
                    <span className="navs nav-sub">-</span>
                    <span className="navs nav-sub">Chat URL</span>
                  </div>
                </div>
                <div className="col-md-12 content-block">
                  <div className="container tz-admin-manage-block edit-data">
                    <h1>Chat Channel URL</h1>
                    <p className={this.state.responsetype}>
                      {this.state.message}
                    </p>

                    {this.state.patient ? (
                      this.state.patient.length !== 0 ? (
                        <React.Fragment>
                          <p className="error">{this.state.errors.validate}</p>
                          <div className="card">
                            <div className="card-header">User details</div>
                            <div className="card-body">
                              <div className="form-group"></div>

                              <div className="form-group">
                                <label>Chat Channel URL</label>
                                <input
                                  type="text"
                                  name="chatUrl"
                                  className="form-control half"
                                  value={this.state.patient.profile.chatUrl}
                                  onChange={e => this.handleChange(e)}
                                />
                                {this.state.errors.chatUrl !== "" ? (
                                  <div className="validate_error">
                                    <p>{this.state.errors.chatUrl}</p>
                                  </div>
                                ) : null}
                              </div>
                            </div>
                          </div>

                          <div className="submit">
                            <button
                              className="submit-btn"
                              onClick={this.updateChat}
                            >
                              {this.state.submitStatus === true ? (
                                <Spinner
                                  animation="border"
                                  variant="light"
                                  size="sm"
                                />
                              ) : (
                                ""
                              )}
                              Submit
                            </button>
                            <Link className="cancel-btn" to="/admin/users">
                              Cancel
                            </Link>
                          </div>
                        </React.Fragment>
                      ) : (
                        <div className="admin-spinner">
                          <Spinner animation="grow" variant="dark" />
                          <span>Connecting...</span>
                        </div>
                      )
                    ) : (
                      <div className="admin-spinner">
                        <Spinner animation="grow" variant="dark" />
                        <span>Connecting...</span>
                      </div>
                    )}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default ChatUrl;
