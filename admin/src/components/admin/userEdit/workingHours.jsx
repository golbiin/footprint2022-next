import React, { Component } from "react";
import Adminheader from "../common/adminHeader";
import Adminsidebar from "../common/adminSidebar";
import Spinner from "react-bootstrap/Spinner";
import * as manageUser from "../../../services/admin/manageUser";

import Joi from "joi-browser";
import { Link } from "react-router-dom";
// import DatePicker from "react-datepicker";
// import "react-datepicker/dist/react-datepicker.css";
import "antd/dist/antd.css";
import { DatePicker } from "antd";
import moment from "moment-timezone";
import { Editor } from "react-draft-wysiwyg";
import "react-draft-wysiwyg/dist/react-draft-wysiwyg.css";
import {
  EditorState,
  convertToRaw,
  convertFromRaw,
  ContentState
} from "draft-js";
import draftToHtml from "draftjs-to-html";
import htmlToDraft from "html-to-draftjs";

//moment.tz.setDefault("America/New_York");
class WorkingHours extends Component {
  constructor(props) {
    super(props);

    this.state = {
      editorState: EditorState.createEmpty(),
      validated: false,
      userStatus: false,
      submitStatus: false,
      errors: {},
      upload_data: "",
      workDetails: {
        date: new Date(),
        workHours: 0,
        post_title:"",
        postDetails: ""
      },
      post_check: false
    };
  }

  UserSchema = {
    workHours: Joi.string()
      .required()
      .error(() => {
        return {
          message:
            "Please be sure you’ve filled in the Working Hour. It is not allowed to be empty."
        };
      }),
      post_title: Joi.optional()
  };
  componentDidMount = async () => {
    console.log("propsssssss", this.props.location);
    if (this.props.location.state !== undefined) {
      this.workingDetails(this.props.location.state.report_date);
    } else {
      this.workingDetails(this.state.workDetails.date);
    }
  };
  workingDetails = async date => {
    console.log("FFFF", date);
    let workDetails = { ...this.state.workDetails };
    const project_id = this.props.match.params.project_id;
    const responseDetails = await manageUser.getWorkingDetails(project_id, date);
    if (responseDetails.data.status === 1) {
      if (responseDetails.data.data) {
        workDetails = {
          date: date,
          workHours: responseDetails.data.data.working_hours,
          post_title: responseDetails.data.data.post_title,
          postDetails: responseDetails.data.data.post_details
        };
        var post_check = responseDetails.data.data.post_status;

        let editorState = EditorState.createEmpty();
        if (responseDetails.data.data.post_details) {
          const contentBlock = htmlToDraft(
            responseDetails.data.data.post_details
          );

          if (contentBlock) {
            const contentState = ContentState.createFromBlockArray(
              contentBlock.contentBlocks
            );
            editorState = EditorState.createWithContent(contentState);
          }
        }

        this.setState({
          workDetails,
          post_check,
          userStatus: true,
          editorState
        });
      } else {
        workDetails = {
          date: date,
          workHours: 0,
          postDetails: ""
        };
        this.setState({
          userStatus: true,
          workDetails,
          post_check: false
        });
      }
    } else {
      workDetails = {
        date: date,
        workHours: 0,
        postDetails: ""
      };
      this.setState({
        userStatus: true,
        workDetails,
        post_check: false
      });
    }
  };
  /****************handle change************* */
  onEditorStateChange = editorState => {
    let workDetails = { ...this.state.workDetails };
    if (workDetails) {
      workDetails["postDetails"] = draftToHtml(
        convertToRaw(editorState.getCurrentContent())
      );
    }
    this.setState({
      editorState,
      workDetails
    });
  };
  handleChangeDate = date => {
    let workDetails = { ...this.state.workDetails };
    workDetails.date = date;
    workDetails.workHours = 0;
    workDetails.postDetails = "";

    this.setState({
      workDetails,
      userStatus: false,
      editorState: EditorState.createEmpty()
    });
    this.workingDetails(date);
  };
  handleChange = (event, type = null) => {
    let workDetails = { ...this.state.workDetails };
    const errors = { ...this.state.errors };
    this.setState({ message: "" });
    delete errors.validate;
    let name = event.target.name; //input field  name
    let value = event.target.value; //input field value
    const errorMessage = this.validateUserdata(name, value);
    if (errorMessage) errors[name] = errorMessage;
    else delete errors[name];
    if (workDetails) {
      workDetails[name] = value;
    }
    this.setState({ workDetails, errors });
  };

  //--------------------------------------------------------------//

  validateUserdata = (name, value) => {
    const obj = { [name]: value };
    const UserSchema = { [name]: this.UserSchema[name] };
    const { error } = Joi.validate(obj, UserSchema);
    return error ? error.details[0].message : null;
  };

  /****************submit chnages************** */

  updateWorkingHours = async () => {
    this.setState({
      submitStatus: true
    });
    let workDetails = { ...this.state.workDetails };
    const project_id = this.props.match.params.project_id;
    const user_id = this.props.match.params.user_id;
    const response = await manageUser.updateWorkingHours(workDetails, project_id, user_id);
    if (response.data.status === 1) {
      window.scrollTo({ top: 0, left: 0, behavior: "smooth" });
      this.setState({
        submitStatus: false,
        message: response.data.message,
        responsetype: "sucess"
      });
    } else {
      window.scrollTo({ top: 0, left: 0, behavior: "smooth" });
      this.setState({
        submitStatus: false,
        message: response.data.message,
        responsetype: "error"
      });
    }
  };
  isEmpty = obj => {
    for (var key in obj) {
      if (obj.hasOwnProperty(key)) return false;
    }
    return true;
  };
  PostCheck = () => {
    const workDetails = { ...this.state.workDetails };
    workDetails["postDetails"] = "";
    this.setState({ post_check: !this.state.post_check, workDetails });
  };

  render() {
    return (
      <React.Fragment>
        <div className="container-fluid admin-body">
          <div className="admin-row">
            <div className="col-md-2 col-sm-12 sidebar">
              <Adminsidebar props={this.props} />
            </div>
            <div className="col-md-10 col-sm-12 content">
              <div className="row content-row">
                <div className="col-md-12 header">
                  <Adminheader />
                </div>
                <div className="col-md-12 top-black-bar">
                  <div className="bread-crumbs">
                    <span className="navs users">Users</span>
                    <span className="navs nav-sub">-</span>
                    <span className="navs nav-sub">Working Hours</span>
                  </div>
                </div>
                <div className="col-md-12 content-block">
                  <div className="container tz-admin-manage-block edit-data">
                    <h1>Working Hours</h1>
                    <p className={this.state.responsetype}>
                      {this.state.message}
                    </p>

                    <React.Fragment>
                      <p className="error">{this.state.errors.validate}</p>
                      <div className="card">
                        <div className="card-header">Working details</div>
                        <div className="card-body">
                          <div className="form-group"></div>
                          <div className="form-group date-class">
                            <label>Selected Date</label>
                            <DatePicker
                              format="YYYY-MM-D"
                              onChange={e => this.handleChangeDate(e)}
                              defaultValue={moment(
                                this.props.location.state !== undefined
                                  ? this.props.location.state.report_date
                                  : this.state.workDetails.date
                              )}
                            />
                            {/* <DatePicker
                              dateFormat="dd/MM/yyyy"
                              selected={myDatetimeString}
                              onChange={e => this.handleChangeDate(e)}
                            /> */}
                          </div>
                          {this.state.userStatus ? (
                            <>
                              <div className="form-group">
                                <label>Working Hours</label>
                                <input
                                  type="number"
                                  name="workHours"
                                  className="form-control half"
                                  value={this.state.workDetails.workHours}
                                  onChange={e => this.handleChange(e)}
                                />
                                {this.state.errors.workHours !== "" ? (
                                  <div className="validate_error">
                                    <p>{this.state.errors.workHours}</p>
                                  </div>
                                ) : null}
                              </div>
                              <div className="form-group post_check">
                                <input
                                  type="checkbox"
                                  name="post_check"
                                  checked={this.state.post_check}
                                  onChange={this.PostCheck}
                                  value={this.state.post_check}
                                  className="form-check-input"
                                  id="exampleCheck1"
                                />
                                <label
                                  className="form-check-label"
                                  htmlFor="exampleCheck1"
                                >
                                  Add Post
                                </label>
                              </div>

                              {this.state.post_check ? (
                                <>
                                <div className="form-group">
                                  <label>Post Title</label>
                                  <input
                                  type="text"
                                  name="post_title"
                                  className="form-control"
                                  value={this.state.workDetails.post_title}
                                  onChange={e => this.handleChange(e)}
                                />

                                  {this.state.errors.shortBio !== "" ? (
                                    <div className="validate_error">
                                      <p>{this.state.errors.postDetails}</p>
                                    </div>
                                  ) : null}
                                </div>
                                 <div className="form-group">
                                 <label>Post Details</label>
                                 <Editor
                                   editorState={this.state.editorState}
                                   toolbarClassName="toolbarClassName"
                                   wrapperClassName="draft-editor-wrapper"
                                   editorClassName="editorClassName"
                                   onEditorStateChange={
                                     this.onEditorStateChange
                                   }
                                 />

                                 {this.state.errors.shortBio !== "" ? (
                                   <div className="validate_error">
                                     <p>{this.state.errors.postDetails}</p>
                                   </div>
                                 ) : null}
                               </div>
                               </>
                              ) : null}

                              <div className="submit">
                                <button
                                  className="submit-btn"
                                  onClick={this.updateWorkingHours}
                                >
                                  {this.state.submitStatus === true ? (
                                    <Spinner
                                      animation="border"
                                      variant="light"
                                      size="sm"
                                    />
                                  ) : (
                                    ""
                                  )}
                                  Submit
                                </button>
                                <Link className="cancel-btn" to={"/admin/users/working-hours-list/" + this.props.match.params.project_id + "/" + this.props.match.params.user_id}>
                                  Back
                                </Link>
                              </div>
                            </>
                          ) : (
                            <div className="admin-spinner">
                              <Spinner animation="grow" variant="dark" />
                              <span>Connecting...</span>
                            </div>
                          )}
                        </div>
                      </div>
                    </React.Fragment>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default WorkingHours;
