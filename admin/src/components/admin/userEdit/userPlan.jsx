import React, { Component } from "react";
import Adminheader from "../common/adminHeader";
import Adminsidebar from "../common/adminSidebar";
import Spinner from "react-bootstrap/Spinner";
import * as manageUser from "../../../services/admin/manageUser";

import Joi from "joi-browser";
import { Link } from "react-router-dom";

class UserPlan extends Component {
  state = {
    user: [],
    profile: {
      user_plan: "",
      emp_number: ""
    },
    validated: false,
    userStatus: true,
    submitStatus: false,
    errors: {},
    upload_data: ""
  };
  UserSchema = {
    user_plan: Joi.string()
      .required()
      .error(() => {
        return {
          message:
            "Please be sure you’ve filled in the User Plan. It is not allowed to be empty."
        };
      }),
    emp_number: Joi.string()
      .required()
      .error(() => {
        return {
          message:
            "Please be sure you’ve filled in the Employees number. It is not allowed to be empty."
        };
      })
  };
  componentDidMount = async () => {
    const id = this.props.match.params.id;
    const response = await manageUser.getSingleUser(id);
    if (response.data.status === 1) {
      if (response.data.data.user_data.profile) {
        this.setState({
          user: response.data.data.user_data
        });
      } else {
        //case2:user with profile data not entered
        let newarray = {
          _id: response.data.data.user_data._id,
          profile: this.state.profile
        };
        this.setState({
          user: newarray
        });
      }
    } else {
      this.setState({
        userStatus: false,
        message: response.data.message,
        responsetype: "error"
      });
    }
  };
  /****************handle change************* */
  handleChange = (event, type = null) => {
    let user = { ...this.state.user };
    const errors = { ...this.state.errors };
    this.setState({ message: "" });
    delete errors.validate;
    let name = event.target.name; //input field  name
    let value = event.target.value; //input field value
    const errorMessage = this.validateUserdata(name, value);
    if (errorMessage) errors[name] = errorMessage;
    else delete errors[name];
    let profile = user["profile"];
    if (profile) {
      profile[name] = value;
    }
    console.log("profile", profile);
    this.setState({ user, errors });
  };
  //--------------------------------------------------------------//

  validateUserdata = (name, value) => {
    const obj = { [name]: value };
    const UserSchema = { [name]: this.UserSchema[name] };
    const { error } = Joi.validate(obj, UserSchema);
    return error ? error.details[0].message : null;
  };
  validateProfiledata = (name, value) => {
    const obj = { [name]: value };
    const Profileschema = { [name]: this.profileSchema[name] };
    const { error } = Joi.validate(obj, Profileschema);
    return error ? error.details[0].message : null;
  };
  /****************submit chnages************** */

  updateData = async () => {
    let user = { ...this.state.user };
    const response = await manageUser.updateUserdata(user);
    if (response.data.status === 1) {
      window.scrollTo({ top: 0, left: 0, behavior: "smooth" });
      this.setState({
        submitStatus: false,
        message: response.data.message,
        responsetype: "sucess"
      });
    } else {
      window.scrollTo({ top: 0, left: 0, behavior: "smooth" });
      this.setState({
        submitStatus: false,
        message: response.data.message,
        responsetype: "error"
      });
    }
  };
  isEmpty = obj => {
    for (var key in obj) {
      if (obj.hasOwnProperty(key)) return false;
    }
    return true;
  };

  render() {
    return (
      <React.Fragment>
        <div className="container-fluid admin-body">
          <div className="admin-row">
            <div className="col-md-2 col-sm-12 sidebar">
              <Adminsidebar props={this.props} />
            </div>
            <div className="col-md-10 col-sm-12 content">
              <div className="row content-row">
                <div className="col-md-12 header">
                  <Adminheader />
                </div>
                <div className="col-md-12 top-black-bar">
                  <div className="bread-crumbs">
                    <span className="navs users">Users</span>
                    <span className="navs nav-sub">-</span>
                    <span className="navs nav-sub">Assign User Plan</span>
                  </div>
                </div>
                <div className="col-md-12 content-block">
                  <div className="container tz-admin-manage-block edit-data">
                    <h1>User plan details </h1>
                    <p className={this.state.responsetype}>
                      {this.state.message}
                    </p>

                    {this.state.user ? (
                      this.state.user.length !== 0 ? (
                        <React.Fragment>
                          <p className="error">{this.state.errors.validate}</p>
                          <div className="card">
                            <div className="card-header">Plan details</div>
                            <div className="card-body">
                              <div className="form-group"></div>
                              <div className="form-group">
                                <label>Assigned Number of Employees</label>
                                <input
                                  type="number"
                                  name="emp_number"
                                  className="form-control"
                                  value={this.state.user.profile.emp_number}
                                  onChange={e => this.handleChange(e)}
                                />
                                {this.state.errors.emp_number !== "" ? (
                                  <div className="validate_error">
                                    <p>{this.state.errors.emp_number}</p>
                                  </div>
                                ) : null}
                              </div>

                              <div className="form-group">
                                <label>Plan details</label>
                                <textarea
                                  type="text"
                                  name="user_plan"
                                  className="form-control"
                                  value={this.state.user.profile.user_plan}
                                  onChange={e => this.handleChange(e)}
                                />
                                {this.state.errors.user_plan !== "" ? (
                                  <div className="validate_error">
                                    <p>{this.state.errors.user_plan}</p>
                                  </div>
                                ) : null}
                              </div>
                            </div>
                          </div>

                          <div className="submit">
                            <button
                              className="submit-btn"
                              onClick={this.updateData}
                            >
                              {this.state.submitStatus === true ? (
                                <Spinner
                                  animation="border"
                                  variant="light"
                                  size="sm"
                                />
                              ) : (
                                ""
                              )}
                              Submit
                            </button>
                            <Link className="cancel-btn" to="/admin/users">
                              Cancel
                            </Link>
                          </div>
                        </React.Fragment>
                      ) : (
                        <div className="admin-spinner">
                          <Spinner animation="grow" variant="dark" />
                          <span>Connecting...</span>
                        </div>
                      )
                    ) : (
                      <div className="admin-spinner">
                        <Spinner animation="grow" variant="dark" />
                        <span>Connecting...</span>
                      </div>
                    )}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default UserPlan;
