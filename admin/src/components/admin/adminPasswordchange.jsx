import React, { Component } from "react";
import Adminheader from "./common/adminHeader";
import Adminsidebar from "./common/adminSidebar";
import SpinnerBtn from "./common/spinnerBtn";
import Joi from "joi-browser";
import * as settingsService from "../../services/admin/settings";


class AdminpwdChange extends Component {
  state = {
    data: { currentPassword: "", newPassword: "", confirmPassword: "" },
    errors: {},
    message: "",
    responsetype: "",
    submit_status: "",
    validate: "",
  };
  schema = {
    currentPassword: Joi.string()
      .required()
      .error(() => {
        return {
          message:
            "Please be sure you’ve filled in the Current password. It is not allowed to be empty.",
        };
      }),
    newPassword: Joi.string()
      .required()
      .error(() => {
        return {
          message:
            "Please be sure you’ve filled in the New password. It is not allowed to be empty.",
        };
      }),
    confirmPassword: Joi.string()
      .required()
      .error(() => {
        return {
          message:
            "Please be sure you’ve filled in the Confirm password. It is not allowed to be empty.",
        };
      }),
  };
  handleChange = (event) => {
    let data = { ...this.state.data };
    let errors = { ...this.state.errors };
    const errorMessage = this.validateProperty(
      event.target.name,
      event.target.value
    );
    if (errorMessage) errors[event.target.name] = errorMessage;
    else delete errors[event.target.name];
    data[event.target.name] = event.target.value;
    this.setState({ data, errors });
  };
  validateProperty = (name, value) => {
    const obj = { [name]: value };
    const schema = { [name]: this.schema[name] };
    const { error } = Joi.validate(obj, schema);
    return error ? error.details[0].message : null;
  };
  changePassword = async () => {
    let data = { ...this.state.data };
    let errors = { ...this.state.errors };
    let validate = Joi.validate(data, this.schema);
    this.setState({ submitStatus: true });
    if (validate.error) {
      let path = validate.error.details[0].path[0];
      let errormessage = validate.error.details[0].message;
      errors[path] = errormessage;
      this.setState({ submitStatus: false, errors });
    } else {
      if (data.confirmPassword !== data.newPassword) {
        errors["confirmPassword"] =
          "Password you have enterd is does not match.";
        this.setState({ submitStatus: false, errors });
      } else {
        const response = await settingsService.changePassword(data);
        if (response.data.status === 1) {
          this.setState({
            submitStatus: false,
            message: response.data.message,
            responsetype: "sucess",
          });
        } else {
          this.setState({
            submitStatus: false,
            message: response.data.message,
            responsetype: "error",
          });
        }
      }
    }
  };
  render() {
    return (
      <React.Fragment>
        <div className="container-fluid admin-body">
          <div className="admin-row">
            <div className="col-md-2 col-sm-12 sidebar">
              <Adminsidebar props={this.props} />
            </div>
            <div className="col-md-10 col-sm-12 content">
              <div className="row content-row">
                <div className="col-md-12 header">
                  <Adminheader />
                </div>
                <div className="col-md-12 top-black-bar">
                  <div className="bread-crumbs">
                    <span className="navs settings">Settings</span>
                    <span className="navs nav-sub">-</span>
                    <span className="navs nav-sub">Change password</span>
                  </div>
                </div>
                <div className="col-md-12 content-block">
                  <div className="container tz-admin-manage-block edit-data">
                    <h1>Change Password</h1>
                    <p className={this.state.responsetype}>
                      {this.state.message}
                    </p>

                    <React.Fragment>
                      <p className="error">{this.state.errors.validate}</p>
                      <div className="card">
                        <div className="card-header">Password settings</div>
                        <div className="card-body">
                          <div className="form-group">
                            <label>Current password</label>
                            <input
                              type="text"
                              name="currentPassword"
                              className="form-control half"
                              value={this.state.data.currentPassword}
                              onChange={this.handleChange}
                            />
                            {this.state.errors.currentPassword !== "" ? (
                              <div className="validate_error">
                                <p>{this.state.errors.currentPassword}</p>
                              </div>
                            ) : null}
                          </div>
                          <div className="form-group">
                            <label>New password</label>
                            <input
                              type="text"
                              name="newPassword"
                              className="form-control half"
                              value={this.state.data.newPassword}
                              onChange={this.handleChange}
                            />
                            {this.state.errors.newPassword !== "" ? (
                              <div className="validate_error">
                                <p>{this.state.errors.newPassword}</p>
                              </div>
                            ) : null}
                          </div>
                          <div className="form-group">
                            <label>Confirm password</label>
                            <input
                              type="text"
                              name="confirmPassword"
                              className="form-control half"
                              value={this.state.data.confirmPassword}
                              onChange={this.handleChange}
                            />
                            {this.state.errors.confirmPassword !== "" ? (
                              <div className="validate_error">
                                <p>{this.state.errors.confirmPassword}</p>
                              </div>
                            ) : null}
                          </div>
                        </div>
                      </div>

                      <div className="submit">
                        <button
                          className="submit-btn"
                          onClick={this.changePassword}
                        >
                          {this.state.submitStatus === true ? (
                            <SpinnerBtn
                            animation="border"
                            variant="dark"
                            size="sm"
                            btnClass="admin-login-btn"
                          />
                          ) : (
                            ""
                          )}
                          Submit
                        </button>
                      </div>
                    </React.Fragment>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default AdminpwdChange;
