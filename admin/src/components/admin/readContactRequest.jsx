import React, { Component } from "react";
import Adminheader from "./common/adminHeader";
import * as userServices from "../../services/admin/manageUser";
import Spinner from "react-bootstrap/Spinner";
import { Link } from "react-router-dom";
import Moment from "react-moment";
import Adminsidebar from "./common/adminSidebar";

class ReadContactRequest extends Component {
  state = {
    requests: [],
    loading: true,
    submitting: false,
    response_type: "",
    response_message: ""
  };
  componentDidMount = async () => {
    const id = this.props.match.params.id;
    const response = await userServices.getSingleRequest(id);
    if (response && response.data) {
      if (response.data.status === 1) {
        let requests = [...this.state.requests];
        requests = response.data.data[0];
        this.setState({ requests, loading: false });
      } else {
        this.setState({
          loading: false,
          response_type: response.data.status,
          response_message: response.data.message
        });
      }
    }
  };
  render() {
    const { requests, loading, response_message, response_type } = this.state;

    return (
      <React.Fragment>
        <div className="container-fluid admin-body">
          <div className="admin-row">
            <div className="col-md-2 col-sm-12 sidebar">
              <Adminsidebar props={this.props} />
            </div>
            <div className="col-md-10 col-sm-12 content">
              <div className="row content-row">
                <div className="col-md-12 header">
                  <Adminheader />
                </div>
                <div className="col-md-12 top-black-bar">
                  <div className="bread-crumbs">
                    <span className="navs conatct">
                      {requests.conatct_type === "contact"
                        ? "Contact Request"
                        : requests.conatct_type === "book-call"
                        ? "Book a call"
                        : ""}
                    </span>
                  </div>
                </div>
                <div className="col-md-12 content-block">
                  <div className="container tz-admin-manage-block edit-data">
                    <h1>
                      {requests.contact_type === "contact"
                        ? "Contact Request"
                        : requests.contact_type === "book-call"
                        ? "Book a call"
                        :requests.contact_type === "banner-form"
                        ? "Banner Form"
                        :requests.contact_type === "blog-form"
                        ? "Blog Form": ""}
                    </h1>
                    <p className={response_type ? "sucess" : "error"}>
                      {response_message}
                    </p>
                    {loading === true ? (
                      <div className="admin-spinner">
                        <Spinner animation="grow" variant="dark" />
                        <span>Connecting...</span>
                      </div>
                    ) : (
                      <React.Fragment>
                        <div className="renew-subscription">
                          <div className="card">
                            <div className="card-header">Contact Details</div>
                            <div className="card-body">
                              <div className="subscriptionData">
                                <div className="row">
                                  <div className="col-md-6 title">NAME</div>
                                  <div className="col-md-6 value">
                                    <span className="seprator"></span>
                                    {requests.contact_data.name
                                      ? requests.contact_data.name
                                      : requests.contact_data.first_name +
                                        " " +
                                        (requests.contact_data.last_name? requests.contact_data.last_name : "")}
                                  </div>
                                </div>
                                <div className="row">
                                  <div className="col-md-6 title">EMAIL</div>
                                  <div className="col-md-6 value">
                                    <span className="seprator"></span>
                                    {requests.contact_data.email}
                                  </div>
                                </div>
                                <div className="row">
                                  <div className="col-md-6 title">PHONE</div>
                                  <div className="col-md-6 value">
                                    <span className="seprator"></span>
                                    {requests.contact_data.phone}
                                  </div>
                                </div>
                                {requests.contact_type === "contact" ? (
                                  <React.Fragment>
                                    <div className="row">
                                      <div className="col-md-6 title">
                                        TOPIC
                                      </div>
                                      <div className="col-md-6 value">
                                        <span className="seprator"></span>
                                        {requests.contact_data.topic}
                                      </div>
                                    </div>
                                    <div className="row">
                                      <div className="col-md-6 title">
                                        MESSAGE
                                      </div>
                                      <div className="col-md-6 value">
                                        <span className="seprator"></span>
                                        {requests.contact_data.message}
                                      </div>
                                    </div>
                                  </React.Fragment>
                                ) : (requests.contact_type === "book-call" || requests.contact_type === "banner-form" || requests.contact_type === "blog-form" )? (
                                  <React.Fragment>
                                    {requests.contact_data.company_name?
                                    <div className="row">
                                      <div className="col-md-6 title">
                                        COMPANY NAME
                                      </div>
                                      <div className="col-md-6 value">
                                        <span className="seprator"></span>
                                        {requests.contact_data.company_name}
                                      </div>
                                    </div>
                                    :null}
                                    <div className="row">
                                      <div className="col-md-6 title">
                                        SUBMITTED PAGE
                                      </div>
                                      <div className="col-md-6 value">
                                        <span className="seprator"></span>
                                        {requests.page}
                                      </div>
                                    </div>
                                    <div className="row">
                                      <div className="col-md-6 title">IP</div>
                                      <div className="col-md-6 value">
                                        <span className="seprator"></span>
                                        {requests.ip}
                                      </div>
                                    </div>
                                    <div className="row">
                                      <div className="col-md-6 title">Time</div>
                                      <div className="col-md-6 value">
                                        <span className="seprator"></span>
                                        <Moment format="MMMM Do YYYY, H: mm: ss">
                                        {requests.submitted_on}
                                      </Moment>
                                      </div>
                                    </div>
                                    
                                  </React.Fragment>
                                ) : (
                                  ""
                                )}
                              </div>
                            </div>
                          </div>
                        </div>
                      </React.Fragment>
                    )}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default ReadContactRequest;
