import React, { Component } from "react";
import Adminheader from "./common/adminHeader";
import * as settings from "../../services/admin/settings";
import Spinner from "react-bootstrap/Spinner";
import { Link } from "react-router-dom";
import Moment from "react-moment";
import Adminsidebar from "./common/adminSidebar";

class leadDetails extends Component {
  state = {
    requests: [],
    loading: true,
    submitting: false,
    response_type: "",
    response_message: ""
  };
  componentDidMount = async () => {
    const id = this.props.match.params.id;
    const response = await settings.getLeadDetails(id);
    if (response && response.data) {
      if (response.data.status === 1) {
        let requests = [...this.state.requests];
        requests = response.data.data[0];
        this.setState({ requests, loading: false });
      } else {
        this.setState({
          loading: false,
          response_type: response.data.status,
          response_message: response.data.message
        });
      }
    }
  };
  render() {
    const { requests, loading, response_message, response_type } = this.state;
    console.log("requests", requests)

    return (
      <React.Fragment>
        <div className="container-fluid admin-body">
          <div className="admin-row">
            <div className="col-md-2 col-sm-12 sidebar">
              <Adminsidebar props={this.props} />
            </div>
            <div className="col-md-10 col-sm-12 content">
              <div className="row content-row">
                <div className="col-md-12 header">
                  <Adminheader />
                </div>
                <div className="col-md-12 top-black-bar">
                  <div className="bread-crumbs">
                    <span className="navs conatct">
                    Lead Request
                    </span>
                  </div>
                </div>
                <div className="col-md-12 content-block">
                  <div className="container tz-admin-manage-block edit-data">
                    <h1>
                    Lead Request
                    </h1>
                    <p className={response_type ? "sucess" : "error"}>
                      {response_message}
                    </p>
                    {loading === true ? (
                      <div className="admin-spinner">
                        <Spinner animation="grow" variant="dark" />
                        <span>Connecting...</span>
                      </div>
                    ) : (
                      <React.Fragment>
                        <div className="renew-subscription">
                          <div className="card">
                            <div className="card-header">Lead Details</div>
                            <div className="card-body">
                              <div className="subscriptionData leadsData">
                                <div className="row">
                                  <div className="col-md-6 title">PROJECT NAME</div>
                                  <div className="col-md-6 value">
                                    <span className="seprator"></span>
                                    {requests.project_name
                                      ? requests.project_name
                                      : requests.project_name }
                                  </div>
                                </div>
                                <div className="row">
                                  <div className="col-md-6 title">PROJECT DETAILS</div>
                                  <div className="col-md-6 value">
                                    <span className="seprator"></span>
                                    {requests.project_details
                                      ? requests.project_details
                                      : requests.project_details }
                                  </div>
                                </div>
                                <div className="row">
                                  <div className="col-md-6 title">PROJECT TYPES</div>
                                  <div className="col-md-6 value">
                                    <span className="seprator"></span>
                                    {Array.isArray(requests.project_type) && requests.project_type.length
                                      ? requests.project_type.map((type)=>(
                                        <><span>{type.toUpperCase()}</span>, </>
                                      ))
                                      : null }
                                  </div>
                                </div>
                                <div className="row">
                                  <div className="col-md-6 title">PROJECT SKILLS</div>
                                  <div className="col-md-6 value">
                                    <span className="seprator"></span>
                                    {Array.isArray(requests.project_skill) && requests.project_skill.length
                                      ? requests.project_skill.map((skill)=>(
                                        <><span>{skill.name}</span>, </>
                                      ))
                                      : null }
                                  </div>
                                </div>
                                <div className="row">
                                  <div className="col-md-6 title">PROJECT FILES</div>
                                  <div className="col-md-6 value">
                                    <span className="seprator"></span>
                                    {Array.isArray(requests.project_file) && requests.project_file.length
                                      ? requests.project_file.map((skill)=>(
                                        <p><a href={skill.location}>{skill.name}</a></p>
                                      ))
                                      : null }
                                  </div>
                                </div>
                                <div className="row">
                                  <div className="col-md-6 title">ADDED ON</div>
                                  <div className="col-md-6 value">
                                    <span className="seprator"></span>
                                    
                                    {requests.added_on
                                      ? <Moment format="YYYY/MM/DD HH:mm:ss">
                                      {requests.added_on}
                                     </Moment>
                                      : null }
                                  </div>
                                </div>
                                
                                 
                              </div>
                            </div>
                          </div>
                        </div>
                      </React.Fragment>
                    )}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default leadDetails;
