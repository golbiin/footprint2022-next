import React, { Component } from "react";
import { Route, Redirect } from "react-router-dom";
import * as authServices from "../../services/admin/login";
const ProtectedRoutea = ({ component: Component, render, ...rest }) => {
  const user = authServices.getCurrentUser();
  
  console.log("PPPPPPP", user);
  console.log("props", rest);

  return (
    <Route
      {...rest}
      render={(props) => {
        if (!authServices.getCurrentUser())
          return <Redirect to="/admin/login" />;
/*         if (user.role && user.role === "editor" && rest.path === "/admin/blogs")
          return <Redirect to="/admin/home" />; */
        return Component ? <Component {...props} /> : render(props);
      }}
    />
  );
};

export default ProtectedRoutea;
