import React, { useState, useEffect } from "react";
import Adminsidebar from "../common/adminSidebar";
import Adminheader from "../common/adminHeader";
import UploadImage from "../common/uploadImage";
import * as manageReview from "../../../services/admin/manageReview";
import { Link } from "react-router-dom";
import Spinner from "react-bootstrap/Spinner";
import Editreview from "../common/editModal";
import SERVER_URL from "../../../constants/server";
import Pagination from "react-js-pagination";

const Addreview = props => {
  const [review, setreview] = useState([]);
  const [categories, setCategories] = useState([]);
  const [parent, setParent] = useState("");
  const [childcat, setChild] = useState([]);
  const [language, setLanguage] = useState([]);
  const [listreview, setreviewList] = useState([]);
  const [loader, setLoader] = useState(true);
  const [message, setMessage] = useState("");
  const [responseType, setMessageType] = useState("");
  const [modal, setModal] = useState(false);
  const [reviewId, setreviewId] = useState();
  const [reviewIndex, setreviewIndex] = useState();
  const [reviewOrder, setreviewOrder] = useState(0);
  const [activePage, setActivePage] = useState(1);
  const [number_of_pages, setNumofPages] = useState(10);
  const [totalCount, setCount] = useState("");

  useEffect(() => {
    getAllcategories();
    getreviews();
    addRow();
  }, 1);

  const toogle = () => {
    setModal(!modal);
  };

  const getAllcategories = async () => {
    let response = await manageReview.getAllcats();
    if (response && response.data.status == 1) {
      setCategories(response.data.data);
    }
  };

  const getreviews = async pageNum => {
    setLoader(true);
    const activePageNum = pageNum ? pageNum : activePage;

    const countRes = await manageReview.getTotalreview();
    if (countRes.data.status === 1) {
      setCount(countRes.data.data.total);
    }
    let response = await manageReview.getAllreview(
      activePageNum,
      number_of_pages
    );
    if (response && response.data) {
      setreviewList(response.data.data);
      setreviewOrder(response.data.data.length);
      setLoader(false);
    }
  };

  const updateParentCat = e => {
    let childcats = [];
    let languages = [];
    setParent(e.target.value);
    if (categories[e.target.value].length !== 0) {
      categories[e.target.value].sub_categories.map((child, sindex) => {
        let addData = {
          name: child.sub_cat_name,
          id: child._id,
          status: false
        };
        childcats.push(addData);
      });
      if (categories[e.target.value].languages) {
        categories[e.target.value].languages.map((lang, lindex) => {
          let addLanguageData = {
            name: lang.language_name,
            id: lang._id,
            status: false
          };
          languages.push(addLanguageData);
        });
      }
    }
    setChild(childcats);
    setLanguage(languages);
  };

  const updateChildCat = (e, index) => {
    const catList = [...childcat];
    catList[index]["status"] = !catList[index]["status"];
    setChild(catList);
  };

  const updateLanguage = (e, index) => {
    const languageList = [...language];
    languageList[index]["status"] = !languageList[index]["status"];
    setLanguage(languageList);
  };

  const addRow = () => {
    let p_array = [{ image: "", title: "", url: "" }];
    let review_items = [...review];
    review_items.push.apply(review_items, p_array);
    setreview(review_items);
    console.log(review);
  };

  const onuplaodProfile = async (value, index, item) => {
    let file = value.target.files[0];
    let review_items = [...review];
    setreview(review_items);
    const formData = new FormData();
    formData.append("image", file);
    const config = {
      headers: {
        "content-type": "multipart/form-data"
      }
    };
    // const response = await manageReview.uploadFile(formData, config);
    const response = await manageReview.uploadFile(file);
    if (response && response.data) {
      review_items[index]["image"] = response.data.data.file_location;
      setreview(review_items);
      console.log(review_items);
    }
  };

  const handleChange = (e, index) => {
    let review_items = [...review];
    let currentIndex = review_items[index];
    currentIndex[e.target.name] = e.target.value;
    setreview(review_items);
  };

  const sumbit = async () => {
    let review_items = [...review];
    if (parent == "") {
      setMessage("Please select category");
      setMessageType("error");
    } else {
      let p_cats = [...categories];
      let main_cat = p_cats[parent]._id;
      let response = await manageReview.addreview(
        review_items,
        main_cat,
        childcat,
        language
      );
      if (response && response.data) {
        if (response.data.status === 1) {
          setMessage("review added!");
          setMessageType("sucess");
          getreviews();
          setreview([]);
        }
      }
    }
  };

  const deleteRow = index => {
    let review_items = [...review];
    review_items.splice(index, 1);
    setreview(review_items);
  };

  const deletereview = async (index, id) => {
    let response = await manageReview.deletereview(id);
    if (response.data.status === 1) {
      getreviews();
    }
  };

  const editreview = async (index, id) => {
    setreviewId(id);
    setreviewIndex(index);
    toogle();
  };
  const handlePageChange = pageNum => {
    setActivePage(pageNum);
    getreviews(pageNum);
    //setTimeout(() => getCoupons(), 5000);
  };
  return (
    <React.Fragment>
      <div className="container-fluid admin-body">
        <div className="admin-row">
          <div className="col-md-2 col-sm-12 sidebar">
            <Adminsidebar props={props} />
          </div>
          <div className="col-md-10 col-sm-12 content">
            <div className="row content-row">
              <div className="col-md-12 header">
                <Adminheader />
              </div>
              <div className="col-md-12 top-black-bar">
                <div className="bread-crumbs">
                  <span className="navs users">review</span>
                  <span className="navs nav-sub">-</span>
                  <span className="navs nav-sub">Add</span>
                </div>
              </div>
              <div className="col-md-12 content-block">
                <div className="container tz-admin-manage-block edit-data">
                  <h1>Add review</h1>
                  <p className={responseType}>{message}</p>
                  <div className="card">
                    <div className="card-header">Add review</div>
                    <div className="card-body">
                      <div className="category_row">
                        <div className="form-group">
                          <label>Parent category</label>
                          <select
                            className="form-control half"
                            onChange={updateParentCat}
                          >
                            <option value="">---select---</option>
                            {categories
                              ? categories.map((cat, index) => (
                                  <option value={index} key={index}>
                                    {cat.parent}
                                  </option>
                                ))
                              : ""}
                          </select>
                        </div>
                        {childcat.length != 0 ? (
                          <div className="form-group">
                            <label>Sub category</label>
                            <div className="check-box">
                              {childcat.length != 0
                                ? childcat.map((scat, index) => (
                                    <label className="container">
                                      <input
                                        type="checkbox"
                                        name={scat.name}
                                        className="check-cats"
                                        value={scat.id}
                                        checked={scat.status}
                                        onChange={e => updateChildCat(e, index)}
                                      />
                                      {scat.name}
                                      <span className="checkmark"></span>
                                    </label>
                                  ))
                                : ""}
                            </div>
                          </div>
                        ) : (
                          ""
                        )}
                        {language.length != 0 ? (
                          <div className="form-group">
                            <label>Languages</label>
                            <div className="check-box">
                              {language.length != 0
                                ? language.map((lng, index) => (
                                    <label className="container">
                                      <input
                                        type="checkbox"
                                        name={lng.name}
                                        className="check-cats"
                                        value={lng.id}
                                        checked={lng.status}
                                        onChange={e => updateLanguage(e, index)}
                                      />
                                      {lng.name}
                                      <span className="checkmark"></span>
                                    </label>
                                  ))
                                : ""}
                            </div>
                          </div>
                        ) : (
                          ""
                        )}
                      </div>
                      <div className="p_add_container">
                        {review
                          ? review.map((item, index) => (
                              <div className="add_row" key={index}>
                                <UploadImage
                                  onuplaodProfile={onuplaodProfile}
                                  index={index}
                                />
                                <input
                                  type="text"
                                  name="title"
                                  className="form-control quarter"
                                  placeholder="Title"
                                  value={item["title"]}
                                  onChange={e => handleChange(e, index)}
                                />
                                <input
                                  type="text"
                                  name="url"
                                  className="form-control quarter"
                                  placeholder="Url"
                                  value={item["url"]}
                                  onChange={e => handleChange(e, index)}
                                />
                                <a
                                  className="delete"
                                  onClick={() => deleteRow(index)}
                                >
                                  ✕
                                </a>
                              </div>
                            ))
                          : ""}
                      </div>
                      <div className="btn-actions">
                        <input
                          type="submit"
                          value="Add row"
                          onClick={addRow}
                          className="green-btn"
                        />
                        <input
                          type="submit"
                          value="Submit"
                          onClick={sumbit}
                          className="submit-btn"
                        />
                      </div>
                    </div>
                  </div>
                  <div className="card">
                    <div className="card-header">reviews</div>
                    <div className="card-body">
                      <div className="listusers table-responsive">
                        <table className="table">
                          <thead>
                            <tr>
                              <th scope="col" className="name">
                                review image
                              </th>
                              <th scope="col" className="date">
                                Title
                              </th>
                              <th scope="col" className="email">
                                Url
                              </th>
                              <th scope="col" className="reg-status">
                                Catrgory
                              </th>
                              <th scope="col" className="approve">
                                Sub category
                              </th>
                              <th scope="col" className="approve">
                                Languages
                              </th>
                              <th scope="col" className="actions"></th>
                            </tr>
                          </thead>
                          <tbody>
                            {loader ? (
                              <tr>
                                <td
                                  colSpan="5"
                                  style={{
                                    textAlign: "center",
                                    background: "#fff"
                                  }}
                                >
                                  <div className="admin-spinner">
                                    <Spinner animation="grow" variant="dark" />
                                    <span>Connecting...</span>
                                  </div>
                                </td>
                              </tr>
                            ) : listreview.length !== 0 ? (
                              listreview.map((item, index) => (
                                <tr className="odd" key={index}>
                                  <td className="name">
                                    <img src={item.review_image} />
                                  </td>
                                  <td>{item.title}</td>
                                  <td>{item.url}</td>
                                  <td>
                                    {categories.length !== 0
                                      ? categories.map((cat, cindex) =>
                                          cat._id === item.main_category
                                            ? cat.parent
                                            : ""
                                        )
                                      : ""}
                                  </td>
                                  <td>
                                    {item.sub_category.length !== 0
                                      ? item.sub_category.map((scat, sindex) =>
                                          scat.status === true
                                            ? scat["name"] + ","
                                            : ""
                                        )
                                      : ""}
                                  </td>
                                  <td>
                                    <div className="items">
                                      {item.languages
                                        ? item.languages.length !== 0
                                          ? item.languages.map((lng, sindex) =>
                                              lng.status === true
                                                ? lng["name"] + ","
                                                : ""
                                            )
                                          : ""
                                        : ""}
                                    </div>
                                  </td>
                                  <td className="actions">
                                    <Link
                                      className="btn-dlt"
                                      onClick={() =>
                                        deletereview(index, item._id)
                                      }
                                    >
                                      Delete
                                    </Link>

                                    <Link
                                      className="btn-edit"
                                      onClick={() =>
                                        editreview(index, item._id)
                                      }
                                    >
                                      Edit
                                    </Link>
                                  </td>
                                </tr>
                              ))
                            ) : (
                              <tr>
                                <td
                                  colSpan="8"
                                  className="odd"
                                  style={{ textAlign: "center" }}
                                >
                                  No records found.
                                </td>
                              </tr>
                            )}
                          </tbody>
                        </table>
                        {listreview.length !== 0 ? (
                          <div className="paginate">
                            <Pagination
                              hideFirstLastPages
                              prevPageText={<i className="left" />}
                              nextPageText={<i className="right" />}
                              activePage={activePage}
                              itemsCountPerPage={1}
                              totalItemsCount={totalCount}
                              pageRangeDisplayed={4}
                              onChange={handlePageChange}
                            />
                          </div>
                        ) : (
                          ""
                        )}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      {listreview.length > 0 ? (
        <Editreview
          modal={modal}
          toogle={toogle}
          id={reviewId}
          categories={categories}
          review_item={listreview[reviewIndex]}
          updateParentCat={updateParentCat}
          getreviews={getreviews}
        />
      ) : (
        ""
      )}
    </React.Fragment>
  );
};

export default Addreview;
