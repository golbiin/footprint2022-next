import React, { Component } from "react";
import Adminheader from "./../common/adminHeader";
import Adminsidebar from "./../common/adminSidebar";
import * as manageUser from "./../../../services/admin/manageUser";
import Spinner from "react-bootstrap/Spinner";
import Pagination from "react-js-pagination";
import Moment from "react-moment";
import DeleteConfirm from "../common/deleteConfirm";
import { Link } from "react-router-dom";
import { CSVLink } from "react-csv";
import * as manageBlog from "../../../services/admin/manageBlog";

class ManageBlogs extends Component {
  state = {
    data: { type: "user", activePage: 1, number_of_pages: 10, key: "" },
    blogData: [],
    blogDataType: "",
    checkStatus: false,
    loader: true,
    searchSpec: "",
    modal: false,
    csvData: [],
    headers: [
      { label: "Name", key: "name" },
      { label: "Email", key: "email" },
      { label: "Join date", key: "doj" }
    ]
  };
  handlePageChange = pageNumber => {
    let data = { ...this.state.data };
    let blogData = [...this.state.blogData];
    data.activePage = pageNumber;
    blogData = [];
    this.setState({ data, blogData, loader: true });
    setTimeout(() => this.getallusers(), 1);
  };

  componentDidMount = async () => {
    this.getallusers();
  };
  getallusers = async () => {
    const totalresponse = await manageBlog.getCount(this.state.data);
    if (totalresponse) {
      if (totalresponse.data.status === 1) {
        let totalUsers = totalresponse.data.data.total;
        this.setState({ totalUsers: totalUsers });
      }
    }

    const response = await manageBlog.getAllBlogs(this.state.data);
    if (response.data.status === 1) {
      this.setState({ blogData: response.data.data, loader: false });
      console.log("res", response.data.data);
      //console.log(this.state.lab[4].profile_data.services[0]);
    } else {
      this.setState({ loader: false });
    }
  };
  changeStatus = async (id, status, index) => {
    const response = await manageBlog.changeStatus(id, status);
    if (response.data.status === 1) {
      let blogData = [...this.state.blogData];
      blogData[index]["status"] = status;
      this.setState(blogData);
      console.log(blogData);
    }
  };

  deleteBlog = async () => {
    console.log("ddd",this.state.user ,this.state.index);
   
    let id = this.state.user;
    let lab_index = this.state.index;
    const response = await manageBlog.deleteBlog(id);
    if (response) {
      if (response.data.status === 1) {
        let newblogData = [...this.state.blogData];
        newblogData.splice(lab_index, 1);
        this.setState({ blogData: newblogData });
        const response = await manageBlog.getCount(this.state.data.type);
        if (response) {
          let totalUsers = response.data.data.total;
          this.setState({ totalUsers: totalUsers });
          this.getallusers();
          this.toogle();
        }
      }
    }
  };
  saveAndtoogle = (id, lab_index) => {
    this.setState({ user: id, index: lab_index });
    this.toogle();
  };
  toogle = () => {
    let status = !this.state.modal;
    this.setState({ modal: status });
  };
  searchUser = async e => {
    let value = e.target.value;
    let data = { ...this.state.data };
    data.key = value;
    data.activePage = 1;
    this.setState({ data });
    setTimeout(() => this.getallusers(), 20);

    // if (value == "") {
    //   this.getallusers();
    // } else {
    //   this.setState({ loader: true });
    //   const response = await manageUser.searchUser({
    //     key: value,
    //     type: "blogData",
    //   });
    //   if (response.data.status === 1) {
    //     let blogData = [...this.state.blogData];
    //     blogData = response.data.data ? response.data.data : "";
    //     this.setState({ blogData, loader: false });
    //   } else {
    //     this.setState({ loader: false });
    //   }
    // }
  };

  render() {
    const images = require.context("./../../../assets/images/admin", true);
    console.log("blogDatas", this.state.blogData);
    return (
      <React.Fragment>
        <div className="container-fluid admin-body">
          <div className="admin-row">
            <div className="col-md-2 col-sm-12 sidebar">
              <Adminsidebar props={this.props} />
            </div>
            <div className="col-md-10 col-sm-12 content">
              <div className="row content-row">
                <div className="col-md-12 header">
                  <Adminheader />
                </div>
                <div className="col-md-12 top-black-bar">
                  <div className="bread-crumbs col-md-6 col-sm-12">
                    <span className="navs blogs">Blogs</span>
                  </div>
                  <div className="searchbox col-md-6 col-sm-12">
                    <span></span>
                    <input
                      type="text"
                      placeholder="Search"
                      name="search"
                      className="search form-control"
                      onChange={e => this.searchUser(e)}
                      value={this.state.data.key}
                    />
                  </div>
                </div>
                <div className="col-md-12 content-block">
                  <div className="tz-admin-manage-block">
                    <div className="title-bar">
                      <div className="title">Blog List</div>
                      <div className="export">
                            <Link
                                          class="add-new"
                                          to={
                                            "/admin/add-blog/"
                                          }
                                        >
                              Add New
                              </Link>
                             
                      </div>
                    </div>
                    <div className="listusers table-responsive">
                      <table className="table">
                        <thead>
                          <tr>
                            <th scope="col" className="name">
                            Blog Title
                            </th>
                            <th scope="col" className="date">
                            Page Slug
                            </th>
                            <th scope="col" className="email">
                            Published On
                            </th>
                           
                            <th scope="col" className="approve">
                              Blog Status
                            </th>
                            
                            <th scope="col" className="actions"></th>
                          </tr>
                        </thead>
                        <tbody>
                          {this.state.loader ? (
                            <tr>
                              <td
                                colSpan="6"
                                style={{
                                  textAlign: "center",
                                  background: "#fff"
                                }}
                              >
                                <div className="admin-spinner">
                                  <Spinner animation="grow" variant="dark" />
                                  <span>Connecting...</span>
                                </div>
                              </td>
                            </tr>
                          ) : this.state.blogData.length !== 0 ? (
                            this.state.blogData.map((blogData, index) => (
                              <tr className="odd" key={index}>
                                <td className="name">
                                  {blogData &&
                                  blogData.blog_title
                                    ? blogData.blog_title
                                    : "NA"}
                                </td>
                               
                                <td className="email">{blogData.page_slug}</td>
                                <td className="date">
                                  <Moment format="DD/MM/YYYY">
                                    {blogData.published_on?blogData.published_on:blogData.created_on} 
                                  </Moment>
                                </td>
                                <td className="approve">
                                  <label className="switch ">
                                    <input
                                      type="checkbox"
                                      className="default"
                                      defaultChecked={blogData.status}
                                      onChange={() =>
                                        this.changeStatus(
                                          blogData._id,
                                          !blogData.status,
                                          index
                                        )
                                      }
                                    />
                                    <span className="slider round"></span>
                                    <span className="approve-label">
                                      {blogData.status}
                                      {blogData.status ? (
                                        <span className="active">
                                          Published
                                        </span>
                                      ) : (
                                        "Draft"
                                      )}
                                    </span>
                                  </label>
                                </td>
          
                                <td className="actions">
                                  {/* <span
                                      data-toggle="dropdown"
                                      aria-expanded="false"
                                    ></span> */}
                                  <div className="lg-screen-actions">
                                    <div class="dropdown">
                                      <button
                                        class="btn  dropdown-toggle"
                                        type="button"
                                        data-toggle="dropdown"
                                        aria-haspopup="true"
                                        aria-expanded="false"
                                      ></button>
                                      <div class="dropdown-menu">
                                        <Link
                                          class="dropdown-item"
                                          onClick={() =>
                                            this.saveAndtoogle(
                                              blogData._id,
                                              index
                                            )
                                          }
                                        >
                                          Delete
                                        </Link>
                                        <Link
                                          class="dropdown-item"
                                          to={
                                            "/admin/edit-blog/" + blogData._id
                                          }
                                        >
                                          Edit
                                        </Link>
                                        
                                      </div>
                                    </div>
                                  </div>
                                  <div className="sm-screen-actions">
                                     
                                    <Link
                                      class="btn-edit btn"
                                      to={"/admin/users/edit/" + blogData._id}
                                    >
                                      Edit
                                    </Link>
                                    <Link
                                      className="btn btn-dlt"
                                      onClick={() =>
                                        this.saveAndtoogle(blogData._id, index)
                                      }
                                    >
                                      Delete
                                    </Link>
                                  </div>
                                </td>
                              </tr>
                            ))
                          ) : (
                            <tr>
                              <td
                                colSpan="8"
                                className="odd"
                                style={{ textAlign: "center" }}
                              >
                                No records found.
                              </td>
                            </tr>
                          )}
                        </tbody>
                      </table>
                      {this.state.blogData.length !== 0 ? (
                        <div className="paginate">
                          <Pagination
                            hideFirstLastPages
                            prevPageText={<i className="left" />}
                            nextPageText={<i className="right" />}
                            activePage={this.state.data.activePage}
                            itemsCountPerPage={10}
                            totalItemsCount={this.state.totalUsers}
                            pageRangeDisplayed={4}
                            onChange={this.handlePageChange}
                          />
                        </div>
                      ) : (
                        ""
                      )}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <DeleteConfirm
          modal={this.state.modal}
          toogle={this.toogle}
          deleteUser={this.deleteBlog}
        />
      </React.Fragment>
    );
  } 
}
export default ManageBlogs;
