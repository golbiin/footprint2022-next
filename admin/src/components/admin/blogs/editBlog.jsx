import React, { Component } from 'react'
import Adminheader from '../common/adminHeader'
import Adminsidebar from '../common/adminSidebar'
import Spinner from 'react-bootstrap/Spinner'
import * as manageBlog from '../../../services/admin/manageBlog'
import Joi from 'joi-browser'
import { Link } from 'react-router-dom'
import { MultiSelect } from 'react-multi-select-component'
import { CKEditor } from '@ckeditor/ckeditor5-react'
import ClassicEditor from '@ckeditor/ckeditor5-build-classic'
import UploadImage from "../common/uploadImage";
import "antd/dist/antd.css";
import { DatePicker } from "antd";
import moment from "moment-timezone";

class AddBlog extends Component {
  state = {
    blog: {
      blog_title: '',
      page_slug: '',
      meta_key: '',
      meta_desc: '',
      short_desc: '',
      author: '',
      created_on: '',
      publish_date: '',
      blog_content: ''
    },
    loadStatus: false,
    submitStatus: false,
    errors: {},
    upload_data: '',
    category: [],
    category_options: [],
    category_selected: [],
    image: ''
  }

  UserSchema = {
    blog_title: Joi.string()
      .required()
      .error(() => {
        return {
          message:
            'Please be sure you’ve filled in the Blog Title. It is not allowed to be empty.'
        }
      }),
    page_slug: Joi.string()
      .required()
      .error(() => {
        return {
          message:
            'Please be sure you’ve filled in the Page Slug. It is not allowed to be empty.'
        }
      }),
    meta_key: Joi.string()
      .required()
      .error(() => {
        return {
          message:
            'Please be sure you’ve filled in the Meta Keywords. It is not allowed to be empty.'
        }
      }),
    meta_desc: Joi.string()
      .required()
      .error(() => {
        return {
          message:
            'Please be sure you’ve filled in the Meta Description. It is not allowed to be empty.'
        }
      }),
      short_desc: Joi.string()
      .required()
      .error(() => {
        return {
          message:
            'Please be sure you’ve filled in the Short Description. It is not allowed to be empty.'
        }
      }),
    author: Joi.string()
      .required()
      .error(() => {
        return {
          message:
            'Please be sure you’ve filled in the Author Name. It is not allowed to be empty.'
        }
      }),
    created_on: Joi.optional().label('created_on'), 
    publish_date: Joi.optional().label('Publish Date'), 
    blog_content: Joi.optional().label('Blog Content')
  }
  componentDidMount = async () => {
    const id = this.props.match.params.id
    const response = await manageBlog.getSingleBlog(id)
    let blog = { ...this.state.blog }
    if (response.data.status === 1) {
      blog['blog_title'] = response.data.data.blog_title
      blog['page_slug'] = response.data.data.page_slug
      blog['meta_key'] = response.data.data.meta_key
      blog['meta_desc'] = response.data.data.meta_desc
      blog['short_desc'] = response.data.data.short_desc
      blog['author'] = response.data.data.author
      blog['created_on'] = response.data.data.created_on
      blog['publish_date'] = response.data.data.published_on
      blog['blog_content'] = response.data.data.blog_content

      this.setState({
        category: response.data.data.category
          ? response.data.data.category
          : [],
        image: response.data.data.blog_image ? response.data.data.blog_image : '',
        blog: blog,
        loadStatus:true
      })

      this.getAllBlogCategory()
    }
  }

  handleChange = (event, type = null) => {
    let blog = { ...this.state.blog }
    const errors = { ...this.state.errors }

    this.setState({ message: '' })
    delete errors.validate
    let name = event.target.name //input field  name
    let value = event.target.value //input field value
    const errorMessage = this.validateUserdata(name, value)
    if (errorMessage) errors[name] = errorMessage
    else delete errors[name]

    if (blog) {
      blog[name] = value
    }

    if(event.target.name === 'blog_title'){
      blog['page_slug'] = this.convertToSlug(event.target.value.trim());
    }
    this.setState({ blog, errors });
    
  }


  convertToSlug = (Text) => {
    return Text
        .toLowerCase()
        .replace(/ /g,'-')
        .replace(/[^\w-]+/g,'')
        ;
  }

  handleChangeEditor = (data, editorName) => {
    let blog = { ...this.state.blog }

    if (blog) {
      blog[editorName] = data
    }
    this.setState({ blog })
  }

  handleChangeDate = date => {
    let blog = { ...this.state.blog };
    blog.publish_date = date;

    this.setState({ blog });
  };
  validateUserdata = (name, value) => {
    const obj = { [name]: value }
    const UserSchema = { [name]: this.UserSchema[name] }
    const { error } = Joi.validate(obj, UserSchema)
    return error ? error.details[0].message : null
  }

  submitBlog = async () => {
    this.setState({ submitStatus: true })
    let blog = { ...this.state.blog }
    const errors = { ...this.state.errors }
    let user_validate = Joi.validate(blog, this.UserSchema)

    if (user_validate.error) {
      if (user_validate.error) {
        let path = user_validate.error.details[0].path[0]
        let errormessage = user_validate.error.details[0].message
        errors[path] = errormessage
      }
      errors['validate'] = 'Please be sure you’ve filled all fields....'
      window.scrollTo({ top: 0, left: 0, behavior: 'smooth' })
      this.setState({ errors })
      this.setState({
        submitStatus: false,
        message: '',
        responsetype: ''
      })
    } else {
      if (this.isEmpty(errors)) {
        this.UpdateBlog()
      }
    }
  }

  getAllBlogCategory = async () => {
    const response = await manageBlog.getAllBlogCategory()
    if (response.data.status == 1) {
      const Setdata = { ...this.state.category }
      const data_related_row = []
      const data_selected_row = []

      response.data.data.map((Cats, index) => {
        const Setdata = {}
        Setdata.value = Cats._id
        Setdata.label = Cats.cat_name

        if (this.state.category) {
          if (this.state.category.indexOf(Cats._id) > -1) {
            data_selected_row.push(Setdata)
          }
        }

        data_related_row.push(Setdata)
      })

      this.setState({ category_options: data_related_row })
      this.setState({ category_selected: data_selected_row })
    }
  }
  UpdateBlog = async () => {
    const id = this.props.match.params.id
    let blog = { ...this.state.blog }
    blog['category'] = this.state.category ? this.state.category : []
    blog['image'] = this.state.image;
    blog['publish_date'] = blog.publish_date?blog.publish_date: blog.created_on;
    const response = await manageBlog.UpdateBlog(id, blog)
    if (response.data.status === 1) {
      window.scrollTo({ top: 0, left: 0, behavior: 'smooth' })
      this.setState({
        submitStatus: false,
        message: response.data.message,
        responsetype: 'sucess'
      })
    } else {
      window.scrollTo({ top: 0, left: 0, behavior: 'smooth' })
      this.setState({
        submitStatus: false,
        message: response.data.message,
        responsetype: 'error'
      })
    }
  }

  isEmpty = obj => {
    for (var key in obj) {
      if (obj.hasOwnProperty(key)) return false
    }
    return true
  }

  setSelected = event => {
    const data_selected_row = []
    event.map((selectValues, index) => {
      data_selected_row[index] = selectValues.value
    })

    this.setState({ category: data_selected_row })
    this.setState({ category_selected: event })
  }

  onuplaodProfile = async (value, index, item) => {

    
    this.setState({ submitStatus: true })
    let file = value.target.files[0];
    const formData = new FormData();
    formData.append("image", file);
    const config = {
      headers: {
        "content-type": "multipart/form-data"
      }
    };
    // const response = await managePortfolio.uploadFile(formData, config);
    const response = await manageBlog.uploadFile(file);
    if (response && response.data) {
      this.setState({
        image: response.data.data.file_location,
        submitStatus: false
      })
    }
  };

  render() {
    const { blog ,image } = this.state
    console.log('blog---',blog);
    return (
      <React.Fragment>
        <div className='container-fluid admin-body'>
          <div className='admin-row'>
            <div className='col-md-2 col-sm-12 sidebar'>
              <Adminsidebar props={this.props} />
            </div>
            <div className='col-md-10 col-sm-12 content'>
              <div className='row content-row'>
                <div className='col-md-12 header'>
                  <Adminheader />
                </div>
                <div className='col-md-12 top-black-bar'>
                  <div className='bread-crumbs'>
                    <span className='navs blogs'>Blog</span>
                    <span className='navs nav-sub'>-</span>
                    <span className='navs nav-sub'>Edit</span>
                  </div>
                  <div className="export">
                            <Link
                                          class="add-new"
                                          to={
                                            "/admin/add-blog/"
                                          }
                                        >
                              Add New
                              </Link>
                             
                      </div>
                </div>
                {this.state.loadStatus?
                <div className='col-md-12 content-block'>
                  <div className='container tz-admin-manage-block edit-data'>
                    <h1>Edit Blog</h1>
                    <p className={this.state.responsetype}>
                      {this.state.message}
                    </p>
                    <React.Fragment>
                      <p className='error'>{this.state.errors.validate}</p>
                      <div className='card'>
                        <div className='card-header'>Blog details</div>
                        <div className='card-body'>
                          <div className='form-group'></div>

                          <div className='form-group'>
                            <label>Title</label>
                            <input
                              type='text'
                              name='blog_title'
                              className='form-control'
                              value={blog.blog_title ? blog.blog_title : ''}
                              onChange={e => this.handleChange(e)}
                            />
                            {this.state.errors.blog_title !== '' ? (
                              <div className='validate_error'>
                                <p>{this.state.errors.blog_title}</p>
                              </div>
                            ) : null}
                          </div>
                          <div className='form-group'>
                            <label>Page Slug</label>  <label style={{color:'green'}}> ( Allowed Characters: A-Z , a-z , 0-9 , - , _ )</label>
                            <input
                              type='text'
                              name='page_slug'
                              className='form-control'
                              value={blog.page_slug ? blog.page_slug : ''}
                              onChange={e => this.handleChange(e)}
                            />
                            {this.state.errors.page_slug !== '' ? (
                              <div className='validate_error'>
                                <p>{this.state.errors.page_slug}</p>
                              </div>
                            ) : null}
                          </div>
                          <div className='form-group'>
                            <label>Meta Keywords</label>
                            <input
                              type='text'
                              name='meta_key'
                              className='form-control'
                              value={blog.meta_key ? blog.meta_key : ''}
                              onChange={e => this.handleChange(e)}
                            />
                            {this.state.errors.blog_title !== '' ? (
                              <div className='validate_error'>
                                <p>{this.state.errors.meta_key}</p>
                              </div>
                            ) : null}
                          </div>
                          <div className='form-group'>
                            <label>Meta Description</label>
                            <input
                              type='text'
                              name='meta_desc'
                              className='form-control'
                              value={blog.meta_desc ? blog.meta_desc : ''}
                              onChange={e => this.handleChange(e)}
                            />
                            {this.state.errors.meta_desc !== '' ? (
                              <div className='validate_error'>
                                <p>{this.state.errors.meta_desc}</p>
                              </div>
                            ) : null}
                          </div>
                          <div className='form-group'>
                            <label>Short Description</label>
                            <input
                              type='text'
                              name='short_desc'
                              className='form-control'
                              value={blog.short_desc ? blog.short_desc : ''}
                              onChange={e => this.handleChange(e)}
                            />
                            {this.state.errors.short_desc !== '' ? (
                              <div className='validate_error'>
                                <p>{this.state.errors.short_desc}</p>
                              </div>
                            ) : null}
                          </div>
                          <div className='form-group'>
                            <label>Content Section</label>
                            <CKEditor
                              editor={ClassicEditor}
                              data={blog.blog_content ? blog.blog_content : ''}
                              onReady={editor => {
                                // You can store the "editor" and use when it is needed.
                              }}
                              onChange={(event, editor) => {
                                const data = editor.getData()

                                this.handleChangeEditor(data, 'blog_content')
                              }}
                              onBlur={(event, editor) => { }}
                              onFocus={(event, editor) => { }}
                            />
                          </div>
                          <div className='form-group'>
                            <label>Category</label>
                            <MultiSelect
                              options={this.state.category_options}
                              value={this.state.category_selected}
                              onChange={this.setSelected} //  labelledBy={"Select"}
                            />
                          </div>


                          <div className="form-group blog-image">
                            <label>Image</label>
                            <div className="add_row"  >
                              <UploadImage
                                onuplaodProfile={this.onuplaodProfile}
                                image={image}
                              />

                            </div>

                          </div>
                          <div className='form-group'>
                            <label>Author</label>
                            <input
                              type='text'
                              name='author'
                              className='form-control'
                              value={blog.author ? blog.author : ''}
                              onChange={e => this.handleChange(e)}
                            />
                            {this.state.errors.author !== '' ? (
                              <div className='validate_error'>
                                <p>{this.state.errors.author}</p>
                              </div>
                            ) : null}
                          </div>
                          <div className='form-group'>
                            <label>Publish Date</label>
                            
                           <DatePicker
                           format="YYYY-MM-D"
                           className='form-control ant-datepicker'
                           onChange={e => this.handleChangeDate(e)}
                           defaultValue={blog.publish_date?moment(blog.publish_date):null}
                         />

                            {this.state.errors.publish_date !== '' ? (
                              <div className='validate_error'>
                                <p>{this.state.errors.publish_date}</p>
                              </div>
                            ) : null}
                          </div>


                        </div>
                      </div>

                      <div className='submit'>
                        <button
                          className='submit-btn'
                          onClick={this.submitBlog}
                          disabled={this.state.submitStatus}
                        >
                          {this.state.submitStatus === true ? (
                            <Spinner
                              animation='border'
                              variant='light'
                              size='sm'
                            />   
                          ) : (
                              ''
                            )}
                          Submit
                        </button>
                        <Link className='cancel-btn' to='/admin/blogs'>
                          Cancel
                        </Link>
                      </div>
                    </React.Fragment>
                  </div>
                </div>
                :null }
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    )
  }
}

export default AddBlog
