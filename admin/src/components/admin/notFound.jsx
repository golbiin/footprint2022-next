import React, { Component } from "react";
import { Link } from "react-router-dom";
class NotFound extends Component {

  render() {
    return (
      <React.Fragment>
        <div className="container-fluid admin-body">
          <div id="admin_login">
            <div className="loginbox">
              <h1>404 - PAGE NOT FOUND</h1>
              <Link to="/" style={{ display:'block', textAlign: 'center'}}>Back to Login</Link>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default NotFound;
