import React, { Component } from "react";
import Adminheader from "./common/adminHeader";
import Adminsidebar from "./common/adminSidebar";
import * as authServices from "../../services/admin/login";
class AdminDashboard extends Component {
  state = {};
  render() {
    console.log(
      this.props.history.location.state &&
        this.props.history.location.state.from.pathname
    );
   
    return (
      <React.Fragment>
        <div className="container-fluid admin-body">
          <div className="admin-row">
            <div className="col-md-2 col-sm-12 sidebar">
              <Adminsidebar props={this.props} />
            </div>
            <div className="col-md-10 col-sm-12 content">
              <div className="row content-row">
                <div className="col-md-12 header">
                  <Adminheader  />
                </div>
                <div className="col-md-12 top-black-bar">
                  <div className="bread-crumbs">
                    <span className="navs dashboard">Dashboard</span>
                  </div>
                </div>
                <div
                  className="col-md-12 contents"
                  style={{ height: "500px", background: "#fff" }}
                ></div>
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default AdminDashboard;
