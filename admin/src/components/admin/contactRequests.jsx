import React, { Component } from "react";
import Adminsidebar from "./common/adminSidebar";
import Adminheader from "./common/adminHeader";
import * as userServices from "../../services/admin/manageUser";
import Spinner from "react-bootstrap/Spinner";
import { Link } from "react-router-dom";
import Moment from "react-moment";
import moment from "moment";
import Pagination from "react-js-pagination";

class ContactRequests extends Component {
  state = {
    requests: [],
    loading: true,
    response_message: "",
    paginate: { activePage: 1, number_of_pages: 15, key: "" },
    total: 10
  };
  componentDidMount = async () => {
    this.getAllrequests();
  };
  getAllrequests = async () => {
    try {
      let response = await userServices.getContactRequest(this.state.paginate);
      if (response && response.data) {
        if (response.data.status === 1) {
          let requests = [...this.state.requests];
          requests = response.data.data;
          this.setState({
            requests,
            loading: false
          });
        } else {
          this.setState({
            response_message: response.data.message,
            loading: false
          });
        }
      }
    } catch (err) {
      console.log(err);
    }
  };
  handlePageChange = pageNumber => {
    let paginate = { ...this.state.paginate };
    let subscriptions = [...this.state.subscriptions];
    paginate.activePage = pageNumber;
    subscriptions = [];
    this.setState({ paginate, subscriptions, loader: true });
    setTimeout(() => this.getAllsubscriptions(), 1);
  };
  render() {
    let { requests, loading, response_message, paginate, total } = this.state;
    console.log("requests", requests)
    return (
      <React.Fragment>
        {" "}
        <div className="container-fluid admin-body">
          <div className="admin-row">
            <div className="col-md-2 col-sm-12 sidebar">
              <Adminsidebar props={this.props} />
            </div>
            <div className="col-md-10 col-sm-12 content">
              <div className="row content-row">
                <div className="col-md-12 header">
                  <Adminheader />
                </div>
                <div className="col-md-12 top-black-bar">
                  <div className="bread-crumbs col-md-6 col-sm-12">
                    <span className="navs conatct">Contact Requests</span>
                  </div>
                </div>
                <div className="col-md-12 content-block">
                  <div className="tz-admin-manage-block">
                    <div className="title-bar">
                      <div className="title">Contact List</div>
                    </div>
                    <div className="listSubscriptions table-responsive">
                      <table className="table">
                        <thead>
                          <tr>
                            <th scope="col" className="name">
                              Name
                            </th>
                            <th scope="col" className="date">
                              Email
                            </th>
                            <th scope="col" className="email">
                              IP
                            </th>
                            <th scope="col" className="email">
                              Page
                            </th>
                            <th scope="col" className="name">
                              Date
                            </th>
                            <th scope="col" className="email">
                              View Message
                            </th>
                          </tr>
                        </thead>
                        <tbody>
                          {loading ? (
                            <tr>
                              <td
                                colSpan="6"
                                style={{
                                  textAlign: "center",
                                  background: "#fff"
                                }}
                              >
                                <div className="admin-spinner">
                                  <Spinner animation="grow" variant="dark" />
                                  <span>Connecting...</span>
                                </div>
                              </td>
                            </tr>
                          ) : requests && requests.length !== 0 ? (
                            requests.map((value, index) => {
                              return (
                                value.contact_data ?
                                <tr
                                  className={
                                    value.viewd_status ? "viewd" : "not-viewd"
                                  }
                                  key={index}
                                >
                                  <td className="project_name">
                                    {value.contact_data.name
                                      ? value.contact_data.name
                                      : value.contact_data.first_name +
                                        "" +
                                        (value.contact_data.last_name? value.contact_data.last_name:"")}
                                  </td>

                                  <td className="amount">
                                    {value.contact_data.email}
                                  </td>
                                  <td className="amount">{value.ip}</td>
                                  <td className="amount">
                                    <span className="flag">{value.page}</span>
                                  </td>
                                  <td className="name">
                                    <Moment fromNow>
                                      {value.submitted_on}
                                    </Moment>
                                  </td>

                                  <td className="actions">
                                    <Link
                                      className="red-btn"
                                      to={
                                        "/admin/contact-requests/read/" +
                                        value._id
                                      }
                                    >
                                      View
                                    </Link>
                                  </td>
                                </tr>
                                : ''
                              );
                            })
                          ) : (
                            <tr>
                              <td
                                colSpan="8"
                                className="odd"
                                style={{ textAlign: "center" }}
                              >
                                No data found
                              </td>
                            </tr>
                          )}
                        </tbody>
                      </table>
                      {requests.length !== 0 ? (
                        <div className="paginate">
                          <Pagination
                            hideFirstLastPages
                            prevPageText={<i className="left" />}
                            nextPageText={<i className="right" />}
                            activePage={paginate.activePage}
                            itemsCountPerPage={10}
                            totalItemsCount={total}
                            pageRangeDisplayed={4}
                            onChange={this.handlePageChange}
                          />
                        </div>
                      ) : (
                        ""
                      )}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default ContactRequests;
