/* eslint react/no-multi-comp: 0, react/prop-types: 0 */

import React from "react";
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from "reactstrap";

const DeleteConfirm = (props) => {
  //const { buttonLabel, className } = props;
  console.log(props);
  let modal = props.modal;

  const toggle = () => {
    modal = false;
    props.toogle();
  };
  const doSometing = () => {
    props.deleteUser();
  };
  return (
    <div>
      <Modal isOpen={modal} toggle={toggle} className="delete-popup">
        <ModalHeader toggle={toggle}>Delete Confirmation</ModalHeader>
        <ModalBody>Are you sure want to delete? </ModalBody>
        <ModalFooter>
          <Button color="danger" onClick={doSometing}>
            Delete
          </Button>{" "}
          <Button color="secondary" onClick={toggle}>
            Cancel
          </Button>
        </ModalFooter>
      </Modal>
    </div>
  );
};

export default DeleteConfirm;
