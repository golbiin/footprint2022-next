import React, { Component } from "react";
import Spinner from "react-bootstrap/Spinner";
class SpinnerBtn extends Component {
  state = {};
  render() {
    const { props } = this;
    return (
      <button className={props.btnClass} disabled>
        <Spinner animation={props.animation} size={props.size} />
        Submitting...
      </button>
    );
  }
}

export default SpinnerBtn;
