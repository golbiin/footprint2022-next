import React, { Component } from "react";
import { Link } from "react-router-dom";
import { slide as Menu } from "react-burger-menu";
import * as authServices from "../../../services/admin/login";

class Adminsidebar extends Component {
  state = {
    show: false,
    menuopen: false,
    showSettings: false,
    selectedMenu: ""
  };
  hideElement = () => {
    console.log("1", this.state.menuopen);
    let cart_open = !this.state.menuopen;
    this.setState({ menuopen: cart_open });
    console.log("2", cart_open, this.state.menuopen);
  };
  isMenuOpen = state => {
    if (state.isOpen === false) {
      let menushow = "cart-on";
      if (this.state.slidemenu === "cart-on") {
        menushow = "cart-off";
      }
      this.setState({ slidemenu: menushow });
    }
  };
  shouldDisableOverlayClick = () => {
    return false;
  };
  toogleDropdown = () => {
    let toogled = !this.state.show;
    this.setState({ show: toogled, showSettings: false });
    console.log("exit", this.state.show);
  };
  toogleSettingsDropdown = selectedMenu => {
    let toogled = true;
    if (this.state.selectedMenu === selectedMenu)
      toogled = !this.state.showSettings;
    this.setState({ showSettings: toogled, show: false, selectedMenu });
    console.log("exit", this.state.showSettings);
  };
  hideMenu = () => {
    this.setState({ menuopen: false });
  };
  Signout = () => {
    authServices.logout();
    this.props.props.history.push({
      pathname: "/admin/login"
    });
  };
  render() {
    const images = require.context("../../../assets/images/admin", true);
    return (
      <React.Fragment>
        <div className="leftbar big-screen">
          <div className="logo-section">
            <a href="/">
              {" "}
              <div className="logo">
                Footprint
                {/* <img src={images("./logo.png")} alt="logo" /> */}
              </div>
            </a>
            <div className="menubar"></div>
          </div>
          <div className="menu">
            <ul className="list ">
              <li className="list-item dashboard">
                <Link to="/admin/home">Dashboard</Link>
              </li>
              <li className="list-item users">
                <Link to="/admin/users">Users</Link>
              </li>
              <li className="list-item portfolio">
                <Link to="/admin/portfolio/add">Portfolio</Link>
              </li>
               <li className="list-item logins">
                <Link to="/admin/projects">Projects</Link>
              </li>
              <li className="list-item subscription">
                <Link to="/admin/subscriptions">Subscriptions</Link>
               </li>
              {/* <li className="list-item blogs">
                <Link to="/admin/blogs">Blogs</Link>
              </li> */}

              {/* <li
                className={
                  this.state.showSettings &&
                  this.state.selectedMenu === "coupons"
                    ? "list-item coupons show-dropdown"
                    : "list-item coupons hide-dropdown"
                }
              >
                <Link
                  className="dropdown"
                  onClick={() => this.toogleSettingsDropdown("coupons")}
                >
                  Coupons
                </Link>
                <div
                  className={
                    this.state.showSettings &&
                    this.state.selectedMenu === "coupons"
                      ? "users-dropdown sub-item"
                      : "users-dropdown sub-item disable"
                  }
                >
                  <Link to="/admin/coupons">Coupons</Link>
                  <Link to="/admin/user-wallets">User Wallets</Link>
                </div>
              </li>

            
              <li className="list-item logins">
                <Link to="/admin/leads">Leads</Link>
              </li> */}
               <li className="list-item reviews">
                <Link to="/admin/reviews">Reviews</Link>
              </li>
              <li className="list-item subscriptions">
                <Link to="/admin/contact-requests">Contacts</Link>
              </li>
              
              <li className="list-item leads">
                <Link to="/admin/leads">Leads</Link>
              </li>
              <li className="list-item newsletter">
                <Link to="/admin/newsLetter">Newsletter</Link>
              </li>
              {/* <li className="list-item settings">
                <Link to="/admin/trackings">Page Visits</Link>
              </li> */}
              {/* <li className="list-item logins">
                <Link to="/admin/tracking">Tracking</Link>
              </li> */}
              {/* <li className="list-item settings">
                <Link to="/admin/activities">Uer Activities</Link>
              </li> */}
               <li className="list-item blog">
                <Link to="/admin/blogs">Blogs</Link>
              </li>  
              <li
                className={
                  this.state.showSettings &&
                  this.state.selectedMenu === "settings"
                    ? "list-item settings show-dropdown"
                    : "list-item settings hide-dropdown"
                }
              >
                <Link
                  className="dropdown"
                  onClick={() => this.toogleSettingsDropdown("settings")}
                >
                  Settings
                </Link>
                <div
                  className={
                    this.state.showSettings &&
                    this.state.selectedMenu === "settings"
                      ? "users-dropdown sub-item"
                      : "users-dropdown sub-item disable"
                  }
                >
                  <Link to="/admin/settings/change-password">
                    Change Password
                  </Link>
                </div>
              </li>
            </ul>
          </div>
          <div className="signout">
            <i className="fa fa-sign-out" aria-hidden="true"></i>
            <Link to="#" onClick={this.Signout}>
              Sign out
            </Link>
          </div>
        </div>
        <div className="leftbar small-screen">
          <div className="logo-section">
            <a href="/">
              <div className="logo">OutsourceMe</div>
            </a>
            <div className="menubar" onClick={this.hideElement}></div>
          </div>
          <div className="menu">
            <div
              id={
                this.state.menuopen === true
                  ? "admin-menu-on"
                  : "admin-menu-off"
              }
            >
              <Menu
                isOpen={this.state.menuopen}
                onStateChange={this.isMenuOpen}
                right
                disableOverlayClick={() => this.shouldDisableOverlayClick}
                width={"380px"}
              >
                <span className="close" onClick={this.hideMenu}></span>
                <ul className="list ">
                  <li className="list-item dashboard">
                    <Link to="/admin/home">Dashboard</Link>
                  </li>
                  <li className="list-item users">
                    <Link to="/admin/users">Users</Link>
                  </li>
                  <li className="list-item portfolio">
                    <Link to="/admin/portfolio/add">Portfolio</Link>
                  </li>
                  
                  <li
                    className={
                      this.state.showSettings &&
                      this.state.selectedMenu === "coupons"
                        ? "list-item coupons show-dropdown"
                        : "list-item coupons hide-dropdown"
                    }
                  >
                    <Link
                      className="dropdown"
                      onClick={() => this.toogleSettingsDropdown("coupons")}
                    >
                      Coupons
                    </Link>
                    <div
                      className={
                        this.state.showSettings &&
                        this.state.selectedMenu === "coupons"
                          ? "users-dropdown sub-item"
                          : "users-dropdown sub-item disable"
                      }
                    >
                      <Link to="/admin/coupons">Coupons</Link>
                      <Link to="/admin/user-wallets">User Wallets</Link>
                    </div>
                  </li>

                  <li className="list-item logins">
                    <Link to="/admin/leads">Leads</Link>
                  </li>
                  <li className="list-item subscription">
                    <Link to="/admin/subscriptions">Subscriptions</Link>
                  </li>
                  <li className="list-item subscriptions">
                    <Link to="/admin/contact-requests">Contacts</Link>
                  </li>
                  <li className="list-item settings">
                    <Link to="/admin/trackings">Page Visits</Link>
                  </li>
                  <li className="list-item logins">
                    <Link to="/admin/login-trackings">Uer Logins</Link>
                  </li>
                  <li className="list-item settings">
                    <Link to="/admin/activities">Uer Activities</Link>
                  </li>
                  <li className="list-item blog">
                    <Link to="/admin/blogs">Blogs</Link>
                  </li>
                  <li
                    className={
                      this.state.showSettings &&
                      this.state.selectedMenu === "settings"
                        ? "list-item settings show-dropdown"
                        : "list-item settings hide-dropdown"
                    }
                  >
                    <Link
                      className="dropdown"
                      onClick={() => this.toogleSettingsDropdown("settings")}
                    >
                      Settings
                    </Link>
                    <div
                      className={
                        this.state.showSettings &&
                        this.state.selectedMenu === "settings"
                          ? "users-dropdown sub-item"
                          : "users-dropdown sub-item disable"
                      }
                    >
                      <Link to="/admin/settings/change-password">
                        Change Password
                      </Link>
                    </div>
                  </li>
                </ul>
              </Menu>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default Adminsidebar;