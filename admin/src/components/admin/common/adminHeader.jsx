import React, { Component } from "react";
import * as authServices from "../../../services/admin/login";
import { BrowserRouter, Link } from "react-router-dom";
//import * as notificationServices from "../../../services/admin/notifications";

class Adminheader extends Component {
  state = { unreadList: [], unreadCount: 0, fetchedNotfn: false };

  render() {
    const images = require.context("../../../assets/images/admin", true);
    const user = authServices.getCurrentUser();

    return (
      <React.Fragment>
        <div className="row header-container">
          <div className="col-md-12 tob-bar">
            <div className="actions">
              <nav className="navbar navbar-expand-sm bg-light navbar-light sticky-top first">
                <Link to="/notifications">
                  <div
                    className="notifications mobile-only-notify"
                    //onClick={this.goAllNotifications}
                  >
                    {this.state.unreadCount > 0 ? (
                      <span class="badge badge-light">
                        {this.state.unreadCount}
                      </span>
                    ) : (
                      ""
                    )}
                  </div>
                </Link>

                <div className="profile_menu">
                  <div className="profile-img pr-0">
                    <img src={user.profile_img ? user.profile_img : images("./no-img.png")} alt="profile-img" />
                    <span>{user.first_name}</span>
                  </div>
                </div>
              </nav>
            </div>
          </div>
          <div className="col-md-12 bread-crumb"></div>
        </div>
      </React.Fragment>
    );
  }
}

export default Adminheader;
