import React, { useState, useEffect } from "react";

const UploadImage = props => {
  const [image, setImage] = useState(props.image ? props.image : "");
  const uplaodHandle = async event => {
    event.preventDefault();
    let files = event.target.files[0];
    if (files) {
      if (checkMimeType(event)) {
        let image = URL.createObjectURL(event.target.files[0]);
        setImage(image);
        props.onuplaodProfile(event, props.index, "image");
      }
    }
  };
  const checkMimeType = event => {
    let files = event.target.files[0];
    const types = ["image/png", "image/jpeg", "image/gif"];
    let error = "";
    if (!types.includes(files.type)) {
      error = files.type + " is not a supported format\n";
      props.onuplaodProfile(error, "errors");
    } else {
      return true;
    }
  };
  return (
    <React.Fragment>
      <div className="profile_up">
        <label htmlFor="image">
          <React.Fragment>
            <input
              type="file"
              name="image"
              id="image"
              onChange={uplaodHandle}
            />
             {image != "" ? (
              <img src={image} className="img-fluid normal-img" />
            ) : (
              props.image ? (<img src={props.image} className="img-fluid normal-img" />) : ''
            )}
          </React.Fragment>
        </label>
      </div>
    </React.Fragment>
  );
};
export default UploadImage;
