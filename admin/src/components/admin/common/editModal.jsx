/* eslint react/no-multi-comp: 0, react/prop-types: 0 */
import React, { Component } from "react";
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from "reactstrap";
import { Link } from "react-router-dom";
import UploadImage from "./uploadImage";
import * as managePortfolio from "../../../services/admin/portfolio";
class EditPortfolio extends Component {
  state = { portfolio: {}, languages: [], categories: [] };
  componentWillReceiveProps = props => {
    console.log(props);
    let portfolio = { ...this.state.portfolio };
    let categories = [...this.state.categories];
    if (props.portfolio_item != undefined) {
      this.setState({
        portfolio: props.portfolio_item
      });
      setTimeout(() => {
        this.initializeLanguage(props.portfolio_item["main_category"]);
      }, 500);
    }
    if (props.categories != undefined) {
      this.setState({
        categories: props.categories
      });
    }
  };
  toggle = () => {
    let modal = false;
    this.props.toogle();
  };
  initializeLanguage = id => {
    let languages = [...this.state.languages];
    let categories = [...this.state.categories];
    let portfolio = { ...this.state.portfolio };
    let l_array = [];
    let c_array = [];
    if (categories.length != 0) {
      categories.map((cat_id, index) => {
        if (cat_id._id === id) {
          if (categories[index].length !== 0) {
            if (categories[index].languages) {
              categories[index].languages.map((lang, lindex) => {
                let addLanguageData = {
                  name: lang.language_name,
                  id: lang._id,
                  status: false
                };
                l_array.push(addLanguageData);
              });
              if (portfolio.languages === undefined) {
                portfolio["languages"] = l_array;
              } else {
                portfolio["languages"].map((item, sindex) => {
                  l_array.map((new_item, n_index) => {
                    if (new_item.id == item.id) {
                      new_item.status = item.status;
                    }
                  });
                });
                portfolio["languages"] = l_array;
              }
            }
            if (categories[index].sub_categories) {
              categories[index].sub_categories.map((sub_cat, sindex) => {
                let addCatData = {
                  name: sub_cat.sub_cat_name,
                  id: sub_cat._id,
                  status: false
                };
                c_array.push(addCatData);
              });
              if (portfolio.sub_category === undefined) {
                portfolio["sub_category"] = c_array;
              } else {
                portfolio["sub_category"].map((item, sindex) => {
                  c_array.map((new_item, n_index) => {
                    if (new_item.id == item.id) {
                      new_item.status = item.status;
                    }
                  });
                });
                portfolio["sub_category"] = c_array;
              }

              this.setState({ portfolio });
            }
          }
        }
      });

      //this.setState({ languages });
    }
  };
  onuplaodProfile = async (value, index, item) => {
    let file = value.target.files[0];
    let portfolio = { ...this.state.portfolio };

    // const response = await managePortfolio.uploadFile(formData, config);
    const response = await managePortfolio.uploadFile(file);
    if (response && response.data) {
      portfolio["portfolio_image"] = response.data.data.file_location;
      this.setState({ portfolio });
      console.log("update", this.state.portfolio);
    }
  };
  updateChildCat = (e, index) => {
    let portfolio = { ...this.state.portfolio };
    portfolio.sub_category[index]["status"] = !portfolio.sub_category[index][
      "status"
    ];
    this.setState({ portfolio });
  };
  updateLanguages = (e, index) => {
    let portfolio = { ...this.state.portfolio };
    portfolio.languages[index]["status"] = !portfolio.languages[index][
      "status"
    ];
    this.setState({ portfolio });
  };
  handleChange = (e, index) => {
    let portfolio = { ...this.state.portfolio };
    portfolio[e.target.name] = e.target.value;
    this.setState({ portfolio });
  };

  doSubmit = async () => {
    let portfolio = { ...this.state.portfolio };
    let languages = [...this.state.languages];
    let id = this.props.id;

    const response = await managePortfolio.updatePortfolio(portfolio, id);
    if (response.data.status == 1) {
      this.setState({
        response: response.data.message,
        response_type: "success"
      });
      this.props.getPortfolios();
      this.props.toogle();
    }
  };
  render() {
    let { portfolio, languages } = this.state;

    return (
      <div>
        <Modal
          isOpen={this.props.modal}
          toggle={this.toggle}
          className="edit-popup"
        >
          <ModalHeader toggle={this.toggle}>Edit portfolio</ModalHeader>
          <ModalBody>
            <p className={this.state.response_type}>{this.state.response}</p>
            <UploadImage
              onuplaodProfile={this.onuplaodProfile}
              image={portfolio.portfolio_image}
            />
            {/* <label>Parent category</label> */}
            {/* <select
              className="form-control half"
              value={
                this.props.portfolio_item
                  ? this.props.portfolio_item.main_category
                  : ""
              }
            >
              {this.props.categories
                ? this.props.categories.map((cat, index) => (
                    <option value={cat._id} key={index}>
                      {cat.parent}
                    </option>
                  ))
                : ""}
            </select> */}
            <label>Select Sub category:</label>

            <div className="check-box">
              {portfolio.sub_category != undefined
                ? portfolio.sub_category.map((scat, index) => (
                    <label class="container" key={index}>
                      <input
                        type="checkbox"
                        name={scat.name}
                        className="check-cats"
                        value={scat.id}
                        checked={scat.status}
                        onChange={e => this.updateChildCat(e, index)}
                      />
                      {scat.name}
                      <span className="checkmark"></span>
                    </label>
                  ))
                : ""}
            </div>
            {portfolio && portfolio.languages !== undefined ? (
              portfolio.languages.length !== 0 ? (
                <React.Fragment>
                  <label>Select Language:</label>

                  <div className="check-box">
                    {portfolio.languages != undefined &&
                    portfolio.languages.length != 0
                      ? portfolio.languages.map((lang, lindex) => (
                          <label class="container" key={lindex}>
                            <input
                              type="checkbox"
                              name={lang.name}
                              className="check-cats"
                              value={lang.id}
                              checked={lang.status}
                              onChange={e => this.updateLanguages(e, lindex)}
                            />
                            {lang.name}
                            <span className="checkmark"></span>
                          </label>
                        ))
                      : ""}
                  </div>
                </React.Fragment>
              ) : (
                ""
              )
            ) : (
              ""
            )}

            <label>Title</label>
            <input
              type="text"
              name="title"
              className="form-control"
              value={portfolio ? portfolio.title : ""}
              onChange={this.handleChange}
            />
            <label>URL</label>
            <input
              type="text"
              name="url"
              className="form-control"
              value={portfolio ? portfolio.url : ""}
              onChange={this.handleChange}
            />
            <label>Order</label>
            <input
              type="text"
              name="order"
              className="form-control"
              value={
                portfolio
                  ? portfolio.order !== "999"
                    ? portfolio.order
                    : ""
                  : ""
              }
              onChange={this.handleChange}
            />
          </ModalBody>
          <ModalFooter>
            <Button color="danger" onClick={this.doSubmit}>
              Update
            </Button>{" "}
            <Button color="secondary" onClick={this.toggle}>
              Cancel
            </Button>
          </ModalFooter>
        </Modal>
      </div>
    );
  }
}

export default EditPortfolio;
