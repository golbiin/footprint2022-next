import React, { Component } from "react";
import Adminheader from "./common/adminHeader";
import Adminsidebar from "./common/adminSidebar";
import * as manageUser from "../../services/admin/manageUser";
import Spinner from "react-bootstrap/Spinner";
import Pagination from "react-js-pagination";
import Moment from "react-moment";
import DeleteConfirm from "./common/deleteConfirm";
import { Link } from "react-router-dom";
import { CSVLink } from "react-csv";

class ManageUsers extends Component {
  state = {
    data: { type: "user", activePage: 1, number_of_pages: 10, key: "" },
    userData: [],
    userDataType: "",
    checkStatus: false,
    loader: true,
    searchSpec: "",
    modal: false,
    csvData: [],
    headers: [
      { label: "Name", key: "name" },
      { label: "Email", key: "email" },
      { label: "Join date", key: "doj" }
    ]
  };
  handlePageChange = pageNumber => {
    let data = { ...this.state.data };
    let userData = [...this.state.userData];
    data.activePage = pageNumber;
    userData = [];
    this.setState({ data, userData, loader: true });
    setTimeout(() => this.getallusers(), 1);
  };

  componentDidMount = async () => {
    const exportData = await manageUser.exportData("user");
    if (exportData) {
      if (exportData.data.status === 1) {
        let csvDatas = exportData.data.data;
        if (csvDatas) {
          let dataPush = [];
          let csvData = [...this.state.csvData];

          csvDatas.map((data, index) => {
            dataPush = {
              name: data.billing ? data.billing.first_name : "NA",
              email: data.email,
              doj: data.created_on
            };

            csvData.push(dataPush);
          });
          this.setState({ csvData });
          console.log("csv", this.state.csvData);
        }
      }
    }
    this.getallusers();
  };
  getallusers = async () => {
    const totalresponse = await manageUser.getCount(this.state.data);
    if (totalresponse) {
      if (totalresponse.data.status === 1) {
        let totalUsers = totalresponse.data.data.total;
        this.setState({ totalUsers: totalUsers });
      }
    }

    const response = await manageUser.getAllusers(this.state.data);
    if (response.data.status === 1) {
      this.setState({ userData: response.data.data, loader: false });
      console.log("res", response.data.data);
      //console.log(this.state.lab[4].profile_data.services[0]);
    } else {
      this.setState({ loader: false });
    }
  };
  changeStatus = async (id, status, index) => {
    const response = await manageUser.changeStatus(id, status);
    if (response.data.status === 1) {
      let userData = [...this.state.userData];
      userData[index]["status"] = status;
      this.setState(userData);
      console.log(userData);
    }
  };

  deleteUser = async () => {
    console.log("ddd");
    let id = this.state.user;
    let lab_index = this.state.index;
    const response = await manageUser.deleteUser(id);
    if (response) {
      if (response.data.status === 1) {
        let newuserData = [...this.state.userData];
        newuserData.splice(lab_index, 1);
        this.setState({ userData: newuserData });
        const response = await manageUser.getCount(this.state.data.type);
        if (response) {
          let totalUsers = response.data.data.total;
          this.setState({ totalUsers: totalUsers });
          this.getallusers();
          this.toogle();
        }
      }
    }
  };
  saveAndtoogle = (id, lab_index) => {
    this.setState({ user: id, index: lab_index });
    this.toogle();
  };
  toogle = () => {
    let status = !this.state.modal;
    this.setState({ modal: status });
  };
  searchUser = async e => {
    let value = e.target.value;
    let data = { ...this.state.data };
    data.key = value;
    data.activePage = 1;
    this.setState({ data });
    setTimeout(() => this.getallusers(), 20);

    // if (value == "") {
    //   this.getallusers();
    // } else {
    //   this.setState({ loader: true });
    //   const response = await manageUser.searchUser({
    //     key: value,
    //     type: "userData",
    //   });
    //   if (response.data.status === 1) {
    //     let userData = [...this.state.userData];
    //     userData = response.data.data ? response.data.data : "";
    //     this.setState({ userData, loader: false });
    //   } else {
    //     this.setState({ loader: false });
    //   }
    // }
  };

  render() {
    const images = require.context("./../../assets/images/admin", true);
    console.log("userDatas", this.state.userData);
    return (
      <React.Fragment>
        <div className="container-fluid admin-body">
          <div className="admin-row">
            <div className="col-md-2 col-sm-12 sidebar">
              <Adminsidebar props={this.props} />
            </div>
            <div className="col-md-10 col-sm-12 content">
              <div className="row content-row">
                <div className="col-md-12 header">
                  <Adminheader />
                </div>
                <div className="col-md-12 top-black-bar">
                  <div className="bread-crumbs col-md-6 col-sm-12">
                    <span className="navs users">Users</span>
                  </div>
                  <div className="searchbox col-md-6 col-sm-12">
                    <span></span>
                    <input
                      type="text"
                      placeholder="Search"
                      name="search"
                      className="search form-control"
                      onChange={e => this.searchUser(e)}
                      value={this.state.data.key}
                    />
                  </div>
                </div>
                <div className="col-md-12 content-block">
                  <div className="tz-admin-manage-block">
                    <div className="title-bar">
                      <div className="title">Users List</div>
                   
                    </div>
                    <div className="listusers table-responsive">
                      <table className="table">
                        <thead>
                          <tr>
                            <th scope="col" className="name">
                              NAME
                            </th>
                            <th scope="col" className="date">
                              DATE JOINED
                            </th>
                            <th scope="col" className="email">
                              EMAIL
                            </th>
                            {/* <th scope="col" className="reg-status">
                              REGISTRATION STATUS
                            </th> */}
                            <th scope="col" className="approve">
                              USER STATUS
                            </th>
                            {/* <th scope="col" className="reg-status">
                              SUBMISSION PAGE
                            </th> */}
                            <th scope="col" className="actions"></th>
                          </tr>
                        </thead>
                        <tbody>
                          {this.state.loader ? (
                            <tr>
                              <td
                                colSpan="6"
                                style={{
                                  textAlign: "center",
                                  background: "#fff"
                                }}
                              >
                                <div className="admin-spinner">
                                  <Spinner animation="grow" variant="dark" />
                                  <span>Connecting...</span>
                                </div>
                              </td>
                            </tr>
                          ) : this.state.userData.length !== 0 ? (
                            this.state.userData.map((userData, index) => (
                              <tr className="odd" key={userData._id}>
                                <td className="name">
                                  {userData.billing &&
                                  userData.billing.first_name
                                    ? userData.billing.first_name +
                                      " " +
                                      userData.billing.last_name
                                    : "NA"}
                                </td>
                                <td className="date">
                                  <Moment format="DD/MM/YYYY">
                                    {userData.created_on}
                                  </Moment>
                                </td>
                                <td className="email">{userData.email}</td>
                                {/* <td className="reg-status">
                                  {userData.profile &&
                                  userData.profile.first_name ? (
                                    <span className="finish">Finished</span>
                                  ) : (
                                    <span className="unfinish">Unfinished</span>
                                  )}
                                </td> */}
                                <td className="approve">
                                  <label className="switch ">
                                    <input
                                      type="checkbox"
                                      className="default"
                                      defaultChecked={userData.status}
                                      onChange={() =>
                                        this.changeStatus(
                                          userData._id,
                                          !userData.status,
                                          index
                                        )
                                      }
                                    />
                                    <span className="slider round"></span>
                                    <span className="approve-label">
                                      {userData.status}
                                      {userData.status ? (
                                        <span className="active">
                                          Activated
                                        </span>
                                      ) : (
                                        "Suspended"
                                      )}
                                    </span>
                                  </label>
                                </td>
                                {/* <td>{userData.page}</td> */}
                                <td className="actions">
                                  {/* <span
                                      data-toggle="dropdown"
                                      aria-expanded="false"
                                    ></span> */}
                                  <div className="lg-screen-actions">
                                    <div class="dropdown">
                                      <button
                                        class="btn  dropdown-toggle"
                                        type="button"
                                        data-toggle="dropdown"
                                        aria-haspopup="true"
                                        aria-expanded="false"
                                      ></button>
                                      <div class="dropdown-menu">
                                        <Link
                                          class="dropdown-item"
                                          onClick={() =>
                                            this.saveAndtoogle(
                                              userData._id,
                                              index
                                            )
                                          }
                                        >
                                          Delete
                                        </Link>
                                       {/*  <Link
                                          class="dropdown-item"
                                          to={
                                            "/admin/users/edit/" + userData._id
                                          }
                                        >
                                          Edit
                                        </Link> */}
                                        {/* <Link
                                          class="dropdown-item"
                                          to={
                                            "/admin/users/chat-url/" +
                                            userData._id
                                          }
                                        >
                                          Chat URL
                                        </Link>
                                        <Link
                                          class="dropdown-item"
                                          to={
                                            "/admin/users/task-management/" +
                                            userData._id
                                          }
                                        >
                                          Task Management
                                        </Link>
                                        <Link
                                          class="dropdown-item"
                                          to={
                                            "/admin/users/working-hours-list/" +
                                            userData._id
                                          }
                                        >
                                          Working Hours Details
                                        </Link>
                                        <Link
                                          class="dropdown-item"
                                          to={
                                            "/admin/users/assign-user-plan/" +
                                            userData._id
                                          }
                                        >
                                          Assign Plan
                                        </Link> */}
                                      </div>
                                    </div>
                                  </div>
                                  <div className="sm-screen-actions">
                                    <Link
                                      class="btn-edit btn"
                                      to={
                                        "/admin/users/chat-url/" + userData._id
                                      }
                                    >
                                      Chat URL
                                    </Link>
                                    <Link
                                      class="btn-edit btn"
                                      to={"/admin/users/edit/" + userData._id}
                                    >
                                      Edit
                                    </Link>
                                    <Link
                                      className="btn btn-dlt"
                                      onClick={() =>
                                        this.saveAndtoogle(userData._id, index)
                                      }
                                    >
                                      Delete
                                    </Link>
                                  </div>
                                </td>
                              </tr>
                            ))
                          ) : (
                            <tr>
                              <td
                                colSpan="8"
                                className="odd"
                                style={{ textAlign: "center" }}
                              >
                                No records found.
                              </td>
                            </tr>
                          )}
                        </tbody>
                      </table>
                      {this.state.userData.length !== 0 ? (
                        <div className="paginate">
                          <Pagination
                            hideFirstLastPages
                            prevPageText={<i className="left" />}
                            nextPageText={<i className="right" />}
                            activePage={this.state.data.activePage}
                            itemsCountPerPage={10}
                            totalItemsCount={this.state.totalUsers}
                            pageRangeDisplayed={4}
                            onChange={this.handlePageChange}
                          />
                        </div>
                      ) : (
                        ""
                      )}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <DeleteConfirm
          modal={this.state.modal}
          toogle={this.toogle}
          deleteUser={this.deleteUser}
        />
      </React.Fragment>
    );
  }
}

export default ManageUsers;
