import React, { useState, useEffect } from "react";
import Adminsidebar from "../common/adminSidebar";
import Adminheader from "../common/adminHeader";
import * as managecoupons from "../../../services/admin/coupons";
import { Link } from "react-router-dom";
import Spinner from "react-bootstrap/Spinner";
import Joi from "joi-browser";
import "antd/dist/antd.css";
import { DatePicker } from "antd";
import moment from "moment-timezone";
import Pagination from "react-js-pagination";
import Moment from "react-moment";
const Coupons = props => {
  const [users, setUsers] = useState([]);
  const [init_coupon, initCoupon] = useState([]);
  const [coupons, setCoupons] = useState([]);
  const [submitStatus, setsubmitStatus] = useState(false);
  const [errors, setErrors] = useState([]);
  const [responseMessage, setResponseMessage] = useState();
  const [responseType, setResponseType] = useState();
  const [activePage, setActivePage] = useState(1);
  const [number_of_pages, setNumofPages] = useState(20);
  const [totalCount, setCount] = useState("");
  const CouponSchema = {
    user: Joi.string()
      .required()
      .error(() => {
        return {
          message:
            "Please be sure you’ve selected user. It is not allowed to be empty."
        };
      }),
    coupon_code: Joi.string()
      .required()
      .error(() => {
        return {
          message:
            "Please be sure you’ve filled coupon code. It is not allowed to be empty."
        };
      }),
    coupon_amount: Joi.string()
      .required()
      .error(() => {
        return {
          message:
            "Please be sure you’ve filled coupon ampount. It is not allowed to be empty."
        };
      }),
    expire_on: Joi.object()
      .required()
      .error(() => {
        return {
          message:
            "Please be sure you’ve filled Expire date. It is not allowed to be empty."
        };
      })
  };
  useEffect(() => {
    getUsers();
    getCoupons();
    initializeCoupon();
  }, 1);

  const initializeCoupon = () => {
    let c_array = {
      user: "",
      coupon_code: "",
      coupon_amount: "",
      expire_on: ""
    };
    initCoupon(c_array);
  };
  const getUsers = async () => {
    const response = await managecoupons.getAllusers();
    if (response.data.status === 1) {
      setUsers(response.data.data);
    }
  };

  const getCoupons = async pageNum => {
    const activePageNum = pageNum ? pageNum : activePage;
    console.log("activePage--3-------3", activePage);
    const countRes = await managecoupons.getTotalCoupons();
    if (countRes.data.status === 1) {
      setCount(countRes.data.data.total);
    }
    const response = await managecoupons.getAllCoupons(
      activePageNum,
      number_of_pages
    );
    if (response.data.status === 1) {
      setCoupons(response.data.data);
    }
  };

  const deleteCoupon = async (index, id) => {
    let response = await managecoupons.deleteCoupon(id);
    if (response.data.status === 1) {
      getCoupons();
    }
  };

  const sendCoupon = async () => {
    const e_array = [...errors];
    setsubmitStatus(true);
    let validate = Joi.validate(init_coupon, CouponSchema);
    if (validate.error) {
      let path = validate.error.details[0].path[0];
      let errormessage = validate.error.details[0].message;
      e_array[path] = errormessage;
      setsubmitStatus(false);
      console.log(e_array);
      setErrors(e_array);
    } else {
      const response = await managecoupons.addCoupon(init_coupon);
      //console.log("init_coupon", init_coupon);
      if (response.data.status === 1) {
        window.scrollTo({ top: 0, left: 0, behavior: "smooth" });
        setsubmitStatus(false);
        setResponseMessage(response.data.message);
        setResponseType("sucess");
        getCoupons();
        initializeCoupon();
      } else {
        window.scrollTo({ top: 0, left: 0, behavior: "smooth" });
      }
    }
  };

  const handleChange = event => {
    const e_array = [...errors];
    let name = event.target.name; //input field  name
    let value = event.target.value; //input field value
    const errorMessage = validateCoupon(name, value);
    if (errorMessage) e_array[name] = errorMessage;
    else delete e_array[name];
    if (event.target.name === "coupon_code")
      init_coupon[name] = value.trim().toUpperCase();
    else init_coupon[name] = value;
    initCoupon(init_coupon);
    setErrors(e_array);
  };

  const validateCoupon = (name, value) => {
    const obj = { [name]: value };
    const Schema = { [name]: CouponSchema[name] };
    const { error } = Joi.validate(obj, Schema);
    return error ? error.details[0].message : null;
  };

  const handleChangeDate = date => {
    




    const e_array = [...errors];
    let name = 'expire_on'; //input field  name
    let value = date; //input field value
    const errorMessage = validateCoupon(name, value);
    if (errorMessage) e_array[name] = errorMessage;
    else delete e_array[name];
    
    init_coupon["expire_on"] = date;
    initCoupon(init_coupon);
    setErrors(e_array);
  };

  const handlePageChange = pageNum => {
    console.log("Selected page", pageNum);
    setActivePage(pageNum);
    getCoupons(pageNum);
    //setTimeout(() => getCoupons(), 5000);
  };

  return (
    <React.Fragment>
      <div className="container-fluid admin-body">
        <div className="admin-row">
          <div className="col-md-2 col-sm-12 sidebar">
            <Adminsidebar props={props} />
          </div>
          <div className="col-md-10 col-sm-12 content">
            <div className="row content-row">
              <div className="col-md-12 header">
                <Adminheader />
              </div>
              <div className="col-md-12 top-black-bar">
                <div className="bread-crumbs">
                  <span className="navs coupons">Coupons</span>
                </div>
              </div>
              <div className="col-md-12 content-block">
                <div className="container tz-admin-manage-block edit-data">
                  <h1>Add Coupons</h1>
                  <p className={responseType}>{responseMessage}</p>
                  <div className="card">
                    <div className="card-header">Add Coupons</div>
                    <div className="card-body">
                      <div className="category_row">
                        <div className="form-group">
                          <label>Select user</label>
                          <select
                            className="form-control half"
                            name="user"
                            onChange={e => handleChange(e)}
                            value={init_coupon.user}
                          >
                            <option value="">---select---</option>
                            {users.length != 0
                              ? users.map((item, index) =>
                                  item.profile ? (
                                    <option value={item._id} key={index}>
                                      {item.profile.first_name +
                                        " " +
                                        item.profile.last_name}
                                    </option>
                                  ) : (
                                    ""
                                  )
                                )
                              : ""}
                          </select>
                          {errors.user !== "" ? (
                            <div className="validate_error">
                              <p>{errors.user}</p>
                            </div>
                          ) : null}
                        </div>
                        <div className="form-group">
                          <label>Coupon code</label>
                          <input
                            type="text"
                            name="coupon_code"
                            value={init_coupon.coupon_code}
                            className="form-control half"
                            onChange={e => handleChange(e)}
                          />
                          {errors.coupon_code !== "" ? (
                            <div className="validate_error">
                              <p>{errors.coupon_code}</p>
                            </div>
                          ) : null}
                        </div>
                        <div className="form-group">
                          <label>Coupon Amount ($)</label>
                          <input
                            type="number"
                            name="coupon_amount"
                            value={init_coupon.coupon_amount}
                            className="form-control half"
                            onChange={e => handleChange(e)}
                          />
                          {errors.coupon_amount !== "" ? (
                            <div className="validate_error">
                              <p>{errors.coupon_amount}</p>
                            </div>
                          ) : null}
                        </div>
                        <div className="form-group">
                          <label>Coupon Expiry Date</label>
                          <br />
                          {console.log(
                            "init_coupon.expire_on",
                            init_coupon.expire_on
                          )}
                          <DatePicker
                            format="YYYY-MM-D"
                            onChange={e => handleChangeDate(e)}
                            defaultValue={null}
                          />
                          {errors.expire_on !== "" ? (
                            <div className="validate_error">
                              <p>{errors.expire_on}</p>
                            </div>
                          ) : null}
                        </div>
                        <div className="form-group">
                          <div className="submit">
                            <button className="submit-btn" onClick={sendCoupon}>
                              {submitStatus === true ? (
                                <Spinner
                                  animation="border"
                                  variant="light"
                                  size="sm"
                                />
                              ) : (
                                ""
                              )}
                              Submit
                            </button>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="card">
                    <div className="card-header">Coupons</div>
                    <div className="card-body">
                      <div className="listusers table-responsive">
                        <table className="table">
                          <thead>
                            <tr>
                              <th scope="col" className="name">
                                User
                              </th>
                              <th scope="col" className="date">
                                Coupon code
                              </th>
                              <th scope="col" className="email">
                                Coupon amount
                              </th>
                              <th scope="col" className="reg-status">
                                Redeem status
                              </th>
                              <th scope="col" className="reg-status">
                                Expire
                              </th>
                              <th scope="col" className="actions"></th>
                            </tr>
                          </thead>
                          <tbody>
                            {coupons.length != 0
                              ? coupons.map((item, index) => (
                                  <tr key={index}>
                                    <td>
                                      {item.users.profile.first_name}{" "}
                                      {item.users.profile.last_name}
                                    </td>
                                    <td>{item.coupon_code}</td>
                                    <td>{item.coupon_amount}</td>
                                    <td>
                                      {item.redeem_status == true
                                        ? "Redeemed"
                                        : "-"}
                                    </td>
                                    <td>
                                      {" "}
                                      <Moment format="DD/MM/YYYY">
                                        {item.expire_on}
                                      </Moment>
                                    </td>
                                    <td className="actions">
                                      {item.redeem_status == false ? (
                                        <Link
                                          className="btn-dlt"
                                          onClick={() =>
                                            deleteCoupon(index, item._id)
                                          }
                                        >
                                          Delete
                                        </Link>
                                      ) : (
                                        ""
                                      )}
                                    </td>
                                  </tr>
                                ))
                              : ""}
                          </tbody>
                        </table>
                        {coupons.length !== 0 ? (
                          <div className="paginate">
                            <Pagination
                              hideFirstLastPages
                              prevPageText={<i className="left" />}
                              nextPageText={<i className="right" />}
                              activePage={activePage}
                              itemsCountPerPage={1}
                              totalItemsCount={totalCount / number_of_pages}
                              pageRangeDisplayed={4}
                              onChange={handlePageChange}
                            />
                          </div>
                        ) : (
                          ""
                        )}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};

export default Coupons;
