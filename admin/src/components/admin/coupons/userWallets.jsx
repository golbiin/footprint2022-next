import React, { useState, useEffect } from "react";
import Adminsidebar from "../common/adminSidebar";
import Adminheader from "../common/adminHeader";
import * as managecoupons from "../../../services/admin/coupons";
import { Link } from "react-router-dom";
import Spinner from "react-bootstrap/Spinner";
import Joi from "joi-browser";
import "antd/dist/antd.css";
import { DatePicker } from "antd";
import moment from "moment-timezone";
import Pagination from "react-js-pagination";
import Moment from "react-moment";
const UserWallets = props => {
  const [users, setUsers] = useState([]);
  const [init_coupon, initCoupon] = useState([]);
  const [coupons, setCoupons] = useState([]);
  const [submitStatus, setsubmitStatus] = useState(false);
  const [errors, setErrors] = useState([]);
  const [responseMessage, setResponseMessage] = useState("");
  const [responseType, setResponseType] = useState();
  const [activePage, setActivePage] = useState(1);
  const [number_of_pages, setNumofPages] = useState(20);
  const [totalCount, setCount] = useState("");

  const [transactions, setTransactions] = useState([]);
  const [loading, setLoading] = useState(false);
  const [totalAmount, setTotalAmount] = useState(0);
  const [walletType, setWalletType] = useState("credit");


  const CouponSchema = {
    user: Joi.string()
      .required()
      .error(() => {
        return {
          message:
            "Please be sure you’ve selected user. It is not allowed to be empty."
        };
      }),
    coupon_amount: Joi.string()
      .required()
      .error(() => {
        return {
          message:
            "Please be sure you’ve filled coupon ampount. It is not allowed to be empty."
        };
      }),
  
  };
  useEffect(() => {
    getUsers();
    initializeCoupon();
  }, 1);

  const initializeCoupon = () => {
    let c_array = {
      user: "",
      coupon_amount: "",
    };
    initCoupon(c_array);
  };
  const getUsers = async () => {
    const response = await managecoupons.getAllusers();
    if (response.data.status === 1) {
      setUsers(response.data.data);
    }
  };

 

  const sendCoupon = async () => {
    console.log("init_coupon", init_coupon);
    const timezone = Intl.DateTimeFormat().resolvedOptions().timeZone;
    const e_array = [...errors];
    setsubmitStatus(true);
    let validate = Joi.validate(init_coupon, CouponSchema);
    if (validate.error) {
      let path = validate.error.details[0].path[0];
      let errormessage = validate.error.details[0].message;
      e_array[path] = errormessage;
      setsubmitStatus(false);
      console.log(e_array);
      setErrors(e_array);
    } else {
      init_coupon["transacion_type"] = walletType;
      console.log("init_coupon", init_coupon);
      const response = await managecoupons.walletAction(init_coupon, timezone);
      //console.log("init_coupon", init_coupon);
      if (response.data.status === 1) {
        window.scrollTo({ top: 0, left: 0, behavior: "smooth" });
        setsubmitStatus(false);
        setResponseMessage(response.data.message);
        setResponseType("sucess");
        getTransactions(init_coupon.user);
        let c_array = {
          user: init_coupon.user,
          coupon_amount: "",
        };
        initCoupon(c_array);
       
        //initializeCoupon();
      } else {
        window.scrollTo({ top: 0, left: 0, behavior: "smooth" });
      }  
    }
  };

  const handleChange = event => {
    const e_array = [...errors];
    let name = event.target.name; //input field  name
    let value = event.target.value; //input field value
    const errorMessage = validateCoupon(name, value);
    if (errorMessage) e_array[name] = errorMessage;
    else delete e_array[name];
    init_coupon[name] = value;
    initCoupon(init_coupon);
    setErrors(e_array);
  };


  const validateCoupon = (name, value) => {
    const obj = { [name]: value };
    const Schema = { [name]: CouponSchema[name] };
    const { error } = Joi.validate(obj, Schema);
    return error ? error.details[0].message : null;
  };

 

 


  ///////////////////////////////////////////////////



  const getTransactions = async (user_id) => {
    setLoading(true);
    setTransactions([]);
    const response = await managecoupons.getTransactions(user_id);
    console.log("response", response)
    if (response && response.data) {
      if (response.data.status == 1) {
        setTransactions(response.data.data);
        let totalAmount =0;
        if(response.data.data.length)
          setTotalAmount(response.data.data[0].balance);
          setLoading(false);
      } else {
          setLoading(false);
          setResponseMessage(response.data.message);
      }
    }
  }
  const handleChangeUser = event => {
    setTransactions([]);
    setTotalAmount(0);
    const e_array = [...errors];
    let name = event.target.name; //input field  name
    let value = event.target.value; //input field value
    console.log("valuevalue", value)
    const errorMessage = validateCoupon(name, value);
    if (errorMessage) e_array[name] = errorMessage;
    else delete e_array[name];
    init_coupon[name] = value;
    initCoupon(init_coupon);
    setErrors(e_array);
    if(value !==""){
      getTransactions(value);
    }
  };

  return (
    <React.Fragment>
      <div className="container-fluid admin-body">
        <div className="admin-row">
          <div className="col-md-2 col-sm-12 sidebar">
            <Adminsidebar props={props} />
          </div>
          <div className="col-md-10 col-sm-12 content">
            <div className="row content-row">
              <div className="col-md-12 header">
                <Adminheader />
              </div>
              <div className="col-md-12 top-black-bar">
                <div className="bread-crumbs">
                  <span className="navs coupons">User Wallets</span>
                </div>
              </div>
              <div className="col-md-12 content-block">
                <div className="container tz-admin-manage-block edit-data">
                  <h1>User Wallets</h1>
            
                 
                  <div className="card">
                    <div className="card-header"><div className="form-group">
                          <label>Select user</label>
                          <select
                            className="form-control half"
                            name="user"
                            onChange={e => handleChangeUser(e)}
                            value={init_coupon.user}
                          >
                            <option value="">---select---</option>
                            {users.length != 0
                              ? users.map((item, index) =>
                                  item.profile ? (
                                    <option value={item._id} key={index}>
                                      {item.profile.first_name +
                                        " " +
                                        item.profile.last_name}
                                    </option>
                                  ) : (
                                    ""
                                  )
                                )
                              : ""}
                          </select>
                          {errors.user !== "" ? (
                            <div className="validate_error">
                              <p>{errors.user}</p>
                            </div>
                          ) : null}
                        </div>
                         </div>
                         {init_coupon.user || responseMessage !==""?
                    <div className="card-body">
                    <p className={responseType}>{responseMessage}</p>
                      <div className="category_row">
                      <h2>Wallet Action</h2>
                        <div className="form-group">
                          <label>Transaction Type</label>
                          <div className="use-wallet">
                            <label>        
                            <input 
                                type="radio"  
                                checked={
                                  walletType === "credit"
                                    ? true
                                    : ""
                                } 
                                name="use-wallet" 
                                className="use-wallet-input" 
                                value="credit" 
                                onChange={(event)=>event.target.value?setWalletType("credit"):null} 
                                />
                              Credit
                            </label>  
                            <label>        
                            <input type="radio" name="use-wallet" className="use-wallet-input" value="debit" onChange={(event)=>event.target.value?setWalletType("debit"):null} />
                              Debit (Deduct)
                            </label>
                          </div>
    
                        </div>
                        <div className="form-group">
                          <label>Amount ($)</label>
                          <input
                            type="number"
                            name="coupon_amount"
                            value={init_coupon.coupon_amount}
                            className="form-control half"
                            onChange={e => handleChange(e)}
                          />
                          {errors.coupon_amount !== "" ? (
                            <div className="validate_error">
                              <p>{errors.coupon_amount}</p>
                            </div>
                          ) : null}
                        </div>
                         
                        <div className="form-group">
                          <div className="submit">
                            <button className="submit-btn" onClick={sendCoupon}>
                              {submitStatus === true ? (
                                <Spinner
                                  animation="border"
                                  variant="light"
                                  size="sm"
                                />
                              ) : (
                                ""
                              )}
                              Submit
                            </button>
                          </div>
                        </div>
                      </div>
                    </div>
                    : null }
                  </div>
                  
                  <div className="card">
                    <div className="card-header">User Wallet History {transactions && transactions.length > 0 ? <span style={{float: "right"}}>Wallet Balance: ${totalAmount}</span>:null}</div>
                    <div className="card-body">
                      <div className="listusers table-responsive">
                        <table className="table">

                        <thead>
                        <tr>
                          <th>Date</th>
                          <th>Description</th>
                          <th>Withdrawals</th>
                          <th>Deposits</th>
                          <th>Balance</th>
                        </tr>
                      </thead>
                      <tbody>
                        {
                          loading?(
                           <tr>
                              <td
                                colSpan="6"
                                style={{
                                  textAlign: "center",
                                }}
                              >
                                <div className="admin-spinner">
                                  <Spinner animation="grow" variant="dark" />
                                  <span>Connecting...</span>
                                </div>
                              </td>
                            </tr>
                          ):
                         transactions && transactions.length > 0 ? (
                          transactions.map((value, index) => (
                            <tr key= {value._id}>
                              <td>{moment(value.added_on).format("DD MMM YYYY")}</td>
                              <td style={{maxWidth: '400px'}}>
                              {value.added_by && value.added_by ==="admin"? value.transacion_type==="debit" ? "Wallet Amount Deducted" : " Gift From Mike ": value.transacion_type==="debit" ? value.details : " Coupon Redeemed -  Coupon Code: " + value.coupon_code }
                             
                                 
                              </td>
                              <td>{value.transacion_type==="debit" ? '$'+value.amount : null }</td>
                              <td> {value.transacion_type==="credit" ? '$'+value.amount : null }
                              </td>
                              <td> ${value.balance}
                              </td>
                              
                            </tr>
                          ))
                        ) : (
                          <tr>
                            <td
                              colSpan="5"
                              style={{ textAlign: "center" }}
                              className="no-plans"
                            >
                              No Records Found
                            </td>
                          </tr>
                        )}
                      </tbody>
                        </table>
                         
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};

export default UserWallets;
