import React, { Component } from "react";
import Adminheader from "./common/adminHeader";
import Adminsidebar from "./common/adminSidebar";
import * as settings from "../../services/admin/settings";
import Spinner from "react-bootstrap/Spinner";
import Pagination from "react-js-pagination";
import Moment from "react-moment";
import moment from "moment-timezone";
import { Link } from "react-router-dom";
import { CSVLink } from "react-csv";
import TrackingBarChart from "./elements/barChart";

class userActivities extends Component {
  state = {
    data: { activePage: 1, number_of_pages: 10, key: "" },
    trackingData: [],
    checkStatus: false,
    loader: true,
    searchSpec: "",
    modal: false,
    csvData: [],
    headers: [
      { label: "Name", key: "name" },
      { label: "Email", key: "email" },
      { label: "Join date", key: "doj" }
    ],
    total: 0,
    total_logins: 0,
    barData: {
      labels: [],
      datasets: [
        {
          label: "Page Views",
          backgroundColor: "#74B9DC",
          data: []
        }
      ]
    }
  };

  componentDidMount = async () => {
    this.getAllActivities();
    //this.getChartData();
  };

  getAllActivities = async () => {
    this.setState({ loader: false });
    const totalresponse = await settings.getTotalActivities();
    if (totalresponse) {
      if (totalresponse.data.status === 1) {
        let totalTrackings = totalresponse.data.data.total;
        this.setState({ total: totalTrackings });
      }
    }

    const response = await settings.getAllActivities(this.state.data);
    if (response.data.status === 1) {
      this.setState({ trackingData: response.data.data, loader: false });
    } else {
      this.setState({ loader: false });
    }
  };

  handleChange = async e => {
    let key = e.target.value;
    if (key != "") {
      this.setState({ loader: true });
      const totalresponse = await settings.getTotalActivities(key);
      if (totalresponse) {
        if (totalresponse.data.status === 1) {
          let totalTrackings = totalresponse.data.data.total;
          this.setState({ total: totalTrackings });
        }
      }

      const response = await settings.getAllActivities(this.state.data, key);
      if (response.data.status === 1) {
        this.setState({ trackingData: response.data.data, loader: false });
      } else {
        this.setState({ loader: false });
      }
    } else {
      this.getAllTrackings();
    }
  };
  handlePageChange = pageNum => {
    let data = { ...this.state.data };
    data.activePage = pageNum;
    this.setState({ data, loader: true });
    setTimeout(() => this.getAllActivities(), 1);
  };
  // getChartData = async () => {
  //   let barData = { ...this.state.barData };
  //   var labels = [];
  //   var hours = [];
  //   const response = await settings.getChartData();
  //   if (response.data.status === 1) {
  //     let data = response.data.data;
  //     for (let i = data.length - 1; i >= 0; i--) {
  //       console.log(">>>>>>", data[i]);
  //       labels.push(data[i].page_slug);
  //       hours.push(data[i].totalDuration);
  //     }
  //     hours.push(0);
  //     barData.labels = labels;
  //     barData.datasets[0].data = hours;
  //     this.setState({ barData });
  //   }
  // };

  render() {
    return (
      <React.Fragment>
        <div className="container-fluid admin-body">
          <div className="admin-row">
            <div className="col-md-2 col-sm-12 sidebar">
              <Adminsidebar props={this.props} />
            </div>
            <div className="col-md-10 col-sm-12 content">
              <div className="row content-row">
                <div className="col-md-12 header">
                  <Adminheader />
                </div>
                <div className="col-md-12 top-black-bar">
                  <div className="bread-crumbs col-md-6 col-sm-12">
                    <span className="navs users">User Activities</span>
                  </div>
                  <div className="searchbox col-md-6 col-sm-12">
                    <span></span>
                    <input
                      type="text"
                      placeholder="Search"
                      name="search"
                      className="search form-control"
                      onChange={e => this.searchUser(e)}
                      value={this.state.data.key}
                    />
                  </div>
                </div>
                <div className="col-md-12 content-block">
                  <div className="tz-admin-manage-block">
                    <div className="title-bar">
                      <div className="title">User Activities</div>
                    </div>
                    <div className="listusers table-responsive activities">
                      <table className="table">
                        <thead>
                          <tr>
                            <th scope="col" className="date">
                              Activity
                            </th>
                            <th scope="col" className="email">
                              User
                            </th>
                            <th scope="col" className="reg-status">
                              Date
                            </th>
                            <th scope="col" className="actions"></th>
                          </tr>
                        </thead>
                        <tbody>
                          {this.state.loader ? (
                            <tr>
                              <td
                                colSpan="6"
                                style={{
                                  textAlign: "center",
                                  background: "#fff"
                                }}
                              >
                                <div className="admin-spinner">
                                  <Spinner animation="grow" variant="dark" />
                                  <span>Connecting...</span>
                                </div>
                              </td>
                            </tr>
                          ) : this.state.trackingData.length !== 0 ? (
                            this.state.trackingData.map((data, index) => {
                              // var b = moment(data.visit_end).format(
                              //   "MM/DD/YYYY HH:mm:ss"
                              // );

                              // var a = moment(data.visit_start).format(
                              //   "MM/DD/YYYY HH:mm:ss"
                              // );
                              console.log(data);
                              return (
                                <tr className="odd" key={index}>
                                  <td className="name">
                                    {data.details ? data.details.message : "NA"}
                                  </td>
                                  <td className="name">
                                    {data.users
                                      ? data.users.profile &&
                                        data.users.profile.first_name
                                        ? data.users.profile.first_name +
                                          " " +
                                          data.users.profile.last_name
                                        : "NA"
                                      : "NA"}
                                  </td>
                                  <td className="email">
                                    {" "}
                                    {data.date ? (
                                      <Moment format="YYYY/MM/DD">
                                        {data.date}
                                      </Moment>
                                    ) : (
                                      "NA"
                                    )}
                                  </td>
                                </tr>
                              );
                            })
                          ) : (
                            <tr>
                              <td
                                colSpan="8"
                                className="odd"
                                style={{ textAlign: "center" }}
                              >
                                No records found.
                              </td>
                            </tr>
                          )}
                        </tbody>
                      </table>
                      {this.state.trackingData.length !== 0 ? (
                        <div className="paginate">
                          <Pagination
                            hideFirstLastPages
                            prevPageText={<i className="left" />}
                            nextPageText={<i className="right" />}
                            activePage={this.state.data.activePage}
                            itemsCountPerPage={this.state.data.number_of_pages}
                            totalItemsCount={this.state.total}
                            pageRangeDisplayed={4}
                            onChange={this.handlePageChange}
                          />
                        </div>
                      ) : (
                        ""
                      )}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default userActivities;
