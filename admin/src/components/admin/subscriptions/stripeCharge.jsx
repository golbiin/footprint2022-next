import React, { useMemo, useState, useEffect, useReducer } from "react";
import { loadStripe } from "@stripe/stripe-js";
import { Elements, useElements, useStripe } from "@stripe/react-stripe-js";
import * as paymentServices from "../../../services/paymentService";
import Pagination from "react-js-pagination";
import * as userServices from "../../../services/userService";
import Spinner from "react-bootstrap/Spinner";
import { Link } from "react-router-dom";
import SpinnerBtn from "../common/spinnerBtn";
import Moment from "react-moment";
import moment from "moment-timezone";
import STRIPE_PUBLISHABLE from "../../../constants/stripe";
const CheckoutForm = props => {
  const stripe = useStripe();
  const elements = useElements();
  const [paymentResponse, setPaymentResponse] = useState();
  const [paymentStatus, setPaymentStatus] = useState();
  const [submittingStatus, setsubmittingStatus] = useState(false);
  const [loader, setLoader] = useState(true);
  let pricingpackage = props.subscription.plan;
  let billingData = props.subscription.user.billing;
  let payment_method_id = props.subscription.payment_method_id;
  let customer_id = props.subscription.customer_id;
  let subscription_id = props.subscription._id;

  const handleSubmit = async event => {
    setPaymentResponse("");
    event.preventDefault();
    setsubmittingStatus(true);
    try {
      proceedtoPayment();
    } catch (error) {
      setPaymentResponse(error.message);
      setPaymentStatus(0);
      setsubmittingStatus(false);
    }
  };

  const proceedtoPayment = async id => {
    try {
      const billingDetails = {
        name: billingData.first_name + billingData.last_name,
        email: billingData.work_email,
        address: {
          city: billingData.city,
          line1: billingData.contact_address,
          state: billingData.state,
          postal_code: billingData.zip
        }
      };
      let currency = "";
      if (billingData.country === "US") {
        currency = "USD";
      } else if (billingData.country === "AU") {
        currency = "AUD";
      } else if (billingData.country === "CA") {
        currency = "CAD";
      }
      const timezone = Intl.DateTimeFormat().resolvedOptions().timeZone;
      var timemoment = moment();
      var currentTime = timemoment.tz(timezone).format("MM-DD-YYYY HH:mm:ss");
      const paymentdata = {
        id: payment_method_id,
        customer_id: customer_id,
        billingDetails: billingDetails,
        pricingpackage: pricingpackage,
        currency: currency,
        subscription_id: subscription_id,
        charged_by: "admin",
        user_id: props.subscription.user_id,
        timezone: timezone,
        currentTime: currentTime
      };
      const response = await paymentServices.stripeCharge(paymentdata);
      if (response && response.data) {
        props.history.push({
          pathname: "/admin/subscriptions/charge-response",
          state: {
            status: response.data.status,
            message: response.data.message,
            paymentId: response.data.paymentId
          }
        });
      }
    } catch (error) {
      setPaymentResponse(error.message);
      setPaymentStatus(0);
      setsubmittingStatus(false);
    }
  };

  return (
    <form onSubmit={handleSubmit}>
      <div className="row card-form">
        <div className="col-md-12 response">
          <span className={paymentStatus === 1 ? "res-sucess" : "res-error"}>
            {paymentResponse}
          </span>
        </div>

        <div className="col-md-12">
          {submittingStatus ? (
            <SpinnerBtn
              animation="border"
              variant="dark"
              size="sm"
              btnClass="btn submit-btn submitting"
            />
          ) : (
            <button
              className="btn  submit-btn submitting"
              type="submit"
              disabled={!stripe}
            >
              Charge →
            </button>
          )}
        </div>
      </div>
    </form>
  );
};

const stripePromise = loadStripe(STRIPE_PUBLISHABLE);
const StripeCharge = props => {
  return (
    <Elements stripe={stripePromise}>
      <CheckoutForm {...props} />
    </Elements>
  );
};

export default StripeCharge;
