import React, { Component } from "react";
import Adminsidebar from "../common/adminSidebar";
import Adminheader from "../common/adminHeader";
import * as SubscriptionsServices from "../../../services/admin/subscriptions";
import Spinner from "react-bootstrap/Spinner";
import { Link } from "react-router-dom";
import Moment from "react-moment";
import StripeCharge from "./stripeCharge";
import moment from "moment";

class RenewSubscription extends Component {
  state = {
    subscription: [],
    loading: true,
    submitting: false,
    response_type: "",
    response_message: ""
  };
  componentDidMount = async () => {
    const id = this.props.match.params.id;
    const response = await SubscriptionsServices.getSingleSubscriptions(id);
    if (response && response.data) {
      if (response.data.status === 1) {
        let subscription = [...this.state.subscription];
        subscription = response.data.data[0];
        this.setState({ subscription, loading: false });
      } else {
        this.setState({
          loading: false,
          response_type: response.data.status,
          response_message: response.data.message
        });
      }
    }
  };
  render() {
    const {
      subscription,
      loading,
      response_message,
      response_type
    } = this.state;
    let todays = moment().format("MM-DD-YYYY");
    let expire = subscription.expire_on;
    var datediff = new Date(expire).getTime() - new Date(todays).getTime(); //Future date - current date
    var remainingDays = Math.floor(datediff / (1000 * 60 * 60 * 24));
    return (
      <React.Fragment>
        <div className="container-fluid admin-body">
          <div className="admin-row">
            <div className="col-md-2 col-sm-12 sidebar">
              <Adminsidebar props={this.props} />
            </div>
            <div className="col-md-10 col-sm-12 content">
              <div className="row content-row">
                <div className="col-md-12 header">
                  <Adminheader />
                </div>
                <div className="col-md-12 top-black-bar">
                  <div className="bread-crumbs">
                    <span className="navs subscriptions">Subscriptions</span>
                    <span className="navs nav-sub">-</span>
                    <span className="navs nav-sub">Renewal</span>
                  </div>
                </div>
                <div className="col-md-12 content-block">
                  <div className="container tz-admin-manage-block edit-data">
                    <h1>Renew Subscription</h1>
                    <p className={response_type ? "sucess" : "error"}>
                      {response_message}
                    </p>
                    {loading === true ? (
                      <div className="admin-spinner">
                        <Spinner animation="grow" variant="dark" />
                        <span>Connecting...</span>
                      </div>
                    ) : (
                        <React.Fragment>
                          <div className="renew-subscription">
                            <div className="card">
                              <div className="card-header">
                                Subscription Details
                            </div>
                              <div className="card-body">
                                <div className="subscriptionData">
                                  <div className="row">
                                    <div className="col-md-6 title">
                                      SUBSCRIBER
                                  </div>
                                    <div className="col-md-6 value">
                                      <span className="seprator"></span>
                                      {subscription.user.profile.first_name}{" "}
                                      {subscription.user.profile.last_name}
                                    </div>
                                  </div>
                                  <div className="row">
                                    <div className="col-md-6 title">PLAN</div>
                                    <div className="col-md-6 value">
                                      <span className="seprator"></span>
                                      {subscription.plan.category}/
                                    {subscription.plan.sub_category}
                                    </div>
                                  </div>
                                  <div className="row">
                                    <div className="col-md-6 title">AMOUNT</div>
                                    <div className="col-md-6 value">
                                      <span className="seprator"></span>$
                                    {subscription.plan.amount}
                                    </div>
                                  </div>
                                  <div className="row">
                                    <div className="col-md-6 title">CURRANCY</div>
                                    <div className="col-md-6 value">
                                      <span className="seprator"></span>
                                      {subscription.plan.currancy}
                                    </div>
                                  </div>
                                  <div className="row">
                                    <div className="col-md-6 title">
                                      {remainingDays < 0
                                        ? "EXPIRED ON"
                                        : "EXPIRE DATE"}
                                    </div>
                                    <div className="col-md-6 value">
                                      <span className="seprator"></span>
                                      <Moment format="DD/MM/YYYY">
                                        {subscription.expire_on}
                                      </Moment>
                                    </div>
                                  </div>
                                  <div className="row">
                                    <div className="col-md-12">
                                      {remainingDays <= 5 ? (
                                        <StripeCharge
                                          {...this.props}
                                          subscription={subscription}
                                        />
                                      ) : (
                                          ""
                                        )}
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </React.Fragment>
                      )}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default RenewSubscription;
