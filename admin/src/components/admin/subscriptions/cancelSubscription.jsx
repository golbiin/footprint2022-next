import React, { Component } from "react";
import Adminsidebar from "../common/adminSidebar";
import Adminheader from "../common/adminHeader";
import * as SubscriptionsServices from "../../../services/admin/subscriptions";
import Spinner from "react-bootstrap/Spinner";
import { Link } from "react-router-dom";
import Moment from "react-moment";
import moment from "moment";
import SpinnerBtn from "../common/spinnerBtn";

class CancelSubscription extends Component {
  state = {
    subscription: [],
    loading: true,
    submitting: false,
    response_type: "",
    cancel_response_type: 0,
    response_message: "",
    submittingStatus: false
  };

  componentDidMount = async () => {
    const id = this.props.match.params.id;
    const response = await SubscriptionsServices.getSingleSubscriptions(id);
    if (response && response.data) {
      if (response.data.status === 1) {
        let subscription = [...this.state.subscription];
        subscription = response.data.data[0];
        this.setState({ subscription, loading: false });
      } else {
        this.setState({
          loading: false,
          response_type: response.data.status,
          response_message: response.data.message
        });
      }
    }
  };

  cancelUserSubscription = async () => {
    this.setState({ loading: true, submittingStatus: true });
    //const id = this.state.subscription.user_id;
    const id = this.props.match.params.id;
    const response = await SubscriptionsServices.cancelSingleSubscriptions(id);
    if (response && response.data) {
      if (response.data.status === 1) {
        this.setState({ loading: false, submittingStatus: false, cancel_response_type : response.data.status, response_type: response.data.status, response_message: response.data.message });
      } else {
        this.setState({
          loading: false,
          submittingStatus: false,
          response_type: response.data.status,
          response_message: response.data.message
        });
      }
    }
  };

  render() {
    const {
      subscription,
      loading,
      response_message,
      response_type,
      cancel_response_type,
      submittingStatus
    } = this.state;
    let todays = moment().format("MM-DD-YYYY");
    let expire = subscription.expire_on;
    var datediff = new Date(expire).getTime() - new Date(todays).getTime(); //Future date - current date
    var remainingDays = Math.floor(datediff / (1000 * 60 * 60 * 24));
    return (
      <React.Fragment>
        <div className="container-fluid admin-body">
          <div className="admin-row">
            <div className="col-md-2 col-sm-12 sidebar">
              <Adminsidebar props={this.props} />
            </div>
            <div className="col-md-10 col-sm-12 content">
              <div className="row content-row">
                <div className="col-md-12 header">
                  <Adminheader />
                </div>
                <div className="col-md-12 top-black-bar">
                  <div className="bread-crumbs">
                    <span className="navs subscriptions">Subscriptions</span>
                    <span className="navs nav-sub">-</span>
                    <span className="navs nav-sub">Cancel</span>
                  </div>
                </div>
                <div className="col-md-12 content-block">
                  <div className="container tz-admin-manage-block edit-data">
                    <h1>Cancel Subscription</h1>
                    <p className={response_type ? "sucess" : "error"}>
                      {response_message}
                    </p>
                    {loading === true ? (
                      <div className="admin-spinner">
                        <Spinner animation="grow" variant="dark" />
                        <span>Connecting...</span>
                      </div>
                    ) : (
                      <React.Fragment>
                        <div className="renew-subscription">
                          <div className="card">
                            <div className="card-header">
                              Subscription Details
                            </div>
                            <div className="card-body">
                              <div className="subscriptionData">
                                <div className="row">
                                  <div className="col-md-6 title">
                                    SUBSCRIBER
                                  </div>
                                  <div className="col-md-6 value">
                                    <span className="seprator"></span>
                                    {subscription.user.email}{" "}
                             
                                  </div>
                                </div>
                                <div className="row">
                                  <div className="col-md-6 title">PLAN</div>
                                  <div className="col-md-6 value">
                                    <span className="seprator"></span>
                                    {subscription.plan.category}
                                    {subscription.plan.sub_category}
                                  </div>
                                </div>
                                <div className="row">
                                  <div className="col-md-6 title">SUBSCRIPTION PERIOD</div>
                                  <div className="col-md-6 value">
                                    <span className="seprator"></span>
                                    {subscription.plan
                                    ? subscription.plan.term === "bi_weekly" ? "Bi-Weekly Plan" : subscription.plan.term === "weekly" ? "Weekly Plan" : "Monthly Plan"
                                    : ""} {subscription.plan && subscription.plan.weeklyamount
                                      ? " (Weekly Renewal)" : null }
                                  </div>
                                </div>
                                <div className="row">
                                  <div className="col-md-6 title">AMOUNT</div>
                                  <div className="col-md-6 value">
                                    <span className="seprator"></span>
                                    {subscription.plan
                                    ?'$'+subscription.plan.planValue:"" }    
                                  </div>
                                </div>
                                <div className="row">
                                  <div className="col-md-6 title">SUBSCRIPTION DATE</div>
                                  <div className="col-md-6 value">
                                    <span className="seprator"></span>
                                    <Moment format="DD/MM/YYYY">
                                      {subscription.subscribed_on}
                                    </Moment>
                                  </div>
                                </div>
                                <div className="row">
                                  <div className="col-md-6 title">RENEW ON</div>
                                  <div className="col-md-6 value">
                                    <span className="seprator"></span>
                                    <Moment format="DD/MM/YYYY">
                                      {subscription.expire_on}
                                    </Moment>
                                  </div>
                                </div>
                                <div className="row">
                                  <div className="col-md-12">
                                    {/* <StripeCharge
                                      {...this.props}
                                      subscription={subscription}
                                    /> */}
                                    {submittingStatus ? (
                                      <SpinnerBtn
                                        animation="border"
                                        variant="dark"
                                        size="sm"
                                        btnClass="btn submit-btn submitting"
                                      />
                                    ) : cancel_response_type ? null : (
                                      <button
                                        className="btn  submit-btn submitting"
                                        type="submit"
                                        onClick={() => {if(window.confirm('Are you sure?')){this.cancelUserSubscription()};}}
                                      >
                                        CANCEL →
                                      </button>
                                    )}
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </React.Fragment>
                    )}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default CancelSubscription;
