import React, { Component } from "react";
import Adminsidebar from "../common/adminSidebar";
import Adminheader from "../common/adminHeader";
import * as SubscriptionsServices from "../../../services/admin/subscriptions";
import Spinner from "react-bootstrap/Spinner";
import { Link } from "react-router-dom";
import Moment from "react-moment";
import StripeCharge from "./stripeCharge";

class ChargeResponse extends Component {
  render() {
    const images = require.context("../../../assets/images", true);
    return (
      <React.Fragment>
        <div className="container-fluid admin-body">
          <div className="admin-row">
            <div className="col-md-2 col-sm-12 sidebar">
              <Adminsidebar props={this.props} />
            </div>
            <div className="col-md-10 col-sm-12 content">
              <div className="row content-row">
                <div className="col-md-12 header">
                  <Adminheader />
                </div>
                <div className="col-md-12 top-black-bar">
                  <div className="bread-crumbs">
                    <span className="navs subscriptions">Subscriptions</span>
                    <span className="navs nav-sub">-</span>
                    <span className="navs nav-sub">Renewal</span>
                  </div>
                </div>
                <div className="col-md-12 content-block">
                  <div className="container tz-admin-manage-block edit-data">
                    <h1>Renew Subscription</h1>

                    <div className="renew-subscription">
                      <div className="card">
                        <div className="card-header">Subscription Details</div>
                        <div className="card-body">
                          <div className="subscriptionData">
                            <div
                              className={
                                this.props.location.state.status
                                  ? "response sucess"
                                  : "response error"
                              }
                            >
                              <div className="icon"></div>
                              <h3>{this.props.location.state.message}</h3>
                              {this.props.location.state.status ? (
                                <p>
                                  Your Payment id:{" "}
                                  {this.props.location.state.paymentId}
                                </p>
                              ) : (
                                ""
                              )}
                              <Link
                                className="btn submit-btn"
                                to="/admin/subscriptions"
                              >
                                Go to Subscriptions →
                              </Link>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default ChargeResponse;
