import React, { Component } from "react";
import Adminsidebar from "../common/adminSidebar";
import Adminheader from "../common/adminHeader";
import * as SubscriptionsServices from "../../../services/admin/subscriptions";
import Spinner from "react-bootstrap/Spinner";
import { Link } from "react-router-dom";
import Moment from "react-moment";
import moment from "moment";
import Pagination from "react-js-pagination";

class Subscriptions extends Component {
  state = {
    subscriptions: [],
    loading: true,
    response_message: "",
    paginate: { activePage: 1, number_of_pages: 15, key: "" },
    total: "",
    category: 1
  };
  componentDidMount = async () => {
    this.getAllsubscriptions(1);
  };

  changeCategory = (category) =>{
    let paginate = { ...this.state.paginate };
    let subscriptions = [...this.state.subscriptions];
    paginate.activePage = 1;
    subscriptions = [];

    this.setState({
      paginate, subscriptions,
      category,
      loading: true
    });
    setTimeout(() => this.getAllsubscriptions(category), 1);
  };
  getAllsubscriptions = async (category) => {
    
    try {
      let response = await SubscriptionsServices.getSubscriptionsBylimit(
        this.state.paginate,
        category
      );
      if (response && response.data) {
        let responsedata = response.data;

        if (responsedata.status === 1) {
          let subscriptions = [...this.state.subscriptions];
          if (responsedata.data.getbyLimit) {
            let response1 = responsedata.data.getbyLimit;
            subscriptions = response1.status === 1 ? response1.data : [];
            if (response1.status === 1) {
              subscriptions = response1.data;
              this.setState({
                subscriptions
              });
            } else {
              this.setState({
                subscriptions : [],
                response_message: response1.message,
                loading: false
              });
            }
          }
          if (responsedata.data.getTotalScubscriptions) {
            let response2 = responsedata.data.getTotalScubscriptions;
            let total = response2.status === 1 ? response2.data.total : 0;
            this.setState({
              total: total
            });
          }
          this.setState({
            loading: false
          });
        } else {
          this.setState({
            response_message: response.data.message,
            loading: false
          });
        }
      }
    } catch (err) {
      console.log(err);
    }
  };
  handlePageChange = pageNumber => {
    let paginate = { ...this.state.paginate };
    let subscriptions = [...this.state.subscriptions];
    let {category} = this.state;
    paginate.activePage = pageNumber;
    subscriptions = [];
    this.setState({ paginate, subscriptions, loading: true });
    setTimeout(() => this.getAllsubscriptions(category), 1);
  };
  render() {
    let {
      subscriptions,
      loading,
      response_message,
      paginate,
      total
    } = this.state;
    return (
      <React.Fragment>
        {" "}
        <div className="container-fluid admin-body">
          <div className="admin-row">
            <div className="col-md-2 col-sm-12 sidebar">
              <Adminsidebar props={this.props} />
            </div>
            <div className="col-md-10 col-sm-12 content">
              <div className="row content-row">
                <div className="col-md-12 header">
                  <Adminheader />
                </div>
                <div className="col-md-12 top-black-bar">
                  <div className="bread-crumbs col-md-6 col-sm-12">
                    <span className="navs subscriptions">Subscriptions</span>
                  </div>
                </div>
                <div className="col-md-12 content-block">
                  <div className="tz-admin-manage-block">
                    <div className="title-bar">
                      <div className="title">Subscription List</div>
                    </div>
                    <div className="category-box">
                      <a className={this.state.category ? "btn active" : "btn"} onClick={() => this.changeCategory(1)}>Current</a>
                      <a className={!this.state.category ? "btn active" : "btn"} onClick={() => this.changeCategory(0)}>Canceled</a>
                    </div>
                    <div className="listSubscriptions table-responsive">
                      <table className="table">
                        <thead>
                          <tr>
                            <th scope="col" className="name">
                              SUBSCRIBER NAME
                            </th>
                            <th scope="col" className="date">
                              PLAN NAME
                            </th>
                            <th scope="col" className="date">
                              PLAN AMOUNT
                            </th>
                            <th scope="col" className="date">
                              TOTAL AMOUNT
                            </th>
                            <th scope="col" className="email">
                              SUBSCRIBED ON
                            </th>
                            <th scope="col" className="email">
                              LAST RENEWED
                            </th>
                            <th scope="col" className="approve">
                              RENEW ON
                            </th>
                            <th scope="col" className="actions">{!this.state.category? "CANCELED ON" : null}</th>
                          </tr>
                        </thead>
                        <tbody>
                          {loading ? (
                            <tr>
                              <td
                                colSpan="6"
                                style={{
                                  textAlign: "center",
                                  background: "#fff"
                                }}
                              >
                                <div className="admin-spinner">
                                  <Spinner animation="grow" variant="dark" />
                                  <span>Connecting...</span>
                                </div>
                              </td>
                            </tr>
                          ) : subscriptions && subscriptions.length !== 0 ? (
                            subscriptions.map((value, index) => {
                              let todays = moment().format("MM-DD-YYYY");
                              let expire = value.expire_on;
                              var datediff =
                                new Date(expire).getTime() -
                                new Date(todays).getTime(); //Future date - current date
                              var remainingDays = Math.floor(
                                datediff / (1000 * 60 * 60 * 24)
                              );
                              return (
                                <tr
                                  className="odd"
                                  key={index}
                                >
                                  <td className="name">
                                    {value.user.billing?value.user.billing.first_name+" "+value.user.billing.last_name:value.user.email}
                                  </td>
                                  <td className="plan_name">
                                    {value.plan.category}
                                    {value.plan.sub_category}  
                                    {value.plan
                                    ? value.plan.term === "bi_weekly" ? " / Bi-Weekly Plan" : value.plan.term === "weekly" ? " / Weekly Plan" : " / Monthly Plan"
                                    : ""} {value.plan && value.plan.weeklyamount
                                      ? " (Weekly Renewal)" : null }
                                  </td>
                                  <td className="amount">
                                  {value.plan
                                    ?'$'+value.plan.planValue:"" }     
                                  {/* {value.plan.currancy === 'USDUSD'? '$' : value.plan.currancy === 'USDEUR' ? '€' : value.plan.currancy === 'USDGBP' ? '£' : value.plan.currancy === 'USDAUD' ? '$' : value.plan.currancy === 'USDCAD' ? '$' : '$'}{value.plan.weeklyamount? value.plan.weeklyamount: value.plan.amount} */}
                                  </td>
                                  <td className="amount">
                                  {value.plan
                                    ?'$'+value.plan.totalAmount:"" }     
                                  {/* {value.plan.currancy === 'USDUSD'? '$' : value.plan.currancy === 'USDEUR' ? '€' : value.plan.currancy === 'USDGBP' ? '£' : value.plan.currancy === 'USDAUD' ? '$' : value.plan.currancy === 'USDCAD' ? '$' : '$'}{value.plan.weeklyamount? value.plan.weeklyamount: value.plan.amount} */}
                                  </td>
                                  <td className="date">
                                    <Moment format="DD/MM/YYYY">
                                      {value.subscribed_on}
                                    </Moment>
                                  </td>
                                  <td className="date">
                                    {value.renewd_on? (
                                      <Moment format="DD/MM/YYYY">
                                        {value.renewd_on}
                                      </Moment>
                                    ) : (
                                      "-"
                                    )}
                                  </td>
                                  <td className="date">
                                    <Moment format="DD/MM/YYYY">
                                      {value.expire_on}
                                    </Moment>
                                  </td>

                                  <td className="actions">
                                    {value.status?(
                                      <Link
                                        className="red-btn cancel"
                                        to={
                                          "/admin/subscriptions/cancel/" +
                                          value._id
                                        }
                                      >
                                        Cancel Subscription
                                    </Link>

                                    ): value.canceled_on? <Moment format="DD/MM/YYYY">
                                    {value.canceled_on}
                                  </Moment>: null}
                                  
                                    {/* {remainingDays <= 5 ? (
                                      <Link
                                        className="red-btn"
                                        to={
                                          "/admin/subscriptions/renew/" +
                                          value._id
                                        }
                                      >
                                        Charge →
                                      </Link>
                                    ) : (
                                      <button className="red-btn" disabled>
                                        Charge →
                                      </button>
                                    )} */}
                                  </td>
                                </tr>
                              );
                            })
                          ) : (
                            <tr>
                              <td
                                colSpan="8"
                                className="odd"
                                style={{ textAlign: "center" }}
                              >
                                {response_message}
                              </td>
                            </tr>
                          )}
                        </tbody>
                      </table>
                      {subscriptions.length !== 0 ? (
                        <div className="paginate">
                          <Pagination
                            hideFirstLastPages
                            prevPageText={<i className="left" />}
                            nextPageText={<i className="right" />}
                            activePage={paginate.activePage}
                            itemsCountPerPage={15}
                            totalItemsCount={total}
                            pageRangeDisplayed={4}
                            onChange={this.handlePageChange}
                          />
                        </div>
                      ) : (
                        ""
                      )}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default Subscriptions;
