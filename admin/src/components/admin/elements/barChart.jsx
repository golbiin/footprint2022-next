import React, { Component } from "react";
import { Bar } from "react-chartjs-2";

class TrackingBarChart extends Component {
  state = {};

  render() {
    console.log("________", this.props.barData);
    return (
      <Bar
        data={this.props.barData}
        options={{
          title: {
            display: true,
            text: "Page views",
            fontSize: 16,
            fontFamily: "Inter-Semi-Bold",
            fontColor: "#000000",
            position: "left"
          },
          legend: {
            display: false
          },
          scales: {
            yAxes: [
              {
                ticks: {
                  fontColor: "#000000"
                }
              }
            ],
            xAxes: [
              {
                barPercentage: 0.58
              }
            ]
          }
        }}
      />
    );
  }
}

export default TrackingBarChart;
