import React, { Component } from "react";
import { Link } from "react-router-dom";
import Joi from "joi-browser";
import * as authServices from "../../services/admin/login";
import SpinnerBtn from "./common/spinnerBtn";
import jwtDecode from "jwt-decode";
class Adminlogin extends Component {
  state = {
    data: { username: "", password: "" },
    errors: {},
    submit_status: false
  };
  /*****************Joi validation schema**************/
  schema = {
    username: Joi.string()
      .required()
      .error(() => {
        return {
          message: "Please enter your username/email"
        };
      }),
    password: Joi.string()
      .required()
      .error(() => {
        return {
          message: "Please enter your password"
        };
      })
  };
  componentDidMount = () => {
    const loggedUserData = authServices.getCurrentUser();
    console.log("loggedUserData", loggedUserData);
    if (loggedUserData) {
      this.props.history.push({
        pathname: "/admin/home"
      });
    }
  };
  /********on chnage input save data*********/
  handleChange = ({ currentTarget: input }) => {
    const errors = { ...this.state.errors };
    const data = { ...this.state.data };
    const errorMessage = this.validateProperty(input);
    if (errorMessage) errors[input.name] = errorMessage;
    else delete errors[input.name];
    data[input.name] = input.value;
    console.log(input.name, input.value);
    this.setState({ data, errors });
    console.log("data", this.state.data);
    console.log("err", this.state.errors);
  };
  /*****************JOI VALIDATION CALL**************/
  validateProperty = ({ name, value }) => {
    const obj = { [name]: value };
    const schema = { [name]: this.schema[name] };
    const { error } = Joi.validate(obj, schema);
    return error ? error.details[0].message : null;
  };

  loginSubmit = async () => {
    const data = { ...this.state.data };
    const errors = { ...this.state.errors };
    let result = Joi.validate(data, this.schema);
    this.setState({
      message: "",
      message_type: ""
    });
    if (result.error) {
      console.log("Dddddddddddddd");
      let path = result.error.details[0].path[0];
      let errormessage = result.error.details[0].message;
      errors[path] = errormessage;
      this.setState({ errors });
      console.log("errors", result.error);
    } else {
      this.setState({ submit_status: true });
      try {
        const response = await authServices.authenticate(data);

        if (response) {
          this.SessionManage(response);
        }
      } catch (err) {}
    }
  };
  SessionManage = async response => {
    const data = { ...this.state.data };
    data.username = "";
    data.password = "";
    this.setState({ data });
    if (response.data.status === 0) {
      this.setState({
        message: response.data.message,
        message_type: "error",
        submit_status: false,
        verification: true
      });
    } else {
      const token = response.data.data["token"];
      this.setState({
        message: response.data.message,
        message_type: "sucess",
        submit_status: false
      });
      const result = await authServices.loginWithJwt(
        token,
        this.state.remember
      );
      if (result) {
        setTimeout(
          () =>
            this.props.history.push({
              pathname: "/admin/home"
            }),
          3000
        );
      }
    }
  };
  render() {
    return (
      <React.Fragment>
        <div className="container-fluid admin-body">
          <div id="admin_login">
            <div className="loginbox">
              <h1>Login</h1>
              <p>Welcome back! Please login to continue.</p>
              <div className="login-form">
                {this.state.message !== "" ? (
                  <div className={this.state.message_type}>
                    <p>{this.state.message}</p>
                  </div>
                ) : null}
                <div className="form-group">
                  <span class="username"></span>
                  <input
                    type="text"
                    placeholder="Username or email"
                    className="form-control field"
                    name="username"
                    onChange={this.handleChange}
                    value={this.state.data.username}
                  />
                  {this.state.errors.username ? (
                    <div className="danger">{this.state.errors.username}</div>
                  ) : (
                    ""
                  )}
                </div>
                <div className="form-group">
                  <span class="password"></span>
                  <input
                    type="password"
                    placeholder="Password"
                    className="form-control field"
                    name="password"
                    onChange={this.handleChange}
                    value={this.state.data.password}
                  />
                  {this.state.errors.password ? (
                    <div className="danger">{this.state.errors.password}</div>
                  ) : (
                    ""
                  )}
                </div>
                <div className="form-group login-optons">
                  <div className="custom-control custom-checkbox col-md-6">
                    <input
                      type="checkbox"
                      name="forgot"
                      className="custom-control-input"
                      id="forgot"
                      onChange={this.rememberMe}
                    ></input>
                    <label className="custom-control-label" htmlFor="forgot">
                      <span>Remember Me</span>
                    </label>
                  </div>
                  <div className="forgotpassword col-md-6">
                    <Link to="/">forgot password?</Link>
                  </div>
                </div>
                {this.state.submit_status ? (
                    <SpinnerBtn
                      animation="border"
                      variant="dark"
                      size="sm"
                      btnClass="admin-login-btn"
                    />
                  ) : (
                    <button
                      className="admin-login-btn"
                      onClick={this.loginSubmit}
                     >
                      Sign in
                    </button>
                  )}
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default Adminlogin;
