const SERVER_URL =
  process.env.NODE_ENV === "production"
    ? "https://api.footprint.io"
    : "http://localhost:4000/api"; 

export default SERVER_URL;

/* const SERVER_URL =
  process.env.NODE_ENV === "production"
    ? "https://api.outsourceme.com/api"
    : "http://localhost:4000/api";

    https://ww2.osm.webeteerprojects.com/api
export default SERVER_URL; */
