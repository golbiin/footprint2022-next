const SITE_URL =
  process.env.NODE_ENV === "production"
    ? "https://outsourceme.com/"
    : "http://localhost:3000/";

export default SITE_URL;
