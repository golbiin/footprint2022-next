import React, { Component, Suspense, lazy } from "react";
import {
  Route,
  Redirect,
  Switch,
  BrowserRouter as Router
} from "react-router-dom";

import "./App.css";

const NotFound = lazy(() => import("./components/admin/notFound"));
const ProtectedRouteAdmin = lazy(() => import("./components/admin/adminProtectedRoute"));
const CancelSubscription = lazy(() => import("./components/admin/subscriptions/cancelSubscription"));


const EditBlog = lazy(() => import("./components/admin/blogs/editBlog"));
const Adminlogin = lazy(() => import("./components/admin/adminLogin"));

const AdminDashboard = lazy(() => import("./components/admin/adminDashboard"));
const ManageUsers = lazy(() => import("./components/admin/manageUsers"));
const EditUser = lazy(() => import("./components/admin/userEdit/editUser"));
const AdminpwdChange = lazy(() =>
  import("./components/admin/adminPasswordchange")
);

const RenewSubscription = lazy(() =>
  import("./components/admin/subscriptions/renewSubscription")
);
const Subscriptions = lazy(() =>
  import("./components/admin/subscriptions/subscriptions")
);
const AddPortfolio = lazy(() =>
  import("./components/admin/portfolio/addPortfolio")
);
const UserWallets = lazy(() =>
  import("./components/admin/coupons/userWallets")
);
const Coupons = lazy(() => import("./components/admin/coupons/coupons"));
const ChargeResponse = lazy(() =>
  import("./components/admin/subscriptions/chargeResponse")
);

const ChatUrl = lazy(() => import("./components/admin/userEdit/chatUrl"));
const TaskManagement = lazy(() =>
  import("./components/admin/userEdit/TaskManagement")
);

const LoginTracing = lazy(() => import("./components/admin/LoginTrack"));
const LeadsList = lazy(() => import("./components/admin/leadsList"));
const ProjectsList = lazy(() => import("./components/admin/projectsList"));
const Activities = lazy(() => import("./components/admin/activities"));


const ContactRequests = lazy(() =>
  import("./components/admin/contactRequests")
);
const NewsList  = lazy(() =>
  import("./components/admin/newsletter")
);
const ReadContactRequest = lazy(() =>
  import("./components/admin/readContactRequest")
);
const LeadDetails = lazy(() =>
  import("./components/admin/leadDetails")
);



const WorkingHours = lazy(() =>
  import("./components/admin/userEdit/workingHours")
);
const WorkingHoursList = lazy(() =>
  import("./components/admin/userEdit/workingHoursList")
);
const UserPlan = lazy(() => import("./components/admin/userEdit/userPlan"));


const ManageBlogs = lazy(() => import("./components/admin/blogs/manageBlogs"));

const AddBlog = lazy(() => import("./components/admin/blogs/addBlog"));
const AddReview = lazy(() => import('./components/admin/review/addReview'));
 
const EditReview = lazy(() => import("./components/admin/review/editReview"));
const ManageReviews = lazy(() => import("./components/admin/review/manageReviews"));

class App extends Component {
  state = {};

 
 
  render() {
   
    const images = require.context("./assets/images", true);
      return (
        <React.Fragment>
          <Router>
            <Suspense
              fallback={
                <div
                  style={{
                    height: "100vh",
                    display: "flex",
                    position: "absolute",
                    background: "#fff",
                    zIndex: "999",
                    width: "100%",
                    justifyContent: "center",
                    alignItems: "center"
                  }}
                >
                  Loading...
                </div>
              }
            >
              <Switch>
               
               /**** Admin Routes ***/
                
                <Route exact path="/admin" component={Adminlogin} />
                <Route exact path="/admin/login" component={Adminlogin} />
                <ProtectedRouteAdmin
                  exact
                  path="/admin/home"
                  component={AdminDashboard}
                />
                <ProtectedRouteAdmin
                  exact
                  path="/admin/users"
                  component={ManageUsers}
                />
                <ProtectedRouteAdmin
                  exact
                  path="/admin/users/edit/:id"
                  component={EditUser}
                />
                <ProtectedRouteAdmin
                  exact
                  path="/admin/users/chat-url/:id"
                  component={ChatUrl}
                />
                <ProtectedRouteAdmin
                  exact
                  path="/admin/users/working-hours/:project_id/:user_id"
                  component={WorkingHours}
                />
                <ProtectedRouteAdmin
                  exact
                  path="/admin/users/working-hours-list/:project_id/:user_id"
                  component={WorkingHoursList}
                />
                <ProtectedRouteAdmin
                  exact
                  path="/admin/users/assign-user-plan/:id"
                  component={UserPlan}
                />
                <ProtectedRouteAdmin
                  exact
                  path="/admin/users/task-management/:id"
                  component={TaskManagement}
                />
                <ProtectedRouteAdmin
                  exact
                  path="/admin/settings/change-password"
                  component={AdminpwdChange}
                />
                <ProtectedRouteAdmin
                  exact
                  path="/admin/portfolio/add"
                  component={AddPortfolio}
                />
               
                <ProtectedRouteAdmin
                  exact
                  path="/admin/tracking"
                  component={LoginTracing}
                />
                <ProtectedRouteAdmin
                  exact
                  path="/admin/leads"
                  component={LeadsList}
                />
                <ProtectedRouteAdmin
                  exact
                  path="/admin/projects"
                  component={ProjectsList}
                />
                <ProtectedRouteAdmin
                  exact
                  path="/admin/newsLetter"
                  component={NewsList}
                />
                <ProtectedRouteAdmin
                  exact
                  path="/admin/user-activity/:id"
                  component={Activities}
                />
                <ProtectedRouteAdmin
                  exact
                  path="/admin/coupons"
                  component={Coupons}
                />
                <ProtectedRouteAdmin
                  exact
                  path="/admin/user-wallets"
                  component={UserWallets}
                />
                <ProtectedRouteAdmin
                  exact
                  path="/admin/subscriptions"
                  component={Subscriptions}
                />
                <ProtectedRouteAdmin
                  exact
                  path="/admin/subscriptions/renew/:id"
                  component={RenewSubscription}
                />
                <ProtectedRouteAdmin
                  exact
                  path="/admin/subscriptions/cancel/:id"
                  component={CancelSubscription}
                />
                <ProtectedRouteAdmin
                  exact
                  path="/admin/subscriptions/charge-response"
                  component={ChargeResponse}
                />
                <ProtectedRouteAdmin
                  exact
                  path="/admin/contact-requests"
                  component={ContactRequests}
                />
                <ProtectedRouteAdmin
                  exact
                  path="/admin/contact-requests/read/:id"
                  component={ReadContactRequest}
                />
                <ProtectedRouteAdmin
                  exact
                  path="/admin/lead-details/:id"
                  component={LeadDetails}
                />
                <ProtectedRouteAdmin
                  exact
                  path="/admin/blogs"
                  component={ManageBlogs}
                />
                <ProtectedRouteAdmin
                  exact
                  path="/admin/add-blog"
                  component={AddBlog}
                />
                <ProtectedRouteAdmin
                  exact
                  path="/admin/edit-blog/:id"
                  component={EditBlog}
                />
                <ProtectedRouteAdmin
                  exact
                  path="/admin/edit-review/:id"
                  component={EditReview}
                />
                 
                 <ProtectedRouteAdmin
                  exact
                  path="/admin/review/add"
                  component={AddReview}
                />
                <ProtectedRouteAdmin
                  exact
                  path="/admin/reviews"
                  component={ManageReviews}
                />

                <Route exact path="/not-found" component={NotFound} />
                <Redirect from="/" exact to="/admin" />
               
                <Redirect to="/not-found" />
              </Switch>
            </Suspense>
          </Router>
        </React.Fragment>
      );
    
  }
}

export default App;
